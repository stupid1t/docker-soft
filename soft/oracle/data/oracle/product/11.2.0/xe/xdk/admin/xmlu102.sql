Rem
Rem $Header: xmlu102.sql 06-mar-2008.02:32:43 tyu Exp $
Rem
Rem xmlu102.sql
Rem
Rem Copyright (c) 2005, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      xmlu102.sql - Upgrade from 10.2.0 database
Rem
Rem    DESCRIPTION
Rem      Upgrade from 10.2.0 database
Rem
Rem    NOTES
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    tyu         02/26/08 - update for 11.2 release
Rem    kkarun      10/24/06 - Created
Rem

Rem =========================================================================
Rem BEGIN STAGE 1: Remove 10.2.0 XML Classes and packages
Rem =========================================================================

EXECUTE dbms_registry.upgrading('XML', 'Oracle XDK');

-- Drop Java Packages
create or replace procedure xdk_drop_package(pkg varchar2) is
   CURSOR classes is select dbms_java.longname(object_name) class_name
      from all_objects
      where object_type = 'JAVA CLASS'
	and dbms_java.longname(object_name) like '%' || pkg || '%';
begin
   FOR class IN classes LOOP
      dbms_java.dropjava('-r -v -synonym ' || class.class_name);
   END LOOP;
end xdk_drop_package;
/

EXECUTE xdk_drop_package('oracle/xml/jaxp');
EXECUTE xdk_drop_package('oracle/xml/util');
EXECUTE xdk_drop_package('oracle/xml/binxml');
EXECUTE xdk_drop_package('oracle/xml/comp');
EXECUTE xdk_drop_package('oracle/xml/mesg');
EXECUTE xdk_drop_package('oracle/xml/async');
EXECUTE xdk_drop_package('oracle/xml/parser/v2/XML');
EXECUTE xdk_drop_package('oracle/xml/parser/v2/XSL');
EXECUTE xdk_drop_package('oracle/xml/parser/v2');
EXECUTE xdk_drop_package('oracle/xml/parser/schema');
EXECUTE xdk_drop_package('oracle/xml/xqxp');
EXECUTE xdk_drop_package('oracle/xml/sql');
EXECUTE xdk_drop_package('OracleXML');
EXECUTE xdk_drop_package('oracle/xdb');
EXECUTE xdk_drop_package('oracle/xquery');

drop procedure xdk_drop_package;

drop java class "OracleXML";
drop java class "OracleXMLStore";

Rem =========================================================================
Rem END STAGE 1: Remove 10.2.0 XML Classes and packages
Rem =========================================================================

Rem =========================================================================
Rem BEGIN STAGE 2: Initialize with 11.0.0 Classes and packages
Rem =========================================================================

@@initxml.sql

Rem =========================================================================
Rem END STAGE 2: Initialize with 11.0.0 Classes and packages
Rem =========================================================================

