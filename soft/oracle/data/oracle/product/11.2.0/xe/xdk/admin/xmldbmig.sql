Rem
Rem $Header: xmldbmig.sql 26-feb-2008.11:59:35 tyu Exp $
Rem
Rem xmldbmig.sql
Rem
Rem Copyright (c) 2001, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      xmldbmig.sql - XML DB Migration script
Rem
Rem    DESCRIPTION
Rem      This script invokes a migration script based on the DB version
Rem
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    tyu         02/26/08 - update for 11.2
Rem    kkarun      02/28/05 - Fix 4189351 (reload if version == current)
Rem    kkarun      02/05/04 - add 10.1.0
Rem    kkarun      11/19/02 - add check_server_instance
Rem    kkarun      08/20/02 - fix @@
Rem    kkarun      05/30/02 - Add 920 script
Rem    kkarun      12/17/01 - Merged kkarun_update_mig_scripts_main
Rem    kkarun      12/05/01 - Created
Rem

WHENEVER SQLERROR EXIT
EXECUTE dbms_registry.check_server_instance;
WHENEVER SQLERROR CONTINUE;

COLUMN :file_name NEW_VALUE comp_file NOPRINT
VARIABLE file_name VARCHAR2(30)

BEGIN
  IF substr(dbms_registry.version('XML'),1,5)='8.1.7' THEN
    :file_name := '@xmlu817.sql';
  ELSIF substr(dbms_registry.version('XML'),1,5)='9.0.1' THEN
    :file_name := '@xmlu901.sql';
  ELSIF substr(dbms_registry.version('XML'),1,5)='9.2.0' THEN
    :file_name := '@xmlu920.sql';
  ELSIF substr(dbms_registry.version('XML'),1,6)='10.1.0' THEN
    :file_name := '@xmlu101.sql';
  ELSIF substr(dbms_registry.version('XML'),1,6)='10.2.0' THEN
    :file_name := '@xmlu102.sql';
  ELSIF substr(dbms_registry.version('XML'),1,6)='11.1.0' THEN
    :file_name := '@xmlrelod.sql';
  ELSE
    :file_name := '?/rdbms/admin/nothing.sql';
  END IF;
END;
/

SELECT :file_name FROM DUAL;
@&comp_file
