Rem
Rem $Header: xmle920.sql 17-apr-2003.17:56:45 kkarun Exp $
Rem
Rem xmle920.sql
Rem
Rem Copyright (c) 2002, 2003, Oracle Corporation.  All rights reserved.  
Rem
Rem    NAME
Rem      xmle920.sql - Drop current XDK components (part of downgrade to 9.2.0)
Rem
Rem    DESCRIPTION
Rem      Drop current XDK components (part of downgrade to 9.2.0)
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    kkarun      04/16/03 - use execute instead of call
Rem    kkarun      05/30/02 - kkarun_add_10i_upgrade_scripts
Rem    kkarun      05/30/02 - Created
Rem

Rem =========================================================================
Rem BEGIN STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

EXECUTE dbms_registry.downgrading('XML');

@@rmxml.sql

EXECUTE dbms_registry.downgraded('XML', '9.2.0');

Rem =========================================================================
Rem END STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

