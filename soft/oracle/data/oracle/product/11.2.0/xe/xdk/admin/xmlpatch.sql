Rem
Rem $Header: xmlpatch.sql 25-mar-2003.14:41:31 kkarun Exp $
Rem
Rem xmlpatch.sql
Rem
Rem Copyright (c) 2002, 2003, Oracle Corporation.  All rights reserved.  
Rem
Rem    NAME
Rem      xmlpatch.sql - Script used to upgrade to a patch-set release
Rem
Rem    DESCRIPTION
Rem      Script used to upgrade to a patch-set release
Rem
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    kkarun      03/25/03 - use dbms_registry vars
Rem    kkarun      11/19/02 - add check_server_instance
Rem    kkarun      11/12/02 - update version
Rem    kkarun      06/28/02 - kkarun_add_patch_relod_scripts
Rem    kkarun      06/12/02 - Created
Rem

WHENEVER SQLERROR EXIT
EXECUTE dbms_registry.check_server_instance;
WHENEVER SQLERROR CONTINUE;

EXECUTE dbms_registry.loading('XML', 'Oracle XML Developers Kit', 'xmlvalidate');

COLUMN file_name NEW_VALUE comp_file NOPRINT;
SELECT dbms_registry.script('XML','@initxml.sql') AS file_name FROM DUAL;
@&comp_file

EXECUTE dbms_registry.loaded('XML');
EXECUTE xmlvalidate;
