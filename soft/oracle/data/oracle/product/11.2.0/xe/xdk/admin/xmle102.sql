Rem
Rem $Header: xmle102.sql 26-feb-2008.13:13:10 tyu Exp $
Rem
Rem xmle102.sql
Rem
Rem Copyright (c) 2006, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      xmle102.sql - Drop current XDK components (downgrade to 10.2.0)
Rem
Rem    DESCRIPTION
Rem      Drop current XDK components (downgrade to 10.2.0)
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    tyu        02/26/08 - update for 11.2
Rem    bihan      10/24/06 - Created
Rem

Rem =========================================================================
Rem BEGIN STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

EXECUTE dbms_registry.downgrading('XML');

@@rmxml.sql

EXECUTE dbms_registry.downgraded('XML', '10.2.0');

Rem =========================================================================
Rem END STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

