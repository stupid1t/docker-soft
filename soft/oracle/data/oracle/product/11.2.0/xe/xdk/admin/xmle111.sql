Rem
Rem $Header: xmle111.sql 26-feb-2008.01:31:12 tyu Exp $
Rem
Rem xmle111.sql
Rem
Rem Copyright (c) 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      xmle111.sql - Drop current XDK components (downgrade to 11.1.0)
Rem
Rem    DESCRIPTION
Rem      Drop current XDK components (downgrade to 11.1.0)
Rem
Rem    NOTES
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    tyu         02/26/08 - Created
Rem

Rem =========================================================================
Rem BEGIN STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

EXECUTE dbms_registry.downgrading('XML');

@@rmxml.sql

EXECUTE dbms_registry.downgraded('XML', '11.1.0');

Rem =========================================================================
Rem END STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

