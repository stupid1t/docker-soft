Rem
Rem $Header: xmle101.sql 22-nov-2004.10:59:31 kkarun Exp $
Rem
Rem xmle101.sql
Rem
Rem Copyright (c) 2004, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      xmle101.sql - Drop current XDK components (downgrade to 10.1.0)
Rem
Rem    DESCRIPTION
Rem      Drop current XDK components (downgrade to 10.1.0)
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    kkarun      05/13/04 - kkarun_fix_downgrade_script_bug
Rem    kkarun      02/05/04 - Created
Rem

Rem =========================================================================
Rem BEGIN STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

EXECUTE dbms_registry.downgrading('XML');

@@rmxml.sql

EXECUTE dbms_registry.downgraded('XML', '10.1.0');

Rem =========================================================================
Rem END STAGE 1: Remove current release XML Classes and packages
Rem =========================================================================

