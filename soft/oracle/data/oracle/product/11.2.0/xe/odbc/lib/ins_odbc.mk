# Entering /ade/b/865225302/oracle/odbc/install/cus_odbc.mk
include $(ORACLE_HOME)/odbc/lib/env_odbc.mk

$(INSTALL_SHORTCUT_TARGETS):
	$(MAKE) -f $(ODBCLIB)ins_odbc.mk relink EXENAME=$@
$(INSTALL_SHORTCUT_32_64_TARGETS):
	$(MAKE) -f $(ODBCLIB)ins_odbc.mk relink EXENAME=$@ $(PL_LINK_OVERRIDES)

ORACLEBIN=$(ORACLE_HOME)/rdbms/lib
ODBCBIN=$(ORACLEBIN)

relink: $(ODBCLIB)$(EXENAME)
	$(RMF) $(LIBHOME)$(EXENAME)
	$(CP) $(ODBCLIB)$(EXENAME) $(LIBHOME)

SQORA_OFILES=
SQOCI_OFILES=
UTILITY_OFILES=

update_archive:
	ar -r $(SQORALIB) $(SQORA_OFILES)
	ar -r $(SQOCILIB) $(SQOCI_OFILES)
	ar -r $(UTILITYLIB) $(UTILITY_OFILES)

isqora: $(SQORADLL)

# Exiting /ade/b/865225302/oracle/odbc/install/cus_odbc.mk
# Entering link.mk

$(SQORADLL) : $(ALWAYS) $(SQORA_LINK_FILES)
	$(SILENT)$(ECHO)
	$(SILENT)$(ECHO) " - Linking sqora"
	$(SILENT)$(ECHO)
	$(RMF) $@
	$(SQORA_LINKLINE)


# Exiting link.mk
