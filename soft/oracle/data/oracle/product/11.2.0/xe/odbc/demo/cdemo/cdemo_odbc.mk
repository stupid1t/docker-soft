# This Makefile builds the sample ODBC programs in $(ORACLE_HOME)/odbc/demo/cdemo
# and can serve as a template for linking customer applications.
# Here is the example to compile odbcconnect.c demo
#       $ gmake -f cdemo_odbc.mk odbcconnect
# Note: Please set the DM_HOME environment variable to Driver Manager installed
#       location; otherwise compiler may not be able to open include files
#       supplied by DM (e.g, sql.h, sqlext.h, etc.).


include $(ORACLE_HOME)/odbc/lib/env_odbc.mk

# SAMPLES is a list of the C ODBC sample programs.
SAMPLES=odbcconnect odbcselect odbcinsert1 odbcinsert2 odbcdemo

# These targets build all of a class of samples in one call to make.
samples: $(SAMPLES)

#------------------------------------------------------------------------
# The target 'build' puts together an executable $(EXE) from the .o files
# in $(OBJS) and the libraries in $(PROLDLIBS).  It is used to build the
# c ODBC sample programs.

build: $(OBJS)
	$(DEMO_ODBC_BUILD_SHARED)

build_static: $(OBJS)
	$(DEMO_ODBC_BUILD_STATIC)

$(SAMPLES) $(OBJECT_SAMPLES):
	$(MAKE) -f $(MAKEFILE) OBJS=$@.$(OBJ_EXT) EXE=$@ build

#---------------------------------------------------------------------------
# Clean up all executables and *.o
clean:
	`if [ "z$(OBJS)" = "z" -a "z$(EXE)" = "z" ]; then $(RMF) $(SAMPLES) *.$(OBJ_EXT);fi`
	`if [ "z$(OBJS)" != "z" ]; then $(RMF) $(OBJS);fi`
	`if [ "z$(EXE)" != "z" ]; then $(RMF) $(EXE);fi`

#---------------------------------------------------------------------------
# Here are some rules for converting .c -> .o
#
# We use a macro INCLUDE to hadle the other required header files. The general 
# format of the INCLUDE macro is 
#   INCLUDE= $(I_SYM)dir1 $(I_SYM)dir2 ...
#
# Normally, I_SYM=-I, for the c compiler.

.SUFFIXES: .c  .$(OBJ_EXT) 

.c.$(OBJ_EXT):
	$(C2O)

odbcconnect.$(OBJ_EXT): odbcconnect.c 
	$(C2O)

odbcselect.$(OBJ_EXT): odbcselect.c 
	$(C2O)

odbcinsert1.$(OBJ_EXT): odbcinsert1.c 
	$(C2O)

odbcinsert2.$(OBJ_EXT): odbcinsert2.c 
	$(C2O)
odbcdemo..$(OBJ_EXT): odbcdemo.c
	$(C2O)

# These macro definitions fill in some details or override some defaults
MAKEFILE=cdemo_odbc.mk

