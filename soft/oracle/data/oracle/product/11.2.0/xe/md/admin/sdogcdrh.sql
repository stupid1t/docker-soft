Rem
Rem $Header: sdo/admin/sdogcdrh.sql /main/21 2009/03/23 15:39:50 jxyang Exp $
Rem
Rem sdogcdrh.sql
Rem
Rem Copyright (c) 2002, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdogcdrh.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    jxyang      03/12/09 - change geocode functions to deterministic
Rem    jxyang      12/06/04 - add reverse geocoding interface taking geometry 
Rem                           input 
Rem    jxyang      11/09/04 - add reverse geocoding interface 
Rem    jxyang      04/09/04 - add schema name to package name 
Rem    jxyang      03/18/04 - move data processing out 
Rem    jxyang      10/20/03 - remove match mode flag in geocode_as_geometry 
Rem    jxyang      10/07/03 - move functions into package 
Rem    jxyang      08/04/03 - remove internal functions 
Rem    jxyang      07/09/03 - interface change
Rem    sravada     07/30/03 - add new interfaces
Rem    jxyang      04/30/03 - grant_access change
Rem    jxyang      03/24/03 - create_gc_table change
Rem    sravada     02/05/03 - remove SETs
Rem    jxyang      01/16/03 - grant_access change
Rem    jxyang      11/26/02 - create gc tables in username instead of in gc_username
Rem    syuditsk    10/04/02 - adding data processing functionality for suffixed gc tables
Rem    syuditsk    10/02/02 - adding procedure to grant access to gc data
Rem    syuditsk    09/17/02 - separating creation of profile tables from geocoding tables
Rem    syuditsk    09/17/02 - new version, with all data processing scripts working
Rem    syuditsk    08/30/02 - adding header for SDO_GCDR.gc_drop_all
Rem    syuditsk    08/29/02 - adding interface to geocoder data creation procedures
Rem    syuditsk    08/22/02 - adding interface to geocoding methods
Rem    syuditsk    08/16/02 - replacing keywordArray with SDO_keywordArray
Rem    syuditsk    08/15/02 - removing type definition for keywordArray, moving to sdotypes.sql
Rem    syuditsk    08/13/02 - syuditsk_08_06_02_adding_sql_files_to_admin
Rem    syuditsk    08/12/02 - changing name of type geo_addr to SDO_geo_addr
Rem    syuditsk    08/06/02 - Created
Rem


create or replace package mdsys.SDO_GCDR AUTHID current_user as
  procedure validateCountryRow( country VARCHAR2, 
    keys SDO_keywordArray);
       
  -- used by country profile validation trigger, should not be documented
  procedure validateInLineStreetTypeRow( country VARCHAR2, 
    pos VARCHAR2, 
    sep VARCHAR2, 
    out VARCHAR2, 
    keys SDO_keywordArray);
       
  -- used by country profile validation trigger, should not be documented
  procedure validateStreetTypeRow( country VARCHAR2,
    pos VARCHAR2,
    sep VARCHAR2,
    out VARCHAR2, 
    keys SDO_keywordArray);
       
  -- used by country profile validation trigger, should not be documented
  procedure validateSecondUnitRow( country VARCHAR2,
    pos VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validateStreetPrefixRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);
 
  -- used by country profile validation trigger, should not be documented
  procedure validateStreetSuffixRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validateRegionRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validateCityRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validateLocalityDictRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);
 
  -- used by country profile validation trigger, should not be documented
  procedure validateStreetDictRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validatePoBoxRow( country VARCHAR2,
    out VARCHAR2,
    keys SDO_keywordArray);

  -- used by country profile validation trigger, should not be documented
  procedure validatePlaceNameRow( country VARCHAR2,
    out VARCHAR2);

  -- create parser profile tables
  procedure create_profile_tables ;

  -- used by java geocoder client, should not be documented
  function batch_geocode(address_list in varchar2, gc_username in varchar2, max_result_count number)
  return sdo_keywordarray deterministic;

  -- geocode an input address specified by address lines and return 
  -- the first matched address as a SDO_GEO_ADDR object
  function geocode( 
    username varchar2,
    addr_lines SDO_keywordArray, 
    country VARCHAR2, 
    match_mode VARCHAR2) return SDO_GEO_Addr deterministic ;

  -- geocode an input address specified by address lines and return 
  -- the the coordinates of the first matched address as sdo_geometry
  function geocode_as_geometry( 
    username varchar2,
    addr_lines SDO_keywordArray, 
    country VARCHAR2) return SDO_GEOMETRY deterministic;

  -- geocode an input address specified by a SDO_GEO_ADDR object and return 
  -- the first matched address as a SDO_GEO_ADDR object
  function geocode_addr( 
    gc_username varchar2,
    address SDO_GEO_ADDR) return SDO_geo_Addr deterministic; 

  -- geocode an input address specified by a address lines and return all
  -- matched addresses as a VARRAY of SDO_GEO_ADDR objects
  function geocode_all( 
    gc_username varchar2,
    addr_lines SDO_keywordArray,
    country VARCHAR2,
    match_mode varchar2, 
    max_res_num number default 4000) return SDO_ADDR_ARRAY deterministic; 

  -- geocode an input address specified by a SDO_GEO_ADDR object and return all
  -- matched addresses as a VARRAY of SDO_GEO_ADDR objects
  function geocode_addr_all( 
    gc_username varchar2,
    address SDO_GEO_ADDR,
    max_res_num number default 4000) return SDO_ADDR_ARRAY deterministic ;

  -- reverse-geocode a location specified by longitude and latitude into
  -- address as a sdo_geo_addr object
  function reverse_geocode( 
    username varchar2,
    longitude number, 
    latitude number,
    country VARCHAR2) return SDO_GEO_Addr deterministic;

  -- reverse-geocode a location specified by longitude and latitude into
  -- address as a sdo_geo_addr object
  function reverse_geocode( 
    username varchar2,
    location sdo_geometry,
    country VARCHAR2) return SDO_GEO_Addr deterministic ;

end SDO_GCDR;
/
show errors;

         
   
 

