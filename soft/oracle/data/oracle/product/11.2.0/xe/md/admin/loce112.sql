Rem
Rem $Header: sdo/admin/loce112.sql /st_sdo_11.2.0/1 2010/05/03 10:31:24 sravada Exp $
Rem
Rem loce112.sql
Rem
Rem Copyright (c) 2010, Oracle and/or its affiliates. All rights reserved. 
Rem
Rem    NAME
Rem      loce112.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     05/03/10 - Created
Rem


ALTER SESSION SET CURRENT_SCHEMA = MDSYS;

-- Drop objects related to sdopopt.sql
----------------------------------------------------------------------
drop public synonym SDO_PQRY force;
drop public synonym sdoridtable force;
drop function MDSYS.SDO_PQRY;
drop function MDSYS.qry2opt;
drop function MDSYS.sdo_simple_filter;
drop type MDSYS.sdoridtable force;


@@sdoeoper.sql


ALTER SESSION SET CURRENT_SCHEMA = SYS;

