begin
  execute immediate
'CREATE or replace TYPE RDF_MATCH_impl_t authid current_user AS OBJECT
(
  curnum           integer,
  nCols            integer,
  done             integer,
  cnt              integer,        -- count, for count(*) optimization
  el_typ           SYS.AnyType,    -- return type
  attrset          SYS.ODCINumberList,
  isCLOBvec        SYS.ODCINumberList,
  startcnt         integer,
  iUseMagicSets    integer,
  modelIDs         SYS.ODCINumberList,
  rulebaseIds      SYS.ODCINumberList,
  filter           varchar2(4000),
  aliases          MDSYS.RDF_Aliases,
  index_status     varchar2(30),
  options          varchar2(4000),
  iBadFilter       integer,            -- 1 if we are handling bad filter special case
  iUnionQuery      integer,            -- 1 if this is a SPARQL UNION query
  coveringColsPos  SYS.ODCINumberList, -- positions of value_id columns in covering order
  currCoveringRow  SYS.ODCINumberList, -- value_ids of the current covering row
  unionKey         varchar2(4000),     -- identifier the current union component ... used to
                                       -- determine a covering relationship for union queries

  STATIC PROCEDURE record_given_and_trans_query(
    Query_Trans_Tabname     varchar2
  , trans_start_time        timestamp
  , trans_end_time          timestamp
  , given_query             varchar2
  , trans_query             varchar2
  , comments                varchar2 default NULL
  ),

  static function ODCIGetInterfaces(ifclist OUT SYS.ODCIObjectList)
  return number,

  static function ODCITableDescribe(rtype OUT SYS.AnyType, 
                                    query         varchar2,
                                    models        MDSYS.RDF_Models,
                                    rulebases     MDSYS.RDF_Rulebases,
                                    aliases       MDSYS.RDF_Aliases,
                                    filter        varchar2,
                                    index_status  varchar2 default NULL,
                                    options       varchar2 default null)
  return number,

  static function ODCITableRewrite(sctx         OUT RDF_MATCH_impl_t,
                                   ti           IN SYS.ODCITabFuncInfo,
                                   str          OUT varchar2,
                                   query        varchar2,
                                   models_in    MDSYS.RDF_Models,
                                   rulebases_in MDSYS.RDF_Rulebases,
                                   aliases      MDSYS.RDF_Aliases,
                                   filter       varchar2,
                                   index_status varchar2 default NULL,
                                   options      varchar2 default null)
  return number,

  static function ODCITablePrepare(sctx    OUT NOCOPY RDF_MATCH_impl_t, 
                                   ti      IN         SYS.ODCITabFuncInfo,
                                   query              varchar2,
                                   models             MDSYS.RDF_Models,
                                   rulebases          MDSYS.RDF_Rulebases,
                                   aliases            MDSYS.RDF_Aliases,
                                   filter             varchar2,
                                   index_status       varchar2 default NULL,
                                   options            varchar2 default null)
  return number,

  STATIC FUNCTION ODCITableStart(sctx    IN OUT NOCOPY RDF_MATCH_impl_t,
                                 query                 varchar2,
                                 models_in             MDSYS.RDF_Models,
                                 rulebases_in          MDSYS.RDF_Rulebases,
                                 aliases               MDSYS.RDF_Aliases,
                                 filter                varchar2,
                                 index_status          varchar2 default NULL,
                                 options               varchar2 default null)
  RETURN PLS_INTEGER ,
 
  MEMBER FUNCTION ODCITableFetch(self   IN OUT NOCOPY RDF_MATCH_impl_t,
                                 nrows  IN            NUMBER, 
                                 outSet OUT NOCOPY    SYS.AnyDataSet)
  RETURN PLS_INTEGER,

  MEMBER FUNCTION Fetch_CountStar(self   IN OUT NOCOPY RDF_MATCH_impl_t,
                                  nrows  IN            NUMBER, 
                                  outSet OUT NOCOPY    SYS.AnyDataSet)
  RETURN PLS_INTEGER,

  MEMBER FUNCTION ODCITableClose(self IN RDF_MATCH_impl_t)
  RETURN PLS_INTEGER,

  STATIC FUNCTION getCollectionElementType(typ SYS.AnyType) 
  RETURN SYS.AnyType,


  MEMBER PROCEDURE dump,

  STATIC FUNCTION R_PARSE_SPARQL(sctx             RDF_MATCH_impl_t,
                               ps_attrset         SYS.ODCINumberList,
                               query              varchar2,
                               models             MDSYS.RDF_Models,
                               modelIDs           SYS.ODCINumberList,
                               rulebaseIDs        SYS.ODCINumberList,
                               aliases            MDSYS.RDF_Aliases,
                               filter             varchar2,
                               reqIdxStatus       varchar2,
                               precompIdx     OUT varchar2,
                               precompIdxId   OUT number,
                               sql_str        OUT varchar2,
                               options            varchar2,
                               vmViewName         varchar2,
                               flag_out       OUT int)
  return number,

  STATIC FUNCTION PARSE_SPARQL(sctx IN OUT NOCOPY RDF_MATCH_impl_t,
                               query               varchar2,
                               models  IN OUT      MDSYS.RDF_Models,
                               modelIDs IN OUT     SYS.ODCINumberList,
                               rulebaseIDs         SYS.ODCINumberList,
                               aliases             MDSYS.RDF_Aliases,
                               filter              varchar2,
                               reqIdxStatus        varchar2,
                               precompIdx     OUT  varchar2,
                               options             varchar2,
                               vmViewName          varchar2)
  return number,

  MEMBER FUNCTION belongsInResult(self IN OUT NOCOPY RDF_MATCH_impl_t)
  return boolean,

  STATIC PROCEDURE SPARQL_to_SQL(attrs       SYS.ODCINumberList,
                                 query       varchar2,
                                 models      RDF_Models,
                                 precompIdx  varchar2,
                                 idxStatus   varchar2,
                                 nsp         MDSYS.RDF_Aliases,
                                 flag        number,
                                 str_out OUT RDF_longVarcharArray,
                                 sig_out OUT RDF_varcharArray) is
  language java name 
    ''oracle.spatial.rdf.server.SQLEntryPoints.translateQueryPattern(
       oracle.sql.ARRAY,
       java.lang.String,
       oracle.sql.ARRAY,
       java.lang.String,
       java.lang.String,
       oracle.sql.ARRAY, 
       int,
       oracle.sql.ARRAY[],
       oracle.sql.ARRAY[])'',


  STATIC PROCEDURE SPARQL_to_SQL(attrs              SYS.ODCINumberList,
                                 query              varchar2,
                                 models             RDF_Models,
                                 precompIdx         varchar2,
                                 idxStatus          varchar2,
                                 nsp                MDSYS.RDF_Aliases,
                                 flag               number,
                                 str_out        OUT RDF_longVarcharArray,
                                 sig_out        OUT RDF_varcharArray,
                                 options            varchar2,
                                 vmViewName         varchar2,
                                 flag_out       OUT number,
                                 valIdCover_out OUT SYS.ODCINumberList,
                                 orderBy_out    OUT varchar2) is
  language java name 
    ''oracle.spatial.rdf.server.SQLEntryPoints.translateQueryPattern(
       oracle.sql.ARRAY,
       java.lang.String,
       oracle.sql.ARRAY,
       java.lang.String,
       java.lang.String,
       oracle.sql.ARRAY, 
       int,
       oracle.sql.ARRAY[],
       oracle.sql.ARRAY[],
       java.lang.String,
       java.lang.String,
       int[],
       oracle.sql.ARRAY[],
       java.lang.String[])'',

  STATIC FUNCTION SPARQL_to_PRED(attrs       SYS.ODCINumberList,
                                 query       varchar2,
                                 models      RDF_Models,
                                 precompIdx  varchar2,
                                 idxStatus   varchar2,
                                 nsp         MDSYS.RDF_Aliases,
                                 flag        number)
 return sem_pred_array as
  language java name 
    ''oracle.spatial.rdf.server.SQLEntryPoints.translateQuerytoPredicates(
       oracle.sql.ARRAY,
       java.lang.String,
       oracle.sql.ARRAY,
       java.lang.String,
       java.lang.String,
       oracle.sql.ARRAY, 
       int)
  return oracle.sql.ARRAY'',

  STATIC FUNCTION SPARQL_get_cols(query varchar2)
  return RDF_varcharArray as
  language java name
    ''oracle.spatial.rdf.server.SQLEntryPoints.getPatternVariables(
       java.lang.String)
  return oracle.sql.STRUCT''
)';
 
end;
/
show errors;
create or replace type body RDF_MATCH_impl_t wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
a303 25f1
7knScyK836DrkfvRHi10gKrfIMwwg80A9seGYMA6Rw9/4lOeDh8locK70cTCcTdnaW11FFOm
hY0GReb6mKDqlGINswuQGtKG604/KhzP2HKhO4qNWnAcAAAAAHPsQfwnQfFOz/XpGLNTQdRc
m+W8OqUIYT8Uk60EL+G1nFxxAb1YH0LxSm/p8hgh+m3MEPdi65OkcmAyXNsDwj/tWm0KL8Uv
FSIYxlzPIm2wPhBZp1L3NBhtthaO8/mro5blRZeivc5rT+4p9UiJ8t6WYIjEjgcCMgxHLBpq
SuahIpDoA+UU/9BWomApV4dZxWW6WRtpC1rz6ZlHbc7gq7cwzEMDz+yW8iL5VybCz5+XGF/i
yTI6I+ELQStUBe6/gICAhKMzeu2Fydjc3xuoQP/YkS90oYHFQ2y9Et0S8kkBxRyjkLx6769n
nxIDP1RVJBVPxtrTyMHBm+V6dpCu2XvR2aBNy+sxHumHYyrNKNaoq8ZQ16U6MRG3yKUI+SNz
aSR6h5okPNB4LCvz7q4rHr8GmMzs5CNO3g3itU7eMrWawg7O4KcjEv7uaidzznwhHD9FvBCj
AMilQ1Kih0HdnSDumr+lu3LCw7jFIhqc3sJNpt58xIxWQK5vK8OMbWUTTDBGKdNrsqvkBHcX
zFjqadOcdRox5+icquggwvJ6nVklrWOrmzmLp118aun6izF17vtkmmFvaw4NrMZtiO9g8dDx
imY7wpONb4ZXodl8/Um32NhavE3nbB2jZiS1xCEmfQsYcmY7b0REcCNBb26LLmZDPTc05INr
cxZ0jxIvRNK9LyUUAMtAs6n1+SHE8ddgKWygZV1188iXm6Ei1QpqwPfdV4gGPoN6dG7L7MyV
DnZjAQmRwd5Uw4kXCaK9DgYxlBBdLt/5Cy0svqWMh1ywOuc8DeRlURCC4tcc/vZRVFklTlNZ
DJi9gVMP1QgUt3l1O+PGc4MEF7bdcyswcE4usRcNFlK8aGSQbmLCwgKdKFtXQ145uoJ7S1pn
0UO9Q0pQ0ekYEXjRGM4YnXJgnCfK4JWEQzbJ81We51jdFcTFfQIXIPN96Gnbk1g8CM8i+vF1
y1PcbWaWFda2fe6F28MOkaab7MJwmwiSCFIynRssQRwc+nLVwC48vx7WgevNYgGFLCBI6luJ
a2qcKkv4gYY4fKCtE/Aol6SLUvtxH6eHstAMy9/DBmxEqygxzvFmRGCkwrL0ImIoKSAY6OwD
qVrcrOWu86gS2lmbldhmF12FFUBNwATQYFJclKikThF/kIdE869F/FFZYHkG/lKRz6Qj/QhX
lrRoF4LAzzARajqwtmX6Tr78otM2DllvdGu5eEldqXYorly+FCvX6yxhD0ZDBjRGa/TfBHFJ
hN2iS1nhmYisTab2wp3U5m6e+3bynLA81Qvj3DHFZDrlCGeSnwT3jyCltnSQ1FmXswC0t8mM
kVNVRzpOW2nvghHgPGdeZa9QoJQa+hQkuyhj+JikRzHgIHCbJmi9UDWZPVQPBOvgd0xym+Uc
aquIKVIJZmu7iTBbmimbNXXQdj56jnTprLOcMUtVJGYCjyF0eystGptqVygF1NgwjkBh3zyf
kTIBhJ33iQu9lpAJ3Pe8PmV96FnlDL/ENvSA1fQx9csdHg1yK4jC1vZCy5JjuGnDMyfLzuc1
ZwVaRqUy3df8x/cK/8nUjHldf2y/KehhlJdWWH4f+kfTR2Hd8GyQDYUr7asDoqA7Ng3LLZPC
yNGvznd+Vv2NYdwVmBqXSgUc1YNxVph1Q+nldUkQO6NXrsmUUmzp+zfPAXpPXly4e8ElY9Pv
rvl60pmR77Bkm1x6l5pa66zl5M335XEaZGtR78cTRAT4TRecoxIAPWDzgXR9egrmk99mXY8g
mpBN31aY54ZKDlEslxngk18TZjYsUEtu5IGWqqR7ASfIzYtFR3+Fd0c55hkAEM7OpdJOB/3I
uJp/1tZhb6VFcQPU/G8KxL/+u+X+1cVsmtgc9S5yGmPYw5+9am5DjXDf40OJqq1yTqGrgjgY
iLqRa0P1WGMgRkwuN84fADHZPlllNb32rVwzV25p1LfOUW+e0WN8g/izPrMYxmda8DFfnjLg
8GCvIbyF8QOeNkoshfNrJeJzh37RFwuHrNlsovMEujQjlgujnsKM/XF5QczUvWgnb67LthhN
lXE7M2l/KVpLpn/dSjl38SguuXbey6hE5w1n+wD8bB/B6YDAY+b7yzfPCqCUGUAKGlQgZ2mI
KehoUUpdRqp+anlyGzmym0LvZQ9j+tCMROp7XiKSaS5qqc5xJUuk3iKIJyk8XTSOa7KkH95Y
rqeH0OKFKdJos6vmthJER6LlK7myoJiqiAIwTDQY2SOtLPsSOOYsmgbmvZpQh7jX9bNXMR3g
AxbyztPj6VhvzzycXjwE46UD5cPtgBuXBz8Ti4WR+KwpvjYgdN27VRUgkX4jIxV+PR3421Ow
cQ2mZKuV7lfVl5fDvjZ4XAxx0l+Uw1YF2xPsNG2gJenmnjUUq9AaBCF6eSz48a5T2GMTgu4R
tV1qKnL7EkNThbDZJqYK3tykwi2Ld9OZYtqN6chAiTWIFRZkPqIj86/2UIprx8ffiNdHwd44
gWUaJ2C0JaKW76j0pW9nFzZF7BpcRQuCdIlrDIbRG8juKlDHB+0oautOA61AqWh6yIFV774b
/sop5EW8Ok892sD+CAiLSC4/vphJKNCx0K4oXbkfowYpbzYOE4GYPapEIkS0NnCeHejPyVod
HVlCLYUIxsDS6AQHr52nbU7eK1wp43wIzWwtWaFMPVw1OIbP+npwooPvYoZ1UOMWs4uU3FP9
9Vo/vr4CHOkHNHkDgOvVGmfII1BBcI+3OspwaWhj8+e/IgCtnCAZQVbVgf2XpfxCcaY4wJl8
gnyiyuQcCfi2fNPulrd+usd1NSxzWI2J8uLd3pE7a0pA0t/zkyMJGo2ylQIFlOrX9e9sWvZH
ewadB2HiECwFYmpO2OGIf4nt6V1U2BYvr3OcSeQBDzgO9DZNkkSP1dKpYXjdDmqnAOo7UAGa
q7pn8IEPneJNodFLya9OtJKWjU48z2xHJW/+hARt2Sy6MAdqDIQn6F0rk09tzplTkAx8YHL2
FRFA10yoaFE7lon9V5DpxvNOWjh9mQRASS30ZYPTTCFUDWcjTnI/ZXb5gc0nEDlNQRoDbfxc
540HcnKvmv2flAhtcwR3yQQ1eiut3afanE7lNOijuBGWcw7se/m9Bdzm8PtAJhYKKR7sslX8
qTlaz6RUzaWVhLVQnFmV2smPZfdskBqh6byow32sxFedNxjLeqAmOI+MmY3OOWAtXs0Mfvlj
9SuegYissSuyGcPtaMBAPaOEkOw5qsemeYv2Z9p+loPPLvAEA3r6weq8Kex+4JjfVu0Fgzv+
C8cbUze//ROCPP8fE04cfv+TKpDv4iNOeCk4NbbaIfbNQzEIPsGFBtUqhVjmq4wwa+rskUhC
sEpRIlSm6SH6SYlE+2ndGBHb04HmAso7r7hYLNiMvWKxJko+aEz24lW3PB3S7wBg+QXS2khd
f7ZuGus41JHLlSCpLoqFwC/RLTHgZ3FeBVKe23gNKgpnBJWnNOHujhPuWziU2riZRurSOB9O
vbH/0aBP5YDOFtjyi1XzKjpqQofy2u0/FlXyPIRqFxos4fitD18IU++rnGcPJZ3C9JWO3qwS
eex23+gmDfq2yYNputmJeUh6x/b20u77rgpySQiKgOv4UlZx2uYjCehzRwNDHnU7kKSH6Mb/
/mpUb8qszmhU5JPCFjDKq+OY6Xvomgw+9QhyCw4wiGIUnk6xAHYu8Icay7VeB23eof58ls46
tG0Qnz9YpwjX5CQYI6Akxxf+rihbabCZ5zLhk6ySvEw1oAA40w4TPO7npx2CBM8yYKoSo2bM
Y3UVZp00V98Cx6jjoEVuVqgs8NKDPPSg9x+han1Ted/AtFff6lupzWhamOGR59ZZvyUeySC5
7K884klgvuHLxrsFSOLLIlVoJCJMmvEov7srxGjRc6XSCIZVc4b8pDrNiwgAN0izU+s3htzC
bwwPKK4k/v+HENp9T0zkZRG3YTLdOpV3z0U8WDDVvq6HfT22CQtEwuA4JB0trf2zARdptvZH
uKKLlUBiWKeHPHWtFbKDC/EbIAmekNitCJV5J63zPLVa9LJfXCUf2EH+PCDdiVcT7N4Whngb
XJVAn+Sx/Ntf6fkd1r7VvAtWB14DqdkZ0uoKdY6ba+POEe1v8dqUfLRJHmNsjYTzTphcaZ5o
aWDPoIfZ8PxaO+ikekPWSgClojJR32cIVZvuv1MkOy7Hx/7BF9KquYo6aFDBvvfr67LKGTHN
viidsJZ3fPC9QKxXnatMhxtk/dKzChNxh+jqCpxOl8uIpQHoN+nrbkN8hK6joVHomDYSIoxb
elPInhOdPTuPQsEIWnwCgmqoAtk78r42jXsdiznwlugDyFGcpLipAJfCd4bA0do5Uqz0vuOg
Kqtey/uEHS7PzCvdbIvpP/MVnHee293bD83+w6ltguNHJircCl2NpkHE/pip/4zeRXDqL8d2
XoctWhN/8DxuXWfNcbWRKs20qt9Wa5RsQGVGfcXRPkKAiF/hUIMIJrVl1Sb7C1gbfESUX3h6
HGaXDLYPCN05DUCW3nvRmfUnGFI4Dgc/RlVAT/h/jw3c5FQd37GJ9Y/Mvewzpe20rKKIUv/0
vCosB2mJCXS7/QBQjJheBJ4BUsSG6MDD3tv9u6MDNAJaq1hAQQOAivbaHTDC76nUqBvJ1f1R
DugF33L98OM4v+/X295eq3NOFWM6i5bhjOf+AIueq2F4fPtRWbCBUex2J42r9dSeIdTxeKdd
Dmis8en6q6nxjP3fDj+WMMbIvPPU0P1KQTGu275Rh0CNyUcYeG2Iiq9o25pLwKOAcMCEdf44
mYFUvMDNxSx033+Zc/thDL4S5DhhgK4Vx9we1QNLgg21fUztqGXPCqajPkj9YUly15phnJP1
ux7yc7OgAIgjBCeoNQuaeLilvsqB6Ov7DWza2zlLzeGDpsF1iZ7nSPB31eiz+YpYT1Jj+I+0
J4kWwcTfXPoG/DeDZhPwJaRCeH4KvdrJ3ALdiy/vLTWC88yuDArPaMoB/F8sAGZ6CGrkFyS4
qoGBcsQK8egBqCT7Lw4crt40jc4RPmqvmvr7QhIS/qaepQWw+AeY+Kpvf0zD2NtZ/xjvdIk6
WBL/sD5tX6UAwXFbxev6MJZKTpP3nT47Z6RDwvxepCOwWZem5rCfBJWUETHJb47bqtWQA0HV
KwES3XNhLsuTJbm3lCw6hAQbXiV2SaRwXXqQXQNtCUX+Vv6D3Rj88W3Tix2TiR2ZDMuTM5l9
wNjZoCyLrnlUhFHMYGwq61J9I8sxIYDhL0f8u6wR9dpFXeiv+yQ+Ty4JM3SOxdx6QCI5yuWV
lzUObDZ3z0lwB2hX3eNron/qAFsh/y+fqBJbUgLpoicOffN5kQNmYh+3xduigJjVHlPK5cFF
rF/Nx8Pw9EGrXWBKXpLGw6Ju8+Ot3mUSRZk7v/nQP0mWtBoIC87L7iHobo9jESM2qw6UcvEh
oOXj/k5WLiV43TMV7m9App4rbagOajmPCrVzKqpgMFXFMhNFu4YceP2I1egH/5bzmbURWEsm
DlCRxHZq+iLUykJIHspG4QVUbyp2fqZGUF8FZ2kTWoSTaBVQBd3KeZ7jJZiNKjfwwIL2Fbt6
9oVuzSILTwHpe236DG9OrerTrC40/qAozQaN85fpML7fBs6rlG9HaxTK6FqFlsndDCNYXLI4
H8mu//8Hva3gTT/Aq/Z+EIzqAUvl1cCYZcMkWJjGi0WMR+1bawUu1vum+i+hlsnTmcZvkmaF
1CGua8ih7yJLa3/QoG4SoykDvWJT3eCR87aMjvTbpiEMqREfh6PpnLJGie3H2NFIga1XXHic
m1fDLLlG9IN7JJQV8Xwij9JKJodvxF6rjZG60k77/mTNhWeJ//+Kb26qphBTVoz2Owlt9DDo
cJEFC4Tcaiv9DLBilW42TgTrcF5+Uy2118Hv4ovJ34WoiFRz48Z71hTjNdyUpB9HY3zw6QEp
GEk++fIB9NIPbGPnhpWH8G0++eXyWl7WLXS7gYzxje2ELY+bmvSBonIh0meNBgqtssiM/JmV
vdkmn0eycvMsS1rXgAfzUuPaMU3bSNs4DV5ql5wjUWJFsVjikzPKSbFi73pU8E5EuLPpXK7C
k6s5B3fcYT+SpfGA8XokttlwaMPekGhVIjQU5dMLDaQyGVU76pb04oSVt0rFILHQlvTM6ZKl
16+Ykbl2tBta8lN5kUBCkwzXPADMsIE3hyVqSlv8M/VB/9znPCFBHLKTiGqf3hEPsJufeonB
bg+0oEWN/35Ojqo8+3FSL6CA+fI3v6cENHxh5PCSSK1ntLBFMmHgTvwrM9HLenwZuRhq6HQj
BlDQi8HkNK7rUk8B2DAdDSzfiyQrzb0MU5LcMYQuLOzmz/ZgAS9siteFWkTGdS5Oa1GIqrv8
matXoYBv2+Q4WEyPYCOhMrkmWDNOeqWnQ60qSobjuZfsbY9BKPszAT4e30rIsHFT22XuKcMw
inb5zQ0TmSBYian+U12Oea9H/JfgIW63k3mra4VtzKNuD7NcAlm2mkyBCOG3Ms89q1vZFPk/
QzJo5QGQ/LpYxtiDSRkSsDyf+FLmrr9KAIHP2NRIZlnboxDp02BhXWIds1mckcEfklhVs0zA
t488/0xsBpl5E8nD/aP4MsfmSyiwfTh0pJxhFl6fyf3bWCVfqZWHHsrq2esGGtjddhRZu1yg
64pgMV/KREPtkzei+hG7c4GwjS6HX9Icmh15ORz6GPxeI9HBDql7JA8kUMOlLD+8kw9eRbJ8
gVWjRDvV+APwu+uBzdeYcniX0oKuvdpO0s4/6Rwi4hkZ5MAdz+Z8B2B9Pc4jKIyv6uO0eoGp
zSQ5k9q54NXbna91OP3hI5EyOAwlpWJLaRvlr8OIgJJs2k5qUWbxoTkTBrE1jqWuegFtR/tq
Qp5ycwbi6hckGrpwNk/2+dLYGGdCO04xXZ4iObAQ4Dhf2Ge5yNtMvaYfWpNrPBMR0ZtmExiY
ttL74YKDD06FNR6+N4znYYZavVT8dabp4wBUgpDKGU3cf2NDN4W0zjJm4D5yBMT94y78rl3b
WTmKQdnJiHxf3QKaW6HGqqIUiPUgTXfUbxqrpYaLJjRZs8XTsi2rr97Wgtso5ptVBUFVVDJK
VPACHbqEec5LgWrceGHjZjUJBr0OKSzs0z6sLWv/ZphuEvhcoXmrPpgUGKEA/Eo2y7N4SUhn
wANydwWUM7ISj0c4LO+ggDl6F+p7UjqnnRxcZFY1XARUDJ+ITjrPUhWxuchg65QmWIVH0vbu
7jVU8tqW5mkRCsbn9g0hjDyC7smhQmeiCWQWFdFI3qh+ToNYfLa/oI+/RnXtAjFQo2Dm5ua6
m2QIZ6N7rXl0oidSXoOxJYUxgsbG6JQYNI8DnV+EpLKKIYuLmbenjtp2Cj6WjwGEe1DiNKdb
W13N+LCBqqlnTqJikGb4aMO4bw3cEqDWGsJqw1b93kXVSmE39lpRjhYinoU2npCCUXPMwpN8
FoWpfGFGBn2wjzqIkj8/y4fx+idzHoYS1SlBqV9jGdra+K9vmdp7dw1S+vMI0LtYnuU0GEnP
BsivCIgERU6taUqoOL3ErQxtSYXad74KrhAft2FLF5nnYQMX9gthJ8mZ/dI/VjESfwu74GoZ
lantbYEjH05Sbc3ObRD+KPaa8JnpwcOqArmAKv0iH2FHSfJ+9DLLbJhGWstcCFo4Ohwrr6Bd
v5P64Xtbt1bKmkcleP/9CuVCT7cVMdC0agHT0JvgYLjqUq4em2GxLfVM1CUylG0ys45gOWAn
BiPx/4o2k4MUGcYbSuthhpctu+D3RSCNamZjbFWxdkqj+K1Vzd1hkhjHD8BI/GEeP6cNr480
NofS7a/jaNJwQLWdgvDOH5zdu4bTL6KHqgi9daFgq4rkvDUCb6DI/pXxtZXxtQdhXSDgl8QW
EoQLIuDX/m+XtR8SIkU+69XHrtegctRCwa/dgcXybMsBdAwLmt/B3WZck1qDwkfYt4QF4Aqy
BMmosZHV/ZHs7aBjDYjy8AZqMwRU1i9bvAcNba7sZvLaYIoao6SP+AWpmeWvTg2J6zaMkKtL
RFqYDplDHkS4JVI55rzhjr4ZJkQluVUmQve6yIOVRUveMwVuCvFUgMzMT1QsKOQoubxHDRQQ
u1elNuUWk/S9w2bRoX1ht4UOQRtr5hLrpRqTJ07B9N2mFEEiCZbLPwQqR36f4zrcEXknuXmY
VeYRiawdbpIg5vk/05w9aa82O2F5dm/OghGDPrs1sBCrijRZbSRRRAF5PoUj7fYb/Dk5wpNO
Gwyn08iPqq+dVAVQ3RgoOyn0eBjbjyk4Mp2jqlFBQQcTv698ntTrDMFJBN87pS7xaaAUgw3t
TTP8fGhNU2ngGXrWO6HLYKa1f/55L6WZAVDoRbEOh3n/3NmQoBEKAHrEcnI1iRXJnW7Q/+UO
zcI3GRxx36Ms+A+4pJOYTEYLjnl2asLyNuCLsim2D49dJ6wV47HMLNNoqIao6KI/ltkNAYSi
TcYzMcLMApp0LrEgKIB5jApv0OHd7MtaUJYc5/PjVYB3TLnyqda5PeCAMH6+EMS3MJYnSFRM
kBRIt067Z7wQJsRiVNuwU+02XyGw6SHmYDP3/qUMtIW1CEv3p1c1IXOHhDz2SfqmKH7IDOs9
Dm5dQtTxJ/SaalvosXq8P8jFGYIjDE+aWQqT4bLlOkriibuKDoR4rmuHxDPeXtVVyRLjskEC
XGQwFEc5F1v9NoNXKWe/ng01BXsp9/umgdMIjqI78y8Zc3UYwOlWvZ2/zFZiQsZRqoqUKYN0
0lccy79K+6ugB2QE7zjYg1No989yAyt7oxgQKlGtCRmYBM03swmE5mWR0rmMH9u9z0JrUqyi
CgRSIo5EIzV7QNlMfLMd82ByT/P9YJZ1gCaxLmETzL6eHgXOnrSL+A8T0aoyID/NUzMN/pBk
E3HORYF5ZP1MgUUeAGQFcZsnaKVFvNpTD3Ofqk5Tktqv4hIy4y/SuJul+bnEYQpw/RSpi7oM
AdM/lCXxcjegRwrCOR3VsKaZitOhvyngcH+PI4GAepijGtVHBp6FPSIBQeNHOWCmk3K1VeSZ
SDN4WKlrbHdH4zPzLRulZzbkBCnhkKanAeA03U4oCQhN2IcBAfedLWpUBrPjAMFSSTzVfdby
KwZTDbmzV/4HITiRLYTxW0X6iS5vUfjW31AaCazwLE1WUECOyT1I4oqKxHV6xQlxjsIilYgj
dx3VtFKNlCuAlysnNuWgPnEpCDHlK9AhKwTfZ8oVtvY4L54poW8gR+Ml9uzEW5NrqfKO8YrX
GpjYbx6Sxom9r1F0Rb4YFxyBm/XQsp/zEmyt+qi18+uVdHEYeFR7G3heKlmwFBEPMTNAfRtj
iTGGYYo9VimIOPWMD5Aq9wYrfBIX7vlpRtmwzGWbOc/FFRlqaHklXPhG+qddz/rl+FhsmpYn
RwQCRyfrKcJyI2i1GGjjAlEcDUF3zq0pLbeNZpP4g1cmXmiSZhQcCkVZoc3yzKVu8sm10p0d
tgJ7

/
show errors;
CREATE OR REPLACE FUNCTION RDF_MATCH wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
224 107
/cZDE+QtAQTU0CNsp7TEOZHK4fwwg5Dxf8sVfHTpWK0CgMgvHdW33nar+i7Wzcp7Q/nEDvBp
a7pGSR7ZP8L5xHItA3xgL8HZqTyBB4AbWHAmSfKxjHHcEhEKOPXbo9qwt0HVoCfX5/pZeMMl
LNvlKl7IhAHkHJuz7IgiS/fkQ4TjSofpsmIeRbJKesnf41i+iCrobBwCLsr9w7YpV669k6fn
bSwESQffBTjI5mbL93aRsgj5EEbTQNuV4ph47qonRg==

/
show errors;
grant execute on MDSYS.RDF_MATCH to public;
create or replace public synonym SDO_RDF_MATCH for MDSYS.RDF_MATCH;
create or replace public synonym SEM_MATCH for MDSYS.RDF_MATCH;
