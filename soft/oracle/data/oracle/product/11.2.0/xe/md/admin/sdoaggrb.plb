create  or replace library ORDMD_AG_LIBS wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
16
29 5d
ZR2LNLjHgVPt4O8eXQh3A8P8bcEwg04I9Z7AdBjDpSjACDL0v/XQ/gj1nivnvZ6yy1IyzLh0
K+fLUnQI9WHJpqaBlpki

/
create or replace type body SDOAggr wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
16de 532
CbwR630FhZcpfnGfk7tcwupxr4gwg1VUTCAFyo5V9nPVubp5Yhir6etF6RC4fgajwmvhaCoS
CAua/rUsOc6ZfLQhoSreWjK2ItHBduI3tj8/NbgUOohU3CX+/iTM+lHnBK5CWK8mr4iWvTnx
FwGjjo6UPoBs0UJYK0MgjgNfnfveU6tig7fnqWYZaXRTJtDj5vrKrO/F8lYVl1FA5M+rYNtT
JuWyTbBzzCLxhHjY0MU3kTaCvAEup6jUooIVvjBgae8qjFshgMPkbYjl3GngpNLoEzWN4fej
37Gt2gN5mjeZ9/GIln8YsZYn3bSYjQ/zsBf0q8RCPtZSw8rlxVCWzF4GtrNg4+zfYyGNhHuQ
jshWO+Xf77KLCVbNBeq0PmHCibAcfF7l+6pgMBQDDnVW5ip/+XA2g8ulXKIVpMwqy1VmYcVX
eWUizPTwcAbWJwtSUQztVMOm3MUtEVeEhNMOGKgo0/R3yej6GjaOEUGhy664llJ4VBT67fpI
L+B3cNSd1WoVbfSy5VsKT8u4RgoyZeeTNwf1g0pwhjjs1y9XVK62i2CEt3240m7Ij2iXPYAj
JF5saa2WhPN3WRPbL8foiaQRD8NgbkrBCdt8wpQBZZUDiZpGTuBF2IsMqcvhu2FqbUvOXuHE
dJQqFW6tZedRmOK0rdNuo0oG3GSZISD+OqrKmHB/PrsvHEgmc0WS9puZHGR9zZOFI74zHvHR
eTm2rLSDUxvgv7ZRqF/L53arOUlGWLBBdOeDSlYb/79yvun4b7MMV6xnUhAXoRbsiDKcoziW
IxsZy/KX5E9DkySdjQH/FG3teBoahBtjt3MOGyvduScWZPcTVWqEZrS7yikDoB9x9O8SWl+Y
3uB1ow/r5SkTTPoAWvnzBJriN3/Q8MXDxAm+ME92SDZSKEZKpcqR279Qb9uLoCQEPe+I72IJ
GAm11IbBKiC9AToWVJLfc6nuJDmX/6ZAptAHAtxIrYehPXeFc0rbmKKBs7n8L+JpDxC/bfAr
qo1z/RMgugVnfVja5b5YFMdBA6v1Hl2kePOva7CCNlSBvtnf6lNxNYB4Rcu3mAl750zFWhN6
v8Qsvk9u5aBw01oXbreFf5w45Um22B0p/hMpoIfCxJAJbeY942+5Dx2s0zU3eyeSP+T++9eK
cqQnH6oK6v9fJ02wc8MPMR2QZtKUCrkl2a8p0R3NQQKLrDGcihk9h/ZFCAXCTL/uD7F6bmHr
Ukw4v3Zd7jzkIMrd8cG+isAgaOh5vHimiu17D8CiYPljf2v8ALHcFa2NAtYgcx8MHpRBgWCJ
03tut5sIQ5Yk1/k/

/
show errors;
grant execute on SDOAggr to public;
create or replace type body AggrUnion wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
90a 36c
xO2xgxDAOZ5HuKoXKzZL+wKmGDQwgw1cr0gFfHRVMQ810pEdArEzUw5b6ehc+2HDWpunivHZ
GMoHkWdQc7UWMS2K1Eis4uuo94UX/AizGlWSiDgmbSpLzTO0dp+dKPSgvVTGbl1p3xjaJpR9
rAwbRjWvdvJskJGN5PQmQ0hE8eTwHbsgjmsDNt0O9l/hPXcMIWc5cG/F6KJqNuH+OU1dJXwD
OXkYRbR8LajWshyVJ0YTc2QkQVC5SxLtvYssJQD7sZful0RqgoJG9RFWGq9nCk6aQf9BBn1n
z5n6WOtAJSGy0FBKoIvChpBi4yPwOCSMREzcqiaV5Xq2UShZDA7iAA7RioOLYVAcQ1r5X+IE
CkOZaYWj5QmAzkBW/cl0upbhh/URe/rglCjc6ghLoqhcyqjZ3cr+JH1a9d413oBQ5eH+RsUJ
gfO8AGG4g14/GIZmkq5354ZC9xk7X/c4e0UY368X634WhYH2RxT2JnABUvG3ck3WTaybSP5s
XAUeopRTB3oHW02gP9xF5W3RWAPCStO+AEy3kHfIAYMi+QQZGMrpSP3/s4N+VtXiGm1myybX
RjULoIhbKcWEqg+7u5uYImidw9uAHk6IJZsSZLNmiKBAF6E5m2LRITm9WhpAO38xb4IK7bna
Cqt0YpQFDpbUQ8oBlUaBeMRzyLK2iHTo0K7osZyGIlnoxNbe4NHtiAyHie4/nxayiXV81yJv
wCykZvV2SUqYigku9iHgSHevKjvNxrtqfUxXTz95biixkbXAaNw9XbYMJRcljJigGQpwia11
sKBBq2e7z+r/vBYteKPzJmrG+T92kfqwM69Kq+4k9rle/bkXN4mGcBI9DLuwMZydZyZ23Q==


/
show errors;
grant execute on AggrUnion to public;
create or replace function SDO_Aggr_Union wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
75 9e
r6JsRRjeDhfQtnfjlyXR9p7SbRowg8eZgcfLCNL+XuefCNC/Wa6WGCjX2Udy67j1vyjAMr90
UjK/gf4ysr0Yw6UyvUp0PB3FHaECojlTJ8A+Fja+QEY5OqhdBu6Xvo8bFkY5EpK+1anOaz9n
CtiIppvGqCo=

/
grant execute on SDO_Aggr_Union to public;
create or replace  public synonym SDO_AGGR_UNION for MDSYS.SDO_AGGR_UNION;
create or replace type body AggrMBR wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
3bf 1ca
yVYXyId88P0xsTF59mTHtScoHccwg/AJmEhqfHSKimSPb/3nX/+ye4Vl7+dLGmcaGEUM1Ipz
tWuJQxTLBqvuZ/vPGZCFhr1rmXPn/MA6MkzYVlxn2LQnVxtkMTkoa7EriLazFmAeYroqtCzY
oU54RtvItyVow6bzrekoIr7L71SsnnshPAzyfvzOzjTfd+Kw96P8hMt3IrJqHBiqmmrcBhcm
g9ctiuyLaRvXEmUhpHpSDKH2FPEhkofPRMtJdLVQcF+bd0Zq89gPJbScsreIdNGwIGdyrax2
xov05aAt2i6kckMHW6Ti7eGQCBMFXBEoBkvkw5AHVQEWesTelxYXc/F9nvp51Wmdd8i4CSlY
BR4DpkrWZYlSjjHsJwGx1hq/PadnXUDig2KOGbxtk0su7XZgAz+OpZ76vXHEyELN8oft1qxj
X37BGAWdF/4zW+jDQy4=

/
show errors;
grant execute on AggrMBR to public;
create or replace function SDO_Aggr_MBR wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
65 8d
0U3HMSZDAsKxzMGsc3qZfsXqZ1Mwg8eZgcfLCNL+XuefCNC/Wa6WGP4yniW4v4H+XKUyvUp0
PB301lPiqDX37HGenjXiqGs1P1QUlFN2VVXWcVWkhHGU5tYAc2iUZ88O3jx0phXKduE=

/
grant execute on SDO_Aggr_MBR to public;
create or replace  public synonym SDO_AGGR_MBR for MDSYS.SDO_AGGR_MBR;
create or replace type body AggrLRSConcat wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
3dc 1e2
zmiblJQ5T1Ujf8FESh0iuSPTBeQwg/AJAK5qfC8BeT+VeS4aFuDo4LhuCgH6n8X4rKunnn/1
Hb0ItZUxSAwM6/H90foQvP7AIUgvg7z9NYr+D9RbOjoI+TNMfWlnyjWjFSO48fW7Drsab/JC
w6xp2i6jBHMYAcigAj/K4/pXUb8JaVymMBqv2rjWebPWy2lnbfLa3C1jvLDB2WElnzh0Ln2S
BNFVcFmyMSQ8S13OSva/vjCTsFn1XA2XXija71TRnndGNfJ0N+t9ezNEVFkwSC/v0uQWHewq
Eb8u2P8S8WJvbeRRS8QtkuEALVOUu/jVHSjKmKFisJJM5KKbhvzmjAuWZBtoCYK9KmErgD+1
UIZvClWtrxNdubo5M2ZHqaGM9htOF/vY8GmoUfpmLqQoBz8CrZr/SfWJwVnb+LB8DNVth6T1
rCleqZOOtkcz8h4p63X4ToJTekWtw25KykUHLP3VzRE=

/
show errors;
grant execute on AggrLRSConcat to public;
create or replace function SDO_Aggr_LRS_Concat wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
a0 ae
SlJs1QTKW8lBbWIMVCbJR4O4yuUwg8eZgcfLCNL+XuefCNC/Wa6WGP69nkq//3IM3FazuPW/
KMAyv3RSMr+B/jKyvRjDjwnKNs9MrO/WganoNyEHojlTJ+GBNr5ARjk6qF0G8zkVzWnnsjPS
x+qmCDfcuDP+x590LcmmpmrPFhw=

/
grant execute on SDO_Aggr_LRS_Concat to public;
create or replace  public synonym SDO_AGGR_LRS_CONCAT for 
MDSYS.SDO_AGGR_LRS_CONCAT;
create or replace type body AggrLRSConcat3D wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
3f0 1ea
gn1JEGBOV9QOS6hMlc+2hXdVO0owg40J2SdqfC+KKvkY7ZU5C56+KtLKDP3vyEPKOwxobT4A
XTzmKbXrjYkaofx65WqjPLzJQGs48Gm/J6YtVT+IwUPBxvgF/uYBgxePocinS/w7FFWTfYl3
Ud5236xpui6jBHMYvpygN+JJ5/p8GgMJUOaUiKmE/JBEBcQqe1AcZU0ts6dez+DB605CCxnF
JM6XCtEUwAX9XuYY0GxHOhBd/H+N0JkqlgtlRlFU6LOYlfuCsULlz5ZsI3LeoBgLOWJgaR/O
83v5XRzXnqCQBuroLghHyQP13kv2hHO9ROCQmRkw4jkoDp+kusuBC87pwGqbouUQlpZJaGkq
vd+gbxJAeOfADArNf5XV0khBRrnpdRyMw6op5Bdol+PUW1qhJj7vCOtke8YFMdwS8EqJNLnj
mDb2syraLY/x74/R8fMRAKg6oIC5MjXhdUDqmBjW/eYwzvvEFAia

/
show errors;
grant execute on AggrLRSConcat3D to public;
create or replace function SDO_Aggr_LRS_Concat_3D wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
b3 b6
CcrtweszyHM/DTqIyUadVln3f9swg8eZgcfLCNL+XuefCNC/Wa6WGP69nkq//3IM3Fb0eJ+z
uPW/KMAyv3RSMr+B/jKyvRjDjwnKss5FNoA5HvvwW4iU04iP5b0hqIUqFCGFKKgf0HwcSIzm
EUuwT5jh4yj1twOdTwML4y1tKh2mrqnV1Q==

/
grant execute on SDO_Aggr_LRS_Concat_3D to public;
create or replace  public synonym SDO_AGGR_LRS_CONCAT_3D for 
MDSYS.SDO_AGGR_LRS_CONCAT_3D;
create or replace type body AggrConvexHull wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
404 1f2
VfQ/q2HjRQnRZ3FLJ2yGeCKbaIswg/AJLUjWfC/NR+pob8gv/GhNHXXWVBWfRnLMffEml9MN
CWMrc+RBLg5rqCEc0RW/cv4Z2YxIbWl1wtbkMOQvuyAPZJoTcus+8d/hDK2h+43rrBqzcPKo
TkVILf+9Y6cUTmC8cH5Eza9X4ZqOYLu4nVIut6KmYvH2KhGwnn41o+MO3aMnSAi93bDBJFNU
jWEDWGLwndAqD2NQscaOK8+qzqSHY2uGDPHY+BdyDABMHKCJk0k2o9l5d2yx54455nga82Hk
trp+xcgjYKODlp6wYIzkd0YqLWT6jSNg94M53VcQpU9aecRBVIJwQknuQcKTrBzsX+Wn6X+i
oRA+HmEaQ+uCBUPwanDXDYn4muXsNGFBWbvM09d+7M54VPWhrLYhfnLoKeNb+GDJjGtjkdrF
ajPptZGLfrtRmJ4FI398uNBsNdxSc5pHbCkZ2NJHkpwDTtt1iA8cP6bwgfjo

/
show errors;
grant execute on AggrConvexHull to public;
create or replace function SDO_Aggr_ConvexHull wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
b6 b2
634JY53wuXqZokpoKqunVsONbQAwg8eZgcfLCNL+XuefCNC/Wa6WGL//ctUMO6UW1/qzuPW/
KMAyv3RSMr+B/jKyvRjDjwnKmc5FNoA5Hvs7plgE04iP5b0hFoUqFCGFKKgf0EtESC3mEUuw
T5jh47uKux+Lq+7z/IYL16ama/LVEA==

/
grant execute on SDO_Aggr_ConvexHull to public;
create or replace  public synonym SDO_AGGR_CONVEXHULL for 
MDSYS.SDO_AGGR_CONVEXHULL;
create or replace type body AggrCentroid wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
3cb 1da
FYogryS90jXPpe1MatfugAOEX7cwg/AJLvZqfHRVrZ3mxBsvFf/V2ldKJ3QtTH5CIe3Swsyf
MQ6KtYKqGkwDoGsiFlQ8txkS87K6Rjz1HUuGCNskvD/cmk+7AvVEV+AUrX40ixTwOetS+kZL
FgRrd/EUHkMcDMbeW9S224BK7Nj3TFymVBQgKhFko6DKpI5QUch7egK3cJZktsMme3hT1DIb
yQzN+LxAdk/2VALk7nFF5V4iQOWjwaeG3VqJ/c1NUkHH9dF6jLnmVmc4ZseAjmuyTyU4nfsm
F1H8ehukN2CV7L0LnkgrglX76ScTppz8hDRmMTgAf0NfstK9v3doGh9rPenrwhqmzAkPbUVJ
ETqRhyDY/nV+WJlCNLHoFxXpxTFr5VN/3gYZTzWxIRW5bnlFZcdCsmByb2JeEZUVy93ve7Zq
M51VxCuGs09Pk0O811RSk6wEQT/7RTP8hw==

/
show errors;
grant execute on AggrCentroid to public;
create or replace function SDO_Aggr_Centroid wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
b5 aa
z+5VVM8NN7BRQZfS8T5Pqb5Pfk0wg8eZgcfLCNL+XuefCNC/Wa6WGL/c19U+l3JZs7j1vyjA
Mr90UjK/gf4ysr0Yw48Jyh/ORTaAOR77zaaIq9OIj+W9IdWFKhQhhSioH9BL50jE5hFLsE+Y
4eO7ivwf0127KeDXpqYfY8VM

/
grant execute on SDO_Aggr_Centroid to public;
create or replace  public synonym SDO_AGGR_CENTROID for 
MDSYS.SDO_AGGR_CENTROID;
create or replace type body AggrConcat wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
5af 22b
U8p5OKMxi422nx7Wdn3ZfKCdwZ8wgw1eLUgVfC/NbmRXf/Mm/FCN+rQH9j5h982gGgzDzEYE
DtMGtT0Day/m4ZQn7RQ/vzPtEnIf5T4VtBietMC+Qw8TOsbRXiIdLFRpdhtj7jWTHSu4lRr1
FpNnAcNp2Gm7V9J6R2fMNHQvhmir/+ZB+1d0Y4J8Ys87WWf0SmdwHJ7JkfoU4dCmjieKdQrd
e2vp15Euqa7zaiEVcHOtSaMK1yd4Wn0TnpZnAeH/5uHoR8zgZ8E4daajB86GciIlegUvF2vj
JjJ35dU6PoHdE1eEIqxWHfAjX+KunuUMT2vG4MAmavkJ9CvGdW+8m66HtHtmBHHpe0yXNilQ
VebNbwUUwwywy42jZpVv7LXt0+kz17UZ27eJOn2nyNtZ1euJwmKGPQ2WgZcY07dbnKO1BvGh
rxrQYy6IWIuTjPQ82rwFLMIXVUNyD6t3L4wd/ACngyQFbgnaYu25TWBJElsdSJgtLHfBTrDI
hLWFaM36Iy1M+U5HdgWtySwjmCqtvco7s+Ozaniz1FyM

/
show errors;
grant execute on AggrConcat to public;
create or replace function SDO_Aggr_Concat_Lines wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
75 9e
+jWKoIAFaJfWNyvZs8Dm2rmP67Ywg8eZgcfLCNL+XuefCNC/Wa6WGL//cgzcVvT++kcMYjy4
v4H+MrK9GMOlMr1KdDwdPqy93sA+Fja+QEY5Ojj3w4/A9b8owDK/dFJc57Iz0sfqpgiQc3Nu
pITYiKatV6i5

/
show errors;
grant execute on SDO_Aggr_Concat_Lines to public;
create or replace  public synonym SDO_AGGR_CONCAT_LINES for 
MDSYS.SDO_AGGR_CONCAT_LINES;
create or replace function 
 SDO_Aggr_Set_Union wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
e05 50d
7Rk2W1TNV4dafJrFx0V4jPseJ/wwgw3qBUqDfC/Nx7zgGVo2bVq05VVgO9vTt6OXRK1rrLBN
H5+YKETenPmqxpxXYb2ToyZa4zBlnIIX1AsDg47Z7KdEw6ExVhwsTxMnqiffB7CGr66hWLfs
zcqu5UwqQ6s7TZN8YbQuDtGCFt86ADg3W91a7Tjp0HsUGhtWsEQ7J0U2o0LkLufo3FaRzNDY
gEbullEYAOx+Na/TbVCj6VUYkTrCTYGbmTg8wJNIisSI1955ksJEvdevqQLZb7PAauzADeVa
sEfpFO++1Z1JPmJ6aa8kz0aTXwrf3+JqMc34xLtIzEXFiKVx3vuoWbBQm3yNYZOT4i3F9URF
iSLlW+Fx+tOjVicJOCp8n6dEaaobBAQkTH6BIUDnavv6xcChOMqsRu92jr0V9JQWAfihF7E1
UHfg58+8vb6yyGIyVbQjFga7MAbOERviFjUzX3pcnSd9OA52uMGs0gHq4T3VJ0TBivZWGP6q
qIeAw64YMHxRCrB/R9fx0lSHXPcixsFAUa7eLcUS8jHzuTdoPX3ZaI0/IRNV3FmnAnIYs5Fm
53XjdPDQlAlIqCR1qVjm1UOUtsyQNvphYHC8FGor/PHM8mUIqUndv0v7quzxRQXUUXVrBCM3
64c/FLfyHlXscJw4l2CM+q9AP7Iys8zCn5IN2aNX+3ipcNdDDMxh0d7MeHaviNEudMIgEw88
X+GTHIGMb9TL+wvlJnnuyypsvCcKdIGQtNUBzbmcPFIw3+GWM+LLMWDX2UbV77FO3VgoYiTc
mueBLbcn3ccq54j+jikGF6YN52/bO3gArLWrbv4Pj32h2G/g06UpfXDNhJJ7By/UhMtRh1tq
306oE0gzD3jtlr+RIVD3/wTf2sIXiVLaWE03v90mUCj/8gaTRClttrwcoll1KSsjHv7TTB4B
sHwypNLQXwVehSdPHkpnff264n2XC7N+kvtwzH59NKtCHLpZiOmNwavE/OKQTiETwN5e4r26
pdnt0IB1evK4lwIDMTagyieuZQDxkPN1SUYtPrpQGCFCJY9RkfkWrmC2BDgh5L69we5VVTFr
ISWqFI/4sWruMKwgEs5zwrX7KMYXV3PPIzecc71KKMi50J0zsMGwc9laMztFnAjmEx9lKM6x
+/YPpXtd1FX/rSc6qlttfPi71pZ/FtczSnAHvt77Xz1Wipj0x/YmpKu6ce3zLl1IKFYyHldQ
bOqcMf8lmzsQddv0tcDel6t+T8lepctpYJKbJfEYnQgTJL0Kx7o=

/
show errors;
grant execute on SDO_Aggr_Set_Union to public;
create or replace  public synonym SDO_Aggr_Set_Union for
MDSYS.SDO_Aggr_Set_Union;
