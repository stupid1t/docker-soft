Rem
Rem $Header: sdo/admin/sdordfh.sql /st_sdo_11.2.0/4 2010/08/01 23:09:11 sdas Exp $
Rem
Rem sdordfh.sql
Rem
Rem Copyright (c) 2004, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdordfh.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      SDO_RDF_INTERNAL package
Rem
Rem    NOTES
Rem      package BODY in sdordfb.sql
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sdas        07/31/10 - add chklex
Rem    vkolovsk    06/08/10 - add another param to parse_triple
Rem    sdas        05/27/10 - bug 9756570: sem network index info view
Rem    vkolovsk    05/25/10 - add one more param to decr_link_cost..
Rem    vkolovsk    05/13/10 - add policy_name param to decr_link_cost..
Rem    sdas        12/30/09 - change MERGE_BATCH_VALUES interface
Rem    matperry    12/14/09 - add tablespace_name parameter to
Rem                           convert_old_rdf_data
Rem    sdas        08/04/09 - for BASIC compress of OLTP data
Rem                           allow MOVE within same tablespace
Rem    sdas        07/13/09 - add encode_into_index_code and get_rdf_index_info
Rem    matperry    05/18/09 - add an extra parameter to convert_old_rdf_data to
Rem                           enable/disable data migration
Rem    alwu        05/14/09 - change DEFAULT to AUTO for degree
Rem    alwu        05/14/09 - update gather_stats API
Rem    matperry    05/12/09 - add cascade option to drop_sem_network
Rem    sdas        05/11/09 - move type_family from rdf_apis_internal to
Rem                           sdo_rdf_internal
Rem    sdas        05/07/09 - convert old RDF data: float/double and
Rem                           'orardf:null '
Rem    sdas        04/22/09 - double and float should not fold into decimal
Rem    vkolovsk    03/23/09  - add rename_model
Rem    sdas        02/10/09  - simple names for model, vm, rulebase,rules index
Rem    ayalaman    11/18/08  - version enabled RDF
Rem    alwu        12/29/08  - XbranchMerge alwu_new_ha_apis from st_sdo_11.1
Rem    matperry    11/10/08  - add constants for rules index status
Rem    matperry    10/31/08  - add create_virtual_model, drop_virtual_model
Rem    sdas        10/14/08  - flex indexing: add COMPRESS param
Rem    sdas        07/21/08  - event trace options
Rem    alwu        07/14/08  - migrate remove_duplicates or remove_duplicates API
Rem    alwu        07/14/08  - add HA APIs
Rem    alwu        06/25/08  - add new APIs for UTH enhancement requests
Rem    sdas        07/19/08  - event trace options
Rem    spsundar    04/18/08  - update arguments to load_batch_values_table
Rem    sdas        03/04/08  - add update_collision_summary
Rem    sdas        02/27/08  - fif: generalize for inference and rules_index
Rem    sdas        02/20/08  - fif: allow more options on alter
Rem    sdas        02/12/08  - flexible indexing framework
Rem    sdas        12/06/07  - collision testing: expose merge_batch_values
Rem    sdas        12/12/07  - use private table for cursor dependency
Rem    alwu        09/13/07  - add statistics on demand
Rem    alwu        08/22/07  - add get_triple_id, is_triple for CLOB
Rem    sdas        06/02/07  - remove tune_rdf_network_10r2_1 procedure
Rem    sdas        05/11/07  - use current_schema as model owner
Rem    sdas        04/16/07  - remove rdf dummy upgrade/downgrade routines
Rem    sdas        03/28/07  - parse_property_value interface change
Rem    sdas        03/16/07  - add REFRESH_SEM_TABLESPACE_NAMES
Rem    sdas        03/09/07  - allow storing diff RDF models in diff
Rem                            tablespaces
Rem    sdas        03/08/07  - add VALIDATE_AND_CONVERT_NAME function
Rem    alwu        02/23/07  - fix index misuse: remove syn_orig_value
Rem    sdas        01/30/07  - cleanup interfaces: IS_TRIPLE, GET_TRIPLE_ID
Rem    sdas        01/28/07  - model LINK_ID as Virtual Col (in RDF_LINK$
Rem                            table)
Rem    sdas        12/14/06  - mark replace_rdf_prefix as WNDS,RNDS,WNPS,RNPS
Rem    sdas        12/04/06  - mark syn_orig_value,pov_* as wnds,rnds,wnps,rnps
Rem    sdas        11/26/06  - mark get_canon_val_null as wnds,rnds,wnps,rnps
Rem    sdas        11/06/06  - change bulk-load interface param names
Rem    sdas        10/23/06  - GET_MODEL_INFO: access via model name only
Rem    sdas        10/02/06  - bulk-load: add priv check
Rem    alwu        09/29/06  - fix bug 5574755: factor out gather_rdf_stats in
Rem                            a separater sem_perf package
Rem    sdas        09/16/06  - add new model_id param to get_value$_type$
Rem    sdas        09/06/06  - remove funcs: type_family_num, type_fam_ext
Rem    sdas        08/11/06  - cleanup: allow (no-)reuse param in INSERT_TRIPLE
Rem    alwu        08/03/06  - add merge append
Rem    sdas        07/27/06  - adjust to better handle blank nodes 
Rem    sdas        07/07/06  - add flags 
Rem    sdas        07/05/06  - add funcs for extracting components from lexval 
Rem    sdas        06/12/06  - add upgrade/downgrade routines to package spec 
Rem    sdas        06/07/06  - add syn_orig_value 
Rem    alwu        04/21/06  - refactor exchange_partition method 
Rem    alwu        04/06/06  - fix bug: 5146335 add temporary table name
Rem    nalexand    02/09/06  - bug#4926915; is_reified_quad moved to sdo_rdf
Rem    nalexand    02/10/06  - add gather_rdf_stats 
Rem    nalexand    01/31/06  - add get_model_name(); bug#4926894;
Rem    nalexand    10/12/05  - add tune_rdf_network_10r2_1() 
Rem    nalexand    10/12/05  - add tablespace to exchange_model_part$ 
Rem    nalexand    10/10/05  - check lock_model 
Rem    nalexand    10/06/05  - add package value$cache; add lock_model to start_batch$
Rem    nalexand    09/30/05  - no_insert_batch_load (for testing)
Rem    nalexand    09/17/05  - add start_batch() and exchange_model_part$() 
Rem    nalexand    09/16/05  - add parse_triple_batch_mode 
Rem    nalexand    05/25/05  - change parse_triple() call 
Rem    nalexand    05/18/05  - alter signature for create_rdf_model(); 
Rem    nalexand    05/12/05  - add is_reified_quad() 
Rem    nalexand    05/12/05  - remove default tablespace_name 
Rem    nalexand    05/06/05  - remove rdf_geturl() 
Rem    nalexand    05/04/05  - comment rdf_geturl() 
Rem    nalexand    05/02/05  - add delete_link$_id(); rename package sdo_rdf_internal; rename remove_rdf_network 
Rem    sravada     04/28/05  - type methods execute as the definer 
Rem    nalexand    04/13/05  - add parse_object_node() CLOB version; insert_sub_rdf_node$; insert_obj_rdf_node$ 
Rem    nalexand    03/21/05  - edit parse_property_value to output pl_type 
Rem    nalexand    03/16/05  - atonomous txn for inserting obj 
Rem    nalexand    03/15/05  - autonomous txns 
Rem    nalexand    03/02/05  - add get_value$_type$() 
Rem    nalexand    01/05/05  - get_literal() to return clob; get_literaltype() return varchar2; is_reif() 
Rem    nalexand    12/29/04  - edit add_namespaces() 
Rem    nalexand    12/13/04  - make geturl deterministic 
Rem    sravada     11/10/04 -  add create_rdf_model 
Rem    nalexand    09/27/04 - add m_id to is_reified; add is_triple() add get_triple_id() rdf_prefix
Rem    nalexand    09/22/04 - add is_reified()
Rem    nalexand    09/10/04 - add coll support TC_BAG etc.
Rem    nalexand    07/19/04 - add CLOB for OBJECTS
Rem    nalexand    07/07/04 - adding put_namespaces
Rem    nalexand    05/05/04 - get_literal 
Rem    nalexand    04/21/04 - Created
Rem

CREATE OR REPLACE PACKAGE sdo_rdf_internal  AS

  -- constants for rules index status values
  RIDX_VALID      CONSTANT VARCHAR2(5)  := 'VALID';
  RIDX_INCOMPLETE CONSTANT VARCHAR2(10) := 'INCOMPLETE';
  RIDX_INVALID    CONSTANT VARCHAR2(7)  := 'INVALID';
  RIDX_INPROGRESS CONSTANT VARCHAR2(10) := 'INPROGRESS';
  RIDX_NORIDX     CONSTANT VARCHAR2(6)  := 'NORIDX';
  RIDX_VERSIONED  CONSTANT VARCHAR2(10) := 'VERSIONED'; 

  DEFAULT_BATCHLOAD_TMP_TAB_NAME CONSTANT VARCHAR2(30) DEFAULT 'RDF_LINK$_TEMPBM$';

  -- default name for the temp table to be created in MDSYS schema
  batchLoadTmpTabName VARCHAR2(30) := DEFAULT_BATCHLOAD_TMP_TAB_NAME;

  -- events
  RDF$pv_proc_sid            varchar2(30); -- session id 
  RDF$pv_proc_sig            varchar2(200);-- signature
  RDF$pv_invoker             varchar2(30); -- invoker
  RDF$pv_bl_flags            varchar2(32767); -- bulk load flags
  RDF$pv_p_et_tab            varchar2(30) := 'RDF$ET_TAB';--pvt event trace tab

  procedure create_rdf_triggers(model_name varchar2);
  -- 
  -- TYPE_FAMILY: Given a type, returns type's "family": 'STRING', 'NUMERIC',
  -- 'DATE', 'TIME', 'DATETIME', or 'OTHER'.
  --
  FUNCTION TYPE_FAMILY(
      literal_type varchar2, flags varchar2 default null)
    return varchar2 deterministic;
  pragma restrict_references (type_family, WNDS, RNDS, WNPS, RNPS);

  -- exposed for testing collision handling
  PROCEDURE LOAD_BATCH_VALUES_TABLE (
    Batch_Values_Tab       IN  varchar2
  , tbs_name               IN  varchar2
  , Staging_Tab            IN  varchar2
  , modelID                IN  number
  , Event_Trace_Tab        IN  varchar2
  , flags                  IN  varchar2
  , owner                  IN  varchar2 default 'MDSYS'
  );

  PROCEDURE MERGE_BATCH_VALUES (
    Batch_Values_Tab           IN  varchar2
  , modelID                    IN  number
  , owner                      IN  varchar2 default 'MDSYS'
  , collision_cnt              IN  number default 0
  , Event_Trace_Tabname        IN  varchar2 default NULL
  , flags                      IN  varchar2 default NULL
  );

  -- functions to extract parts from lex_value stored in staging table
  FUNCTION pov_value_name (ov varchar2) RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (pov_value_name,WNDS,RNDS,WNPS,RNPS);
  FUNCTION pov_value_type (ov varchar2) RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (pov_value_type,WNDS,RNDS,WNPS,RNPS);
  FUNCTION pov_literal_type (ov varchar2) RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (pov_literal_type,WNDS,RNDS,WNPS,RNPS);
  FUNCTION pov_language_type (ov varchar2) RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (pov_language_type,WNDS,RNDS,WNPS,RNPS);

  -- functions exposed here just for testing: not for general use
  FUNCTION validate_user_options (flags VARCHAR2) 
  RETURN NUMBER deterministic;

  function check_user_option (flags varchar2, item varchar2) 
  return boolean deterministic;

  function get_user_option_choice (flags varchar2, item varchar2) 
  return varchar2 deterministic;

  -- function to obtain canonical value for a lex value (NULL if identical)
  function get_canon_val_null (lexval varchar2, flags varchar2 default null) return varchar2 deterministic parallel_enable;
  pragma restrict_references (get_canon_val_null,WNDS,RNDS,WNPS,RNPS);

  -- function to validate/convert user-supplied names for owner,table,col,tbs..
  FUNCTION validate_and_convert_name (name VARCHAR2) 
  RETURN VARCHAR2 DETERMINISTIC;

  -- function to validate/convert user-supplied names for model, vm, rb, ridx
  FUNCTION val_cvt_rdf_name (name VARCHAR2) 
  RETURN VARCHAR2 DETERMINISTIC;

  -- function to convert numeric pos code (0..7) to list of sub/pred/obj
  FUNCTION get_pos_list_from_code (pos_code NUMBER) 
    RETURN varchar2 deterministic parallel_enable;

  -- procedure to update collision summary
  PROCEDURE UPDATE_COLLISION_SUMMARY;

  -- main routine for batch load from staging table
  PROCEDURE BULK_LOAD_FROM_STAGING_TABLE (
    model               IN            varchar2,
    staging_table_owner IN            varchar2, 
    staging_table_name  IN            varchar2,
    flags               IN            varchar2 default NULL,
    debug               IN            PLS_INTEGER default NULL,
    start_comment       IN            varchar2 default NULL,
    end_comment         IN            varchar2 default NULL
  );

  PROCEDURE RESUME_LOAD_FROM_STAGING_TABLE (
    model               IN              varchar2,
    staging_table_owner IN              varchar2,
    staging_table_name  IN              varchar2,
    session_id          IN              varchar2,
    flags               IN              varchar2 default NULL,
    start_comment       IN              varchar2 default NULL,
    end_comment         IN              varchar2 default NULL
  );

  -- invalidates cursors with unresolved values
  PROCEDURE refresh_query_state;

  FUNCTION check_lexval_for_validity(
    lexval  varchar2
  , min_pos PLS_INTEGER
  , options varchar2 default NULL
  ) RETURN  varchar2 deterministic parallel_enable;
  pragma restrict_references (check_lexval_for_validity,WNDS,RNDS,WNPS,RNPS);

  FUNCTION chklex(
    lexval  varchar2
  ) RETURN  NUMBER deterministic parallel_enable;
  pragma restrict_references (chklex,WNDS,RNDS,WNPS,RNPS);

  -- replace prefix at beginning with expanded form
  FUNCTION replace_rdf_prefix (
    string                       VARCHAR2
  , options                      VARCHAR2 default NULL
  ) RETURN                       VARCHAR2 deterministic;
  pragma restrict_references (replace_rdf_prefix,WNDS,RNDS,WNPS,RNPS);
	
  FUNCTION get_object_node_type (object CLOB) RETURN VARCHAR2 deterministic;
  FUNCTION get_literal (object IN CLOB) RETURN CLOB deterministic;
  FUNCTION get_literaltype (object IN CLOB) RETURN VARCHAR2 deterministic;

  FUNCTION get_model_id (model_name IN VARCHAR2) RETURN NUMBER;
  FUNCTION get_model_name (model_id IN NUMBER) RETURN VARCHAR2;

  PROCEDURE GET_USER_SPEC_BATCH_VALUES_TAB (
    flags              IN  varchar2
  , table_name         OUT varchar2
  , owner_name         OUT varchar2
  );

  -- given model name, return owner, model Id, app table name and col name
  PROCEDURE GET_MODEL_INFO (
    model_owner OUT varchar2
  , model_name  IN  varchar2
  , modelID     OUT number
  , App_Tabname OUT varchar2
  , App_Colname OUT varchar2
  );

  FUNCTION is_triple (
    model_id             NUMBER
  , subject              VARCHAR2
  , property             VARCHAR2
  , object               VARCHAR2
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
    model_id             NUMBER
  , subject              VARCHAR2
  , property             VARCHAR2
  , object          CLOB
  ) RETURN               VARCHAR2;



  FUNCTION is_reified (
    model_id           NUMBER
  , rdf_t_id           VARCHAR2
  ) RETURN             VARCHAR2;

  FUNCTION is_reified (
    model_name         VARCHAR2
  , rdf_t_id           VARCHAR2
  ) RETURN             VARCHAR2;

  FUNCTION is_reified (
    model_id           NUMBER
  , subject            VARCHAR2
  , property           VARCHAR2
  , object             VARCHAR2
  ) RETURN             VARCHAR2;

  FUNCTION is_reified (
    model_name         VARCHAR2
  , subject            VARCHAR2
  , property           VARCHAR2
  , object             VARCHAR2
  ) RETURN             VARCHAR2;

  FUNCTION get_triple_cost (
    model_id         IN  NUMBER
  , sub_id           IN  NUMBER
  , pred_id          IN  NUMBER
  , canon_id         IN  NUMBER
  ) RETURN               NUMBER;

  PROCEDURE set_triple_cost(
    model_id         IN  NUMBER
  , sub_id           IN  NUMBER
  , pred_id          IN  NUMBER
  , canon_id         IN  NUMBER
  );

  PROCEDURE delete_link$_id (
    model_id         IN  NUMBER
  , sub_id           IN  NUMBER
  , pred_id          IN  NUMBER
  , canon_id         IN  NUMBER
  );

  
  PROCEDURE decr_link_cost_del_if_zeroed (
    model_id         IN  NUMBER
  , sub_id           IN  NUMBER
  , pred_id          IN  NUMBER
  , canon_id         IN  NUMBER
  , label_num        IN  NUMBER DEFAULT NULL
  , res_level_ols    IN  BOOLEAN DEFAULT FALSE
  );

  PROCEDURE parse_subject_node (
    v_subject        IN  VARCHAR2
  , sv_type          IN  VARCHAR2
  , sv_id            OUT NUMBER
  , new_sv           OUT BOOLEAN
  );

  PROCEDURE parse_property_value (
    v_property       IN  VARCHAR2
  , pv_type          IN  VARCHAR2
  , pv_id            OUT NUMBER
  , new_pv           OUT BOOLEAN
  );

  PROCEDURE parse_object_node (
    v_object         IN  VARCHAR2
  , lit_type         IN  VARCHAR2
  , lit_lang         IN  VARCHAR2
  , ov_type          IN  VARCHAR2
  , ov_id            OUT NUMBER
  , cov_id           OUT NUMBER
  , new_ov           OUT BOOLEAN 
  );

  PROCEDURE parse_object_node (
    v_object         IN  CLOB
  , lit_type         IN  VARCHAR2
  , lit_lang         IN  VARCHAR2
  , ov_type          IN  VARCHAR2
  , ov_id            OUT NUMBER
  , cov_id           OUT NUMBER
  , new_ov           OUT BOOLEAN 
  );

  -- used in before row insert/update trigger on app-table (triple col)
  PROCEDURE parse_triple (
    m_id             IN  NUMBER
  , pv_id            IN  NUMBER
  , sv_id            IN  NUMBER
  , ov_id            IN  NUMBER
  , cov_id           IN  NUMBER
  , new_sv           IN  BOOLEAN
  , new_ov           IN  BOOLEAN
  , new_pv           IN  BOOLEAN
  , pl_id            OUT NUMBER
  , label_num        IN  NUMBER DEFAULT NULL
  , triple_level_ols IN  BOOLEAN DEFAULT FALSE
  );

  PROCEDURE insert_triple (
    m_id             IN  NUMBER
  , subject          IN  VARCHAR2
  , property         IN  VARCHAR2
  , object           IN  VARCHAR2
  , pl_id            OUT NUMBER
  , sv_id            OUT NUMBER
  , pv_id            OUT NUMBER
  , ov_id            OUT NUMBER
  , batch_mode       IN  BOOLEAN -- true => old batch mode insert(from NTriple)
  , reuse_mode       IN  BOOLEAN -- true => reuse blank nodes from same model
  );

  PROCEDURE insert_triple (
    m_id             IN  NUMBER
  , subject          IN  VARCHAR2
  , property         IN  VARCHAR2
  , object           IN  CLOB -- CLOB version
  , pl_id            OUT NUMBER
  , sv_id            OUT NUMBER
  , pv_id            OUT NUMBER
  , ov_id            OUT NUMBER
  , batch_mode       IN  BOOLEAN -- true => old batch mode insert(from NTriple)
  , reuse_mode       IN  BOOLEAN -- true => reuse blank nodes from same model
  );

  PROCEDURE start_batch$ (
    tempTblName           IN  VARCHAR2
  , model_id              IN  NUMBER
  , model_name            IN  VARCHAR2
  , rdf_tablespace        IN  VARCHAR2
  );

  PROCEDURE start_batch$ (
    tempTblName           IN  VARCHAR2
  , model_id              IN  NUMBER
  , model_name            IN  VARCHAR2
  , rdf_tablespace        IN  VARCHAR2
  , exchange              IN  NUMBER DEFAULT NULL
  );

  PROCEDURE start_batch$ (
    model_id              IN  NUMBER
  , model_name            IN  VARCHAR2
  , rdf_tablespace        IN  VARCHAR2
  );

  PROCEDURE start_batch$ (
    model_id              IN  NUMBER
  , model_name            IN  VARCHAR2
  , rdf_tablespace        IN  VARCHAR2
  , exchange              IN  NUMBER
  );

  -- used in before row insert/update trigger on app-table (triple col)
  PROCEDURE parse_triple_batch_mode (
    m_id             IN  NUMBER
  , pv_id            IN  NUMBER
  , sv_id            IN  NUMBER
  , ov_id            IN  NUMBER
  , cov_id           IN  NUMBER
  , new_sv           IN  BOOLEAN
  , new_ov           IN  BOOLEAN
  , new_pv           IN  BOOLEAN
  , pl_id            OUT NUMBER 
  );
	
  PROCEDURE exchange_model_part$ (
    model_id       IN  NUMBER
  , rdf_tablespace IN  VARCHAR2
  );

  PROCEDURE get_value$_type$ (
    model_id      IN     NUMBER
  , subject       IN OUT VARCHAR2
  , property      IN OUT VARCHAR2
  , object        IN OUT VARCHAR2
  , sv_type       OUT    VARCHAR2
  , pv_type       OUT    VARCHAR2
  , ov_type       OUT    VARCHAR2
  , lit_type      OUT    VARCHAR2
  , lit_lang      OUT    VARCHAR2
  );

  PROCEDURE get_value$_type$ (
    model_id      IN     NUMBER
  , subject       IN OUT VARCHAR2
  , property      IN OUT VARCHAR2
  , object        IN OUT CLOB
  , sv_type          OUT VARCHAR2
  , pv_type          OUT VARCHAR2
  , ov_type          OUT VARCHAR2
  , lit_type         OUT VARCHAR2
  , lit_lang         OUT VARCHAR2
  );

  PROCEDURE refresh_sem_tablespace_names (
    model_name                 VARCHAR2 default NULL  -- NULL => all
  , user_id                    NUMBER default NULL    -- NULL => DBA
  );

  PROCEDURE create_rdf_model (
    model_name       IN  VARCHAR2
  , table_name       IN  VARCHAR2
  , column_name      IN  VARCHAR2
  , user_name        IN  VARCHAR2
  , model_tablespace IN  VARCHAR2 default NULL
  );
  PROCEDURE drop_rdf_model (model_name IN VARCHAR2, user_name IN VARCHAR2);

  /**
   * Note only owner of models and rules index (or sys dba) can 
   * create a virtual model.
   */
  PROCEDURE create_virtual_model (
    vm_name          IN  VARCHAR2
  , user_name        IN  VARCHAR2
  , models	     IN  MDSYS.RDF_Models
  , rulebases        IN  MDSYS.RDF_Rulebases default NULL
  , options          IN  VARCHAR2 default NULL
  );

  /**
   * Note only owner of virtual model (or sys dba) can 
   * drop the virtual model.
   */
  PROCEDURE drop_virtual_model (vm_name IN VARCHAR2, user_name IN VARCHAR2);

  PROCEDURE gather_stats (just_on_values_table in boolean default false,  
                                        degree in number  default DBMS_STATS.AUTO_DEGREE);
  PROCEDURE gather_rdf_stats;

  PROCEDURE create_rdf_network (tablespace_name in varchar2);
  PROCEDURE drop_rdf_network (cascade in boolean default false);

  /**
   * Note only rules index owner is allowed perform this action.Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_rules_index(index_name IN VARCHAR2,
                          estimate_percent IN NUMBER,
                          method_opt       IN VARCHAR2,
                          degree           IN NUMBER,
                          cascade          IN BOOLEAN,
                          no_invalidate    IN BOOLEAN,
                          force            IN BOOLEAN DEFAULT FALSE
                         );

  /**
   * Note only rules index owner is allowed perform this action.Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_model(model_name       IN VARCHAR2,
                          estimate_percent IN NUMBER,
                          method_opt       IN VARCHAR2,
                          degree           IN NUMBER,
                          cascade          IN BOOLEAN,
                          no_invalidate    IN BOOLEAN,
                          force            IN BOOLEAN DEFAULT FALSE
                         );

  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  PROCEDURE delete_model_stats(
      model_name       IN VARCHAR2,
      cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
      cascade_columns  IN BOOLEAN  DEFAULT TRUE,
      cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );


  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  PROCEDURE delete_rules_index_stats(
      index_name       IN VARCHAR2,
      cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
      cascade_columns  IN BOOLEAN  DEFAULT TRUE,
      cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );


  /**
   * This method will swap the names of two models.
   * 
   * The input to this method are two model names. This method requires that the
   * current session user is the owner of both models.
   */
  PROCEDURE swap_model_names(m1  in VARCHAR2,
                             m2  in VARCHAR2);

  PROCEDURE rename_model(old_name  in VARCHAR2,
                         new_name  in VARCHAR2);

  PROCEDURE rename_entailment(old_name  in VARCHAR2,
                              new_name  in VARCHAR2);

  PROCEDURE add_rdf_index (
    index_code      IN  VARCHAR
  , table_name      IN  VARCHAR2 default NULL
  , index_name_pfx  IN  VARCHAR2 default NULL
  , index_name_sfx  IN  VARCHAR2 default NULL
  , status          IN  VARCHAR2 default NULL
  , parallel        IN  PLS_INTEGER default NULL
  , online          IN  BOOLEAN default FALSE
  , tablespace_name IN  VARCHAR2 default NULL
  , compression_length IN  PLS_INTEGER default NULL
  );

  PROCEDURE drop_rdf_index (
    index_code      IN   VARCHAR2
  , index_name_pfx  IN  VARCHAR2 default NULL
  , index_name_sfx  IN  VARCHAR2 default NULL
  );

  PROCEDURE ddl_same_rdf_indexes (
    table_name           IN  VARCHAR2
  , modelID              IN  NUMBER
  , index_name_pfx       IN  VARCHAR2
  , index_name_sfx       IN  VARCHAR2
  , Event_Trace_Tabname  IN  VARCHAR2 default NULL
  , parallel             IN  PLS_INTEGER default NULL
  , online               IN  BOOLEAN default FALSE
  , status               IN  VARCHAR2 default NULL
  , operation            IN  VARCHAR2 default 'CREATE'
  , tablespace_name      IN  VARCHAR2 default NULL
  );

  PROCEDURE alter_index_on_rdf_graph (
    modelID          IN  NUMBER
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , parallel         IN  PLS_INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  , tablespace_name  IN  VARCHAR2 default NULL
  , rules_index_name IN  VARCHAR2 default NULL
  , use_compression  IN  BOOLEAN default NULL
  );

  PROCEDURE refresh_sem_network_index_info (
    model_id_list sys.ODCINumberList
  , options       VARCHAR2 default NULL
  );

  PROCEDURE alter_rdf_graph (
    modelID          IN  NUMBER
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , parallel         IN  PLS_INTEGER default NULL
  , rules_index_name IN  VARCHAR2 default NULL
  );

  PROCEDURE convert_old_rdf_data (
    log_level       IN PLS_INTEGER default 10
  , migrate         IN BOOLEAN     default TRUE
  , flags           IN VARCHAR2    default NULL
  , tablespace_name IN VARCHAR2    default NULL
  );

  PROCEDURE clear_net_data;

  FUNCTION encode_into_index_code (index_name VARCHAR2) 
  RETURN VARCHAR2 DETERMINISTIC;

  PROCEDURE get_rdf_index_info (
    user_name IN VARCHAR2
  , flags     IN VARCHAR2 default NULL
  );
END sdo_rdf_internal;
/
SHOW ERRORS;


