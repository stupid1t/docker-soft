Rem
Rem $Header: sdoloadj.sql 07-may-2007.13:35:43 sdas Exp $
Rem
Rem sdoloadj.sql
Rem
Rem Copyright (c) 2002, 2007, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdoloadj.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sdas        05/07/07 - do not load rdf java classes
Rem    bkazar      03/27/07 - sdonumberarray needed in sdoutl.jar
Rem    bkazar      06/01/06 - removing sdothrdcg
Rem    bkazar      05/26/06 - 
Rem    bgouslin    01/17/06 - changes for demos to demo move 
Rem    bgouslin    09/26/05 - Load Java from jlib now 
Rem    geadon      03/07/05 - add sdordf.jar 
Rem    ningan      08/30/04 - load the NDM package sdonm.jar 
Rem    cfreiwal    06/09/04 - load the router partitioning code 
Rem    sravada     09/12/03 - 
Rem    sravada     08/27/03 - set the right schema 
Rem    sravada     06/20/03 - move sdogr to sdogrxml.sql 
Rem    fjlee       05/21/03 - load sdotopo.jar
Rem    fjlee       05/07/03 - Add entry for sdotype.jar
Rem    qxie        03/27/03 - disable mlibwrapper
Rem    qxie        12/13/02 - grant ordsys temporarily
Rem    sravada     12/09/02 - 
Rem    syuditsk    09/27/02 - add sdogcdr.jar
Rem    qxie        09/12/02 - unload JAI
Rem    qxie        09/05/02 - setup and load geometry and georaster jars
Rem    sravada     08/27/02 - sravada_topo_operators_1
Rem    sravada     08/19/02 - Created
Rem


alter session set current_schema=SYS;

call dbms_java.grant_permission('SYSTEM', 'java.io.FilePermission',
                                 '<<ALL FILES>>', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission', 
                                'md/jlib/*', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission',
                                'sdo/demo/georaster/jlibs/*', 'read');

--
-- For NT
--
call dbms_java.grant_permission('MDSYS','SYS:java.io.FilePermission',
                                'md\jlib\*','read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission',
                                'sdo\demo\georaster\jlibs\*', 'read');

-- should test to avoid runtime exceptions
-- should not grant this permission
call dbms_java.grant_permission('MDSYS', 'SYS:java.lang.RuntimePermission', 
                                'getClassLoader', null);
call dbms_java.grant_permission('ORDSYS', 'SYS:java.lang.RuntimePermission', 
                                'getClassLoader', null);

-- load JAI 
-- this should be disabled for release because interMedia installs JAI
--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        sdo/demo/georaster/jlibs/jai_core.jar');
--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        sdo/demo/georaster/jlibs/jai_codec.jar');
--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        sdo/demo/georaster/jlibs/mlibwrapper_jai.jar');

-- load geometry and util and topo and type and nm
-- first load without resolving class references
call dbms_java.loadjava('  -synonym -schema MDSYS -grant PUBLIC 
                        md/jlib/sdoapi.jar');
call dbms_java.loadjava(' -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/sdoutl.jar');
call dbms_java.loadjava(' -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/sdotopo.jar');
call dbms_java.loadjava(' -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/sdotype.jar');
call dbms_java.loadjava(' -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/sdonm.jar');


-- now resolve the references
call dbms_java.loadjava('-resolve  -synonym -schema MDSYS -grant PUBLIC
             md/jlib/sdoapi.jar md/jlib/sdoutl.jar md/jlib/sdotopo.jar 
             md/jlib/sdotype.jar md/jlib/sdonm.jar');

--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        md/jlib/sdonm.jar');
--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        md/jlib/sdotype.jar');


-- load geocoder
call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/sdogcdr.jar');

-- load sdordf
--call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
--                        md/jlib/sdordf.jar');

call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission',
                                 'md/jlib/*','read');
call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission', 
                                 'sdo/demo/georaster/jlibs/*','read');

-- load Router partitioning code
call dbms_java.loadjava('-resolve -force -synonym -schema MDSYS -grant PUBLIC
                        md/jlib/routepartition.jar');

commit;


Alter session set current_schema=MDSYS;







