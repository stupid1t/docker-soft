--
-- sdocs.sql
--
-- Copyright (c) 2006, 2010, Oracle and/or its affiliates. 
-- All rights reserved. 
--
--    NAME
--      sdocs.sql - SDO OCI Package Header
--
--    DESCRIPTION
--      Declares the SDO OCI functions.  They are simply 
--      callouts.  See sdo3glh.sql for more information.
--
--    NOTES
--
--    MODIFIED   (MM/DD/YY)
--    sravada     05/08/08 - sdo_cs_context_information
--    mhorhamm    09/04/07 - Unlock TRANSFORM_ACROSS_DIMS
--    mhorhamm    08/07/07 - Add function to parse WKT
--    mhorhamm    06/28/07 - Add function to generate new EPSG CRS, based on
--                           WKT
--    mhorhamm    06/15/07 - Add function to generate 3d WKT
--    mhorhamm    06/04/07 - Add function convert_mixed_axes_units
--    mhorhamm    05/18/07 - Add optional parameter to make_2d
--    mhorhamm    03/19/07 - Add tfm between dimensionalities
--    mhorhamm    04/20/07 - Add transform signatures
--    mhorhamm    04/19/07 - Add param
--    mhorhamm    04/02/07 - Add function to disambiguate EPSG PROJECTION name
--    mhorhamm    02/16/07 - Add intl_populate_datum_3params
--    mhorhamm    12/13/06 - Add signature to determine coverage region for
--                           NADCON
--    mhorhamm    12/05/06 - Add conversion signature
--    mhorhamm    12/04/06 - Add signature to convert SRID to 3D
--    mhorhamm    11/01/06 - Add public function to convert 2D to 3D
--    mhorhamm    09/29/06 - Add pragma
--    mhorhamm    08/02/06 - Add pragma for get_crs_dimensionality
--    mhorhamm    06/16/06 - Add function 
--    mhorhamm    06/12/06 - Add function to transform 2D geog geom to 
--                           respective 3D geog geom 
--    mhorhamm    05/19/06 - Add function getCrsDimensionality 
--    sravada     02/16/06 - add table to store SrsNameSpace, srsName mappings 
--    mhorhamm    02/17/06 - Add function to delete NTv2 XML data after use 
--    mhorhamm    02/15/06 - Add signature to handle XML in CLOB, as 
--                           alternative to XMLTYPE 
--    mhorhamm    01/31/06 - Add tolerance parameter to find_srid 
--    mhorhamm    01/16/06 - Add old signature, temporarily 
--    mhorhamm    01/16/06 - Add default values 
--    mhorhamm    01/12/06 - Add parameter to find_srid 
--    syuditsk    01/11/06 - adding public privilege to epsg_param/s 
--    mhorhamm    12/20/05 - Add parameters for find_srid 
--    mhorhamm    10/12/05 - Alter signatures of GeoRaster support functions 
--    mhorhamm    10/06/05 - Add 2 functions for use in GeoRaster 
--    mhorhamm    05/05/05 - Add further function for updating WKTs 
--    mhorhamm    05/04/05 - Add function for updating WKT for EPSG SRID 
--    mhorhamm    01/27/05 - Add NAD27 option to USNG 
--    mhorhamm    01/26/05 - Change interface to USNG 
--    mhorhamm    01/24/05 - Add functions for converting to/from USNG 
--    mhorhamm    12/01/04 - Add function to populate 7 params in datum table 
--    mhorhamm    11/18/04 - unexpose function 
--    mhorhamm    11/10/04 - Change pragma for transform and transform_orig
--    mhorhamm    11/02/04 - Add params 
--    mhorhamm    09/20/04 - Change param name 
--    mhorhamm    08/31/04 - Add parameter max_rel_num_discrepency to
--                           find_geog_crs and find_proj_crs
--    mhorhamm    08/30/04 - Rename function tfm_wkt_to_include_towkt84_tag 
--                           and tfm_wkt_to_exclude_towkt84_tag 
--    mhorhamm    08/27/04 - Add function to transform between WKT formats 
--                           with and without explicit TOWGS84 tag 
--    mhorhamm    08/25/04 - Add find_geog_crs 
--    mhorhamm    08/25/04 - Add parameter IS_LEGACY 
--    mhorhamm    08/25/04 - Add more parameters to find_proj_crs 
--    mhorhamm    08/24/04 - Add function to find projected crs with similar 
--                           params 
--    mhorhamm    08/24/04 - Add function to return EPSG version 
--    mhorhamm    08/23/04 - Make more functions internal 
--    mhorhamm    08/20/04 - Delete some internal functions from the interface 
--    mhorhamm    08/19/04 - Add parameters to find_proj_crs 
--    mhorhamm    08/17/04 - Function for searching CRS 
--    mhorhamm    08/16/04 - Fix function name infix 'SIRD' 
--    mhorhamm    08/12/04 - Make op_id NUMBER 
--    sravada     07/29/04 - add exception handlers 
--    mhorhamm    08/03/04 - Convert NADCON grid format to XML
--    mhorhamm    07/27/04 - Delete internal_create_crs_using_epsg 
--    mhorhamm    07/23/04 - Change TRANSFORM to TRANSFORM_ORIG and 
--                           TRANSFORM_USING_DENSIFICATION to TRANSFORM 
--    mhorhamm    07/20/04 - Add function to densify MBR prior to 
--                           transformation 
--    mhorhamm    06/16/04 - Rename get_decimally_encoded_uom to 
--                           get_decimally_encoded_equ_uom 
--    mhorhamm    06/16/04 - make function name more intuitive 
--    mhorhamm    06/15/04 - Add function to_decimally_encoded_srid 
--    mhorhamm    06/14/04 - Add functions to_EPSG_numeric_encoding and 
--                           to_alphanumeric_encoding 
--    mhorhamm    06/11/04 - Change signature 
--    mhorhamm    06/09/04 - Change two function names 
--    mhorhamm    06/07/04 - Add convenience function for converting WKT to 
--                           use EPSG projection 
--    mhorhamm    06/07/04 - Add convenience function for converting WKT to 
--                           use legacy projection 
--    mhorhamm    06/04/04 - Use new column in SDO_COORD_OP_PARAM_USE table, 
--                           instead of SDO_ALIASES to find param aliases 
--    mhorhamm    05/25/04 - helper function for converting units 
--    mhorhamm    05/17/04 - Delete functions for finding direct and indirect 
--                           chains 
--    mhorhamm    05/14/04 - Use more simple function to transform angle to 
--                           rad 
--    mhorhamm    05/07/04 - Add function for transforming between units of 
--                           measure 
--    mhorhamm    04/26/04 - Add function for finding alternative operations 
--                           between two crs 
--    mhorhamm    04/26/04 - Add function for deleting all rules 
--    mhorhamm    04/26/04 - Add function for creating obvious rules 
--    mhorhamm    04/20/04 - Add procedures for handling EPSG operations and 
--                           their being preferred or not 
--    mhorhamm    04/14/04 - Add function create_pref_concatenated_op 
--    mhorhamm    04/14/04 - Add convenient function for deleteing 
--                           concatenated operation 
--    mhorhamm    04/14/04 - Add convenient function for creating concatenated 
--    mhorhamm    04/06/04 - Add two subfunctions 
--    mhorhamm    01/27/04 - Fix parameter names with double underscores 
--    mhorhamm    01/23/04 - Add "internal" prefix to some functions 
--    mhorhamm    01/22/04 - Move content of package ecs into sdo_cs 
--    mhorhamm    12/15/03 - Eliminate redundant parameter in transform_layer 
--    mhorhamm    12/12/03 - Add transform_layer with ad-hoc tfm plan 
--    bblackwe    09/18/02 - fix international nautical mile local_cs and add UK nautical mile
--    sravada     04/26/02 - add validate WKT function
--    sravada     12/03/01 - bug 2023708
--    sravada     04/17/01 - remove user_transoform
--    sravada     01/29/01 -
--    sravada     10/05/00 - signature updates
--    sravada     07/03/00 - add user defined transforms
--    sravada     04/07/00 - add function signatures without the dimect
--    sravada     08/30/99 - deterministic functions
--    bblackwe    08/13/99 - layer
--    bblackwe    07/26/99 - integrate cs
--    bblackwe    06/03/99 - creation. 


declare
begin
  begin
   execute immediate 
  ' CREATE OR REPLACE TYPE MDSYS.SDO_SRID_CHAIN
   AS VARRAY(1048576) OF NUMBER ';
   execute immediate 
  ' CREATE OR REPLACE TYPE MDSYS.SDO_SRID_LIST
   AS VARRAY(1048576) OF NUMBER ';
  exception when others then NULL;
 end;

  begin
   execute immediate 
  ' CREATE OR REPLACE TYPE MDSYS.SDO_TFM_CHAIN
   AS VARRAY(1048576) OF NUMBER ';
  exception when others then NULL;
  end;

  begin
   execute immediate 
   ' CREATE OR REPLACE TYPE MDSYS.SDO_TRANSIENT_RULE
     AS OBJECT (
       SOURCE_SRID NUMBER,
       TARGET_SRID NUMBER,
       TFM         NUMBER) ';
  exception when others then NULL;
  end;


 begin
  execute immediate 
  ' CREATE OR REPLACE TYPE MDSYS.SDO_TRANSIENT_RULE_SET
   AS VARRAY(1048576) OF SDO_TRANSIENT_RULE ';
  exception when others then NULL;
 end;


 begin
  execute immediate 
  ' CREATE SEQUENCE MDSYS.TMP_COORD_OPS
   START WITH 1000000 MINVALUE 1000000 MAXVALUE 2000000
  INCREMENT BY 1 NOCACHE CYCLE ';
  exception when others then NULL;
 end;


 begin
  execute immediate 
  ' DROP TYPE MDSYS.TFM_PLAN ';
   exception when others then NULL;
 end;
end;
/

declare
begin
   begin
    execute immediate    ' drop table sdo_cs_context_information';
   exception when others then NULL;
  end;
end;
/

create global temporary table sdo_cs_context_information (
  from_srid number,
  to_srid   number,
  context RAW(4)) on commit preserve rows;

grant select on sdo_cs_context_information to public;


CREATE OR REPLACE TYPE MDSYS.TFM_PLAN
AS OBJECT (
  THE_PLAN SDO_TFM_CHAIN,

  CONSTRUCTOR FUNCTION TFM_PLAN
      RETURN SELF AS RESULT,

  CONSTRUCTOR FUNCTION TFM_PLAN(
    source_srid NUMBER)
      RETURN SELF AS RESULT,

  MEMBER PROCEDURE ADD_STEP(
    srid_source  NUMBER,
    tfm         NUMBER,
    srid_target  NUMBER),

  MEMBER FUNCTION GET_NUM_STEPS
      RETURN NUMBER,

  MEMBER FUNCTION GET_STEP(
    num           IN NUMBER,
    source_srid   OUT NUMBER,
    target_srid   OUT NUMBER) RETURN NUMBER,

  MEMBER FUNCTION expand_concat_chain_element(
    source_srid   IN NUMBER,
    chain_element IN NUMBER,
    target_srid   IN NUMBER)
      RETURN TFM_PLAN,

  MEMBER PROCEDURE expand_for_chain_element_core(
    srid_source  IN NUMBER,
    chain_element IN NUMBER,
    srid_target  IN NUMBER),

  MEMBER PROCEDURE expand_inv_chain_element_core(
    srid_source  IN NUMBER,
    chain_element IN NUMBER,
    srid_target  IN NUMBER),

  MEMBER PROCEDURE expand_chain_element_core(
    srid_source  IN NUMBER,
    chain_element IN NUMBER,
    srid_target  IN NUMBER),

  MEMBER FUNCTION expand_concat_chain_elements
      RETURN TFM_PLAN
);
/
show errors;

DROP TYPE MDSYS.epsg_params;

DROP TYPE MDSYS.epsg_param;

CREATE OR REPLACE TYPE MDSYS.epsg_param
AS OBJECT (
  id  NUMBER,
  val NUMBER,
  uom NUMBER);
/

CREATE OR REPLACE TYPE MDSYS.epsg_params
AS VARRAY(1048576) OF MDSYS.epsg_param;
/

declare 
begin
   begin
      EXECUTE IMMEDIATE ' Create Table MDSYS.SrsNameSpace_Table ( ' ||
     ' SrsNameSpace varchar2(2000), SrsName varchar2(2000), SDO_SRID NUMBER,' ||
       ' primary key(SrsNameSpace,SrsName) ) ';
      EXECUTE IMMEDIATE 
         ' grant select,insert,delete on MDSYS.SrsNameSpace_Table to public ';
    exception when others then null;
    end;
end;
/


CREATE OR REPLACE PACKAGE sdo_cs AUTHID current_user AS
/*
  TYPE EPSG_IDS
  IS TABLE OF NUMBER;
*/
  -- for TRANSFORM operator: trusted callout interface
  FUNCTION transform_orig(geom IN mdsys.sdo_geometry, 
                     dim  IN mdsys.sdo_dim_array, 
                     to_srid IN NUMBER)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  FUNCTION transform_orig(geom IN mdsys.sdo_geometry, 
                     tolerance  IN number, 
                     to_srid IN NUMBER)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  FUNCTION transform_orig(geom   IN mdsys.sdo_geometry, 
                     to_srid   IN NUMBER)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  FUNCTION transform_orig(geom   IN mdsys.sdo_geometry, 
                     dim    IN mdsys.sdo_dim_array, 
                     to_srname IN VARCHAR2)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
--  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  FUNCTION transform_orig(geom   IN mdsys.sdo_geometry, 
                     tolerance    IN number, 
                     to_srname IN VARCHAR2)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
--  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  FUNCTION transform_orig(geom   IN mdsys.sdo_geometry, 
                     to_srname IN VARCHAR2)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
--  PRAGMA restrict_references(transform_orig, wnds, rnps, wnps);

  PROCEDURE transform_layer( table_in   IN  VARCHAR2,
                            column_in  IN  VARCHAR2,
                            table_out  IN  VARCHAR2,
                            to_srid    IN  NUMBER   ); 
  FUNCTION viewport_transform(geom IN mdsys.sdo_geometry,
                               to_srid   IN NUMBER)
              RETURN mdsys.sdo_geometry DETERMINISTIC;
  PRAGMA restrict_references(viewport_transform, wnds, rnps, wnps);
--  FUNCTION viewport_transform(geom IN mdsys.sdo_geometry,
--                               to_srname   IN VARCHAR2)
--              RETURN mdsys.sdo_geometry DETERMINISTIC;
--  PRAGMA restrict_references(transform, wnds, rnps, wnps);
  FUNCTION validate_wkt(srid in NUMBER)
     RETURN varchar2;
  PRAGMA restrict_references(validate_wkt, wnds, rnps, wnps);

  FUNCTION intl_validate_wkt(srid in NUMBER)
     RETURN varchar2;
  PRAGMA restrict_references(intl_validate_wkt, wnds, rnps, wnps);

  FUNCTION internal_read_proj_from_wkt(
    wkt VARCHAR2)
      RETURN VARCHAR2;
  PRAGMA restrict_references(internal_read_proj_from_wkt, wnds, rnps, wnps);

  PROCEDURE transform_layer(
    table_in  IN VARCHAR2,
    column_in IN VARCHAR2,
    table_out IN VARCHAR2,
    use_plan  IN TFM_PLAN);

  PROCEDURE transform_layer(
    table_in  IN VARCHAR2,
    column_in IN VARCHAR2,
    table_out IN VARCHAR2,
    use_case  IN VARCHAR2,
    to_srid   IN NUMBER);

  FUNCTION determine_default_chain(
    source_srid IN NUMBER,
    target_srid IN NUMBER)
      RETURN SDO_SRID_CHAIN;

  FUNCTION determine_chain(
    transient_rule_set  IN SDO_TRANSIENT_RULE_SET,
    use_case            IN VARCHAR2,
    source_srid         IN NUMBER,
    target_srid         IN NUMBER)
      RETURN TFM_PLAN;

  FUNCTION internal_det_chain_VARCHAR(
    plan IN TFM_PLAN)
      RETURN VARCHAR2;

  FUNCTION internal_det_chain(
    transient_rule_set  IN SDO_TRANSIENT_RULE_SET,
    use_case            IN VARCHAR2,
    source_srid         IN NUMBER,
    target_srid         IN NUMBER,
    explain             IN BOOLEAN,
    explanation         OUT VARCHAR2)
      RETURN TFM_PLAN;

  FUNCTION internal_det_srid_wkt(
    srid1 NUMBER)
      RETURN VARCHAR2;

  FUNCTION internal_epsg_param_to_legacy(
    param_id  NUMBER,
    method_id NUMBER)
      RETURN VARCHAR2;

  FUNCTION map_oracle_srid_to_epsg(
    legacy_srid NUMBER)
      RETURN NUMBER;

  FUNCTION map_epsg_srid_to_oracle(
    epsg_srid NUMBER)
      RETURN NUMBER;

  PROCEDURE create_pref_concatenated_op(
    op_id     IN NUMBER,
    op_name   IN VARCHAR2,
    use_plan  IN TFM_PLAN,
    use_case  IN VARCHAR2);

  PROCEDURE create_concatenated_op(
    op_id     IN NUMBER,
    op_name   IN VARCHAR2,
    use_plan  IN TFM_PLAN);

  PROCEDURE delete_op(
    op_id     IN NUMBER);

  PROCEDURE add_preference_for_op(
    op_id       IN NUMBER,
    source_crs  IN NUMBER   DEFAULT NULL,
    target_crs  IN NUMBER   DEFAULT NULL,
    use_case    IN VARCHAR2 DEFAULT NULL);

  PROCEDURE revoke_preference_for_op(
    op_id       IN NUMBER,
    source_crs  IN NUMBER   DEFAULT NULL,
    target_crs  IN NUMBER   DEFAULT NULL,
    use_case    IN VARCHAR2 DEFAULT NULL);

  PROCEDURE create_obvious_epsg_rules(
    use_case    IN VARCHAR2 DEFAULT NULL);

  PROCEDURE delete_all_epsg_rules(
    use_case    IN VARCHAR2 DEFAULT NULL);

  FUNCTION transform_to_base_unit(
    value           IN NUMBER,
    source_unit_id  IN NUMBER)
      RETURN NUMBER;

  FUNCTION transform_to_wkt_param_unit(
    value                     IN NUMBER,
    source_unit_id            IN NUMBER,
    target_unit_id_if_length  IN NUMBER)
      RETURN NUMBER;

  PROCEDURE create_crs_using_legacy_proj(
    epsg_srid   IN NUMBER,
    new_srid    IN NUMBER);

  ------------------------------------------------------------------------------

  PROCEDURE internal_get_densification_res(
    crs_srid    IN NUMBER,
    minimum     IN NUMBER,
    maximum     IN NUMBER,
    resolution  OUT NUMBER,
    num_steps   OUT NUMBER);
  PRAGMA restrict_references(internal_get_densification_res, wnds, rnps, wnps);

  FUNCTION internal_densify_prior_to_tfm(
    mbr SDO_GEOMETRY)
      RETURN SDO_GEOMETRY;
  PRAGMA restrict_references(internal_densify_prior_to_tfm, wnds, rnps, wnps);

  ------------------------------------------------------------------------------

  FUNCTION transform(
    geom    IN MDSYS.SDO_GEOMETRY, 
    dim     IN MDSYS.SDO_DIM_ARRAY, 
    to_srid IN NUMBER)
      RETURN MDSYS.SDO_GEOMETRY DETERMINISTIC;
  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform(
    geom      IN MDSYS.SDO_GEOMETRY,
    tolerance IN NUMBER,
    to_srid   IN NUMBER)
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform(
    geom    IN MDSYS.SDO_GEOMETRY,
    to_srid IN NUMBER) 
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform_using_plan(
    geom     IN MDSYS.SDO_GEOMETRY,
    use_plan IN TFM_PLAN)
      RETURN MDSYS.SDO_GEOMETRY;

  FUNCTION transform(
    geom     IN MDSYS.SDO_GEOMETRY,
    use_plan IN TFM_PLAN)
      RETURN MDSYS.SDO_GEOMETRY;

  FUNCTION transform_orig_using_rules(
    geom     IN  mdsys.sdo_geometry,
    dim      IN  mdsys.sdo_dim_array,
    use_case IN VARCHAR2,
    tfmChain IN VARCHAR2,
    to_srid  IN  NUMBER) 
      RETURN mdsys.sdo_geometry;
  PRAGMA restrict_references(transform_orig_using_rules, wnds, rnps, wnps);

  FUNCTION transform_orig_using_rules(
    geom     IN MDSYS.SDO_GEOMETRY,
    use_case IN VARCHAR2,
    tfmChain IN VARCHAR2,
    to_srid  IN NUMBER)
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform_orig_using_rules, wnds, rnps, wnps);

  FUNCTION transform_using_case_name(
    geom     IN MDSYS.SDO_GEOMETRY,
    use_case IN VARCHAR2,
    to_srid  IN NUMBER)
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform_using_case_name, wnds, rnps, wnps);

  FUNCTION transform(
    geom     IN MDSYS.SDO_GEOMETRY,
    use_case IN VARCHAR2,
    to_srid  IN NUMBER)
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform(
    geom      IN MDSYS.SDO_GEOMETRY,
    dim       IN MDSYS.SDO_DIM_ARRAY,
    to_srname IN  VARCHAR2) 
      RETURN MDSYS.SDO_GEOMETRY;
--  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform(
    geom      IN MDSYS.SDO_GEOMETRY,
    tolerance IN NUMBER,
    to_srname IN VARCHAR2)
      RETURN MDSYS.SDO_GEOMETRY;
--  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  FUNCTION transform(
    geom      IN MDSYS.SDO_GEOMETRY,
    to_srname IN VARCHAR2) 
      RETURN MDSYS.SDO_GEOMETRY;
--  PRAGMA restrict_references(transform, wnds, rnps, wnps);

  ------------------------------------------------------------------------------

  PROCEDURE convert_NADCON_to_XML(
    LAA_CLOB  IN CLOB,
    LOA_CLOB  IN CLOB,
    xml_grid  OUT XMLTYPE);

  PROCEDURE convert_NTv2_to_XML(
    NTv2_CLOB IN CLOB,
    xml_grid  OUT XMLTYPE);

  PROCEDURE convert_XML_to_NADCON(
    xml_grid  IN XMLTYPE,
    LAA_CLOB  OUT CLOB,
    LOA_CLOB  OUT CLOB);

  PROCEDURE convert_XML_to_NTv2(
    xml_grid  IN XMLTYPE,
    NTv2_CLOB OUT CLOB);

  PROCEDURE convert_XML_table_to_NTv2(
    ntv2_file_id IN NUMBER,
    NTv2_CLOB    OUT CLOB);

  PROCEDURE convert_NTv2_to_XML_table(
    NTv2_CLOB    IN CLOB,
    ntv2_file_id OUT NUMBER);

  PROCEDURE delete_NTv2_xml_data(
    ntv2_file_id in number);

  function determine_nadcon_coverage(
    coord_op_id number)
      return sdo_geometry;

  FUNCTION find_geog_crs(
    reference_srid          IN NUMBER,
    is_legacy               IN VARCHAR2,
    max_rel_num_difference  IN NUMBER DEFAULT 0.000001)
      RETURN SDO_SRID_LIST;

  FUNCTION find_geog_crs(
    reference_wkt           IN VARCHAR2,
    is_legacy               IN VARCHAR2,
    max_rel_num_difference  IN NUMBER DEFAULT 0.000001)
      RETURN SDO_SRID_LIST;

  FUNCTION find_proj_crs(
    reference_srid          IN NUMBER,
    is_legacy               IN VARCHAR2,
    max_rel_num_difference  IN NUMBER DEFAULT 0.000001)
      RETURN SDO_SRID_LIST;

  FUNCTION find_proj_crs(
    reference_wkt           IN VARCHAR2,
    is_legacy               IN VARCHAR2,
    max_rel_num_difference  IN NUMBER DEFAULT 0.000001)
      RETURN SDO_SRID_LIST;

  FUNCTION get_epsg_data_version
    RETURN VARCHAR2;

  ------------------------------------------------------------------------------

  FUNCTION from_OGC_SimpleFeature_SRS(
    wkt VARCHAR2)
      RETURN VARCHAR2;

  FUNCTION to_OGC_SimpleFeature_SRS(
    wkt VARCHAR2)
      RETURN VARCHAR2;

  ------------------------------------------------------------------------------

  PROCEDURE intl_populate_datum_3params(
    datum_id  NUMBER,
    op_id     NUMBER);

  PROCEDURE intl_populate_datum_7params(
    datum_id  NUMBER,
    op_id     NUMBER);

  ------------------------------------------------------------------------------

  FUNCTION to_USNG(
    geom              SDO_GEOMETRY,
    accuracyInMeters  NUMBER,
    datum             VARCHAR2 DEFAULT 'NAD83')
      RETURN
        VARCHAR2;

  FUNCTION from_USNG(
    usng              VARCHAR2,
    srid              NUMBER,
    datum             VARCHAR2 DEFAULT 'NAD83')
      RETURN SDO_GEOMETRY;

  ------------------------------------------------------------------------------

  PROCEDURE update_wkts_for_all_epsg_crs;

  PROCEDURE update_wkts_for_epsg_crs(
    srid NUMBER);

  PROCEDURE update_wkts_for_epsg_param(
    coord_op_id   NUMBER,
    parameter_id  NUMBER);

  PROCEDURE update_wkts_for_epsg_op(
    coord_op_id   NUMBER);

  PROCEDURE update_wkts_for_epsg_datum(
    datum_id NUMBER);

  PROCEDURE update_wkts_for_epsg_ellips(
    ellipsoid_id NUMBER);

  PROCEDURE update_wkts_for_epsg_pm(
    prime_meridian_id NUMBER);

  function disambiguate_proj_name(
    srid number,
    wkt varchar2)
      return varchar2;

  function disambiguate_proj_name(
    srid number)
      return varchar2;

  ------------------------------------------------------------------------------

  function is_within_tolerance(
    a               number,
    b               number,
    rel_tolerance   number)
      return number;

  FUNCTION lookup_epsg_param(
    coord_op_id     number,
    parameter_id    number)
      return number;

  PROCEDURE find_srid(
    srid                OUT NUMBER,
    epsg_srid_geog      IN NUMBER DEFAULT NULL,
    epsg_srid_proj      IN NUMBER DEFAULT NULL,
    datum_id            IN NUMBER DEFAULT NULL,
    ellips_id           IN NUMBER DEFAULT NULL,
    pm_id               IN NUMBER DEFAULT NULL,
    proj_method_id      IN NUMBER DEFAULT NULL,
    coord_ref_sys_kind  IN VARCHAR2 DEFAULT NULL,
    semi_major_axis     IN NUMBER DEFAULT NULL,
    semi_minor_axis     IN NUMBER DEFAULT NULL,
    inv_flattening      IN NUMBER DEFAULT NULL,
    params              IN MDSYS.epsg_params DEFAULT NULL);

  PROCEDURE find_srid(
    srid                OUT NUMBER,
    epsg_srid_geog      IN NUMBER DEFAULT NULL,
    epsg_srid_proj      IN NUMBER DEFAULT NULL,
    datum_id            IN NUMBER DEFAULT NULL,
    ellips_id           IN NUMBER DEFAULT NULL,
    pm_id               IN NUMBER DEFAULT NULL,
    proj_method_id      IN NUMBER DEFAULT NULL,
    proj_op_id          IN NUMBER DEFAULT NULL,
    coord_ref_sys_kind  IN VARCHAR2 DEFAULT NULL,
    semi_major_axis     IN NUMBER DEFAULT NULL,
    semi_minor_axis     IN NUMBER DEFAULT NULL,
    inv_flattening      IN NUMBER DEFAULT NULL,
    params              IN MDSYS.epsg_params DEFAULT NULL,
    max_rel_num_difference  IN NUMBER DEFAULT 0.000001);

  function fill_in_default_units(
    params mdsys.epsg_params,
    crs number)
      return mdsys.epsg_params;

  PROCEDURE find_epsg_params(
    srid                IN  NUMBER,
    epsg_srid_geog      OUT NUMBER,
    datum_id            OUT NUMBER,
    ellips_id           OUT NUMBER,
    pm_id               OUT NUMBER,
    proj_method_id      OUT NUMBER,
    proj_op_id          OUT NUMBER,
    coord_ref_sys_kind  OUT VARCHAR2,
    semi_major_axis     OUT NUMBER,
    semi_minor_axis     OUT NUMBER,
    inv_flattening      OUT NUMBER,
    params              OUT MDSYS.epsg_params);

  function get_crs_dimensionality(
    srid number)
      return number;
  pragma restrict_references(get_crs_dimensionality, WNDS, WNPS, RNPS);

  function make_3d(
    geom2d in mdsys.sdo_geometry,
    height in number default 0,
    target_srid in number default null)
      return mdsys.sdo_geometry;

  function make_2d(
    geom3d in mdsys.sdo_geometry,
    target_srid in number default null)
      return mdsys.sdo_geometry;

  function convert_2d_to_3d(
    geom2d      mdsys.sdo_geometry,
    height      number             default 0,
    vert_id     number             default 9903,
    ellipsoidal varchar2           default 'TRUE',
    uom_id      number             default 9001)
      return mdsys.sdo_geometry;

  function convert_2d_srid_to_3d(
    srid2d      number,
    vert_id     number             default 9903,
    ellipsoidal varchar2           default 'TRUE',
    uom_id      number             default 9001)
      return number;

  ------------------------------------------------

  FUNCTION transform_across_dims(
    geom      IN MDSYS.SDO_GEOMETRY,
    to_srid   IN NUMBER) 
      RETURN MDSYS.SDO_GEOMETRY;
  PRAGMA restrict_references(transform_across_dims, wnds, rnps, wnps);

  PROCEDURE transform_layer_across_dims(
    table_in  IN VARCHAR2,
    column_in IN VARCHAR2,
    table_out IN VARCHAR2,
    to_srid   IN NUMBER);

  PROCEDURE transform_layer_across_dims(
    table_in  IN VARCHAR2,
    column_in IN VARCHAR2,
    table_out IN VARCHAR2,
    use_plan  IN TFM_PLAN);

  PROCEDURE transform_layer_across_dims(
    table_in  IN VARCHAR2,
    column_in IN VARCHAR2,
    table_out IN VARCHAR2,
    use_case  IN VARCHAR2,
    to_srid   IN NUMBER);

  ------------------------------------------------

  function reformat_gtype(
    geom1 mdsys.sdo_geometry,
    srid2 number)
      return mdsys.sdo_geometry;

    PROCEDURE register_SrsNameSpace ( SrsNameSpace IN varchar2,
                                      SrsName IN varchar2, sdo_srid in number);
    FUNCTION get_SRID_for_NameSpace(  SrsNameSpace IN varchar2, 
                                    SrsName IN varchar2) return NUMBER;
    FUNCTION get_SrsName_for_NameSpace(  SrsNameSpace IN varchar2, 
                                    sdo_srid IN number) return varchar2;

  function convert_mixed_axes_units(
    srid           number,
    unitless_value number,
    output_unit    varchar2)
      return number;

  function get_3d_wkt(
    srid number)
      return varchar2;

  function generate_crs_from_wkt(
    wkt                    varchar2, -- Input WKT
    proj_op_id             number,   -- ID of new proj operation, if required
    proj_op_name           varchar2, -- Name of new proj operation, if required
    datum_tfm_op_id        number,   -- ID of new datum tfm operation, if required
    datum_tfm_op_name      varchar2, -- Name of new datum tfm operation, if required
    exclude_existing_elems varchar2 default 'FALSE') -- Exclude elements that already exist in mdsys
      return CLOB;

  function generate_epsg_rule_for_3785(
    source_srid     number,
    datum_tfm_op_id number,
    concat_op_id    number)
      return CLOB;

  function get_wkt_path(
    wkt           varchar2,
    path          varchar2,
    default_value varchar2 default null)
      return varchar2;
  PRAGMA restrict_references(get_wkt_path, wnds, rnps, wnps);

  function tfm_axis_orientation_to_wkt(
    axis_orientation VARCHAR2)
      return VARCHAR2;

  procedure update_wkt_with_7_params(
    params     varchar2,
    datum_name varchar2);

  procedure update_wkts_with_hc_params;

  procedure determine_srid_units(
    srid        in  number,
    uom_id1     out number,
    uom_id2     out number,
    uom_id3     out number);

  function transform_ogc_cs_wkt_to_srs(
    wktext varchar2)
      return varchar2;

END sdo_cs;
/

show errors;

GRANT EXECUTE ON MDSYS.SDO_SRID_CHAIN TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM SDO_SRID_CHAIN FOR MDSYS.SDO_SRID_CHAIN;

GRANT EXECUTE ON MDSYS.SDO_TFM_CHAIN TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM SDO_TFM_CHAIN FOR MDSYS.SDO_TFM_CHAIN;

GRANT EXECUTE ON MDSYS.TFM_PLAN TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM TFM_PLAN FOR MDSYS.TFM_PLAN;

GRANT SELECT ON MDSYS.TMP_COORD_OPS TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM TMP_COORD_OPS FOR MDSYS.TMP_COORD_OPS;

GRANT EXECUTE ON MDSYS.EPSG_PARAM TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM EPSG_PARAM FOR MDSYS.EPSG_PARAM;

GRANT EXECUTE ON MDSYS.EPSG_PARAMS TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM EPSG_PARAMS FOR MDSYS.EPSG_PARAMS;
