declare
begin
  begin
   execute immediate 
  ' create sequence sdo_idx_tab_sequence start with 1 increment by 1
  maxvalue 999999999999999999999999999 cycle ';
  exception when others then NULL;
  end;
end;
/
grant all on sdo_idx_tab_sequence to public;
commit;
CREATE OR REPLACE PACKAGE  transform_map wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
166 fb
n9JILj+Zk/zLCGSTV8vvNrSDXF4wg/DIfyisZ3QCkHOUpH84wAHlcfdth7sl2vU0S1NrOXO1
80Tp73+8LXOSkqMHOjmxNrFhIofLsGH0LhA50nxbuODHCY0vFnQEKhLxpqGoPKKWozfJj5fY
WiXaIn2z1MWJ97Mvimmb0wFYSttNcBLvMDr1jaoXRDEfK1mKdEAIPv+BH2CYF/Gap67SCzpm
+D8CUWJE8htm9SEtI5kXUAED/S2bXDY=

/
CREATE OR REPLACE PACKAGE  BODY transform_map wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
1ab 11f
Ax4QbGO3zrYwFr07QuY9fTItuzswg/DImMsVfHTpWPiUHG3b24P6eXTwEW6XoTVZzB9h4cXk
neeJeOc4DEpdcSzB6NTgN8esaOUqdvifErSKktH1zRxQNeFpbWJaKuYOUAtQilp6GoItG1ez
niEdeBtJWG5DsVNxC4bZ3RHnpxbp9GWQeo7QpZ+mr5C22rX/nsITk3+o4eKGmvWBcIK0lLfk
YBkaltnGFyCDlfHrccfhhNSNXI2Vt1QB+sEgPuWFXpJDjbvdvytWrot0PDF9Nm2Y4C8=

/
show errors;
grant execute on transform_map to public;
create or replace
FUNCTION sdo_construct_dim_array wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
36b 20f
Xp5SO+p/GsNEXT2+F5pIFNSos4Qwg43xryCsfI7UvzNkMHjKUjZon4hFNinrKms73tnosNR9
AQfXolU4tXODGONap1+4jw0sD5r+EpBcNFDDuFd+v8kD+Ht2/poTjDE/z+yBP535vb6oiftD
Mz1XgK02YvLqThguzGAd9EXxdgTM1ZTVZm4ZXQmL1Q4nprg+EhSTYt3TiPU2SLK+n9OewbC5
FWJl4Baq2QzyX3m9Af+Z0qtxb1O4a3vFufEhrgwqdQZuwL//dPzdCwQ3yLjFh49/4XMY3OHK
x+VLHh5K5tzYhCv4V1mGvRRSm7GdLsF51t+JwaEkp3zKlAsKcOf+3m5weyOr5k44TIyKGHPw
8A/LZFx/0Q3N7kbdINdWHfzokNQI9Ok+2xWAvnVQhTFPEtpbUuYsY7zKO1h4fc25mOnHghpA
H+8WAYomiNXCAsg43ePwpLQUInOE8RS8MasgPIYjvsq1oNzrh/Y4eRr8Rz05A76kj+a4YwFc
fTwunfhExMcFCQ==

/
