Rem
Rem $Header: sdo/admin/sdordfa.sql /st_sdo_11.2.0/3 2010/06/16 07:44:24 vkolovsk Exp $
Rem
Rem sdordfa.sql
Rem
Rem Copyright (c) 2005, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdordfa.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    matperry    06/09/10 - add parallel_enable to filter functions
Rem    vkolovsk    06/04/10 - make autonomousDML/DDL private
Rem    sdas        05/27/10 - bug 9756570: sem network index info view
Rem    vkolovsk    05/05/10 - add delete_triples api
Rem    matperry    11/20/09 - add langMatches as SPARQL FILTER helper function
Rem    matperry    11/12/09 - refactor all value$ filter functions to take all
Rem                           value$ columns as arguments
Rem    matperry    11/11/09 - remove getV$RDFTVal
Rem    matperry    09/04/09 - add getV$STRVal()
Rem    matperry    08/31/09 - remove unused getV$ functions after Filter
Rem                           refactoring
Rem    matperry    08/13/09 - add rdfTermEqual and rdfTermComp
Rem    sdas        08/04/09 - for BASIC compress of OLTP data
Rem                           allow MOVE within same tablespace
Rem    sdas        07/13/09 - add GET_SEM_INDEX_INFO
Rem    matperry    07/13/09 - add constant for syn_orig_value flag
Rem    matperry    06/26/09 - add dateTimeComp() and dateTimeEqual()
Rem    sdas        05/27/09 - add a vc2 compose_rdf_term too (= syn_orig_value)
Rem    sdas        05/20/09 - Add compose_rdf_term func: to handle CLOB values
Rem    alwu        05/14/09 - add set_session_param API
Rem    matperry    05/05/09 - move type definitions to sdordfty.sql
Rem    alwu        05/04/09 - remove APIS start with alter_index_on
Rem    alwu        04/30/09 - add APIs for rules_index to entailment name
Rem                           change
Rem    matperry    04/17/09 - add effective boolean value helper method for
Rem                           FILTER
Rem    alwu        03/30/09 - remove inf upgrade
Rem    vkolovsk    03/29/09 - add upgrade inf to 11_2 function
Rem    vkolovsk    03/25/09 - add rename_model api
Rem    matperry    01/30/09 - add helper functions for SPARQL FILTER
Rem    vkolovsk    01/21/09 - add inc. inf apis
Rem    alwu        12/29/08 - XbranchMerge alwu_new_ha_apis from st_sdo_11.1
Rem    alwu        12/18/08 - add autonomousDDL that returns rowcount
Rem    sdas        11/21/08 - ALTER_INDEX_ON_SEM_GRAPH: add simpler interfaces
Rem    matperry    10/31/08 - add create_virtual_model, drop_virtual_model
Rem    sdas        10/14/08 - flex indexing: add COMPRESS param
Rem    vkolovsk    09/03/08 - factor out clique-related code
Rem    vkolovsk    08/08/08 - adding SCN related function for
Rem                           sameas/incremental optimization
Rem    alwu        07/14/08 - migrate remove_duplicates or remove_duplicates API
Rem    alwu        07/14/08 - add HA APIs
Rem    alwu        07/11/08 - add rename_entailment
Rem    alwu        07/09/08 - add swap_model_names API
Rem    alwu        06/27/08 - add logic to counter act the truncate trigger
Rem    alwu        06/26/08 - add more parameters to clean_model API
Rem    alwu        06/25/08 - add new APIs for UTH enhancement requests
Rem    spsundar    04/19/08 - update bulk_load_from_staging_table signature
Rem    sdas        03/05/08 - bulk-append perf
Rem    sdas        02/28/08 - fif: generalize for inference and rules_index
Rem    sdas        02/20/08 - fif: allow more options on alter
Rem    sdas        02/18/08 - flexible indexing framework
Rem    sdas        12/12/07 - add REFRESH_QUERY_STATE
Rem    alwu        09/13/07 - add statistics on demand
Rem    alwu        08/22/07 - add get_triple_id, is_triple for CLOB
Rem    sdas        06/11/07 - new value_name_[prefix|suffix]_expr
Rem    sdas        06/04/07 - allow table alias in prefix and suffix expr
Rem    sdas        06/01/07 - value idx on RDF_VALUE: use prefix and suffix
Rem    sdas        04/28/07 - perf: avoid calls to user-def func syn_orig_value
Rem    sdas        04/16/07 - remove rdf dummy upgrade/downgrade routines
Rem    sdas        04/16/07 - bug 5998985: GET_TRIPLE_ID should return VARCHAR2
Rem    alwu        04/13/07 - remove gather_rdf_stats
Rem    alwu        03/30/07 - add analyze intermediate table
Rem    sdas        03/16/07 - add REFRESH_SEM_TABLESPACE_NAMES
Rem    sdas        03/09/07 - allow storing diff RDF models in diff tablespaces
Rem    alwu        02/26/07 - add a constant denoting OWL inference closure
Rem    alwu        02/23/07 - add API for syn_orig_value
Rem    sdas        01/30/07 - adjust interfaces: IS_TRIPLE, GET_TRIPLE_ID
Rem    alwu        01/02/07 - fix indentation
Rem    sdas        10/24/06 - use new name BULK_LOAD_FROM_STAGING_TABLE
Rem    alwu        09/29/06 - lift DBA privilege restriction on batch load
Rem    sdas        09/16/06 - remove add_namespaces
Rem    alwu        09/07/06 - sync up 11g document and proposed APIs
Rem    alwu        06/14/06 - API cleanup: add functions point to owlfast 
Rem                           inference and validation 
Rem    geadon      07/14/05 - bug 4486600: make LOOKUP_LITERAL public
Rem    geadon      05/26/05 - Add DROP_USER_INFERENCE_OBJS interface 
Rem    geadon      05/20/05 - remove unnecessary privs and interfaces 
Rem    geadon      05/16/05 - Add notify_model_DML to rdf_apis 
Rem    geadon      05/12/05 - Add raiseURIerror 
Rem    geadon      05/06/05 - Fix add/drop operations 
Rem    geadon      05/05/05 - remove unused code 
Rem    geadon      05/04/05 - max RDF name length is 25 
Rem    geadon      04/26/05 - rename APIs to conform to SDO standards 
Rem    geadon      04/14/05 - IndexName for RulesIndex 
Rem    sravada     04/11/05 - remove echo on 
Rem    geadon      04/04/05 - move private interfaces to RDF_APIS_INTERNAL 
Rem    geadon      04/04/05 - name changes to conform to functional spec 
Rem    geadon      04/01/05 - ignore all CREATE OR REPLACE TYPE exceptions
Rem    geadon      03/30/05 - use authid current_user 
Rem    geadon      03/07/05 - geadon_rdf_query
Rem    geadon      03/07/05 - Created
Rem

grant execute on MDSYS.RDF_Models            to public;
grant execute on MDSYS.RDF_RuleBases         to public;
grant execute on MDSYS.RDF_Alias             to public;
grant execute on MDSYS.RDF_Aliases           to public;
grant execute on MDSYS.RDF_varcharArray      to public;
grant execute on MDSYS.RDF_longVarcharArray  to public;

create or replace public synonym SDO_RDF_Models    for MDSYS.RDF_Models;
create or replace public synonym SDO_RDF_RuleBases for MDSYS.RDF_RuleBases;
create or replace public synonym SDO_RDF_Alias     for MDSYS.RDF_Alias;
create or replace public synonym SDO_RDF_Aliases   for MDSYS.RDF_Aliases;

create or replace public synonym SEM_Models    for MDSYS.RDF_Models;
create or replace public synonym SEM_RuleBases for MDSYS.RDF_RuleBases;
create or replace public synonym SEM_Alias     for MDSYS.RDF_Alias;
create or replace public synonym SEM_Aliases   for MDSYS.RDF_Aliases;
create or replace public synonym SEM_varcharArray     for MDSYS.RDF_varcharArray;
create or replace public synonym SEM_longVarcharArray for MDSYS.RDF_longVarcharArray;


CREATE OR REPLACE PACKAGE rdf_apis authid current_user AS
  REACH_CLOSURE constant integer := 0;
  
  -- flag for syn_orig_value 
  STRIP_TIME_PREFIX constant integer := 1;

  -- invalidates cursors with unresolved values
  PROCEDURE refresh_query_state;

  -- create entailment based on the subset of OWL-DL
  -- that we implemented
  procedure create_entailment(index_name_in     varchar2,
                              models_in         mdsys.rdf_models,
                              rulebases_in      mdsys.rdf_rulebases,
                              passes            integer  default 0,
                              inf_components_in varchar2 default null,
                              options           varchar2 default null,
                              delta_in          mdsys.rdf_models default null,
                              label_gen         mdsys.RDFSA_LABELGEN
                                                                 default null
                              ); 

  procedure drop_entailment(index_name_in     varchar2);

  procedure enable_inc_inference(entailment_name varchar2);
  procedure disable_inc_inference(entailment_name varchar2);
  procedure enable_change_tracking(models_in mdsys.rdf_models);
  procedure disable_change_tracking(models_in mdsys.rdf_models);

  procedure analyze_intermediate;

  function validate_model(models_in         mdsys.rdf_models,
                          criteria_in       varchar2 default null,
                          max_conflict      int default 100,
                          options           varchar2 default null
                          )
  return mdsys.rdf_longVarcharArray;


  function validate_entailment(models_in         mdsys.rdf_models,
                               rulebases_in      mdsys.rdf_rulebases,
                               criteria_in       varchar2 default null,
                               max_conflict      int default 100,
                               options           varchar2 default null
                               )
  return mdsys.rdf_longVarcharArray;

   -- Create a Rulebase 
   PROCEDURE CREATE_RULEBASE (
        rulebase_name   VARCHAR2);

   -- Delete a Rulebase 
   PROCEDURE DROP_RULEBASE (
        rulebase_name   VARCHAR2);

   -- Infer + store triples for specified models + rules 
   PROCEDURE CREATE_RULES_INDEX (
        index_name_in   varchar2,
        models_in       MDSYS.RDF_Models,
        rulebases_in    MDSYS.RDF_Rulebases);

   -- Remove inferred triples for specified rulesindex
   PROCEDURE DROP_RULES_INDEX (
        index_name      varchar2);

   FUNCTION LOOKUP_RULES_INDEX (
        models          MDSYS.RDF_Models,
        rulebases       MDSYS.RDF_Rulebases)
   RETURN varchar2;


   FUNCTION LOOKUP_ENTAILMENT (
        models          MDSYS.RDF_Models,
        rulebases       MDSYS.RDF_Rulebases)
   RETURN varchar2;

   -- CLEANUP_FAILED: cleanup a failed operation
   --   typ: 'RULEBASE' or 'RULES_INDEX'
   --   name: rulebase or ruleindex name.
   PROCEDURE CLEANUP_FAILED(
         rdf_object_type    varchar2,
         rdf_object_name    varchar2);

   PROCEDURE DROP_USER_INFERENCE_OBJS(uname varchar2);

   ---------------------------------------------------------------------------
   -- Interfaces below this line are intended for internal use only;
   -- they should not be documented or directly invoked by the user.
   ---------------------------------------------------------------------------

   FUNCTION  raiseURIError(rulename varchar2, pos int) RETURN int;

   -- 
   -- LOOKUP_LITERAL
   -- bug #4486600: we need LOOKUP_LITERAL to be publicly executable.
   -- For now public doesn't need access to the CLOB variant of
   -- LOOKUP_LITERAL or to LOOKUP_CREATE_LITERAL.
   -- 
   PROCEDURE LOOKUP_LITERAL(
        lit varchar2, typ varchar2, lang varchar2,
        canonID OUT int, exactID OUT int);


  --
  -- START (PROXY) APIS MERGED FROM SDO_RDF PACKAGE (sdordfxh.sql) --
  --
  PROCEDURE BULK_LOAD_FROM_STAGING_TABLE (
    model_name     IN          varchar2,
    table_owner    IN          varchar2, 
    table_name     IN          varchar2,
    flags          IN          varchar2 default NULL,
    debug          IN          PLS_INTEGER default NULL,
    start_comment  IN          varchar2 default NULL,
    end_comment    IN          varchar2 default NULL
  );

  PROCEDURE RESUME_LOAD_FROM_STAGING_TABLE (
    model_name          IN              varchar2,
    table_owner         IN              varchar2,
    table_name          IN              varchar2,
    session_id          IN              varchar2,
    flags               IN              varchar2 default NULL,
    start_comment       IN              varchar2 default NULL,
    end_comment         IN              varchar2 default NULL
  );

  PROCEDURE get_change_tracking_info(model_name          IN     VARCHAR2,
                                     enabled             OUT BOOLEAN,
                                     tracking_start_time OUT TIMESTAMP);

  PROCEDURE get_inc_inf_info(entailment_name     IN     VARCHAR2,
                             enabled             OUT BOOLEAN,
                             prev_inf_start_time OUT TIMESTAMP);

  PROCEDURE merge_models(source_model          IN VARCHAR2,
                         destination_model     IN VARCHAR2,
                         rebuild_apptab_index  IN BOOLEAN DEFAULT TRUE,
                         drop_source_model     IN BOOLEAN DEFAULT FALSE,
                         options               IN varchar2 DEFAULT NULL);

	FUNCTION get_model_id (
		model_name IN VARCHAR2
	) RETURN NUMBER ;

	FUNCTION get_model_name (
		model_id IN NUMBER
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_id IN NUMBER,
		rdf_t_id IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_name IN VARCHAR2,
		rdf_t_id IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_id IN NUMBER,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_name IN VARCHAR2,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_id IN NUMBER,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN CLOB
	) RETURN VARCHAR2 ;

	FUNCTION is_triple (
		model_name IN VARCHAR2,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN CLOB
	) RETURN VARCHAR2 ;


	FUNCTION get_triple_id (
		model_id IN NUMBER,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN CLOB
	) RETURN VARCHAR2 ;

	FUNCTION get_triple_id (
		model_name IN VARCHAR2,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN CLOB
	) RETURN VARCHAR2 ;

	FUNCTION get_triple_id (
		model_id IN NUMBER,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object   IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION get_triple_id (
		model_name IN VARCHAR2,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object   IN VARCHAR2
	) RETURN VARCHAR2 ;

        FUNCTION get_object_id ( 
                object   IN VARCHAR2
        ) RETURN NUMBER; 

	FUNCTION is_reified_quad (
		model_id IN NUMBER,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN VARCHAR2
	) RETURN VARCHAR2 ;

	FUNCTION is_reified_quad (
		model_name IN VARCHAR2,
		subject IN VARCHAR2,
		property IN VARCHAR2,
		object IN VARCHAR2
	) RETURN VARCHAR2 ;

  PROCEDURE refresh_sem_tablespace_names (
    model_name                 VARCHAR2 default NULL  -- NULL => all
  );
  PROCEDURE create_rdf_model (model_name IN VARCHAR2, table_name IN VARCHAR2, column_name IN VARCHAR2, model_tablespace IN VARCHAR2 default NULL);
  PROCEDURE drop_rdf_model (model_name IN VARCHAR2);
  PROCEDURE create_sem_model (model_name IN VARCHAR2, table_name IN VARCHAR2, column_name IN VARCHAR2, model_tablespace IN VARCHAR2 default NULL);
  PROCEDURE drop_sem_model (model_name IN VARCHAR2);

  PROCEDURE create_virtual_model (
    vm_name 		IN VARCHAR2
  , models		IN MDSYS.RDF_Models
  , rulebases		IN MDSYS.RDF_Rulebases default NULL
  , options             IN VARCHAR2 default NULL
  );
  PROCEDURE drop_virtual_model (vm_name IN VARCHAR2);

  PROCEDURE cleanup_batch_load (table_name IN VARCHAR2);
  PROCEDURE cleanup_batch_load (table_name IN VARCHAR2, temp_tab_name IN VARCHAR2);
  PROCEDURE create_rdf_network (tablespace_name in varchar2);
  PROCEDURE drop_rdf_network (cascade in boolean default false);
  PROCEDURE create_sem_network (tablespace_name in varchar2);
  PROCEDURE drop_sem_network (cascade in boolean default false);
  -- PROCEDURE update_user_table$ (model_id  IN NUMBER);
  PROCEDURE create_logical_network (model_name IN VARCHAR2);
  PROCEDURE drop_logical_network (model_name IN VARCHAR2);
  --
  -- DONE  APIS MERGED FROM SDO_RDF PACKAGE (sdordfxh.sql) --
  --
	PROCEDURE start_batch$ (
		tempTblName IN VARCHAR2, model_id IN NUMBER, model_name IN VARCHAR2, rdf_tablespace IN VARCHAR2 
	); 

	PROCEDURE start_batch$ (tempTblName IN VARCHAR2, model_id IN NUMBER, model_name IN VARCHAR2, 
                          rdf_tablespace IN VARCHAR2, exchange IN NUMBER DEFAULT NULL);

	PROCEDURE start_batch$ (
		model_id IN NUMBER, model_name IN VARCHAR2, rdf_tablespace IN VARCHAR2 
	); 

	PROCEDURE start_batch$ (
		model_id IN NUMBER, model_name IN VARCHAR2, rdf_tablespace IN VARCHAR2,
    exchange IN NUMBER
	); 

	PROCEDURE exchange_model_part$ (
		model_id       IN NUMBER, 
    rdf_tablespace IN VARCHAR2);


  FUNCTION syn_orig_value (
    value_name    varchar2, 
    value_type    varchar2,
    literal_type  varchar2,
    language_type varchar2,
    flag          integer default 0
  ) 
  RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (syn_orig_value,WNDS,RNDS,WNPS);

  FUNCTION compose_rdf_term (
    value_name   varchar2, 
    value_type   varchar2,
    literal_type varchar2,
    language_type varchar2
  ) 
  RETURN varchar2 DETERMINISTIC;
  pragma restrict_references (compose_rdf_term,WNDS,RNDS,WNPS);

  FUNCTION compose_rdf_term (
    value_name    varchar2, 
    value_type    varchar2,
    literal_type  varchar2,
    language_type varchar2,
    long_value    CLOB,
    options       varchar2 default NULL
  ) 
  RETURN CLOB DETERMINISTIC;

  -- function to return the body (CASE END statement) of syn_orig_value func.
  FUNCTION syn_orig_value_body (alias VARCHAR2 default NULL)
  RETURN VARCHAR2 DETERMINISTIC;
  pragma restrict_references (syn_orig_value_body,WNDS,RNDS,WNPS,RNPS);

  -- function to return the expr for computing URI prefix and suffix
  FUNCTION value_name_prefix (
    value_name                     VARCHAR2
  , value_type                     VARCHAR2
  ) RETURN                         VARCHAR2 DETERMINISTIC;
  pragma restrict_references (value_name_prefix,WNDS,RNDS,WNPS,RNPS);

  FUNCTION value_name_suffix (
    value_name                     VARCHAR2
  , value_type                     VARCHAR2
  ) RETURN                         VARCHAR2 DETERMINISTIC;
  pragma restrict_references (value_name_suffix,WNDS,RNDS,WNPS,RNPS);

  FUNCTION value_name_prefix_expr (
    vname_expr                     VARCHAR2
  ) RETURN                         VARCHAR2 DETERMINISTIC;
  pragma restrict_references (value_name_prefix_expr,WNDS,RNDS,WNPS,RNPS);

  FUNCTION value_name_suffix_expr (
    vname_expr                     VARCHAR2
  ) RETURN                         VARCHAR2 DETERMINISTIC;
  pragma restrict_references (value_name_suffix_expr,WNDS,RNDS,WNPS,RNPS);

  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_model(
      model_name       IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );

  /**
   * Note only rules index owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_rules_index(
      index_name IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );


  PROCEDURE analyze_entailment(
      entailment_name  IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );

  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  -- PROCEDURE delete_model_stats(
  --     model_name       IN VARCHAR2,
  --     cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
  --     cascade_columns  IN BOOLEAN  DEFAULT TRUE,
  --     cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
  --     no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
  --     force            IN BOOLEAN  DEFAULT FALSE
  --    );


  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  -- PROCEDURE delete_rules_index_stats(
  --     index_name       IN VARCHAR2,
  --     cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
  --     cascade_columns  IN BOOLEAN  DEFAULT TRUE,
  --     cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
  --     no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
  --     force            IN BOOLEAN  DEFAULT FALSE
  --    );
  -- 

  PROCEDURE swap_names(model1  in VARCHAR2,
                       model2  in VARCHAR2);

  PROCEDURE rename_model(old_name  in VARCHAR2,
                         new_name  in VARCHAR2);

  PROCEDURE rename_entailment(old_name  in VARCHAR2,
                              new_name  in VARCHAR2);

  /**
   * This procedure removes duplicates from a model.
   * Note only model owner (or sys dba) is allowed to perform this action. 
   * All information in the existing application table will be lost. 
   * 'Triple' column will be reconstructed.
   * A commit will be performed at the very beginning of the procedure
   * and at the very end of the procedure.
   */
  PROCEDURE remove_duplicates(model_name     IN VARCHAR2,
                        threshold            IN FLOAT DEFAULT 0.3,
                        rebuild_apptab_index IN BOOLEAN DEFAULT TRUE);

  PROCEDURE get_sem_index_info (flags VARCHAR2 default NULL);

  PROCEDURE add_sem_index (
    index_code      IN  VARCHAR2
  , tablespace_name IN  VARCHAR2 default NULL
  , compression_length IN  INTEGER default NULL
  );

  PROCEDURE drop_sem_index (
    index_code      IN   VARCHAR2
  );

  /*
  PROCEDURE alter_index_on_sem_graph (
    graph_name       IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , parallel         IN  INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  , tablespace_name  IN  VARCHAR2 default NULL
  , is_rules_index   IN  BOOLEAN default FALSE
  , use_compression  IN  BOOLEAN default NULL
  );


  PROCEDURE alter_index_on_sem_graph (
    graph_name       IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , parallel         IN  INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  , tablespace_name  IN  VARCHAR2 default NULL
  , is_entailment    IN  BOOLEAN default FALSE
  , use_compression  IN  BOOLEAN default NULL
  );
  */


  PROCEDURE alter_sem_index_on_model (
    model_name       IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , use_compression  IN  BOOLEAN default NULL
  , parallel         IN  INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  );


  /*
  PROCEDURE alter_sem_index_on_rules_index (
    rules_index_name IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , use_compression  IN  BOOLEAN default NULL
  , parallel         IN  INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  );
  */

  PROCEDURE alter_sem_index_on_entailment (
    entailment_name  IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , use_compression  IN  BOOLEAN default NULL
  , parallel         IN  INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  );


  PROCEDURE refresh_sem_network_index_info (
    options             IN VARCHAR2 default NULL
  );


  /*
  PROCEDURE alter_sem_graph (
    graph_name       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2
  , parallel         IN  INTEGER default NULL
  , is_entailment    IN  BOOLEAN default FALSE
  );


  PROCEDURE alter_sem_graph (
    graph_name       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2
  , parallel         IN  INTEGER default NULL
  , is_rules_index   IN  BOOLEAN default FALSE
  );
  */

  PROCEDURE alter_model (
    model_name       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , parallel         IN  INTEGER default NULL
  );


  /*
  PROCEDURE alter_rules_index (
    rules_index_name IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2
  , parallel         IN  INTEGER default NULL
  );
  */


  PROCEDURE alter_entailment (
    entailment_name  IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , parallel         IN  INTEGER default NULL
  );


  -- Helper functions for SPARQL FILTER --
  FUNCTION rdfTermComp(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (rdfTermComp,WNDS,RNDS,WNPS);

  FUNCTION rdfTermEqual(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (rdfTermEqual,WNDS,RNDS,WNPS);

  FUNCTION dateTimeComp(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (dateTimeComp,WNDS,RNDS,WNPS);

  FUNCTION dateTimeEqual(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (dateTimeEqual,WNDS,RNDS,WNPS);

  FUNCTION getV$EBVVal ( 
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              NUMBER DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$EBVVal,WNDS,RNDS,WNPS);

  FUNCTION getV$NumericVal ( 
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              NUMBER DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$NumericVal,WNDS,RNDS,WNPS);

  FUNCTION getV$StringVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$StringVal,WNDS,RNDS,WNPS);

  FUNCTION langMatches(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (langMatches,WNDS,RNDS,WNPS);

  FUNCTION getV$DateTimeTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DateTimeTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$DateTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DateTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$TimeTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$TimeTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$BooleanVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$BooleanVal,WNDS,RNDS,WNPS);

  FUNCTION getV$DatatypeVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DatatypeVal,WNDS,RNDS,WNPS);

  FUNCTION getV$LangVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN               VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$LangVal,WNDS,RNDS,WNPS,RNPS);

  FUNCTION getV$STRVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$STRVal,WNDS,RNDS,WNPS);
  -- End SPARQL FILTER functions --

  procedure set_session_param(param_name   in varchar2,
                              value        in boolean);
  function delete_triples( model_name  varchar2,
                           subject     varchar2,
                           property    varchar2,
                           object      varchar2) return number;
 END rdf_apis;
/
show errors;

