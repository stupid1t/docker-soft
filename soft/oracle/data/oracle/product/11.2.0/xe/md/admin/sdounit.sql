Rem
Rem $Header: sdounit.sql 15-mar-2004.07:39:00 mhorhamm Exp $
Rem
Rem sdounit.sql
Rem
Rem Copyright (c) 2000, 2004, Oracle Corporation.  All rights reserved.  
Rem
Rem NAME
Rem sdounit.sql - <one-line expansion of the name>
Rem
Rem DESCRIPTION
Rem <short description of component this file declares/defines>
Rem
Rem NOTES
Rem <other useful comments, qualifications, etc.>
Rem
Rem MODIFIED(MM/DD/YY)
Rem qxie     12/15/00 - Add angle unit table 
Rem sravada  08/09/00 -
Rem sravada  08/09/00 - Created
Rem


drop table MDSYS.SDO_DIST_UNITS;

Create Table MDSYS.SDO_DIST_UNITS (SDO_UNIT varchar2(32), 
                             UNIT_NAME varchar2(100),
                              CONVERSION_FACTOR NUMBER,
                 CONSTRAINT unique_dist_units
                       PRIMARY KEY (SDO_UNIT));


insert into MDSYS.SDO_DIST_UNITS values('M', 'Meter',  1.0);
insert into MDSYS.SDO_DIST_UNITS values('METER', 'Meter',  1.0);
insert into MDSYS.SDO_DIST_UNITS values('KM', 'Kilometer',  1000.0); 
insert into MDSYS.SDO_DIST_UNITS values('KILOMETER', 'Kilometer', 1000.0);
insert into MDSYS.SDO_DIST_UNITS values('CM', 'Centimeter', 0.01);
insert into MDSYS.SDO_DIST_UNITS values('CENTIMETER', 'Centimeter', 0.01);
insert into MDSYS.SDO_DIST_UNITS values('MM', 'Millemeter',  0.001);
insert into MDSYS.SDO_DIST_UNITS values('MILLIMETER', 'Millemeter',  0.001);
insert into MDSYS.SDO_DIST_UNITS values('MILE', 'Mile', 1609.344);
insert into MDSYS.SDO_DIST_UNITS values('NAUT_MILE', 'Nautical Mile', 1852.000);
insert into MDSYS.SDO_DIST_UNITS values('SURVEY_FOOT', 'U.S. Foot', 0.3048006096012);
insert into MDSYS.SDO_DIST_UNITS values('FOOT', 'Foot (International)', 0.3048000);
insert into MDSYS.SDO_DIST_UNITS values('INCH', 'Inch',   0.02540000);
insert into MDSYS.SDO_DIST_UNITS values('YARD', 'Yard',  0.9144000);
insert into MDSYS.SDO_DIST_UNITS values('CHAIN', 'Chain',   20.11680);
insert into MDSYS.SDO_DIST_UNITS values('ROD', 'Rod', 5.029200);
insert into MDSYS.SDO_DIST_UNITS values('LINK', 'Link',  0.201166195);
insert into MDSYS.SDO_DIST_UNITS values('MOD_USFT', 'Modified American Foot', 
                                             0.304812253);
insert into MDSYS.SDO_DIST_UNITS values('CL_FT', 'Clarke''s Foot', 0.304797265); 
insert into MDSYS.SDO_DIST_UNITS values('IND_FT', 'Indian Foot', 0.304799518);
insert into MDSYS.SDO_DIST_UNITS values('LINK_BEN', 'Link (Benoit)', 0.201167651);
insert into MDSYS.SDO_DIST_UNITS values('LINK_SRS', 'Link (Sears)', 0.201167651);
insert into MDSYS.SDO_DIST_UNITS values('CHN_BEN', 'Chain (Benoit)', 20.1167825);
insert into MDSYS.SDO_DIST_UNITS values('CHN_SRS', 'Chain (Sears)',  20.1167651);
insert into MDSYS.SDO_DIST_UNITS values('IND_YARD', 'Yard (Indian)', 0.914398554);
insert into MDSYS.SDO_DIST_UNITS values('SRS_YARD', 'Yard (Sears)', 0.914398415);
insert into MDSYS.SDO_DIST_UNITS values('FATHOM', 'Fathom', 1.8288);


drop table MDSYS.SDO_AREA_UNITS;

Create Table MDSYS.SDO_AREA_UNITS (SDO_UNIT varchar2(32), 
              UNIT_NAME varchar2(100), CONVERSION_FACTOR NUMBER,
                 CONSTRAINT unique_area_units
                       PRIMARY KEY (SDO_UNIT));

insert into MDSYS.SDO_AREA_UNITS values('SQ_M', 'Square Meter',   1.0);
insert into MDSYS.SDO_AREA_UNITS values('SQ_METER', 'Square Meter', 1.0);
insert into MDSYS.SDO_AREA_UNITS values('SQ_KM', 'Square Kilometer',   1000000);
insert into MDSYS.SDO_AREA_UNITS values('SQ_CM', 'Square Centimeter',  0.0001);
insert into MDSYS.SDO_AREA_UNITS values('SQ_MM', 'Square Millimeter', 0.000001);
insert into MDSYS.SDO_AREA_UNITS values('SQ_KILOMETER', 'Square Kilometer', 1000000);
insert into MDSYS.SDO_AREA_UNITS values('SQ_CENTIMETER', 'Square Centimeter',  
                                           0.0001);
insert into MDSYS.SDO_AREA_UNITS values('SQ_MILLIMETER', 'Square Millimeter', 
                                           0.000001);
insert into MDSYS.SDO_AREA_UNITS values('SQ_CH', 'Square Chain',  404.6856);
insert into MDSYS.SDO_AREA_UNITS values('SQ_FT', 'Square Foot', 0.09290304);
insert into MDSYS.SDO_AREA_UNITS values('SQ_IN', 'Square Inch',   0.00064516);
insert into MDSYS.SDO_AREA_UNITS values('SQ_LI', 'Square Link',  0.04046856);
insert into MDSYS.SDO_AREA_UNITS values('SQ_CHAIN', 'Square Chain', 404.6856);
insert into MDSYS.SDO_AREA_UNITS values('SQ_FOOT', 'Square Foot', 0.09290304);
insert into MDSYS.SDO_AREA_UNITS values('SQ_INCH', 'Square Inch',  0.00064516);
insert into MDSYS.SDO_AREA_UNITS values('SQ_LINK', 'Square Link',  0.04046856); 
insert into MDSYS.SDO_AREA_UNITS values('SQ_MILE', 'Square Mile', 2589988);
insert into MDSYS.SDO_AREA_UNITS values('SQ_ROD', 'Square Rod', 25.29285);
insert into MDSYS.SDO_AREA_UNITS values('SQ_SURVEY_FOOT', 'Square Survey Feet',  
                                           0.09290341);
insert into MDSYS.SDO_AREA_UNITS values('SQ_YARD', 'Square Yard',  0.8361274);
insert into MDSYS.SDO_AREA_UNITS values('ACRE', 'Acre', 4046.856);
insert into MDSYS.SDO_AREA_UNITS values('HECTARE', 'Hectare', 10000); 
insert into MDSYS.SDO_AREA_UNITS values('PERCH', 'Perch', 25.29285);
insert into MDSYS.SDO_AREA_UNITS values('ROOD', 'Rood', 1011.714);


drop table MDSYS.SDO_ANGLE_UNITS;

CREATE TABLE MDSYS.SDO_ANGLE_UNITS
   (sdo_unit VARCHAR2(32), unit_name VARCHAR2(100), conversion_factor NUMBER,
                 CONSTRAINT unique_angle_units
                       PRIMARY KEY (SDO_UNIT));

INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Degree', 'Decimal Degree', 0.0174532925);
INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Radian', 'Radian', 1.0000000000);
INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Second', 'Decimal Second', 0.0000048481);
INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Minute', 'Decimal Minute', 0.0002908882);
INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Gon', 'Gon', 0.0157079633);
INSERT INTO MDSYS.SDO_ANGLE_UNITS VALUES ('Grad', 'Grad', 0.0157079633);


grant select on MDSYS.SDO_AREA_UNITS to public;
grant select on MDSYS.SDO_DIST_UNITS to public;
grant select on MDSYS.SDO_ANGLE_UNITS to public;
create public synonym SDO_AREA_UNITS for MDSYS.SDO_AREA_UNITS;
create public synonym SDO_DIST_UNITS for MDSYS.SDO_DIST_UNITS;
create public synonym SDO_ANGLE_UNITS for MDSYS.SDO_ANGLE_UNITS;




