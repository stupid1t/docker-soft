Rem
Rem $Header: sdo/admin/sdomigj.sql /main/8 2009/04/06 10:48:38 jcwang Exp $
Rem
Rem sdomigj.sql
Rem
Rem Copyright (c) 2006, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdomigj.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    jcwang      04/03/09 - 11.2 NDM upgrade dif
Rem    hgong       04/01/09 - drop md/lib/sdonm.jar
Rem    sravada     10/20/08 - add explicit drops back
Rem    hgong       09/18/08 - drop md/jlib/sdonm.jar
Rem    zzhang      02/20/08 - 
Rem    sravada     11/01/06 - Created
Rem



--drop NDM related class files
alter session set current_schema=SYS;
call dbms_java.grant_permission('SYSTEM', 'java.io.FilePermission',
                                 '<<ALL FILES>>', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission',
                                'md/jlib/*', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);
call dbms_java.grant_permission('ORDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);

-- explicitly drop some of the NDM classes
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/XMLAdapter');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/XMLAdapter$1');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/XMLAdapter$DatabaseMetadata');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/LinkTypeConstraint');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/lod/Dijkstra$VisitedNode');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/lod/LODAnalysisInfoImpl');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/lod/NetworkExplorer$NodeInfo');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/lod/NetworkExplorer$PointOnLink');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/network/RandomGraph');

 
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/jlib/sdonm.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/lib/sdonm.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/jlib/sdogcdr.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/lib/sdogcdr.jar');
 
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/jlib/sdoutl.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/util/RTree$1');
                                                                               --drop georaster related classes
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/BLOBWriter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/BMPWriter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/GeoRasterAdapter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/GeoRasterGeoTIFFInterface');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/GeoRasterProcessUtil');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JGeoRasterSRS');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JP2Writer');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JPEGWriter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/PNGWriter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/TIFFWriter');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/RationalFunction');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/Polynomial');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JRaster');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JGeoRasterMeta');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JGeoRaster');  
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/GeoRasterException'); 
call dbms_java.dropjava('-force -synonym -schema MDSYS  oracle/spatial/georaster/JGeoRasterMeta$BandInfo');  
 
call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission',
                                 'md/jlib/*','read');
commit;
alter session set current_schema=MDSYS;


