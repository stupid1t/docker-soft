Rem
Rem $Header: sdoaggrh.sql 29-jul-2004.13:53:05 sravada Exp $
Rem
Rem sdoaggrh.sql
Rem
Rem Copyright (c) 2000, 2004, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdoaggrh.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     07/29/04 - add exception handlers 
Rem    sravada     04/23/03 - change to SDO_AGGR_CONCAT_LINES
Rem    sravada     04/11/03 - add AggrConcat
Rem    sravada     04/12/02 - add type synonyms
Rem    sravada     04/11/01 - update the sdoaggrtype
Rem    sravada     12/21/00 - move union/cvxhull to oci
Rem    jcwang      09/29/00 - Add convexhull, centroid aggrgrate functions
Rem    sravada     07/27/00 - change according to UDAG signature change 
Rem                           for Terminate call
Rem    jcwang      07/05/00 - Add LRS aggregate function for concatenation
Rem    sravada     06/28/00 - Add headers for AggrUnion and Generic AggrType
Rem    sravada     06/28/00 - Created
Rem

Rem This is a generic type used for all aggregate functions requiring
Rem Geometry and Diminfo as arguements.

declare
begin
  begin
   execute immediate 
'create type SDOAggrType as Object (
Geometry mdsys.sdo_geometry,
Tolerance  NUMBER) ';
  exception when others then NULL;
  end;
end;
/

grant execute on SDOAggrType to public;
CREATE OR REPLACE PUBLIC SYNONYM SDOAggrType for MDSYS.SDOAggrType;


declare
begin
  begin
   execute immediate
'create type SDOAggr AUTHID current_user as Object
(
  Geometry mdsys.sdo_geometry,
  Diminfo mdsys.sdo_dim_array,
  call_count  number,
  call_method varchar2(128),
  call_type   number,
  call_value  number,	
  static function odciaggregateinitialize(sctx OUT SDOAggr) return number,
  member function odciaggregateiterate(self IN OUT SDOAggr, 
               geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN SDOAggr,
                                         returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self IN OUT SDOAggr, 
                    valueB IN  SDOAggr) return number) ';
  exception when others then NULL;
  end;
end;
/

show errors;

Rem type header for Aggregate UNION

declare
begin
  begin
  execute immediate 
   'drop type aggrunion ';
  exception when others then NULL;
 end;
end;
/

create type AggrUnion AUTHID current_user as Object
(
  context raw(4),
  static function odciaggregateinitialize(sctx IN OUT AggrUnion) return number,
  member function odciaggregateiterate(self IN OUT AggrUnion, 
               geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN AggrUnion,
                                 returnValue OUT mdsys.sdo_geometry,
                                 flags IN number)
                     return number,
  member function odciaggregatemerge(self IN OUT AggrUnion, 
                    sctx2 IN  AggrUnion) return number,
  member function sdoaggregateiterate(self IN OUT AggrUnion, 
               geom IN mdsys.sdo_geometry, dim IN mdsys.SDO_DIM_ARRAY) 
  return number);
/

show errors;

Rem Type header for Aggregate MBR
declare
begin
  begin
  execute immediate 
   'drop type AggrMBR ';
  exception when others then NULL;
 end;
end;
/

create type AggrMBR AUTHID current_user as Object
(
  context mdsys.SDOAggr,
  static function odciaggregateinitialize(sctx OUT AggrMBR) return number,
  member function odciaggregateiterate(self IN OUT AggrMBR, 
               geom IN mdsys.sdo_geometry) return number,
  member function odciaggregateterminate(self IN AggrMBR,
                                        returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self IN OUT AggrMBR, 
                    valueB IN  AggrMBR) return number);
/

show errors;

Rem Type header for Aggregate LRS concatenation

declare
begin
  begin
  execute immediate 
   'drop type AggrLRSConcat ';
  exception when others then NULL;
 end;
end;
/
create type AggrLRSConcat AUTHID current_user as Object
(
  context mdsys.SDOAggr,
  static function odciaggregateinitialize(sctx OUT AggrLRSConcat) return number,
  member function odciaggregateiterate(self IN OUT AggrLRSConcat, 
               			       geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN AggrLRSConcat,
                                        returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self   IN OUT AggrLRSConcat, 
                    		     valueB IN  AggrLRSConcat) return number);
/

show errors;


Rem Type header for Aggregate LRS concatenation

declare
begin
  begin
  execute immediate 
   'drop type AggrLRSConcat3D ';
  exception when others then NULL;
 end;
end;
/
create type AggrLRSConcat3D AUTHID current_user as Object
(
  context mdsys.SDOAggr,
  static function odciaggregateinitialize(sctx OUT AggrLRSConcat3D) return number,
  member function odciaggregateiterate(self IN OUT AggrLRSConcat3D, 
               			       geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN AggrLRSConcat3D,
                                           returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self   IN OUT AggrLRSConcat3D, 
                    		     valueB IN  AggrLRSConcat3D) return number);
/

show errors;

Rem Type header for Aggregate COnvexHull

declare
begin
  begin
  execute immediate 
   'drop type AggrConvexHull ';
  exception when others then NULL;
 end;
end;
/
create type AggrConvexHull AUTHID current_user as Object
(
  context mdsys.SDOAggr,
  static function odciaggregateinitialize(sctx OUT AggrConvexHull) return number,
  member function odciaggregateiterate(self IN OUT AggrConvexHull, 
               			       geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN AggrConvexHull,
                                         returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self   IN OUT AggrConvexHull, 
                    		     valueB IN  AggrConvexHull) return number);
/

show errors;

Rem Type header for Aggregate Centroid

declare
begin
  begin
  execute immediate 
   'drop type AggrCentroid ';
  exception when others then NULL;
 end;
end;
/
create type AggrCentroid AUTHID current_user as Object
(
  context mdsys.SDOAggr,
  static function odciaggregateinitialize(sctx OUT AggrCentroid) return number,
  member function odciaggregateiterate(self IN OUT AggrCentroid, 
               			       geom IN mdsys.SDOAggrType) return number,
  member function odciaggregateterminate(self IN AggrCentroid,
                                         returnValue OUT mdsys.sdo_geometry,
                                          flags IN number)
                     return number,
  member function odciaggregatemerge(self   IN OUT AggrCentroid, 
                    		     valueB IN  AggrCentroid) return number);
/

show errors;

declare
begin
  begin
  execute immediate 
   'drop type AggrConcat ';
  exception when others then NULL;
 end;
end;
/
create type AggrConcat AUTHID current_user as Object
(
  context raw(4),
  static function odciaggregateinitialize(sctx IN OUT AggrConcat) 
              return PLS_INTEGER,
  member function odciaggregateiterate(self IN OUT AggrConcat, 
               geom IN mdsys.SDO_GEOMETRY) return PLS_INTEGER,
  member function odciaggregateterminate(self IN AggrConcat,
                                 returnValue OUT mdsys.sdo_geometry,
                                 flags IN number)
                     return PLS_INTEGER,
  member function odciaggregatemerge(self IN OUT AggrConcat, 
                    sctx2 IN  AggrConcat) return PLS_INTEGER);
/

show errors;







