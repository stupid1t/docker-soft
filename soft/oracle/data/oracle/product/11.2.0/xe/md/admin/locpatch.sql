Rem
Rem $Header: sdo/admin/locpatch.sql /main/4 2009/09/09 13:21:37 sravada Exp $
Rem
Rem locpatch.sql
Rem
Rem Copyright (c) 2005, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      locpatch.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    bkazar      09/05/09 - WFS 1.1 issue
Rem    bkazar      09/04/09 - add 2nd extrude signature, deprecated, in
Rem                           sdoutlh.sql
Rem    yhu         07/14/09 - add parallel_enable in sdogeom.sql
Rem    sravada     05/25/05 - sravada_cleanup_label_difs_2
Rem    sravada     05/25/05 - Created
Rem



/* Perform any Locator patch actions if SDO is not installed.
    If SDO is installed, all of these actions are done as part of
       sdopatch.sql  script. */

COLUMN :script_name NEW_VALUE comp_file NOPRINT
Variable script_name varchar2(50)
Variable sdo_cnt  number;

declare
  sdo_status VARCHAR2(20) := NULL;
begin
  -- Check whether SDO is installed.
  -- If not, patch Locator
  select count(*) into :sdo_cnt from all_objects where owner='MDSYS' and
  object_name = 'SDO_GEOMETRY';
  sdo_status := dbms_registry.status('SDO');
  if (:sdo_cnt = 0) then
    :script_name := '?/rdbms/admin/nothing.sql';
  elsif (sdo_status is NULL or sdo_status = 'OPTION OFF') then
    :script_name := '@locpatchi.sql';
  else
    :script_name := '?/rdbms/admin/nothing.sql';
  end if;
end;
/

select :script_name from dual;
@&comp_file


