Rem
Rem $Header: sdo/admin/sdomapd.sql /main/7 2008/10/24 10:14:04 mhorhamm Exp $
Rem
Rem sdomapdef.sql
Rem
Rem Copyright (c) 2001, 2008, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdomapdef.sql - SDO MAP DEFinitions
Rem
Rem    DESCRIPTION
Rem  Defines the metadata tables and views for SDO_MAPS, SDO_STYLES, and
Rem   SDO_Themes
Rem
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    mhorhamm    09/09/08   - 
Rem    rkothuri    04/03/08   - add 3d viz schema
Rem    sravada     02/01/08   - add MCS metadata
Rem    sravada     05/04/07   - bug 6021780
Rem    sravada     07/30/04   - add exception handlers 
Rem    sravada     02/04/04  -  add default styles under MDSYS
Rem    sravada     11/12/01  - Merged sravada_bug-2096079
Rem    sravada     10/08/01 -  move definitions from LBS vob
Rem     jxyang     09/18/01 - remove instead of triggers
Rem    sravada     08/08/01 - fix synonym definitions
Rem    sravada     07/02/01 - Merged sravada_sdomap_definition
Rem    sravada     06/19/01 - Created
Rem


declare
begin
  begin
   execute immediate 
'Create Table SDO_MAPS_TABLE (
  SDO_OWNER VARCHAR2(32) default sys_context(''userenv'', ''CURRENT_SCHEMA''),
  NAME  VARCHAR2(32) NOT NULL,
  DESCRIPTION VARCHAR2(4000),
  DEFINITION  CLOB NOT NULL,
  CONSTRAINT unique_maps
    PRIMARY KEY (SDO_OWNER, NAME)) ';
   exception when others then NULL;
  end;
end;
/


-- drop view USER_SDO_MAPS;
Create or replace  View USER_SDO_MAPS AS
SELECT NAME, DESCRIPTION,DEFINITION
FROM SDO_MAPS_TABLE
WHERE sdo_owner = sys_context('userenv', 'CURRENT_SCHEMA');

Create or replace  View ALL_SDO_MAPS AS
SELECT SDO_OWNER OWNER, NAME, DESCRIPTION,DEFINITION
FROM SDO_MAPS_TABLE;

Create or replace  View DBA_SDO_MAPS AS
SELECT SDO_OWNER OWNER, NAME, DESCRIPTION,DEFINITION
FROM SDO_MAPS_TABLE;
 
declare
begin
  begin
   execute immediate 
'Create Table SDO_STYLES_TABLE (
  SDO_OWNER VARCHAR2(32) default sys_context(''userenv'', ''CURRENT_SCHEMA''),
  NAME  VARCHAR2(32) NOT NULL,
  TYPE  VARCHAR2(32) NOT NULL,
  DESCRIPTION VARCHAR2(4000),
  DEFINITION  CLOB NOT NULL,
  IMAGE  BLOB,
  GEOMETRY   MDSYS.SDO_GEOMETRY,
    CONSTRAINT unique_styles
     PRIMARY KEY (SDO_OWNER, NAME)) ';
   exception when others then NULL;
  end;
end;
/

Create or replace  View USER_SDO_STYLES AS
SELECT NAME, TYPE, DESCRIPTION,DEFINITION, IMAGE,GEOMETRY
FROM SDO_STYLES_TABLE
WHERE sdo_owner = sys_context('userenv', 'CURRENT_SCHEMA');

Create or replace  View ALL_SDO_STYLES AS
SELECT SDO_OWNER OWNER, NAME, TYPE, DESCRIPTION,DEFINITION, 
IMAGE,GEOMETRY
FROM SDO_STYLES_TABLE;

Create or replace  View DBA_SDO_STYLES AS
SELECT SDO_OWNER OWNER, NAME, TYPE, DESCRIPTION,DEFINITION, 
IMAGE,GEOMETRY
FROM SDO_STYLES_TABLE;


declare
begin
  begin
   execute immediate 
' Create Table SDO_THEMES_TABLE (
  SDO_OWNER VARCHAR2(32) default sys_context(''userenv'', ''CURRENT_SCHEMA''),
  NAME  VARCHAR2(32) NOT NULL,
  DESCRIPTION VARCHAR2(4000),
  BASE_TABLE  VARCHAR2(64) NOT NULL,
  GEOMETRY_COLUMN  VARCHAR2(2048) NOT NULL,
  STYLING_RULES CLOB NOT NULL,
    CONSTRAINT unique_themes
        PRIMARY KEY (SDO_OWNER, NAME)) ';
   execute immediate 
  'CREATE INDEX SDO_THEMES_IDX ON SDO_THEMES_TABLE(SDO_OWNER,BASE_TABLE) ';
   exception when others then NULL;
  end;

  -- this for upgrade cases
  begin
   EXECUTE IMMEDIATE 
   ' alter table SDO_THEMES_TABLE modify (BASE_TABLE  varchar2(64)) ';
     exception when others then NULL;
  end;
end;
/


Create or replace  View USER_SDO_THEMES AS
SELECT NAME, DESCRIPTION, BASE_TABLE, GEOMETRY_COLUMN, STYLING_RULES
FROM SDO_THEMES_TABLE
WHERE sdo_owner = sys_context('userenv', 'CURRENT_SCHEMA');

Create  or replace View ALL_SDO_THEMES AS
SELECT SDO_OWNER OWNER, NAME, DESCRIPTION, BASE_TABLE, 
                  GEOMETRY_COLUMN, STYLING_RULES
FROM SDO_THEMES_TABLE
WHERE
(exists
   (select table_name from all_tables
    where table_name=base_table
      and owner = sdo_owner
    union all
      select table_name from all_object_tables
      where table_name=base_table
      and owner = sdo_owner
    union all
    select view_name table_name from all_views
    where  view_name=base_table
      and owner = sdo_owner));

Create or replace  View DBA_SDO_THEMES AS
SELECT SDO_OWNER OWNER, NAME, DESCRIPTION, BASE_TABLE, 
              GEOMETRY_COLUMN, STYLING_RULES
FROM SDO_THEMES_TABLE
WHERE
(exists
   (select table_name from dba_tables
    where table_name=base_table
    union all
      select table_name from dba_object_tables
      where table_name=base_table
    union all
    select view_name table_name from dba_views
    where  view_name=base_table));

grant select,insert,delete,update on user_sdo_maps to public;
grant select,insert,delete,update on user_sdo_styles to public;
grant select,insert,delete,update on user_sdo_themes to public;

grant select on all_sdo_maps to public;
grant select on all_sdo_styles to public;
grant select on all_sdo_themes to public;

grant select on dba_sdo_maps to public;
grant select on dba_sdo_styles to public;
grant select on dba_sdo_themes to public;

create  or replace public synonym user_sdo_maps for mdsys.user_sdo_maps;
create  or replace public synonym user_sdo_styles for mdsys.user_sdo_styles;
create  or replace public synonym user_sdo_themes for mdsys.user_sdo_themes;

create  or replace public synonym all_sdo_maps for mdsys.all_sdo_maps;
create  or replace public synonym all_sdo_styles for mdsys.all_sdo_styles;
create  or replace public synonym all_sdo_themes for mdsys.all_sdo_themes;

create  or replace public synonym dba_sdo_maps for mdsys.dba_sdo_maps;
create  or replace public synonym dba_sdo_styles for mdsys.dba_sdo_styles;
create  or replace public synonym dba_sdo_themes for mdsys.dba_sdo_themes;



-- delete from SDO_STYLES_TABLE where sdo_owner = 'MDSYS';

declare
begin
   begin

INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.BLACK',                          'COLOR',                                    
'black',                                                                        
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#000000">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.BLACK GRAY',                     'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#bc8f8f;fill:#808080">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.BLUE',                           'COLOR',                                    
'blue',                                                                         
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#0000ff">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.COUNTIES',                       'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ffffcc">                           
<rect width="50" height="50"/></g>                                              
</svg>                                                                          
'                                                                               
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.FACILITY',                       'COLOR',                                    
'MQ Facility color',                                                            
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#cc9999;fill:#cc9999">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.FUNNY COLOR',                    'COLOR',                                    
'qwer',                                                                         
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#aaaaaa;fill:#ffffce">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.OCEAN W/O BOUNDARY',             'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#a6caf0">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.PARK FOREST',                    'COLOR',                                    
'park forest',                                                                  
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#adcda3">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_1',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#2a00ff">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_10',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ffe900">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_11',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ff9900">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_12',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ff5d00">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_13',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ff0000">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_2',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#002aff">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_3',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#006eff">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_4',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#00d8ff">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_5',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#00ffcb">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_6',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#00ff7b">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_7',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#00ff21">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_8',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#6eff00">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RB13_9',                         'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#003333;fill:#ccff00">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RED',                            'COLOR',                                    
'red color',                                                                    
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#ff1100">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RED W/ BLACK BORDER',            'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#ee1100">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.RIVER',                          'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#a6caf0;fill:#a6caf0">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.ROSY BROWN',                     'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#bc8f8f">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.ROSY BROWN STROKE',              'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#bc8f8f">                                        
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SANDY BROWN',                    'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#f4a460">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_01',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#ffe6b4">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_02',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#efbd81">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_03',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#e68250">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_04',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#cb8347">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_05',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#b41e00">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.SEQ6_06',                        'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#000000;fill:#9d4a1a">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.US MAP YELLOW',                  'COLOR',                                    
'Primary color for US maps.',                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#bb99bb;fill:#ffffcc">                           
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.WATER',                          'COLOR',                                    
'color for rendering water',                                                    
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#a6caf0">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.WHEAT',                          'COLOR',                                    
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#f5deb3">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.WHITE',                          'COLOR',                                    
'white',                                                                        
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="stroke:#ffffff">                                        
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.YELLOW',                         'COLOR',                                    
'yellow color',                                                                 
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#ffff00">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'C.YELLOW 2',                       'COLOR',                                    
'yellow color 2',                                                               
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="color" style="fill:#ffffa5">                                          
<rect width="50" height="50"/></g>                                              
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.DPH',                            'LINE',                                     
'Divided primary highways',                                                     
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ffff00;stroke-width:5">                            
<line class="parallel" style="fill:#ff0000;stroke-width:1.0" />                 
<line class="base" style="fill:black;stroke-width:1.0" dash="10.0,4.0" />       
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.EXCELLENT_ROADS',                'LINE',                                     
'Excellent Roads',                                                              
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#00ff00;stroke-width:3">                            
<line class="parallel" style="fill:#000000;stroke-width:1" />                   
</g>                                                                            
</svg>                                                                          
'                                                                               
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.FAIR_ROADS',                     'LINE',                                     
'Fair Roads',                                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ffff33;stroke-width:3">                            
<line class="parallel" style="fill:black;stroke-width:1" />                     
</g>                                                                            
</svg>                                                                          
'                                                                               
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.FERRY',                          'LINE',                                     
'ferry line',                                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="stroke-width:1">                                         
<line class="base" style="fill:#000066;stroke-width:1.0" dash="5.0,3.0" />      
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.GOOD_ROADS',                     'LINE',                                     
'Good Roads',                                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#3366ff;stroke-width:3">                            
<line class="parallel" style="fill:#000000;stroke-width:1.0" />                 
</g>                                                                            
</svg>                                                                          
'                                                                               
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.LIGHT DUTY',                     'LINE',                                     
'Light duty roads',                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#404040;stroke-width:2">                            
<line class="base" /></g>                                                       
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.MAJOR STREET',                   'LINE',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ff4040;stroke-width:2">                            
<line class="base" /></g>                                                       
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.MAJOR TOLL ROAD',                'LINE',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#006600;stroke-width:4;stroke-linecap:SQUARE">      
<line class="parallel" style="fill:#99ffff;stroke-width:1.0" />                 
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.MQ_ROAD2',                       'LINE',                                     
'MapQuest road style',                                                          
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#e6e6e6;stroke-width:5;stroke-linecap:BUTT">        
<line class="parallel" style="fill:#afacac;stroke-width:1.0" />                 
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.PH',                             'LINE',                                     
'Primary highways',                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#33a9ff;stroke-width:4">                            
<line class="parallel" style="fill:#aa55cc;stroke-width:1.0" />                 
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.POOR_ROADS',                     'LINE',                                     
'Poor Roads',                                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ff0000;stroke-width:3">                            
<line class="parallel" style="fill:#000000;stroke-width:1" />                   
</g>                                                                            
</svg>                                                                          
'                                                                               
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.PTH',                            'LINE',                                     
'Primary Toll Highway',                                                         
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#66ff66;stroke-width:4">                            
<line class="parallel" style="fill:#66ff33;stroke-width:1.0" />                 
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.RAILROAD',                       'LINE',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#003333;stroke-width:1">                            
<line class="hashmark" style="fill:#003333"  dash="8.5,3.0" />                  
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.RAMP',                           'LINE',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ffc800;stroke-width:2">                            
<line class="base" style="fill:#998899;stroke-width:2.0" />                     
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.SH',                             'LINE',                                     
'secondary highways, ramps et cl',                                              
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#ffc800;stroke-width:2">                            
<line class="base" style="fill:#998899;stroke-width:2.0" />                     
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.STATE BOUNDARY',                 'LINE',                                     
'state boundary',                                                               
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#bb99bb;stroke-width:5">                            
<line class="base" style="fill:#0000ff;stroke-width:1.0" dash="8.0,4.0" />      
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.STREET',                         'LINE',                                     
'Streets',                                                                      
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#a0a0a0;stroke-width:1">                            
<line class="base" /></g>                                                       
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'L.TRANSPARENT',                    'LINE',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="line" style="fill:#cc00cc;fill-opacity:128;stroke-width:9">           
<line class="base" /></g>                                                       
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.CIRCLE',                         'MARKER',                                   
'circle',                                                                       
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#0000ff;fill:#ff0000">                         
<circle cx="0" cy="0" r="40.0" />                                               
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.GREEN STAR',                     'MARKER',                                   
'a green star',                                                                 
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#000000;fill:#006600;width:15;height:15">      
<polygon points="138.0,123.0, 161.0,198.0, 100.0,152.0, 38.0,198.0, 61.0,123.0, 0.0,76.0, 76.0,76.0, 100.0,0.0, 123.0,76.0, 199.0,76.0" />                      
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.HEXAGON',                        'MARKER',                                   
'Hexagon',                                                                      
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#000000;fill:#ff0000">                         
<polygon points="50.0,199.0, 0.0,100.0, 50.0,1.0, 149.0,1.0, 199.0,100.0, 149.0, 199.0" />                                                                       
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.PENTAGON',                       'MARKER',                                   
'Pentagon',                                                                     
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#0000ff;fill:#ffff00">                         
<polygon points="38.0,199.0, 0.0,77.0, 100.0,1.0, 200.0,77.0, 162.0,199.0" />   
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.REDSQ',                          'MARKER',                                   
'RedSQ',                                                                        
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#000000;fill:#ff0000">                         
<polygon points="0.0,0.0, 0.0,100.0, 100.0,100.0, 100.0,0.0, 0.0,0.0" />        
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.SMALL TRIANGLE',                 'MARKER',                                   
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="fill:#6666ff;width:10;height:10">                     
<polygon points="201.0,200.0, 0.0,200.0, 101.0,0.0" />                          
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.STAR',                           'MARKER',                                   
'star',                                                                         
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#000000;fill:#ff0000;width:15;height:15">      
<polygon points="138.0,123.0, 161.0,198.0, 100.0,152.0, 38.0,198.0, 61.0,123.0, 0.0,76.0, 76.0,76.0, 100.0,0.0, 123.0,76.0, 199.0,76.0" />                      
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.TOWN HALL',                      'MARKER',                                   
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#000000;fill:#000000;width:9;height:9">        
<circle cx="0" cy="0" r="10.0" />                                               
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'M.TRIANGLE',                       'MARKER',                                   
'a triangle',                                                                   
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="marker"  style="stroke:#0000ff;fill:#6666ff">                         
<polygon points="201.0,200.0, 0.0,200.0, 101.0,0.0" />                          
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.AIRPORT NAME',                   'TEXT',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:SansSerif;font-size:10pt; fill:#000000">  
       Hello World!   
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.CITY NAME',                      'TEXT',                                     
'Font for City names',                                                          
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:12pt;font-weight:bold;fill:#000000"> 
 Hello World!       
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.MAP TITLE',                      'TEXT',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Serif;font-size:14pt;font-weight:bold;fill:#aa00ff">  
 Hello World!                           
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.PARK NAME',                      'TEXT',                                     
'sf park name',                                                                 
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:8pt;fill:#000000">                        
 Hello World!      
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.RED STREET',                     'TEXT',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:10pt;fill:#ff0000">                 
 Hello World!                                                                   
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.ROAD NAME',                      'TEXT',                                     
'Font for Road names',                                                          
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Serif;font-size:11pt;font-weight:bold;fill:#000000">   
 Hello World!                                       
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.SHIELD1',                        'TEXT',                                     
'used for labels on top of m.shield1',                                          
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:SansSerif;font-size:9pt;fill:#ffffff">        
 Hello World!                
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.SHIELD2',                        'TEXT',                                     
'used for labels on top of m.shield2',                                          
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Serif;font-size:9pt;fill:#000000">              
 Hello World!                            
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.STATE NAME',                     'TEXT',                                     
'name for states',                                                              
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:14pt;font-weight:bold;fill:#0000ff">           
 Hello World!                
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.STREET NAME',                    'TEXT',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:10pt;fill:#0000ff">                       
 Hello World!                    
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.STREET2',                        'TEXT',                                     
'',                                                                             
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:Dialog;font-size:9pt;fill:#000000">                                                                         
 Hello World!         
</g>                                                                            
</svg>'                                                                         
);                                                                              
                                                                                
INSERT INTO SDO_STYLES_TABLE(SDO_OWNER,NAME,TYPE,DESCRIPTION,DEFINITION) VALUES(
 'MDSYS',                                                                       
'T.TITLE',                          'TEXT',                                     
'Default style for map title',                                                  
'<?xml version="1.0" standalone="yes"?>                                         
<svg width="1in" height="1in">                                                  
<desc></desc>                                                                   
<g class="text" style="font-style:plain;font-family:SansSerif;font-size:18pt;font-weight:bold;fill:#0000ff">                  
 Hello World!                            
</g>                                                                            
</svg>'                                                                         
);                                                                              

  exception when others then NULL;
  end;
end;
/




declare
begin
  begin
   execute immediate
'create table mdsys.sdo_cached_maps_table(
  SDO_OWNER VARCHAR2(32) default sys_context(''userenv'', ''CURRENT_SCHEMA''),
  name varchar2(32),
  description varchar2(4000),
  tiles_table varchar2(32),
  is_online varchar2(3) not null,
  is_internal varchar2(4) not null,
  definition clob not null,
  base_map varchar2(32),
  map_adapter blob,
  CONSTRAINT unique_cached_maps
    PRIMARY KEY (SDO_OWNER, NAME)) ';
   exception when others then NULL;
  end;
end;
/

Create or replace  View mdsys.USER_SDO_CACHED_MAPS AS
SELECT NAME, DESCRIPTION, tiles_table, is_online, is_internal, DEFINITION, base_map, map_adapter
FROM mdsys.SDO_CACHED_MAPS_TABLE
WHERE sdo_owner = sys_context('userenv', 'CURRENT_SCHEMA');

Create or replace  View mdsys.ALL_SDO_CACHED_MAPS AS
SELECT SDO_OWNER OWNER, NAME, DESCRIPTION, tiles_table, is_online, is_internal, DEFINITION, base_map, map_adapter
FROM mdsys.SDO_CACHED_MAPS_TABLE ;


grant select,insert,delete,update on mdsys.user_sdo_cached_maps to public;
grant select on mdsys.all_sdo_cached_maps to public;

create  or replace public synonym user_sdo_cached_maps for mdsys.user_sdo_cached_maps;
create  or replace public synonym all_sdo_cached_maps for mdsys.all_sdo_cached_maps;


@@sdovis3dschema.sql
