Rem
Rem $Header: sdomsttp.sql 07-may-2008.09:14:34 sravada  Exp $
Rem
Rem sdomsttp.sql
Rem
Rem Copyright (c) 2008, Oracle.  All rights reserved.  
Rem
Rem    NAME
Rem      sdomsttp.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     05/07/08 - Created
Rem


declare
begin
   begin
     execute immediate ' drop type ST_GEOMETRY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_POINT force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_POINT_ARRAY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_CURVE force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_CURVE_ARRAY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_SURFACE force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_SURFACE_ARRAY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_CURVEPOLYGON force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_LINESTRING force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_LINESTRING_ARRAY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_POLYGON force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_POLYGON_ARRAY force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_GEOMCOLLECTION force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_MULTIPOINT force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_MULTICURVE force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_MULTISURFACE force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_MULTILINESTRING force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_MULTIPOLYGON force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_CIRCULARSTRING force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_COMPOUNDCURVE force ';
           exception when others then NULL;
   end;
   begin
     execute immediate ' drop type ST_GEOMETRY_ARRAY force ';
           exception when others then NULL;
   end;
end;
/


