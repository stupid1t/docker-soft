Rem
Rem $Header: sdo/admin/mddinloc.sql /main/4 2009/09/03 13:08:27 sravada Exp $
Rem
Rem mddinloc.sql
Rem
Rem Copyright (c) 2005, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      mddinloc.sql - MDDInstallLOCator
Rem
Rem    DESCRIPTION
Rem      Deinstalls Locator if Spatial is not installed
Rem
Rem    NOTES
Rem      Should be connected as SYSDBA
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     08/20/09 - bug 8740141
Rem    sravada     05/25/05 - Created
Rem

COLUMN :script_name NEW_VALUE comp_file NOPRINT
Variable script_name varchar2(50)

declare
  sdo_status VARCHAR2(20) := NULL;
begin
 -- Check whether SDO is installed.
 -- If not, it is safe to deinstall Locator
  sdo_status := dbms_registry.status('SDO');
 if  (sdo_status is NULL or sdo_status = 'OPTION OFF') then
   :script_name := '@mddinsl.sql';
 else
   :script_name := '?/rdbms/admin/nothing.sql';
 end if;
end;
/

select :script_name from dual;
@&comp_file



