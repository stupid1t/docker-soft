Rem
Rem $Header: sdo/admin/sdopatch.sql /st_sdo_11.2.0/3 2010/07/15 09:56:50 sravada Exp $
Rem $Header: sdo/admin/sdopatch.sql /st_sdo_11.2.0/3 2010/07/15 09:56:50 sravada Exp $
Rem
Rem sdopatch.sql
Rem
Rem Copyright (c) 2002, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdopatch.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     07/15/10 - bug b9289641
Rem    cfreiwal    11/05/09 - Add sdortprt.sql for bug 9077960
Rem    matperry    06/15/09 - add patching for RDF/OWL
Rem    sravada     05/16/05 - 
Rem    rkothuri    10/01/04  - 
Rem    sravada     05/20/04 -  initialise for 10.2


ALTER SESSION SET CURRENT_SCHEMA = MDSYS;


EXECUTE dbms_registry.loading('SDO','Spatial',NULL,'MDSYS');               

@@locpatchi.sql

ALTER SESSION SET CURRENT_SCHEMA = MDSYS;
@@sdotpmph.sql
@@prvtsam.plb
@@sdowfsmd.sql
@@sdowfsph.sql
@@sdowfspr.plb
@@sdocswph.sql
@@sdocswpr.plb
@@prvtpc.plb
@@prvttnpc.plb
@@prvtgri.plb
@@sdortprt.sql
@@sdotopob.plb


alter type SDO_GEOMETRY compile body;
alter type ST_GEOMETRY compile body;
alter type ST_POLYGON compile body;
alter type ST_GEOMCOLLECTION compile body;
alter type ST_SURFACE compile body;
alter type ST_MULTIPOINT compile body;
alter type ST_MULTILINESTRING compile body;
alter type ST_MULTIPOLYGON compile body;
alter type ST_COMPOUNDCURVE compile body;
alter type ST_CURVEPOLYGON compile body;
alter type ST_CURVE compile body;
alter type ST_MULTICURVE compile body;
alter type ST_MULTISURFACE compile body;

alter package MD_LRS compile body;
alter package SDO_3GL compile body;
alter package SDO_RTREE_ADMIN compile body;
alter package SDO_GEOR compile body;
alter package SDO_WFS_LOCK_GEN compile body;
alter package SDO_LRS compile body;
alter package MD_NET compile body;
alter function SDO_AGGR_SET_UNION compile;
alter function SDO_PQRY compile;

alter type SDOAGGR compile body;
alter type AGGRCONVEXHULL compile body;
alter type ST_ANNOTATION_TEXT compile body;
alter type SDO_TOPO_GEOMETRY compile body;

alter package SDO_NET compile body;
alter package SDO_TOPO_METADATA_INT compile body;
alter package SDO_CS compile body;
alter package SDO_TUNE compile body;
alter package PRVT_TIN compile body;


-- Start RDF/OWL
ALTER SESSION SET CURRENT_SCHEMA = MDSYS;

COLUMN :script_name NEW_VALUE comp_file NOPRINT
VARIABLE script_name VARCHAR2(50)

declare
    iCidFlag integer;
begin
  -- sanity checking existence of 11g TRIPLE type
  select count(*) into iCidFlag from all_type_attrs
   where type_name='SDO_RDF_TRIPLE_S' 
     and attr_name='RDF_C_ID';

  if (iCidFlag = 0) then 
    -- 11g RDF/OWL has not been initialized. 
    -- it does not make sense to patch
    :script_name := dbms_registry.nothing_script;
  else
    :script_name := '@sdosempatch.sql';
  end if;
end;
/

SELECT :script_name FROM DUAL;
set echo off
@&comp_file
-- Done with RDF/OWL


SET ECHO ON

execute dbms_registry.loaded('SDO');                   
execute dbms_registry.valid('SDO');                   


ALTER SESSION SET CURRENT_SCHEMA = SYS;


