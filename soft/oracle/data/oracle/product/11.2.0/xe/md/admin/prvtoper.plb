create  or replace operator
  sdo_nn binding 
     (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,
     (mdsys.st_geometry, mdsys.sdo_geometry, varchar2)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,
     (mdsys.st_geometry, mdsys.st_geometry, varchar2)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,
     (mdsys.sdo_geometry, mdsys.st_geometry, varchar2)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,
     (mdsys.sdo_geometry, mdsys.sdo_geometry)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,       
     (mdsys.st_geometry, mdsys.sdo_geometry)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn,       
     (mdsys.st_geometry, mdsys.st_geometry)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn, 
     (mdsys.sdo_geometry, mdsys.st_geometry)
         return varchar2
         with index context,
         SCAN CONTEXT  sdo_index_method_10i
         compute ancillary data
         using prvt_idx.nn;       
grant execute on sdo_nn to public with grant option;
create or replace operator sdo_nn_distance
  binding (number)
return number
  ancillary to 
  sdo_nn (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
  sdo_nn (mdsys.st_geometry, mdsys.sdo_geometry, varchar2),
  sdo_nn (mdsys.st_geometry, mdsys.st_geometry, varchar2),
  sdo_nn (mdsys.sdo_geometry, mdsys.st_geometry, varchar2),
  sdo_nn (mdsys.sdo_geometry, mdsys.sdo_geometry),
  sdo_nn (mdsys.st_geometry, mdsys.sdo_geometry),
  sdo_nn (mdsys.st_geometry, mdsys.st_geometry),
  sdo_nn (mdsys.sdo_geometry, mdsys.st_geometry)
  using  prvt_idx.nndistance;
grant execute on sdo_nn_distance to public with grant option;
create  or replace operator 
  sdo_relate binding 
 (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate,
 (mdsys.st_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate,
 (mdsys.st_geometry, mdsys.st_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate,
 (mdsys.sdo_geometry, mdsys.st_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate;
grant execute on sdo_relate to public with grant option;
create  or replace operator  
  sdo_anyinteract binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.anyinteract,
 (mdsys.st_geometry, mdsys.sdo_geometry)
return varchar2
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.anyinteract,
 (mdsys.st_geometry, mdsys.st_geometry)
return varchar2
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.anyinteract,
 (mdsys.sdo_geometry, mdsys.st_geometry)
return varchar2
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.anyinteract;
grant execute on sdo_anyinteract to public with grant option;
create  or replace operator 
  sdo_contains binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.contains,
 (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.contains,
  (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.contains,
  (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.contains;
grant execute on sdo_contains to public with grant option;
create  or replace operator 
  sdo_inside binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.inside,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.inside,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.inside,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.inside;
grant execute on sdo_inside to public with grant option;
create  or replace operator 
  sdo_touch binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.touch,
  (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.touch,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.touch,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.touch;
grant execute on sdo_touch to public with grant option;
create  or replace operator 
  sdo_equal binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.equal,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.equal,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.equal,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.equal;
grant execute on sdo_equal to public with grant option;
create  or replace operator 
  sdo_covers binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.covers,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.covers,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.covers,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.covers;
grant execute on sdo_covers to public with grant option;
create  or replace operator 
  sdo_coveredby binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.coveredby,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.coveredby,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.coveredby,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.coveredby;
grant execute on sdo_coveredby to public with grant option;
create  or replace operator 
  sdo_overlaps binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlap,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlap,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlap,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlap;
grant execute on sdo_overlaps to public with grant option;
create  or replace operator 
  sdo_overlapbdydisjoint binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdydisjoint,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdydisjoint,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdydisjoint,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdydisjoint;
grant execute on sdo_overlapbdydisjoint to public with grant option;
create  or replace operator 
  sdo_overlapbdyintersect binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdyintersect,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdyintersect,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdyintersect,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.overlapbdyintersect;
grant execute on sdo_overlapbdyintersect to public with grant option;
create  or replace operator 
  sdo_on binding (mdsys.sdo_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.sdoon,
   (mdsys.st_geometry, mdsys.sdo_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.sdoon,
   (mdsys.st_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.sdoon,
   (mdsys.sdo_geometry, mdsys.st_geometry) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.sdoon;
grant execute on sdo_on to public with grant option;
create  or replace operator 
  sdo_filter 
  binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter,
  (mdsys.sdo_geometry, mdsys.sdo_geometry) 
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter,
  (mdsys.st_geometry, mdsys.sdo_geometry) 
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter,
  (mdsys.st_geometry, mdsys.st_geometry) 
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter,
  (mdsys.sdo_geometry, mdsys.st_geometry) 
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter;
grant execute on sdo_filter to public with grant option;
create  or replace operator 
  sdo_rtree_filter binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter;
grant execute on sdo_rtree_filter to public with grant option;
create  or replace operator 
  sdo_rtree_relate binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.relate;
grant execute on sdo_rtree_relate to public with grant option;
create  or replace operator 
sdo_within_distance binding
 (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.within_distance,
 (mdsys.st_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.within_distance,
 (mdsys.st_geometry, mdsys.st_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.within_distance,
 (mdsys.sdo_geometry, mdsys.st_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.within_distance;
grant execute on sdo_within_distance to public with grant option;
create  or replace operator 
  sdo_int_relate binding (mdsys.sdo_geometry, mdsys.sdo_geometry, ROWID,
                          varchar2, varchar2, varchar2,number,number, number) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate;
grant execute on sdo_int_relate to public with grant option;
create or replace operator 
  sdo_int_filter binding (mdsys.sdo_geometry, mdsys.sdo_geometry, ROWID,
                          varchar2, varchar2, varchar2,number,number, number) 
return varchar2 
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter;
grant execute on sdo_int_filter to public with grant option;
create  or replace operator 
  sdo_int2_relate binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.relate;
grant execute on sdo_int2_relate to public with grant option;
create  or replace operator 
  sdo_int2_filter binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) 
return varchar2 
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_3gl.filter;
grant execute on sdo_int2_filter to public with grant option;
create or replace operator
 locator_within_distance 
  binding(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2)
return varchar2
 with index context,
 SCAN CONTEXT sdo_index_method_10i
using sdo_3gl.within_distance;
grant execute on locator_within_distance to public with grant option;
create or replace function sdo_dummy_function wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
b7 c2
PCj2vebSAxCRUYQ3ZMEMykUkO/Iwg8eZgcfLCNL+Xhbc+tCuPtfXO1+u1dcM0fpHcl5QuL+B
/irDpdJepTK9SnQ8mZ8I0Fu9vkA26fy4Po8dygFgV/XFNlc5rZKsFGp21m4AdtZfIb6scYTm
1i/hnkGvcdziuUE/Z7oOCfzRa7y89/w8ceI/0Tx0pjeV0ZM=

/
grant execute on sdo_dummy_function to public with grant option;
declare
begin
  begin
   execute immediate 
'create  operator
  sdo_dummy binding (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2)
return varchar2 using sdo_dummy_function ';
  exception when others then NULL;
 end;
end;
/
grant execute on sdo_dummy to public with grant option;
declare
begin
  begin
   execute immediate
' create indextype spatial_index for
sdo_dummy (mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2)
using mdsys.sdo_index_method_10i with local range partition with rebuild online';
  exception when others then NULL;
 end;
end;
/
alter indextype spatial_index
using mdsys.sdo_index_method_10i
with local range partition
with order by sdo_nn_distance(number);
declare
begin
  begin
   execute immediate  
  'DISASSOCIATE STATISTICS FROM INDEXTYPES spatial_index FORCE' ;
   execute immediate 
   'DISASSOCIATE STATISTICS FROM PACKAGES sdo_3gl FORCE ';
  execute immediate 
   'DISASSOCIATE STATISTICS FROM PACKAGES prvt_idx FORCE ';
  exception when others then NULL;
 end;
end;
/
declare
begin
  begin
    execute immediate 
    ' alter type sdo_statistics compile specification reuse settings';
    exception when others then NULL;
  end;
  begin
    execute immediate 
       'DROP TYPE sdo_statistics';
    exception when others then NULL;
  end;
end;
/
CREATE OR REPLACE TYPE sdo_statistics wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
939 23f
OnP+zN0II0Sp3orOKMCzFA+xFn8wg0PDDK5qfHSLNA/uC0npdlPAlwnENg2hwRr9AC7n0dpS
tPmduMI7EhGRKBqoygks+auqYZ17M0zWSzC1dWKnZNk64K2quqn1cIByTeTK/aZNp1m6HQ/v
FK+pTiX2ROn2ibE8qdeN0bP2hveeJlABRWmNnxx+Hq8yuSKKVH85MIV2NOcAgk+PsnVeYeW0
eIYXrsa7g44r2FN+tnpxtSpFOzX7FLDz4zpLqKbXGprZux8XiQsCHLa96a7G//QO6hYOAXIO
LkqWJJY1Zoz2IVrp9QQhULbNmNioRxJRcIrUlcU5ccesGsDVmf2tRbRBrIT3B/QUgRQpqHVh
Mez2kwsF5pBb4ZwFk/RhYK6QqX8yxqWNmlibmydapw1drm7wKz9gVMprvcF9VCnh6Bu/7hlT
a84dIzXvoTy5yH0r4UCNL1LWFh7Pb18iJnCKTQTfirI0phTbNDUScxVFyxLu+ALUJo4KWfDF
IuETM7VRjy25N1Jtd80wLCRxsBTyHOjZ+fm1PYZH7lKBaH09gxOr1u6GVUk9KMkN

/
show errors;
CREATE OR REPLACE TYPE BODY sdo_statistics wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
ef7 380
DMp+Hmr5ENgVZR8z5/GFS9NMEOUwg0MrDNATfC+5MjqUx+9aFvy5LbMc44jT6tvJeben6+Un
lhL8qDq15SyvNzbj2WBcPZJnda9BxpwgydjsnjjXTjnLJD+8HEqaSiAHhgX+hKIBNspc6BY3
UcmizP+pwUZIaczoaY6INb4ORD4mw932RML3DAPWZCTvzGCQEf0jvcMNcN38VatEVNn2aGmL
vGEbIgAoVFTSXG1U2VKr/fC93CKVocL8Qn9P5xcJYgEu9eMlz2CnD1udGGAJoKe9lgyGzLWS
U9LaAUSukGopldULrpIDPH2hiJFnkQ+KiG07b6zLqAUS0JJV5PvqfRLOjEH98CQK2SVrrcvo
hMermHYMw1cfe/YrJ9mdjSNaiyIIFql8NdKpRNc0upfA5KtZyP6CkRzpQMhFXBq5fxWc/tZC
BtlxFrtgdWcE4bZBl1RROChVXj1WxTe+0NvnkDSBIhO3bUkCWX7/RrPmSiWfcsOY054zWMoF
WXqI9+oXs4jHAI6fvn/Kx/lFNbyEbOyOh6KCTb9bClmR2jEtIh2Yy4fW5xob6pwODNRDyGAo
jIEL6y0tBePTKHMkHeR0bi6PRSEs6i/y6z5To7LuI2/9rwcLRsuHyYS7b3COtkvXRMnf0Zzk
mJdWAseF3tBxs9aHYTECToqWRLkcjX7vuWg8fhjLkNkXwnsyDKOp6Tgm1jGbbgHaRhfjiG22
abp6TEQ7KWwm/DyVhsYZozLEzCK+mQz0tiuAWQ0HgRD3b/4EeZ/Ik0Yb6BMmMWGzX6XuAiOV
DPBRKW4cSAvEBCExYbmHEMHGAaSf+3vXqd+GjmhwS5SIVUUdzvTuORVwvhkycyCq+E+7pNaV
Bcy/Ht/1aFjBBw8HJaot

/
show errors;
ASSOCIATE STATISTICS WITH INDEXTYPES spatial_index
USING sdo_statistics;
ASSOCIATE STATISTICS WITH PACKAGES sdo_3gl
USING sdo_statistics;
ASSOCIATE STATISTICS WITH PACKAGES prvt_idx
USING sdo_statistics;
GRANT execute on sdo_statistics to public with grant option;
alter indextype spatial_index 
add
sdo_nn(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_nn(mdsys.st_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_nn(mdsys.st_geometry, mdsys.st_geometry, varchar2),
add
sdo_nn(mdsys.sdo_geometry, mdsys.st_geometry, varchar2),
add
sdo_nn(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_nn(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_nn(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_nn(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_nn(mdsys.sdo_geometry, mdsys.sdo_geometry, number),
add
sdo_nn(mdsys.st_geometry, mdsys.sdo_geometry, number),
add
sdo_nn(mdsys.st_geometry, mdsys.st_geometry, number),
add
sdo_nn(mdsys.sdo_geometry, mdsys.st_geometry, number),
add
sdo_relate(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) rewrite JOIN,
add
sdo_relate(mdsys.st_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_relate(mdsys.st_geometry, mdsys.st_geometry, varchar2),
add
sdo_relate(mdsys.sdo_geometry, mdsys.st_geometry, varchar2),
add
sdo_anyinteract(mdsys.sdo_geometry, mdsys.sdo_geometry) rewrite JOIN,
add
sdo_anyinteract(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_anyinteract(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_anyinteract(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_contains(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_contains(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_contains(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_contains(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_inside(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_inside(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_inside(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_inside(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_touch(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_touch(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_touch(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_touch(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_equal(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_equal(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_equal(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_equal(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_covers(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_covers(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_covers(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_covers(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_coveredby(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_coveredby(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_coveredby(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_coveredby(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_overlaps(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_overlaps(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_overlaps(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_overlaps(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_on(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_on(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_on(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_on(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_overlapbdydisjoint(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_overlapbdydisjoint(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_overlapbdydisjoint(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_overlapbdydisjoint(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_overlapbdyintersect(mdsys.sdo_geometry, mdsys.sdo_geometry),
add
sdo_overlapbdyintersect(mdsys.st_geometry, mdsys.sdo_geometry),
add
sdo_overlapbdyintersect(mdsys.st_geometry, mdsys.st_geometry),
add
sdo_overlapbdyintersect(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_filter(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) rewrite JOIN,
add sdo_filter(mdsys.sdo_geometry, mdsys.sdo_geometry) rewrite JOIN,
add sdo_filter(mdsys.st_geometry, mdsys.sdo_geometry),
add sdo_filter(mdsys.st_geometry, mdsys.st_geometry),
add sdo_filter(mdsys.sdo_geometry, mdsys.st_geometry),
add
sdo_rtree_relate(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_rtree_filter(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_int_relate(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2, varchar2, 
               varchar2, varchar2,number,number, number),
add
sdo_int_filter(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2, varchar2, 
               varchar2, varchar2,number,number, number),
add
sdo_within_distance(mdsys.sdo_geometry, mdsys.sdo_geometry, 
                    varchar2) rewrite JOIN,
add
sdo_within_distance(mdsys.st_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_within_distance(mdsys.st_geometry, mdsys.st_geometry, varchar2),
add
sdo_within_distance(mdsys.sdo_geometry, mdsys.st_geometry, varchar2),
add
locator_within_distance(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_int2_relate(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2),
add
sdo_int2_filter(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2) ;
alter indextype spatial_index
drop sdo_dummy(mdsys.sdo_geometry, mdsys.sdo_geometry, varchar2);
drop operator sdo_dummy;
drop function sdo_dummy_function;
grant execute on spatial_index to public with grant option;
create or replace  public  synonym sdo_nn for mdsys.sdo_nn;
create or replace  public  synonym sdo_nn_distance for mdsys.sdo_nn_distance;
create or replace  public  synonym sdo_filter for mdsys.sdo_filter;
create or replace  public  synonym sdo_relate for mdsys.sdo_relate;
create or replace  public  synonym sdo_rtree_filter for mdsys.sdo_rtree_filter;
create or replace  public  synonym sdo_rtree_relate for mdsys.sdo_rtree_relate;
create or replace  public  synonym sdo_within_distance for 
                   mdsys.sdo_within_distance;
create or replace  public  synonym locator_within_distance for
                                           mdsys.locator_within_distance;
create or replace  public  synonym sdo_anyinteract for mdsys.sdo_anyinteract;
create or replace  public  synonym sdo_contains for mdsys.sdo_contains;
create or replace  public  synonym sdo_inside for mdsys.sdo_inside;
create or replace  public  synonym sdo_touch for mdsys.sdo_touch;
create or replace  public  synonym sdo_equal for mdsys.sdo_equal;
create or replace  public  synonym sdo_covers for mdsys.sdo_covers;
create or replace  public  synonym sdo_on for mdsys.sdo_on;
create or replace  public  synonym sdo_coveredby for mdsys.sdo_coveredby;
create or replace  public  synonym sdo_overlapbdydisjoint for 
                                  mdsys.sdo_overlapbdydisjoint;
create or replace  public  synonym sdo_overlapbdyintersect for 
                                     mdsys.sdo_overlapbdyintersect;
create or replace  public  synonym sdo_overlaps for mdsys.sdo_overlaps;
create or replace  public  synonym spatial_index for mdsys.spatial_index;
grant execute on spatial_index to public with grant option;
commit;
declare
begin
    begin
     execute immediate ' drop trigger sdo_st_syn_create ';
     exception when others then null;
    end;
end;
/
create or replace public synonym ST_GEOMETRY for MDSYS.ST_GEOMETRY;
create or replace public synonym ST_POINT for MDSYS.ST_POINT;
create or replace public synonym ST_CURVE for MDSYS.ST_CURVE;
create or replace public synonym ST_SURFACE for MDSYS.ST_SURFACE;
create or replace public synonym ST_CURVEPOLYGON for MDSYS.ST_CURVEPOLYGON;
create or replace public synonym ST_LINESTRING for MDSYS.ST_LINESTRING;
create or replace public synonym ST_POLYGON for MDSYS.ST_POLYGON;
create or replace public synonym ST_GEOMCOLLECTION for MDSYS.ST_GEOMCOLLECTION;
create or replace public synonym ST_MULTIPOINT for MDSYS.ST_MULTIPOINT;
create or replace public synonym ST_MULTICURVE for MDSYS.ST_MULTICURVE;
create or replace public synonym ST_MULTIFURFACE for MDSYS.ST_MULTISURFACE;
create or replace public synonym ST_MULTILINESTRING for MDSYS.ST_MULTILINESTRING;
create or replace public synonym ST_MULTIPOLYGON for MDSYS.ST_MULTIPOLYGON;
create or replace public synonym ST_CIRCULARSTRING for MDSYS.ST_CIRCULARSTRING;
create or replace public synonym ST_COMPOUNDCURVE for MDSYS.ST_COMPOUNDCURVE;
create or replace public synonym ST_GEOMETRY_ARRAY for MDSYS.ST_GEOMETRY_ARRAY;
create or replace public synonym ST_POINT_ARRAY for MDSYS.ST_POINT_ARRAY;
create or replace public synonym ST_CURVE_ARRAY for MDSYS.ST_CURVE_ARRAY;
create or replace public synonym ST_SURFACE_ARRAY for MDSYS.ST_SURFACE_ARRAY;
create or replace public synonym ST_LINESTRING_ARRAY for MDSYS.ST_LINESTRING_ARRAY;
create or replace public synonym ST_POLYGON_ARRAY for MDSYS.ST_POLYGON_ARRAY;
create or replace public synonym ST_INTERSECTS for mdsys.sdo_anyinteract;
create or replace public synonym ST_RELATE for mdsys.sdo_relate;
create or replace public synonym ST_TOUCH for mdsys.sdo_touch;
create or replace public synonym ST_CONTAINS for mdsys.sdo_contains;
create or replace public synonym ST_COVERS for mdsys.sdo_covers;
create or replace public synonym ST_COVEREDBY for mdsys.sdo_coveredby;
create or replace public synonym ST_EQUAL for mdsys.sdo_equal;
create or replace public synonym ST_INSIDE for mdsys.sdo_inside;
create or replace public synonym ST_OVERLAPBDYDISJOINT for mdsys.sdo_OVERLAPBDYDISJOINT;
create or replace public synonym ST_OVERLAPBDYINTERSECT for mdsys.sdo_OVERLAPBDYINTERSECT;
create or replace public synonym ST_OVERLAPS for mdsys.sdo_OVERLAPS;
CREATE OR REPLACE TRIGGER sdo_st_syn_create
  BEFORE CREATE ON DATABASE
 declare
 error boolean;
  st_syn_detected EXCEPTION;
   PRAGMA EXCEPTION_INIT(st_syn_detected, -995);
 BEGIN
   if((sys.dbms_standard.dictionary_obj_type!='SYNONYM')or(sys.dbms_standard.dictionary_obj_owner!='PUBLIC'))
   then
     return;
   end if;
   error :=
      CASE sys.dbms_standard.dictionary_obj_name
         WHEN 'ST_GEOMETRY' THEN TRUE
         WHEN 'ST_SURFACE' THEN TRUE
         WHEN 'ST_POLYGON' THEN TRUE
         WHEN 'ST_POINT' THEN TRUE
         WHEN 'ST_MULTISURFACE' THEN TRUE
         WHEN 'ST_MULTIPOINT' THEN TRUE
         WHEN 'ST_MULTILINESTRING' THEN TRUE
         WHEN 'ST_MULTICURVE' THEN TRUE
         WHEN 'ST_LINESTRING' THEN TRUE
         WHEN 'ST_GEOMCOLLECTION' THEN TRUE
         WHEN 'ST_CURVE' THEN TRUE
         WHEN 'ST_CURVEPOLYGON' THEN TRUE
         WHEN 'ST_COMPOUNDCURVE' THEN TRUE
         WHEN 'ST_CIRCULARSTRING' THEN TRUE
         WHEN 'ST_INTERSECTS' THEN TRUE
         WHEN 'ST_RELATE' THEN TRUE
         WHEN 'ST_TOUCH' THEN TRUE
         WHEN 'ST_CONTAINS' THEN TRUE
         WHEN 'ST_COVERS' THEN TRUE
         WHEN 'ST_COVEREDBY' THEN TRUE
         WHEN 'ST_INSIDE' THEN TRUE
         WHEN 'ST_OVERLAP' THEN TRUE
         WHEN 'ST_OVERLAPS' THEN TRUE
         WHEN 'ST_EQUAL' THEN TRUE
         WHEN 'ST_OVERLAPBDYDISJOINT' THEN TRUE
         WHEN 'ST_OVERLAPBDYINTERSECT' THEN TRUE
         WHEN 'ST_GEOMETRY_ARRAY' THEN TRUE
         WHEN 'ST_POINT_ARRAY' THEN TRUE
         WHEN 'ST_CURVE_ARRAY' THEN TRUE
         WHEN 'ST_SURFACE_ARRAY' THEN TRUE
         WHEN 'ST_LINESTRING_ARRAY' THEN TRUE
         WHEN 'ST_POLYGON_ARRAY' THEN TRUE
         ELSE FALSE
      END;

   if(error) then
     raise st_syn_detected;

   end if;
 END;
/
