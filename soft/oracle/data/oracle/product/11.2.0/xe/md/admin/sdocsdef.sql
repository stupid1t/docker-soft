Rem
Rem $Header: sdocsdef.sql 13-apr-2005.07:33:50 mhorhamm Exp $
Rem
Rem sdocsdef.sql
Rem
Rem Copyright (c) 2000, 2005, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdocsdef.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    mhorhamm    04/13/05 - Airy 1930 is a misnomer - should be Airy 1830 
Rem    sravada     07/29/04 - add exception handlers 
Rem    mhorhamm    03/15/04 - Make uses of MDSYS table names unambiguous 
Rem    mhorhamm    01/20/04 - Change name of MDSYS.SDO_DATUMS_OLD to MDSYS.SDO_DATUMS_OLD_SNAPSHOT
Rem    mhorhamm    11/10/03 - Rename MDSYS.SDO_ellipsoids to MDSYS.SDO_ellipsoids_old and 
Rem                           MDSYS.SDO_datums to MDSYS.SDO_datums_old 
Rem    mhorhamm    11/10/03 - Rename MDSYS.SDO_PROJECTIONS to MDSYS.SDO_PROJECTIONS_OLD 
Rem    sravada     04/26/02 - change ordinance to ordnance.
Rem    sravada     12/03/01 - bug 2128775
Rem    qxie        12/15/00 - Created (scripted by Chuck Murray)
Rem


-- First, clean up by dropping existing tables.
declare
begin
  begin
  execute immediate 
  ' DROP TABLE MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT ';
  exception when others then NULL;
 end;
end;
/

-- The following table deletion has been moved to sdoepsgv, because a view refers to it.
--DROP TABLE MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT;
declare
begin
  begin
  execute immediate 
   'DROP TABLE MDSYS.SDO_DATUMS_OLD_SNAPSHOT ';
  exception when others then NULL;
 end;
end;
/


--  Ellipsoids
CREATE TABLE MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT
   (name VARCHAR2(64), semi_major_axis NUMBER, inverse_flattening NUMBER);

INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1866',	6378206.4, 294.9786982);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('WGS 72', 6378135.0, 298.26);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Australian', 6378160.0, 298.25);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Krassovsky', 6378245.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('International 1924', 6378388.0, 297.0);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Hayford', 6378388.0, 297.0);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880', 6378249.145, 293.465);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('GRS 80', 6378137.0, 298.257222101);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1866 (Michigan)', 6378450.047484481, 294.9786982);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Airy 1830', 6377563.396, 299.3249646);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Bessel 1841', 6377397.155, 299.1528128);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Everest', 6377276.345, 300.8017);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Sphere (6370997m)', 6370997.0, 0.0);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Airy 1830(Ireland 1965)', 6377340.189, 299.3249646);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Bessel 1841 (Schwarzeck)', 6377483.865, 299.1528128);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880 (Arc 1950)', 6378249.145326, 	293.4663076);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880 (Merchich)', 6378249.2, 293.46598);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Everest (Kertau)', 6377304.063, 	300.8017);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Fischer 1960 (Mercury)', 6378166.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Fischer 1960 (South Asia)', 6378155.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Fischer 1968', 6378150.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('GRS 67', 6378160.0, 298.247167427);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Helmert 1906', 6378200.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Hough', 6378270.0, 297.0);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('South American 1969', 6378160.0, 298.25);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('War Office', 6378300.583, 296.0);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('WGS 60', 6378165.0, 298.3);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('WGS 66', 6378145.0, 298.25);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('WGS 84', 6378137.0, 298.257223563);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880 (IGN)', 6378249.2, 293.4660213);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('IAG 75', 6378140.0, 298.257222);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('MERIT 83', 6378137.0, 298.257);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('New International 1967', 6378157.5, 298.25);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Walbeck', 6376896.0, 302.78);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Bessel 1841 (NGO 1948)', 6377492.0176, 	299.15281);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1858', 6378293.639, 294.26068);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880 (Jamaica)', 6378249.136, 293.46631);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Clarke 1880 (Palestine)', 6378300.79, 293.46623);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Everest (Timbalai)', 6377298.556, 300.8017);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Everest (Kalianpur)', 6377301.243, 300.80174);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Indonesian', 6378160.0, 298.247);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('NWL 9D', 6378145.0, 298.25);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('NWL 10D', 6378135.0, 	298.26);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('OSU86F', 6378136.2, 298.25722);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('OSU91A', 6378136.3, 298.25722);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Plessis 1817', 6376523.0, 308.64);
INSERT INTO MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT VALUES ('Struve 1860', 	6378297.0, 294.73);


--  Projections
-- The table creation has been moved to sdoepsgv, because a view refers to it.
--CREATE TABLE MDSYS.SDO_PROJECTIONS_OLD
--   (name VARCHAR2(64));

INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Geographic (Lat/Long)');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Albers Conical Equal Area');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Lambert Conformal Conic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Mercator');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Polar Stereographic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Polyconic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Equidistant Conic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Transverse Mercator');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Stereographic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Lambert Azimuthal Equal Area');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Azimuthal Equidistant');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Gnomonic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Orthographic');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('General Vertical Near-Side Perspective');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Sinusoidal');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Equirectangular');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Miller Cylindrical');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Van der Grinten');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Hotine Oblique Mercator');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Robinson');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Space Oblique Mercator');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Alaska Conformal');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Interrupted Goode Homolosine');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Mollweide');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Interrupted Mollweide');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Hammer');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Wagner IV');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Wagner VII');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Oblated Equal Area');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Transverse Mercator Danish System 45 Bornholm');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Transverse Mercator Danish System 34 Jylland-Fyn');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Transverse Mercator Sjaelland');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Transverse Mercator Finnish KKJ');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Eckert IV');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Eckert VI');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Gall');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Lambert Conformal Conic (Belgium 1972)');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('New Zealand Map Grid');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Cylindrical Equal Area');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Swiss Oblique Mercator');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Bonne');
INSERT INTO MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT VALUES ('Cassini');


--  Datums
CREATE TABLE MDSYS.SDO_DATUMS_OLD_SNAPSHOT
   (name VARCHAR2(64),
    shift_x NUMBER,
    shift_y NUMBER,
    shift_z NUMBER,
    rotate_x NUMBER,
    rotate_y NUMBER,
    rotate_z NUMBER,
    scale_adjust NUMBER);

INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Adindan', -162,  -12,  206,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Afgooye', -43, -163,   45,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Ain el Abd 1970', -150, -251,   -2,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Anna 1 Astro 1965', -491,  -22,  435,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Arc 1950', -143,  -90, -294,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Arc 1960', -160,   -8, -300,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Ascension Island 1958', -207,  107,   52,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Astro Beacon E', 145,   75, -272,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Astro B4 Sorol Atoll', 114, -116, -333,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Astro DOS 71/4', -320,  550, -494,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Astronomic Station 1952' , 124, -234,  -25,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Australian Geodetic 1966', -133,  -48,  148,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Australian Geodetic 1984', -134,  -48,  149,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Bellevue (IGN)', -127, -769,  472,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Bermuda 1957',  -73,  213,  296,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Bogota Observatory', 307,  304, -318,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Campo Inchauspe', -148,  136,   90,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Canton Astro 1966', 298, -304, -375,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Cape', -136, -108, -292,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Cape Canaveral', -2,  150,  181,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Carthage', -263,    6,  431,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Chatham 1971', 175,  -38,  113,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Chua Astro', -134,  229,  -29,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Corrego Alegre', -206,  172,   -6,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Djakarta (Batavia)', -377,  681,  -50,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('DOS 1968', 230, -199, -752,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Easter Island 1967', 211,  147,  111,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('European 1950', -87,  -98, -121,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('European 1979', -86,  -98, -119,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Gandajika Base', -133, -321,   50,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Geodetic Datum 1949', 84,  -22,  209,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('GRS 67', 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('GRS 80', 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Guam 1963', -100, -248,  259,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('GUX 1 Astro', 252, -209, -751,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Hito XVIII 1963', 16,  196,   93,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Hjorsey 1955', -73,   46,  -86,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Hong Kong 1963', -156, -271, -189,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Hu-Tzu-Shan', -634, -549, -201,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Indian (Thailand/Vietnam)', 214,  836,  303,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Indian (Bangladesh, etc.)', 289,  734,  257,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Ireland 1965', 506, -122,  611,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('ISTS 073 Astro 1969', 208, -435, -229,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Johnston Island 1961', 191,  -77, -204,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Kandawala', -97,  787,   86,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Kerguelen Island', 145, -187,  103,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Kertau 1948', -11,  851,    5,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('L.C. 5 Astro', 42,  124,  147,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Liberia 1964', -90,   40,   88,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Luzon (Philippines)', -133,  -77,  -51,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Luzon (Mindanao Island)', -133,  -79,  -72,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Mahe 1971', 41, -220, -134,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Marco Astro', -289, -124,   60,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Massawa', 639,  405,   60,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Merchich', 31,  146,   47,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Midway Astro 1961', 912,  -58, 1227,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Minna', -92,  -93,  122,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Nahrwan (Masirah Island)', -247, -148,  369,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Nahrwan (Un. Arab Emirates)', -249, -156,  381,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Nahrwan (Saudi Arabia)', -231, -196,  482,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Naparima, BWI', -2,  374,  172,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Continental US)', -8,  160,  176,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Alaska)', -5,  135,  172,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Bahamas)', -4,  154,  178,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (San Salvador)', 1,  140,  165,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Canada)', -10,  158,  187,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Canal Zone)', 0,  125,  201,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Caribbean)', -7,  152,  178,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Central America)', 0,  125,  194,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Cuba)', -9,  152,  178,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Greenland)', 11,  114,  195,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Mexico)', -12,  130,  190,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 27 (Michigan)', -8,  160,  176,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NAD 83', 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Observatorio 1966', -425, -169,   81,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Old Egyptian', -130,  110,  -13,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Old Hawaiian', 61, -285, -181,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Oman', -346,   -1,  224,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Ordnance Survey Great Brit', 375, -111,  431,  0, 0, 0, 0); 
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Pico de las Nieves', -307,  -92,  127,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Pitcairn Astro 1967', 185,  165,   42,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Provisional South American', -288,  175, -376,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Puerto Rico', 11,   72, -101,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Qatar National', -128, -283,   22,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Qornoq', 164,  138, -189,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Reunion', 94, -948,-1262,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Rome 1940', -225,  -65,    9,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Santo (DOS)', 170,   42,   84,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Sao Braz', -203,  141,   53,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Sapper Hill 1943', -355,   16,   74,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Schwarzeck', 616,   97, -251,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('South American 1969', -57,    1,  -41,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('South Asia', 7,  -10,  -26,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Southeast Base', -499, -249,  314,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Southwest Base', -104,  167,  -38,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Timbalai 1948', -689,  691,  -46,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Tokyo', -128,  481,  664,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Tristan Astro 1968', -632,  438, -609,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Viti Levu 1916', 51,  391,  -36,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Wake-Eniwetok 1960', 101,   52,  -39,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('WGS 60', 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('WGS 66', 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('WGS 72', 0,    8,   10,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('WGS 84' , 0,    0,    0,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Yacare', -155,  171,   37,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Zanderij', -265,  120, -358,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NTF (Greenwich meridian)', -168,  -60,  320,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('European 1987', -83,  -96, -113,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Netherlands Bessel', 593,   26,  478,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Belgium Hayford', 81,  120,  129,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NWGL 10', -1,   15,    1,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('RT 90 (Sweden)', 498,  -36,  568,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Lisboa (DLx)', -303,  -62,  105,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Melrica 1973 (D73)', -223,  110,   37,  0, 0, 0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('DHDN (Potsdam/Rauenberg)', 582, 105, 414, -1.04, -0.35,  3.08,  8.3);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('Pulkovo 1942', 24, -123, -94, -0.02, 0.25, 0.13, 1.1); 
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('NTF (Paris meridian)', -168,  -60,  320, 0,  0,  0, 0);
INSERT INTO MDSYS.SDO_DATUMS_OLD_SNAPSHOT VALUES ('CH 1903 (Switzerland)', 660.077, 13.551, 369.344, 0.804816, 0.577692, 0.952236, 5.66);


grant select on MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT to public;
grant select on MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT to public;
grant select on MDSYS.SDO_DATUMS_OLD_SNAPSHOT to public;
create or replace  public synonym SDO_ELLIPSOIDS_OLD_SNAPSHOT 
 for MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT;
create or replace  public synonym SDO_PROJECTIONS_OLD_SNAPSHOT 
 for MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT;
create  or replace public synonym SDO_DATUMS_OLD_SNAPSHOT 
 for MDSYS.SDO_DATUMS_OLD_SNAPSHOT;

