<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:epsg="http://www.oracle.com/2004/spatial/epsg/gridfile/schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:output method="text"/>
	<xsl:param name="coordinatePosition">1</xsl:param>
	<xsl:template match="epsg:GridFile">
		<xsl:text><![CDATA[NUM_OREC]]></xsl:text>
		<xsl:text><![CDATA[ 11]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[NUM_SREC]]></xsl:text>
		<xsl:text><![CDATA[ 11]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[NUM_FILE]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="count(epsg:Grids/epsg:Grid)"/>
			<xsl:with-param name="length" select="3"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[GS_TYPE ]]></xsl:text>
		<xsl:text><![CDATA[SECONDS ]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[VERSION ]]></xsl:text>
		<xsl:text><![CDATA[NTv2.0  ]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[DATUM_F ]]></xsl:text>
		<xsl:text><![CDATA[NAD27   ]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[DATUM_T ]]></xsl:text>
		<xsl:text><![CDATA[NAD83   ]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[MAJOR_F ]]></xsl:text>
		<xsl:text><![CDATA[ 6378206.400]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[MINOR_F ]]></xsl:text>
		<xsl:text><![CDATA[ 6356583.800]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[MAJOR_T ]]></xsl:text>
		<xsl:text><![CDATA[ 6378137.000]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[MINOR_T ]]></xsl:text>
		<xsl:text><![CDATA[ 6356752.314]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:apply-templates select="epsg:Grids/epsg:Grid"/>
		<xsl:text><![CDATA[END 0.33E+33]]></xsl:text>
		<xsl:text><![CDATA[
]]></xsl:text>
	</xsl:template>
	<xsl:template match="epsg:Grid">
		<xsl:variable name="minLon" select="epsg:GridProperties/epsg:MinCoords/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="minLat" select="epsg:GridProperties/epsg:MinCoords/epsg:Coordinate[position()=2]"/>
		<xsl:variable name="maxLon" select="epsg:GridProperties/epsg:MaxCoords/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="maxLat" select="epsg:GridProperties/epsg:MaxCoords/epsg:Coordinate[position()=2]"/>
		<xsl:variable name="spacingLon" select="epsg:GridProperties/epsg:CoordSpacing/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="spacingLat" select="epsg:GridProperties/epsg:CoordSpacing/epsg:Coordinate[position()=2]"/>
		<xsl:variable name="parentName" select="epsg:HierarchyParticipation/epsg:Parent[epsg:HierarchyType='http://www.oracle.com/2004/spatial/epsg/gridfile/hierarchical-organization/NTv2']/epsg:ParentNode"/>
		<xsl:variable name="gridName" select="@gridName"/>
		<xsl:variable name="creationDateTime" select="@creation"/>
		<xsl:variable name="updateDateTime" select="@update"/>
		<xsl:text><![CDATA[SUB_NAME]]></xsl:text>
		<xsl:call-template name="left-align">
			<xsl:with-param name="string" select="$gridName"/>
			<xsl:with-param name="length" select="8"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[PARENT  ]]></xsl:text>
		<xsl:choose>
			<xsl:when test="normalize-space($parentName) != ''">
				<xsl:call-template name="left-align">
					<xsl:with-param name="string" select="$parentName"/>
					<xsl:with-param name="length" select="8"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text><![CDATA[NONE    ]]></xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[CREATED ]]></xsl:text>
		<xsl:value-of select="substring($creationDateTime, 3, 8)"/>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[UPDATED ]]></xsl:text>
		<xsl:value-of select="substring($updateDateTime, 3, 8)"/>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[S_LAT   ]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(3600 * $minLat, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[N_LAT   ]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(3600 * $maxLat, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[E_LONG  ]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(-3600 * $maxLon, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[W_LONG  ]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(-3600 * $minLon, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[LAT_INC ]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(3600 * $spacingLat, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[LONG_INC]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number(3600 * $spacingLon, '#.000000')"/>
			<xsl:with-param name="length" select="15"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:text><![CDATA[GS_COUNT]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="count(epsg:GridData//epsg:GridNode)"/>
			<xsl:with-param name="length" select="6"/>
		</xsl:call-template>
		<xsl:text><![CDATA[
]]></xsl:text>
		<xsl:apply-templates select="epsg:GridData/epsg:GridRow"/>
	</xsl:template>
	<xsl:template match="epsg:GridRow">
		<xsl:for-each select="epsg:GridNode">
			<xsl:apply-templates select="epsg:Offset/epsg:Coordinate[position() = 2]"/>
			<xsl:apply-templates select="epsg:Offset/epsg:Coordinate[position() = 1]"/>
			<xsl:apply-templates select="epsg:OffsetPrecision/epsg:Coordinate[position() = 2]"/>
			<xsl:apply-templates select="epsg:OffsetPrecision/epsg:Coordinate[position() = 1]"/>
			<xsl:text><![CDATA[
]]></xsl:text>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="epsg:Coordinate">
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number((3600 * .), '#0.000000')"/>
			<xsl:with-param name="length" select="10"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="right-align">
		<xsl:param name="string" select="'Not Available'" />
		<xsl:param name="length" select="12" />
		<xsl:variable name="valueLength" select="string-length($string)"/>
		<xsl:variable name="paddingLength" select="($length - $valueLength)"/>
		<xsl:value-of select="substring('                                                                ', 1, $paddingLength)"/>
		<xsl:value-of select="$string"/>
	</xsl:template>
	<xsl:template name="left-align">
		<xsl:param name="string" select="'Not Available'" />
		<xsl:param name="length" select="12" />
		<xsl:variable name="valueLength" select="string-length($string)"/>
		<xsl:variable name="paddingLength" select="($length - $valueLength)"/>
		<xsl:value-of select="$string"/>
		<xsl:value-of select="substring('                                                                ', 1, $paddingLength)"/>
	</xsl:template>
</xsl:stylesheet>
