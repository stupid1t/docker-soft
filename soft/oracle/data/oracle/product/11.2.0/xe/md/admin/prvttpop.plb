alter operator 
  sdo_anyinteract add binding 
  (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry)
return varchar2     
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_tpfns.anyinteract;
alter operator 
  sdo_anyinteract add binding 
 (mdsys.sdo_topo_geometry, mdsys.sdo_geometry)
return varchar2
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_tpfns.anyinteract;
alter operator 
  sdo_anyinteract add binding 
 (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array)
return varchar2
  with index context,
  SCAN CONTEXT sdo_index_method_10i
using sdo_tpfns.anyinteract;
alter indextype spatial_index 
add 
sdo_anyinteract(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry);
alter indextype spatial_index 
add 
sdo_anyinteract(mdsys.sdo_topo_geometry, mdsys.sdo_geometry);
alter indextype spatial_index 
add 
sdo_anyinteract(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array);
create or replace public synonym sdo_topo_anyinteract 
  for mdsys.sdo_anyinteract;
drop public  synonym sdo_anyinteract;
create or replace public synonym sdo_anyinteract for mdsys.sdo_anyinteract;
alter operator   sdo_filter add   binding 
  (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry)
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_tpfns.filter;
alter operator   sdo_filter add   binding 
  (mdsys.sdo_topo_geometry, mdsys.sdo_geometry)
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_tpfns.filter;
alter operator   sdo_filter add   binding 
  (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array)
   return varchar2
  with index context,
  SCAN CONTEXT  sdo_index_method_10i
  using sdo_tpfns.filter;
alter indextype spatial_index 
add 
sdo_filter(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry);
alter indextype spatial_index 
add 
sdo_filter(mdsys.sdo_topo_geometry, mdsys.sdo_geometry);
alter indextype spatial_index 
add 
sdo_filter(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array);
alter OPERATOR sdo_contains add   BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.contains;
alter OPERATOR sdo_contains add   BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.contains;
alter OPERATOR sdo_contains add   BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.contains;
alter OPERATOR sdo_covers add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.covers;
alter OPERATOR sdo_covers add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.covers;
alter OPERATOR sdo_covers add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.covers;
alter OPERATOR sdo_coveredby add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.coveredby;
alter OPERATOR sdo_coveredby add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.coveredby;
alter OPERATOR sdo_coveredby add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.coveredby;
alter OPERATOR sdo_equal add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.equal;
alter OPERATOR sdo_equal add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.equal;
alter OPERATOR sdo_equal add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.equal;
alter OPERATOR sdo_inside add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.inside;
alter OPERATOR sdo_inside add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.inside;
alter OPERATOR sdo_inside add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.inside;
alter OPERATOR sdo_on  add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.sdo_on;
alter OPERATOR sdo_on  add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.sdo_on;
alter OPERATOR sdo_on  add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.sdo_on;
alter OPERATOR sdo_overlapbdydisjoint add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdydisjoint;
alter OPERATOR sdo_overlapbdydisjoint add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdydisjoint;
alter OPERATOR sdo_overlapbdydisjoint add  BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdydisjoint;
alter OPERATOR sdo_overlapbdyintersect add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdyintersect;
alter OPERATOR sdo_overlapbdyintersect add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdyintersect;
alter OPERATOR sdo_overlapbdyintersect add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlapbdyintersect;
alter OPERATOR sdo_overlaps add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlap;
alter OPERATOR sdo_overlaps add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlap;
alter OPERATOR sdo_overlaps add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.overlap;
alter OPERATOR sdo_touch add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.touch;
alter OPERATOR sdo_touch add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_geometry) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.touch;
alter OPERATOR sdo_touch add BINDING 
    (mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) RETURN VARCHAR2
      WITH INDEX CONTEXT,
      SCAN CONTEXT sdo_index_method_10i
      USING sdo_tpfns.touch;
ALTER INDEXTYPE spatial_index
  ADD sdo_contains(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_contains(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_contains(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_covers(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_covers(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_covers(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_coveredby(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_coveredby(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_coveredby(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_equal(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_equal(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_equal(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_inside(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_inside(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_inside(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_on(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_on(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_on(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_overlapbdydisjoint(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_overlapbdydisjoint(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_overlapbdydisjoint(mdsys.sdo_topo_geometry, 
                                              mdsys.sdo_topo_object_array),
  ADD sdo_overlapbdyintersect(mdsys.sdo_topo_geometry, 
                                     mdsys.sdo_topo_geometry),
  ADD sdo_overlapbdyintersect(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_overlapbdyintersect(mdsys.sdo_topo_geometry, 
                                             mdsys.sdo_topo_object_array),
  ADD sdo_overlaps(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_overlaps(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_overlaps(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array),
  ADD sdo_touch(mdsys.sdo_topo_geometry, mdsys.sdo_topo_geometry),
  ADD sdo_touch(mdsys.sdo_topo_geometry, mdsys.sdo_geometry),
  ADD sdo_touch(mdsys.sdo_topo_geometry, mdsys.sdo_topo_object_array) ; 
