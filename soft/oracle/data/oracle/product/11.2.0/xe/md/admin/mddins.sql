Rem
Rem $Header: sdo/admin/mddins.sql /main/6 2009/03/11 13:28:43 sravada Exp $
Rem
Rem imdinst.sql
Rem
Rem Copyright (c) 1998, 2009, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME
Rem      mddins.sql - Spatial deinstall procedure
Rem
Rem    DESCRIPTION
Rem      deinstall Sptatial completely if ORDIM is not present and remove MDSYS
REM      If ORDIM is present leaves Locator installed
Rem
Rem    NOTES
Rem       You must connect as SYSDBA prior to running this script
Rem
Rem       

REM MUST connect as a SYSDBA user

COLUMN :script_name NEW_VALUE comp_file NOPRINT
Variable script_name varchar2(50)

alter session set current_schema=MDSYS;
EXECUTE dbms_registry.removing('SDO');

REM Deinstall Spatial Components
@@deinssdo_only.sql

REM If ORDIM is present then we need to leave Locator installed 
REM Otherwise, remove all of SDO 
begin
 if (dbms_registry.is_valid('ORDIM') is NULL ) then
   :script_name := '@deinssdo.sql';
 else
   :script_name := '@catmdloc.sql';
 end if;
end;
/
   

select :script_name from dual;
@&comp_file

EXECUTE dbms_registry.removed('SDO');
alter session set current_schema="SYS";

