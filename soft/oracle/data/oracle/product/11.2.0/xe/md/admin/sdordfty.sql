Rem
Rem $Header: sdo/admin/sdordfty.sql /main/28 2009/11/09 10:41:08 vkolovsk Exp $
Rem
Rem sdordfty.sql
Rem
Rem Copyright (c) 2004, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdordfty.sql - sdo_rdf_type
Rem
Rem    DESCRIPTION
Rem      The sdo_rdf_triple is a persistent datatype for RDF triple storage in NDM
Rem
Rem    NOTES
Rem      OID is requied if the user-defined type is persistent and instances of this type 
Rem      will be shared across databases. Use SYS_OP_GUID to generate valid, unique OIDs.
Rem      
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    vkolovsk    11/03/09 - moved sdosemsam type creation and removal to
Rem                           sdordfty and semremov
Rem    matperry    05/05/09 - move creation of RDF_Models, etc. to here
Rem    matperry    05/05/09 - add type creation for types associated with
Rem                           sem_related operator
Rem    vkolovsk    04/07/09 - add tablespace name for rdf_parameter
Rem    ayalaman    01/05/09 - parameters table for network options
Rem    ayalaman    12/21/08 - ols label generator types
Rem    sdas        02/08/07 - type change: replace RDF_T_ID with RDF_C_ID
Rem    sdas        02/05/07 - bulk-load perf: use default SDO_RDF_TRIPLE_S
Rem                           constructor
Rem    sdas        10/23/06 - make SDO_RDF_TRIPLE_S invokers priv
Rem    sdas        06/15/06 - avoid conflict in SDO_RDF_TRIPLE_S constructor 
Rem    alwu        06/14/06 - add one more constructor 
Rem    nalexand    10/13/05 - remove compile after alter type 
Rem    nalexand    10/10/05 - add alter type syntax. 
Rem    nalexand    10/07/05 - add lock_model to start_batch$ 
Rem    nalexand    10/06/05 - add bnode batch constructors 
Rem    nalexand    09/26/05 - add constructor for long_literals 
Rem    nalexand    09/16/05 - add batch mode constructor 
Rem    sravada     04/28/05 - type methods execute as the definer 
Rem    nalexand    01/24/05 - add reif const (mn, s_rdf_t_id, prop, 
Rem                           o_rdf_t_id) 
Rem    nalexand    01/12/05 - clob bn constructor; add get_sub, get_prop, get_obj 
Rem    nalexand    12/15/04 - increase varchar2(2000) 
Rem    sravada     11/10/04 - change to create or replace 
Rem    nalexand    09/27/04 - add reif constructor
Rem    nalexand    09/20/04 - add reif constructor
Rem    nalexand    09/09/04 - add rdf_value$ table and rdf_p_id to type 
Rem    nalexand    07/20/04 - changing object to CLOB
Rem    nalexand    06/24/04 - blank node constructor 
Rem    nalexand    06/21/04 - changing name to sdo_rdf_triple_s (for storage)
Rem    nalexand    04/16/04 - 
Rem    nalexand    04/07/04 - testing first constructor 
Rem    nalexand    04/06/04 - Make subject and property namespaces required
Rem    nalexand    04/01/04 - Created
Rem

Rem SELECT TO_CHAR(SYSTIMESTAMP, 'YYYY-MM-DD:HH24:MI:SS') FROM DUAL;
Rem SELECT SYS_OP_GUID() FROM DUAL;


declare
begin
   begin
    execute immediate 
    ' CREATE OR REPLACE TYPE sdo_rdf_triple
      AS OBJECT 
	(subject VARCHAR2(4000),
	 property VARCHAR2(4000),
	 object VARCHAR2(10000)) ';
    exception when others then NULL;
    end;
end;
/

SHOW ERRORS;

GRANT EXECUTE ON mdsys.sdo_rdf_triple TO PUBLIC WITH GRANT OPTION;
CREATE OR REPLACE PUBLIC SYNONYM sdo_rdf_triple FOR mdsys.sdo_rdf_triple;


declare
begin
   begin
    execute immediate 
' CREATE OR REPLACE TYPE sdo_rdf_triple_s
  TIMESTAMP ''2004-04-01:14:30:35''
  OID ''D70B0A2BA8AE3606E030578CD3051938''
  authid current_user
  AS OBJECT
	(rdf_c_id NUMBER,
	rdf_m_id NUMBER,
	rdf_s_id NUMBER,
	rdf_p_id NUMBER,
	rdf_o_id NUMBER,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					subject VARCHAR2,
					property VARCHAR2,
					object VARCHAR2)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					subject VARCHAR2,
					property VARCHAR2,
					object CLOB)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					sub_or_bn VARCHAR2,
					property VARCHAR2,
					obj_or_bn VARCHAR2,
					bn_m_id NUMBER)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					sub_or_bn VARCHAR2,
					property VARCHAR2,
					object CLOB,
					bn_m_id NUMBER)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					rdf_t_id NUMBER)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					rdf_t_id NUMBER,
					property VARCHAR2,
					object VARCHAR2)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					subject VARCHAR2,
					property VARCHAR2,
					rdf_t_id NUMBER)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					s_rdf_t_id NUMBER,
					property VARCHAR2,
					o_rdf_t_id NUMBER)
	RETURN SELF AS RESULT DETERMINISTIC,
	CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_name VARCHAR2,
					reif_subject VARCHAR2,
					reif_property VARCHAR2,
					subject VARCHAR2,
					property VARCHAR2,
					object VARCHAR2)
	RETURN SELF AS RESULT DETERMINISTIC,
	MEMBER FUNCTION get_triple RETURN SDO_RDF_TRIPLE DETERMINISTIC,
	MEMBER FUNCTION get_object RETURN CLOB DETERMINISTIC,
	MEMBER FUNCTION get_subject RETURN VARCHAR2 DETERMINISTIC, 
	MEMBER FUNCTION get_property RETURN VARCHAR2 DETERMINISTIC
	) ';
      exception when others then NULL;
    end;
end;
/


Rem
Rem First, remove in an best effort the functions to be added
Rem to make it repeatable
Rem

begin
  execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP MEMBER FUNCTION get_obj_value RETURN VARCHAR2 
    DETERMINISTIC CASCADE';
exception when others then null;
end;
/

begin
  execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP MEMBER FUNCTION is_object_clob RETURN VARCHAR2 
    DETERMINISTIC CASCADE';
exception when others then null;
end;
/



begin
  execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
        subject VARCHAR2,
        property VARCHAR2,
        object VARCHAR2,
        in_mode VARCHAR2)
    RETURN SELF AS RESULT DETERMINISTIC CASCADE';
exception when others then null;
end;
/

begin
  execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
        subject VARCHAR2,
        property VARCHAR2,
        object CLOB,
        in_mode VARCHAR2)
    RETURN SELF AS RESULT DETERMINISTIC CASCADE';
exception when others then null;
end;
/


begin
execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
        sub_or_bn VARCHAR2,
        property VARCHAR2,
        obj_or_bn VARCHAR2,
        in_mode VARCHAR2,
        bn_m_id NUMBER)
    RETURN SELF AS RESULT DETERMINISTIC CASCADE';
exception when others then null;
end;
/

begin
execute immediate 
  ' ALTER TYPE sdo_rdf_triple_s
    DROP CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
        sub_or_bn VARCHAR2,
        property VARCHAR2,
        object CLOB,
        in_mode VARCHAR2,
        bn_m_id NUMBER)
    RETURN SELF AS RESULT DETERMINISTIC CASCADE';
exception when others then null;
end;
/


Rem
Rem Now we add those missing functions into the object
Rem

declare
begin
   begin
    execute immediate
    ' ALTER TYPE sdo_rdf_triple_s
      ADD MEMBER FUNCTION get_obj_value RETURN VARCHAR2 
      DETERMINISTIC CASCADE';

    execute immediate
    ' ALTER TYPE sdo_rdf_triple_s
      ADD MEMBER FUNCTION is_object_clob RETURN VARCHAR2 
      DETERMINISTIC CASCADE';

    execute immediate 
    ' ALTER TYPE sdo_rdf_triple_s
 	    ADD CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
					subject VARCHAR2,
					property VARCHAR2,
					object VARCHAR2,
					in_mode VARCHAR2)
	    RETURN SELF AS RESULT DETERMINISTIC CASCADE';

    execute immediate 
    ' ALTER TYPE sdo_rdf_triple_s
	    ADD CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
					subject VARCHAR2,
					property VARCHAR2,
					object CLOB,
					in_mode VARCHAR2)
	    RETURN SELF AS RESULT DETERMINISTIC CASCADE';

    execute immediate 
    ' ALTER TYPE sdo_rdf_triple_s
	    ADD CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
					sub_or_bn VARCHAR2,
					property VARCHAR2,
					obj_or_bn VARCHAR2,
					in_mode VARCHAR2,
					bn_m_id NUMBER)
	    RETURN SELF AS RESULT DETERMINISTIC CASCADE';

    execute immediate 
    ' ALTER TYPE sdo_rdf_triple_s
	    ADD CONSTRUCTOR FUNCTION sdo_rdf_triple_s(model_id NUMBER,
					sub_or_bn VARCHAR2,
					property VARCHAR2,
					object CLOB,
					in_mode VARCHAR2,
					bn_m_id NUMBER)
	    RETURN SELF AS RESULT DETERMINISTIC CASCADE';

    exception when others then
	   raise;
    end;
end;
/   


GRANT EXECUTE ON mdsys.sdo_rdf_triple_s TO PUBLIC WITH GRANT OPTION;
 
CREATE OR REPLACE PUBLIC SYNONYM sdo_rdf_triple_s FOR mdsys.sdo_rdf_triple_s;

begin
  execute immediate
    'create or replace type RDF_Models as table of varchar2(25)';
   exception when others then NULL;
end;
/

begin
  execute immediate
    'create or replace type RDF_RuleBases as table of varchar2(25)';
  exception when others then NULL;
end;
/

begin
  execute immediate
    'create or replace type RDF_Alias as object (
      namespace_id  varchar2(30),
      namespace_val varchar2(4000))';
  exception when others then NULL;
end;  
/

begin
  execute immediate
    'create or replace type RDF_Aliases as table of RDF_Alias';
  exception when others then NULL;
end;  
/

begin
  execute immediate 'create or replace TYPE sem_sameas_rec as object (s number, e number)';
exception 
  when others then null;
end;
/

begin
  execute immediate 'create or replace TYPE sem_sameas_list is TABLE OF sem_sameas_REC';
exception 
  when others then null;
end;
/  

-- varcharArray, longVarcharArray and int_array for internal use
begin
  execute immediate
    'create or replace type RDF_varcharArray as varray(32767) of varchar2(30)';
  exception when others then NULL;
end;  
/

begin
  execute immediate
    'create or replace type RDF_longVarcharArray as varray(32767)
       of varchar2(4000)';
  exception when others then NULL;
end;  
/

begin
  execute immediate 
    'create or replace type mdsys.int_array as varray(1000) of int';
exception when others then null; 
end;
/

--
--- OLS support: types for extensible label generator 
--
/*****************************************************************************/
/*** Type used for Extensible Label Generator for Inferred data            ***/
/*****************************************************************************/
begin
execute immediate '
create or replace type mdsys.rdfsa_resource as object (
  res_id      VARCHAR2(100),
  res_type    NUMBER,
  res_labels  MDSYS.INT_ARRAY,
  constructor function rdfsa_resource (res_id VARCHAR2, res_type number)
                                            return self as result,
  constructor function rdfsa_resource (res_id VARCHAR2,
         res_type number, res_label number) return self as result,
  constructor function rdfsa_resource (res_id VARCHAR2, res_type number,
                res_labels mdsys.int_array) return self as result,
  member function getResource return VARCHAR2,
  member function getLabelCount return number,
  member function getLabel(idx number default 1) return number
)'; 
exception when others then null; 
end;
/

show errors; 

-- Dummy type implementation: actual type body will be installed when OLS 
-- is enabled for RDF using sdordfsa.sql 
create or replace type body mdsys.rdfsa_resource as 
  constructor function rdfsa_resource (res_id VARCHAR2, res_type number)
                                            return self as result is 
  begin
    null;
  end; 

  constructor function rdfsa_resource (res_id VARCHAR2, res_type number,
            res_label number) return self as result is 
  begin
    null;
  end;

  constructor function rdfsa_resource (res_id VARCHAR2, res_type number,
            res_labels mdsys.int_array) return self as result is
  begin
    null;
  end; 

  member function getResource return VARCHAR2 is 
  begin
    null;
  end; 

  member function getLabelCount return number is
  begin
    null; 
  end; 

  member function getLabel(idx number default 1) return number is
  begin
    null;
  end;
end;
/

show errors; 


/*****************************************************************************/
/**** Label generator stub for Extensible implementations                  ***/
/***  This type can be extended for custom label generators                ***/
/*****************************************************************************/
create or replace type mdsys.rdfsa_labelgen authid current_user as object (
  gen_option   NUMBER,

  -- 
  --- SETDEPRESOURCES : to set the dependent resources for the label   
  --- generator. Information about these resources will be passed to the
  --- the getNumericLabel method at runtime                              
  ---                                                                    
  --- Usage:  setDepResources(sem_rdfsa.USE_SUBJECT_LABEL+
  ---                         sem_rdfsa.USE_RULE_LABEL) 
  --
  final member procedure setDepResources(useres number),

  -- 
  --- FINDOMINATINGOF : Find a clear dominating label out of the labels 
  --- passed in. -1 is returned if a clear dominating label is not found -- 
  final static function findDominatingOf(labels MDSYS.INT_ARRAY) return number,

  --
  --- GETNUMERICLABEL : Extensible implementations for this type should   
  --- override this method to return a custom label based on the resources 
  --- passed in. The exact list of resources passed in is dependent on    
  --- options passed to the setDepResource method                        
  --
  member function getNumericLabel (subject   rdfsa_resource,
                                   predicate rdfsa_resource,
                                   object    rdfsa_resource,
                                   rule      rdfsa_resource,
                                   anteced   rdfsa_resource)
       return number

) not final
/

show errors;

create or replace type body mdsys.rdfsa_labelgen is

  final member procedure setDepResources(useres number) is
  begin
    null;
  end;

  final static function findDominatingOf(labels MDSYS.INT_ARRAY)
       return number is
  begin
    null;
  end; 

  member function getNumericLabel (subject   rdfsa_resource,
                                   predicate rdfsa_resource,
                                   object    rdfsa_resource,
                                   rule      rdfsa_resource,
                                   anteced   rdfsa_resource)
       return number is
  begin
    null;
  end; 
end; 
/ 

SHOW ERRORS;
-- grant execute on mdsys.rdfsa_resource to public;

begin
  EXECUTE IMMEDIATE 'create table mdsys.rdf_parameter 
        (namespace   VARCHAR2(30), 
         attribute   VARCHAR2(30), 
         value       VARCHAR2(512), 
         description VARCHAR2(100), 
         constraint rdf_parameter_key primary key (namespace, attribute))
       organization index tablespace SYSAUX';
exception 
  when others then 
    if (SQLCODE != -955) then 
      raise; 
    end if; 
end;
/

/*****************************************************************************/
/**** Types related to  sem operators                                      ***/
/*****************************************************************************/
-------------- Serves as a reference to temporary btree pointer --------
create or replace type sdo_bt_handle
as object
(
 btree RAW(4),
 shd   RAW(4)
);
/
show errors;

----------------------- indextype specification -----------------------------
CREATE OR REPLACE TYPE sem_indextype_im authid current_user AS OBJECT
( 
  

  ---------- Variables --------------------
  curnum                NUMBER, -- cursor number
  dist_cur_num          NUMBER, -- ancillary cursor
  path_cur_num          NUMBER, -- ancillary cursor
  curr_rules_index      VARCHAR2(30), -- current rules index name
  models_in             MDSYS.RDF_Models,
  rulebases_in          MDSYS.RDF_Rulebases,

  bt_handle		MDSYS.SDO_BT_HANDLE, -- pointer to btree for sem_distance anc op
  using_bt		INTEGER, -- set to 1 if we are using temp bt for sem_distance
  keysz			INTEGER, -- btree keysize in bytes
  datasz		INTEGER, -- btree datasize in bytes

  topk_opt		INTEGER, -- set to 1 if we are optimizing for top-k query
                                   -- else set to 0
  last_cutoff		INTEGER, -- last cutoff during evaluation of top-k query
  rows_covered		NUMBER,  -- number of rows covered so far according to stats
  direction		INTEGER, -- direction of top-k query 1 = asc, 0 = desc
  base_query		VARCHAR2(4000), -- base query for top-k optimization 
  pred_id		INTEGER, -- predicate id for this query
  stats_table		VARCHAR2(65), -- name of stats table for index stats

  distances		VARCHAR2(32000), -- cache for passing distances to anc op
  invalid_cache		INTEGER, -- set to 1 if cache is invalid, 0 otherwise
 		
  ---------- Functions -------------------- 
  STATIC FUNCTION ODCIGetInterfaces (ifclist OUT SYS.ODCIObjectList) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexCreate (ia    SYS.ODCIindexinfo,
                                   parms VARCHAR2,
                                   env   SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexDrop (ia  SYS.ODCIindexinfo,
                                 env SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexTruncate (ia  SYS.ODCIIndexInfo, 
                                     env SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexAlter (ia           SYS.ODCIIndexInfo, 
                                  parms IN OUT VARCHAR2,
                                  alter_option NUMBER,
                                  env          SYS.ODCIEnv) RETURN NUMBER,
  
  STATIC FUNCTION ODCIIndexInsert (ia     SYS.ODCIindexinfo,
                                   rid    varchar2,
                                   newval varchar2, 
                                   env    SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexDelete (ia     SYS.ODCIindexinfo,
                                   rid    varchar2,
                                   oldval varchar2,
                                   env    SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexUpdate (ia     SYS.ODCIindexinfo,
                                   rid    varchar2,
                                   oldval varchar2,
                                   newval varchar2,
                                   env    SYS.ODCIEnv) RETURN NUMBER,

  -- index ststaus optional parameter only --
  STATIC FUNCTION ODCIIndexStart (sctx IN OUT  sem_indextype_im,
                                  ia           SYS.ODCIindexinfo,
                                  op           SYS.ODCIPredInfo,
                                  qi           SYS.ODCIQueryInfo,
                                  strt         NUMBER,
                                  stop         NUMBER,
                                  pred_expr    VARCHAR2,
                                  obj          VARCHAR2,
                                  models       MDSYS.RDF_Models,
                                  rulebases    MDSYS.RDF_Rulebases,       
                                  idxStatus_in VARCHAR2,
                                  env          SYS.ODCIEnv) RETURN NUMBER,
 
  -- upper and lower bound optional parameters only --
  STATIC FUNCTION ODCIIndexStart (sctx IN OUT  sem_indextype_im,
                                  ia           SYS.ODCIindexinfo,
                                  op           SYS.ODCIPredInfo,
                                  qi           SYS.ODCIQueryInfo,
                                  strt         NUMBER,
                                  stop         NUMBER,
                                  pred_expr    VARCHAR2,
                                  obj          VARCHAR2,
                                  models       MDSYS.RDF_Models,
                                  rulebases    MDSYS.RDF_Rulebases,
                                  dist_lower   INTEGER,
                                  dist_upper   INTEGER,       
                                  env          SYS.ODCIEnv) RETURN NUMBER,

  -- index status and upper and lower bound optional parameters -- 
  STATIC FUNCTION ODCIIndexStart (sctx IN OUT  sem_indextype_im,
                                  ia           SYS.ODCIindexinfo,
                                  op           SYS.ODCIPredInfo,
                                  qi           SYS.ODCIQueryInfo,
                                  strt         NUMBER,
                                  stop         NUMBER,
                                  pred_expr    VARCHAR2,
                                  obj          VARCHAR2,
                                  models       MDSYS.RDF_Models,
                                  rulebases    MDSYS.RDF_Rulebases,   
                                  idxStatus_in VARCHAR2,
                                  dist_lower   INTEGER,    
                                  dist_upper   INTEGER,
                                  env          SYS.ODCIEnv) RETURN NUMBER,
  -- no optional parameters --
  STATIC FUNCTION ODCIIndexStart (sctx IN OUT  sem_indextype_im,
                                  ia           SYS.ODCIindexinfo,
                                  op           SYS.ODCIPredInfo,
                                  qi           SYS.ODCIQueryInfo,
                                  strt         NUMBER,
                                  stop         NUMBER,
                                  pred_expr    VARCHAR2,
                                  obj          VARCHAR2,
                                  models       MDSYS.RDF_Models,
                                  rulebases    MDSYS.RDF_Rulebases,       
                                  env          SYS.ODCIEnv) RETURN NUMBER,


  MEMBER FUNCTION ODCIIndexFetch (self IN OUT nocopy sem_indextype_im,
                                  nrows    NUMBER,
                                  rids OUT SYS.ODCIridlist,
                                  env      SYS.ODCIEnv) RETURN NUMBER,

  MEMBER FUNCTION ODCIIndexClose (env SYS.ODCIEnv) RETURN NUMBER,

  STATIC FUNCTION ODCIIndexGetMetadata (ia           SYS.ODCIindexinfo,
                                        expversion   VARCHAR2,
                                        newblock OUT PLS_INTEGER,
                                        env          SYS.ODCIEnv) RETURN VARCHAR2

);
/
show errors; 

----------------------- statistics type specification --------------------------
CREATE OR REPLACE TYPE sdo_sem_stats authid current_user AS OBJECT
(
  curnum NUMBER,

  STATIC FUNCTION ODCIGetInterfaces (ifclist OUT sys.ODCIObjectList)
    RETURN NUMBER,

  STATIC FUNCTION ODCIStatsCollect (col sys.ODCIColInfo,
                                    options sys.ODCIStatsOptions,
                                    rawstats OUT RAW,
                                    env sys.ODCIEnv)
    RETURN NUMBER,

  STATIC FUNCTION ODCIStatsDelete (col sys.ODCIColInfo,
                                    env sys.ODCIEnv)
    RETURN NUMBER,

  STATIC FUNCTION ODCIStatsCollect (ia sys.ODCIIndexInfo,
                                    options sys.ODCIStatsOptions,
                                    rawstats OUT RAW,
                                    env sys.ODCIEnv)
    RETURN NUMBER,

  STATIC FUNCTION ODCIStatsDelete (ia sys.ODCIIndexInfo,
                                   statistics OUT RAW,
                                   env sys.ODCIEnv)
    RETURN NUMBER
);
/
show errors;

