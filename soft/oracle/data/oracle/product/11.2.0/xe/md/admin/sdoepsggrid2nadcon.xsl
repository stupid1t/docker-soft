<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:epsg="http://www.oracle.com/2004/spatial/epsg/gridfile/schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:output method="text"/>
	<xsl:param name="coordinatePosition">1</xsl:param>
	<xsl:template match="epsg:GridFile">
		<xsl:variable name="minLon" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:MinCoords/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="minLat" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:MinCoords/epsg:Coordinate[position()=2]"/>
		<xsl:variable name="maxLon" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:MaxCoords/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="maxLat" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:MaxCoords/epsg:Coordinate[position()=2]"/>
		<xsl:variable name="spacingLon" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:CoordSpacing/epsg:Coordinate[position()=1]"/>
		<xsl:variable name="spacingLat" select="/epsg:GridFile/epsg:Grids/epsg:Grid[position()=1]/epsg:GridProperties/epsg:CoordSpacing/epsg:Coordinate[position()=2]"/>
		<xsl:text><![CDATA[NADCON EXTRACTED REGION                                 NADGRD  ]]></xsl:text>
		<xsl:text><![CDATA[ 
]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="($maxLon - $minLon) div $spacingLon + 1"/>
			<xsl:with-param name="length" select="4"/>
		</xsl:call-template>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="($maxLat - $minLat) div $spacingLat + 1"/>
			<xsl:with-param name="length" select="4"/>
		</xsl:call-template>
		<xsl:text><![CDATA[   1]]></xsl:text>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number($minLon, '#.00000')"/>
			<xsl:with-param name="length" select="12"/>
		</xsl:call-template>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number($spacingLon, '#.00000')"/>
			<xsl:with-param name="length" select="12"/>
		</xsl:call-template>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number($minLat, '#.00000')"/>
			<xsl:with-param name="length" select="12"/>
		</xsl:call-template>
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number($spacingLat, '#.00000')"/>
			<xsl:with-param name="length" select="12"/>
		</xsl:call-template>
		<xsl:text><![CDATA[      .00000]]></xsl:text>
		<xsl:apply-templates select="epsg:Grids/epsg:Grid[position()=1]/epsg:GridData/epsg:GridRow"/>
	</xsl:template>
	<xsl:template match="epsg:GridRow">
		<xsl:for-each select="epsg:GridNode/epsg:Offset">
			<xsl:if test="position() mod 6 = 1">
				<xsl:text><![CDATA[ 
]]></xsl:text>
			</xsl:if>
			<xsl:apply-templates select="epsg:Coordinate[position()=$coordinatePosition]"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="epsg:Coordinate">
		<xsl:call-template name="right-align">
			<xsl:with-param name="string" select="format-number((3600 * .), '#.000000')"/>
			<xsl:with-param name="length" select="12"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="right-align">
		<xsl:param name="string" select="'Not Available'" />
		<xsl:param name="length" select="12" />
		<xsl:variable name="valueLength" select="string-length($string)"/>
		<xsl:variable name="paddingLength" select="($length - $valueLength)"/>
		<xsl:value-of select="substring('                                                                ', 1, $paddingLength)"/>
		<xsl:value-of select="$string"/>
	</xsl:template>
</xsl:stylesheet>
