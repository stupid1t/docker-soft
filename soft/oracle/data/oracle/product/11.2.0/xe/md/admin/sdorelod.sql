Rem
Rem $Header: sdorelod.sql 17-jul-2002.12:44:29 sravada Exp $
Rem
Rem sdorelod.sql
Rem
Rem Copyright (c) 2002, Oracle Corporation.  All rights reserved.  
Rem
Rem    NAME
Rem      sdorelod.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     07/17/02 - sravada_add_sdopath_sdorelod
Rem    sravada     07/17/02 - Created
Rem

Rem reload catmd.sql from 920 to downgrade 
@@catmd.sql

