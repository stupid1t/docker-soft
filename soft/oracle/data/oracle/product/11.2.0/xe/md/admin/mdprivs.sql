Rem
Rem $Header: sdo/admin/mdprivs.sql /main/23 2009/12/10 15:09:17 sravada Exp $
Rem
Rem mdprivs.sql
Rem
Rem Copyright (c) 1998, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      mdprivs.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    vkolovsk    01/22/09 - grant flashback to mdsys (needed for incremental
Rem                           semantic inference)
Rem    ningan      09/24/07 - bug-6415891: add dba_java_classes privilege for NDM
Rem    bgouslin    01/07/07 - New version to fix Windoze format issue caused by
Rem                           some ADE bug
Rem    zzhang      01/05/06 - grant dba_all_tables privilege to georaster
Rem    zzhang      10/20/05 - add more privileges for georaster
Rem    nalexand    05/19/05 - add create any trigger to MDSYS 
Rem    sravada     05/11/05 - 
Rem    geadon      05/04/05 - Add DBMS_SYSTEM privilege 
Rem    zzhang      04/27/05 - remove grant DELETE ANY TABLE privilege to MDSYS
Rem    sravada     04/28/05 - 
Rem    sravada     04/20/04 - bug 3575743 
Rem    wexu        06/24/03 - grant DELETE ANY TABLE privilege to MDSYS
Rem    sravada     10/01/02  - 
Rem    sravada     04/22/02 -  revoke "all privilleges" from MDSYS
Rem    sravada     10/08/01 - 
Rem    bgouslin    02/12/01 - Fix mdsys entries
Rem    sravada     04/20/00 - grant admin to mdsys
Rem    ranwei      10/22/98 - Privileges list for MDSYS
Rem    ranwei      10/22/98 - Created
Rem

REM
REM You must connect as SYS prior to running this script.
REM
 
--
-- System privs needed by MDSYS
--
-- grant all privileges to MDSYS with admin option;
-- the set below should be included in the admin option

grant create session to MDSYS;
grant connect, resource, create library, create procedure to MDSYS;
grant create operator to MDSYS;
grant create indextype to MDSYS;
grant create type to MDSYS;
grant create table to MDSYS;
grant create public synonym to MDSYS;
grant drop public synonym to MDSYS;
--  grant select_catalog_role to MDSYS ;
-- grant execute_catalog_role to MDSYS;
grant create view to MDSYS;
grant create sequence to MDSYS;

-- the following seem to give errors; so explicitly grant them 
--
-- Privs needed for MDSYS to create mdbootstrap package
--
-- grant select on sys.obj$ to MDSYS;
-- grant select on sys.user$ to MDSYS;
-- grant select on sys.ts$ to MDSYS;
-- grant select on sys.col$ to MDSYS;
 
--
-- Privs needed for MDSYS to create mdlib package
--
grant select on sys.dba_objects to MDSYS;
grant select on sys.dba_tablespaces to MDSYS;
grant select on sys.dba_tables to MDSYS;
grant select on dba_views to mdsys with grant option;
grant select on dba_object_tables to mdsys with grant option;
grant select on sys.dba_tab_columns to MDSYS;
grant select on sys.dba_tab_privs to MDSYS;
grant select on sys.dba_constraints to MDSYS;
grant select on sys.dba_cons_columns to MDSYS;
grant select on sys.dba_indexes to MDSYS;
grant select on sys.dba_ind_columns to MDSYS;
grant select on sys.kopm$ to mdsys;
grant EXECUTE on dbms_registry to mdsys;
grant select on dba_registry to mdsys;
grant select on dba_role_privs to mdsys;
grant execute on dbms_lock to mdsys;

 
--
-- Privs needed for MDSYS to use import/export extensibility
--

-- grant insert on sys.exppkgobj$ to MDSYS;
-- grant insert on sys.exppkgact$ to MDSYS;
-- grant insert on sys.expdepobj$ to MDSYS;
-- grant insert on sys.expdepact$ to MDSYS;
-- grant delete on sys.exppkgobj$ to MDSYS;
-- grant delete on sys.exppkgact$ to MDSYS;
-- grant delete on sys.expdepobj$ to MDSYS;
-- grant delete on sys.expdepact$ to MDSYS;

 
--
-- for GeoRaster
--
grant drop any trigger to mdsys;
grant select on sys.dba_triggers to mdsys;
grant select on sys.dba_synonyms to mdsys;
grant select on sys.dba_types to mdsys;
grant select on sys.dba_type_attrs to mdsys;
grant select on sys.dba_all_tables to mdsys;
grant select any table to mdsys;
grant update any table to mdsys;
grant alter any table to mdsys;


--
-- for RDF
--
grant execute on sys.dbms_system to MDSYS;
grant create any trigger to MDSYS;
grant delete any table to  MDSYS;
grant flashback any table to MDSYS;
--
-- for NDM
-- 
grant select on sys.dba_java_classes to MDSYS;

