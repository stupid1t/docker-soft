declare
begin
  begin
    execute immediate
      ' drop view OGIS_SPATIAL_REFERENCE_SYSTEMS ';
    exception when others then NULL;
   end;
  begin
    execute immediate
      ' drop PUBLIC synonym OGIS_SPATIAL_REFERENCE_SYSTEMS  ';
    exception when others then NULL;
   end;
  begin
    execute immediate
      ' drop trigger OGC_INS_TRIG ';
    exception when others then NULL;
   end;

  begin
   execute immediate 
'create table OGIS_SPATIAL_REFERENCE_SYSTEMS (
   SRID        NUMBER CONSTRAINT pk_srid PRIMARY KEY,
   AUTH_NAME   VARCHAR2(100),
   AUTH_SRID   NUMBER,
   SRTEXT      VARCHAR2(1000),
   SRNUM       NUMBER) ';
  exception when others then NULL;
 end;
end;
/
grant select, insert, delete on OGIS_SPATIAL_REFERENCE_SYSTEMS to public;
declare
begin
  begin
   execute immediate 
'create table OGIS_GEOMETRY_COLUMNS (
   F_TABLE_SCHEMA       VARCHAR2(64),
   F_TABLE_NAME         VARCHAR2(64),
   F_GEOMETRY_COLUMN    VARCHAR2(64),
   G_TABLE_SCHEMA       VARCHAR2(64),
   G_TABLE_NAME         VARCHAR2(64),
   STORAGE_TYPE         NUMBER,
   GEOMETRY_TYPE        NUMBER,
   COORD_DIMENSION      NUMBER,
   MAX_PPR              NUMBER,
   SRID                 CONSTRAINT fk_srid
                        REFERENCES OGIS_SPATIAL_REFERENCE_SYSTEMS(SRID))' ;
  exception when others then NULL;
 end;
end;
/
grant select,insert,delete on OGIS_GEOMETRY_COLUMNS to public;
create or replace public synonym OGIS_GEOMETRY_COLUMNS for
MDSYS.OGIS_GEOMETRY_COLUMNS;
commit;
create or replace view DBA_GEOMETRY_COLUMNS as 
select * from OGIS_GEOMETRY_COLUMNS;
grant select on DBA_GEOMETRY_COLUMNS to select_catalog_role;
create or replace  public synonym 
DBA_GEOMETRY_COLUMNS for mdsys.DBA_GEOMETRY_COLUMNS;
create or replace view ALL_GEOMETRY_COLUMNS as 
select * from OGIS_GEOMETRY_COLUMNS where
(exists (select table_name from all_tables
         where table_name=f_table_name and owner=f_table_schema)
 or
 exists (select view_name from all_views
         where view_name=f_table_name and owner=f_table_schema)
 or
 exists (select table_name from all_object_tables
         where table_name=f_table_name and owner=f_table_schema)
)
and 
(exists (select table_name from all_tables
         where table_name=g_table_name and owner=g_table_schema)
 or
 exists (select view_name from all_views
         where view_name=g_table_name and owner=g_table_schema)
 or
 exists (select table_name from all_object_tables
         where table_name=g_table_name and owner=g_table_schema)
);
grant select on ALL_GEOMETRY_COLUMNS to public;
create or replace  public synonym ALL_GEOMETRY_COLUMNS for 
mdsys.ALL_GEOMETRY_COLUMNS;
create or replace view USER_GEOMETRY_COLUMNS as
select * from OGIS_GEOMETRY_COLUMNS
where f_table_schema=user and g_table_schema=user;
grant select on USER_GEOMETRY_COLUMNS to public;
create or replace  public synonym USER_GEOMETRY_COLUMNS for 
mdsys.USER_GEOMETRY_COLUMNS;
grant select on OGIS_SPATIAL_REFERENCE_SYSTEMS to public;
create or replace  public synonym OGIS_SPATIAL_REFERENCE_SYSTEMS
for mdsys.OGIS_SPATIAL_REFERENCE_SYSTEMS;
commit;
create or replace package sdo_catalog wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
4ae 17d
iNcABBdKlrHZSsf6wPUHRC9h+CUwg5W3JAw2fy+VO/5eGsNLwu0n0YgDr+QL8W6HKnae3xWU
knN/MLnNxeLbIbzAj8Y0NEL0yrtnjDAQe5uG7s2lwM9vaZsRicgC5xCL8cg2Gu6E96yiXOcC
CDJBk+Y5/dJDYhe0o10UgPXK5V8f2sqt4VVxKOAutsGv7t4ll950y4LNkzm6biBd9bCuVSmC
Q2cqcP/4V8e8U2IlIAXoxJAtb/JAZxiNUn23vbz3wKXdfyXgJeZs5ZQ6UYIyDYhv7P1eUaxf
bdRuSKWI3upMoyCxLaH+fuVc5G+hHY7YBqS0m7CHQjQPN4vOwX2Uo3HQ+aok9oH4gioO6mYm
fDEZcTQdDFulYg==

/
show errors;
commit;
