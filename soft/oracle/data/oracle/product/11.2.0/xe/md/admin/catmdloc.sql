Rem
Rem $Header: sdo/admin/catmdloc.sql /main/24 2008/07/28 09:24:40 yhu Exp $
Rem
Rem catmdloc.sql
Rem
Rem Copyright (c) 1998, 2008, Oracle. All rights reserved.
Rem
Rem    NAME
Rem      catmdloc.sql - Install the locator/geocoder interface of Spatial
Rem
Rem    DESCRIPTION
Rem      ** Change it back to install all of spatial.
Rem      This now also loads all the related packages required for 
Rem      Locator. This basically does a  basic spatial 
Rem      installation with the packages/types required for locator.
Rem      This script simply loads the files related to the Locator
Rem      (geocoder interface) component of the interMedia cartridge
Rem      Presently this consists of the package(s) and functions:
Rem      PACKAGES:
Rem      geocoder_http
Rem      FUNCTIONS
Rem        geocoder_http.geocode1()
Rem        geocoder_http.dist2latlon() 
Rem        NOTE: dist2latlon() returns APPROXIMATE values and assumes
Rem              WSG84 as the reference ellipsoid
Rem        procedure geocoder_http.setup_locator_index()
Rem      OPERATORS
Rem        MDSYS.locator_within_distance
Rem      OBJECT TYPES
Rem        Geocode_Result
Rem
Rem    NOTES
Rem      Assumes one is connected as MDSYS and that the objects option, 
Rem      extensible indexing framework (catodci.plb) and Spatial 8i
Rem      have all been installed.
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    yhu         07/23/08 - move sdopopt from catmd10i.sql to catmdloc.sql
Rem    sravada     05/12/05 - add text object 
Rem    sravada     11/15/04 - 
Rem    sravada     10/01/04 - 
Rem    sravada     08/31/04 - 
Rem    sravada     08/30/04 - move sdoutl.plb 
Rem    sravada     07/28/04 - 
Rem    mhorhamm    05/04/04 - Recompile tfm_plan, after added constraints make
Rem                           it temporarily invalid
Rem    mhorhamm    02/06/04 - Include sdoepsgd.sql 
Rem    mhorhamm    01/29/04 - Change sequence of files 
Rem    mhorhamm    01/22/04 - Delete reference to sdoepsgh.sql 
Rem    mhorhamm    01/13/04 - Move header of EPSG package
Rem    sravada     11/21/03 - 
Rem    mhorhamm    11/06/03 - Have cs_srs view created, instead of table, call 
Rem    sravada     09/12/03 - move sqlmmtypes up 
Rem    mhorhamm    08/25/03 - Add sdogmmh.sql and sdogmmb.plb (SQL/mm support)
Rem    sravada     04/16/03 - fix indextype creation
Rem    jcwang      10/07/02 - 
Rem    sravada     09/18/02 - 
Rem    sravada     09/20/02 - change to sysaux 
Rem    sravada     08/15/02 - break SDO install
Rem    sravada     07/31/99 - Call catmd from here
Rem    sravada     07/15/99 - add all the locator stuff here .
Rem    jsharma     09/14/98 - Install the interMedia Locator component related
Rem    jsharma     09/14/98 - Created
Rem

-- Alter user MDSYS default tablespace SYSAUX;


REM Creating MD package ...              
--prompt Creating package MD ...         
@@md                                     
show errors;                             
@@prvtmd.plb                             
show errors;                             
                                         
commit;                                  

REM Creating MDERR package ...
--prompt Creating package MDERR ...
@@prvterr.plb
show errors;

REM Install the SDO Object Data Types (ODTs)
REM and grant execute privilege to PUBLIC
--prompt Installing SDO Object Types ...
@@sdotypes.sql
@@sdotypeg.sql

Rem Load SQL/mm types
@@sdogmmh.sql

REM Install the Geometry Metadata Views
@@prvtgmd.plb

REM Install the Index Metadata Views
@@prvtimd.plb

REM Create the LRS Package
@@prvtlrsh.plb
@@sdolrsh.sql

Rem Load the LRS Metadata
@@sdolrsmd.sql

Rem Create the CoordSystems Package
Rem We need this here as other packages refer to these functions
@@sdocs.sql

REM Load the header for Index_Method supporting routines (sdo_idx package)
--prompt Installing the sdo_idx package
@@sdopidx.sql

REM Topology Types
@@sdotptyp.sql

Rem Topo Metadata
@@sdotpmd.sql

Rem Topo Metadata manipulation packages
@@prvttmd.plb

commit;
REM Install the Extensible Indexing package.
--prompt Installing type spatial_im package ...
@@prvtsidx.plb
REM Load the body for Index_Method supporting routines (sdo_idx package)
--prompt Installing the sdo_idx package body
@@prvtpidx.plb
@@prvtsipt.plb


REM Relate table lookup setup
--prompt Installing SDO_GEOM.RELATE() table...
@@relate.sql

REM Install the 3GL package header.
REM  This should be before prvtmd2.
--prompt Installing SDO_3GL package header ...
@@prvt3glh.plb

REM Install the SDO package.
--prompt Installing SDO package ...
@@prvtmd1.plb
@@prvtmd2.plb
@@sdo.sql
@@prvtsdo.plb


REM Install the ADMIN package.
--prompt Installing SDO_ADMIN package ...
@@sdoadmin.sql
@@prvtadmn.plb

REM Install the SDO_TUNE package
@@sdotune.sql

Rem Load SDO_UTIL package
@@sdoutlh.sql

Rem Load SDO Utilities  
@@sdoutl.plb

REM Install the GEOM package.
--prompt Installing SDO_GEOM package ...
@@sdogeom.sql
@@prvtgeom.plb

REM Install the 3GL package body.
--prompt Installing SDO_3GL package body ...
@@prvt3gl.plb

REM Installing OGIS Style catalog
--prompt Installing OGIS style catalog
@@sdocat.plb
@@prvtcat.plb

REM Installing SDO operators
--prompt Installing SDO operators
@@prvtoper.plb

REM Install migration to 8.1 package
--prompt Installing migration package
@@sdomig.sql
@@prvtmig.plb

Rem create the EPSG tables, first without constraints (will be added in sdoepsgl.sql)
@@sdoepsgd.sql

Rem Create the CoordSystems Package Body
@@prvtcs.plb

Rem Load the new CS_SRS view, and others
@@sdoepsgl.plb

set define on;

alter session set current_schema=SYS;
COLUMN file_name NEW_VALUE xml_file NOPRINT;
SELECT dbms_registry.script('XDB', '@sdoepsgx.sql') AS file_name FROM DUAL;
alter session set current_schema=MDSYS;
@&xml_file

@@sdocsvws.sql

@@sdoepsgv.sql

@@prvtcs.plb

Rem Load the WKT/SRID/Datum Table
--@@sdowkt.sql
Rem already done in sdoepsgv.sql

Rem Load Rtree ops for Joins/N-dimensional objects
@@prvtrtr.plb

Rem Install sdo_tune package body
@@prvttune.plb

Rem Load Synonyms for Old Obsoleted SQL functions
@@sdosfs.sql

Rem Load SDO_UNIT names
--@@sdounit.sql
Rem already done in sdoepsgv.sql

Rem Load ellipsoid, datum and projection tables
@@sdocsdef.sql

Rem Load SDO_UTIL package
@@sdoutlb.plb

Rem Load the SDOMAP Definitions
@@sdomapd.sql

Rem Load SDO_PRIDX package
@@prvtprdx.plb

Rem Load SQL/mm types
@@sdogmmb.plb

Rem Load OGC Object  types
@@sdoogc.sql

Rem Load the LRS Packages
@@prvtlrsb.plb
@@sdolrsb.plb
 
Rem Load Types and functions for Aggregate functions
@@sdoaggrh.sql
@@sdoaggrb.plb

Rem Cartographic Text Object
@@sdogmlsc.sql
@@sdotxth.sql

REM Spatial partition pruning using query rewrite
@@sdopopt.plb

alter session set current_schema=SYS;
COLUMN java_fname NEW_VALUE java_file NOPRINT;
SELECT dbms_registry.script('JAVAVM', '@sdoloadj.sql') AS java_fname FROM DUAL;
@&java_file

alter session set current_schema=MDSYS;
