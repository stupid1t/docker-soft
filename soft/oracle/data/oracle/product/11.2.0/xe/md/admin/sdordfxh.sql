Rem
Rem $Header: sdo/admin/sdordfxh.sql /st_sdo_11.2.0/3 2010/06/16 07:44:25 vkolovsk Exp $
Rem
Rem sdordfxh.sql
Rem
Rem Copyright (c) 2005, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdordfxh.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <sdo_rdf package>
Rem
Rem    NOTES
Rem      <package body in sdordfxb.sql>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    matperry    06/09/10 - parallel_enable FILTER functions
Rem    sdas        05/27/10 - bug 9756570: sem network index info view
Rem    matperry    11/20/09 - add langMatches as SPARQL FILTER helper function
Rem    matperry    11/12/09 - refactor all value$ filter functions to take all
Rem                           value$ columns as arguments
Rem    matperry    11/11/09 - remove getV$RDFTVal
Rem    matperry    10/16/09 - add isVCLOBhelper function for FILTER
Rem    matperry    09/09/09 - add constants for xsd type URIs
Rem    matperry    09/04/09 - add getV$STRVal()
Rem    matperry    08/31/09 - remove unused getV$ functions after Filter
Rem                           refactoring
Rem    matperry    08/13/09 - add rdfTermEqual and rdfTermComp
Rem    sdas        08/04/09 - for BASIC compress of OLTP data
Rem                           allow MOVE within same tablespace
Rem    matperry    06/26/09 - add dateTimeComp() and dateTimeEqual()
Rem    vkolovsk    06/11/09 - change order of parameters in merge_models
Rem    vkolovsk    06/09/09 - fix bug 8584453
Rem    matperry    05/12/09 - add cascade option to drop_sem_network
Rem    alwu        04/28/09 - change API names
Rem    matperry    04/17/09 - add effective boolean value helper method for
Rem                           FILTER
Rem    vkolovsk    03/25/09 - add rename_model
Rem    matperry    01/29/09 - add helper functions for SPARQL FILTER
Rem    alwu        12/29/08 - XbranchMerge alwu_new_ha_apis from st_sdo_11.1
Rem    matperry    10/31/08 - add create_virtual_model, drop_virtual_model
Rem    sdas        10/14/08 - flex indexing: add COMPRESS param
Rem    alwu        07/14/08 - migrate remove_duplicates or remove_duplicates API
Rem    alwu        07/14/08 - add HA APIs
Rem    alwu        06/27/08 - add logic to counter act the truncate trigger
Rem    alwu        06/26/08 - add more parameters to clean_model API
Rem    alwu        06/25/08 - add new APIs for UTH enhancement requests
Rem    spsundar    04/19/08 - update bulk_load_from_staging_table signature
Rem    sdas        02/28/08 - fif: generalize for inference and rules_index
Rem    sdas        02/20/08 - fif: allow more options on alter
Rem    sdas        02/15/08 - flexible indexing framework
Rem    alwu        09/13/07 - add statistics on demand
Rem    alwu        08/22/07 - add get_triple_id, is_triple for CLOB
Rem    sdas        04/16/07 - remove rdf dummy upgrade/downgrade routines
Rem    alwu        04/13/07 - remove gather_rdf_stats
Rem    sdas        03/28/07 - cleanup
Rem    sdas        03/16/07 - add REFRESH_SEM_TABLESPACE_NAMES
Rem    sdas        03/09/07 - allow storing diff RDF models in diff tablespaces
Rem    sdas        01/30/07 - change interface: IS_TRIPLE, GET_TRIPLE_ID
Rem    sdas        12/13/06 - put utility functions into SDO_RDF
Rem    sdas        10/24/06 - check INS priv for model in constructor
Rem    sdas        09/16/06 - remove add_namespaces
Rem    alwu        09/07/06 - sync up 11g document and proposed APIs
Rem    sdas        07/07/06 - add flags 
Rem    sdas        06/19/06 - add SEM interfaces 
Rem    sdas        06/14/06 - add upgrade and downgrade procedures 
Rem    sdas        06/08/06 - add BATCH_LOAD_FROM_STAGING_TABLE 
Rem    alwu        04/06/06 - fix bug: 5146335 
Rem    nalexand    02/14/06 - add create_logical_network(), drop_logical_network()
Rem    nalexand    02/10/06 - add gather_rdf_stats 
Rem    nalexand    01/31/06 - add get_model_name() 
Rem    echong      11/17/05 - define update_user_table$()
Rem    nalexand    10/11/05 - add cleanup_batch_load() 
Rem    nalexand    05/20/05 - remove get_triple_cost() 
Rem    nalexand    05/12/05 - add is_reified_quad() 
Rem    nalexand    05/12/05 - remove default tablespace_name 
Rem    nalexand    05/09/05 - nalexand_rdftrig0429
Rem    nalexand    05/06/05 - add get_triple_cost; remove rdf_geturl() 
Rem    nalexand    05/04/05 - new signature for create_rdf_model 
Rem    nalexand    05/03/05 - Created
Rem

CREATE OR REPLACE PACKAGE sdo_rdf AUTHID CURRENT_USER AS

--
--  !!!!! PLEASE READ BEFORE YOU ADD APIS IN THIS PACKAGE !!!!!!!!!!!!
--
--  All APIs in sdo_rdf should be wrapped and duplicated in sdordf.sql
--  That way, we have a consolidated view of sem_apis package
--

 /*---------------------------- Constants --------------------------*/
  XSDNSP                 VARCHAR2(33) := 'http://www.w3.org/2001/XMLSchema#';

  XSD_DECIMAL            VARCHAR2(40) := XSDNSP || 'decimal';
  XSD_DOUBLE             VARCHAR2(39) := XSDNSP || 'double';
  XSD_FLOAT              VARCHAR2(38) := XSDNSP || 'float';
  XSD_INTEGER            VARCHAR2(40) := XSDNSP || 'integer';
  XSD_INT                VARCHAR2(36) := XSDNSP || 'int';
  XSD_NONPOSITIVEINTEGER VARCHAR2(51) := XSDNSP || 'nonPositiveInteger';
  XSD_NEGATIVEINTEGER    VARCHAR2(48) := XSDNSP || 'negativeInteger';
  XSD_LONG               VARCHAR2(37) := XSDNSP || 'long';
  XSD_SHORT              VARCHAR2(38) := XSDNSP || 'short';
  XSD_BYTE               VARCHAR2(37) := XSDNSP || 'byte';
  XSD_NONNEGATIVEINTEGER VARCHAR2(51) := XSDNSP || 'nonNegativeInteger';
  XSD_UNSIGNEDLONG       VARCHAR2(45) := XSDNSP || 'unsignedLong';
  XSD_UNSIGNEDINT        VARCHAR2(44) := XSDNSP || 'unsignedInt';
  XSD_UNSIGNEDSHORT      VARCHAR2(46) := XSDNSP || 'unsignedShort';
  XSD_UNSIGNEDBYTE       VARCHAR2(45) := XSDNSP || 'unsignedByte';
  XSD_POSITIVEINTEGER    VARCHAR2(48) := XSDNSP || 'positiveInteger';
  XSD_BOOLEAN            VARCHAR2(40) := XSDNSP || 'boolean';
  XSD_STRING             VARCHAR2(39) := XSDNSP || 'string';
  XSD_DATE               VARCHAR2(37) := XSDNSP || 'date';
  XSD_TIME               VARCHAR2(37) := XSDNSP || 'time';
  XSD_DATETIME           VARCHAR2(41) := XSDNSP || 'dateTime';

 /*---------------------- Procedures and Functions -----------------*/
  FUNCTION raise_parse_error (
    errtext VARCHAR2, lexval VARCHAR2, new_lexval VARCHAR2 default NULL)
  return varchar2 deterministic;
  pragma restrict_references (raise_parse_error,WNDS,RNDS,WNPS,RNPS);

  FUNCTION replace_rdf_prefix (
    string                       VARCHAR2
  , options                      VARCHAR2 default NULL
  ) RETURN                       VARCHAR2 deterministic;
  pragma restrict_references (replace_rdf_prefix,WNDS,RNDS,WNPS,RNPS);

  FUNCTION pov_typed_literal (
    lexval IN     VARCHAR2
  , options IN    VARCHAR2 default NULL
  ) RETURN        VARCHAR2;
  pragma restrict_references (pov_typed_literal,WNDS,RNDS,WNPS);

  FUNCTION checkAppTabPriv (
    owner        IN  varchar2
  , App_Tabname  IN  varchar2
  )
  return number;

  PROCEDURE BULK_LOAD_FROM_STAGING_TABLE (
    model_name     IN          varchar2,
    table_owner    IN          varchar2, 
    table_name     IN          varchar2,
    flags          IN          varchar2 default NULL,
    debug          IN          PLS_INTEGER default NULL,
    start_comment  IN          varchar2 default NULL,
    end_comment    IN          varchar2 default NULL
  );

  PROCEDURE RESUME_LOAD_FROM_STAGING_TABLE (
    model_name          IN              varchar2,
    table_owner         IN              varchar2,
    table_name          IN              varchar2,
    session_id          IN              varchar2,
    flags               IN              varchar2 default NULL,
    start_comment       IN              varchar2 default NULL,
    end_comment         IN              varchar2 default NULL
  );

  FUNCTION get_model_id (
    model_name       IN  VARCHAR2
  ) RETURN               NUMBER;

  FUNCTION get_model_name(
    model_id         IN  NUMBER
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
    model_id         IN  NUMBER
  , rdf_t_id         IN  VARCHAR2
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
    model_name       IN  VARCHAR2
  , rdf_t_id         IN  VARCHAR2
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
    model_name           VARCHAR2
  , subject              VARCHAR2
  , property             VARCHAR2
  , object               VARCHAR2
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
      model_id        IN NUMBER
    , subject         IN VARCHAR2
    , property        IN VARCHAR2
    , object          IN VARCHAR2
  ) RETURN               VARCHAR2;


  FUNCTION is_triple (
    model_name           VARCHAR2
  , subject              VARCHAR2
  , property             VARCHAR2
  , object          CLOB
  ) RETURN               VARCHAR2;

  FUNCTION is_triple (
      model_id        IN NUMBER
    , subject         IN VARCHAR2
    , property        IN VARCHAR2
    , object     IN CLOB
  ) RETURN               VARCHAR2;

  FUNCTION get_triple_id (
    model_id             NUMBER
  , subject              VARCHAR2
  , property             VARCHAR2
  , object               VARCHAR2
  ) RETURN               VARCHAR2;

  FUNCTION get_triple_id (
    model_name           VARCHAR2
  , subject              VARCHAR2
  , property             VARCHAR2
  , object               VARCHAR2
  ) RETURN               VARCHAR2;


  FUNCTION get_triple_id (
    model_id             NUMBER
  , subject              VARCHAR2
  , property             VARCHAR2
  , object          CLOB
  ) RETURN               VARCHAR2;


  FUNCTION get_triple_id (
    model_name           VARCHAR2
  , subject              VARCHAR2
  , property             VARCHAR2
  , object          CLOB
  ) RETURN               VARCHAR2;


  FUNCTION is_reified_quad (
    model_id                IN  NUMBER
  , subject                 IN  VARCHAR2
  , property                IN  VARCHAR2
  , object                  IN  VARCHAR2
  ) RETURN                      VARCHAR2;

  FUNCTION is_reified_quad (
    model_name              IN  VARCHAR2
  , subject                 IN  VARCHAR2
  , property                IN  VARCHAR2
  , object                  IN  VARCHAR2
  ) RETURN                      VARCHAR2;

  PROCEDURE refresh_sem_tablespace_names (
    model_name                 VARCHAR2 default NULL  -- NULL => all
  );

  PROCEDURE create_rdf_model (
    model_name          IN  VARCHAR2
  , table_name          IN  VARCHAR2
  , column_name         IN  VARCHAR2
  , model_tablespace    IN  VARCHAR2 default NULL
  );
  PROCEDURE drop_rdf_model (model_name IN VARCHAR2);

  PROCEDURE create_sem_model (
    model_name          IN  VARCHAR2
  , table_name          IN  VARCHAR2
  , column_name         IN  VARCHAR2
  , model_tablespace    IN  VARCHAR2 default NULL
  );
  PROCEDURE drop_sem_model (model_name IN VARCHAR2);

  /**
   * Note only owner of model and rules index (or sys dba) is
   * allowed to create a virtual model.
   */ 
  PROCEDURE create_virtual_model (
    vm_name 		IN VARCHAR2
  , models		IN MDSYS.RDF_Models
  , rulebases		IN MDSYS.RDF_Rulebases default NULL
  , options             IN VARCHAR2 default NULL
  );
  /**
   * Note only owner of virtual model (or sys dba) is allowed
   * to drop the virtual model.
   */
  PROCEDURE drop_virtual_model (vm_name IN VARCHAR2);

  PROCEDURE cleanup_batch_load (table_name IN VARCHAR2);
  PROCEDURE cleanup_batch_load (
    table_name                  IN  VARCHAR2
  , temp_tab_name               IN  VARCHAR2
  );

  PROCEDURE create_rdf_network (tablespace_name in varchar2);
  PROCEDURE drop_rdf_network (cascade in boolean default false);

  PROCEDURE create_sem_network (tablespace_name in varchar2);
  PROCEDURE drop_sem_network (cascade in boolean default false);

  PROCEDURE create_logical_network (model_name IN VARCHAR2);
  PROCEDURE drop_logical_network (model_name IN VARCHAR2);


  /**
   * Note only model owner (or sys dba) is allowed to perform this action. 
   * All information in the existing application table will be lost. 
   * 'Triple' column will be reconstructed.
   * A commit will be performed at the very beginning of the procedure.
   */
  PROCEDURE remove_duplicates(model_name       IN VARCHAR2,
                        threshold       IN FLOAT DEFAULT 0.3,
                        rebuild_index    IN BOOLEAN DEFAULT TRUE);

  PROCEDURE get_change_tracking_info(model_name          IN     VARCHAR2,
                                     enabled             OUT BOOLEAN,
                                     tracking_start_time OUT TIMESTAMP);

  PROCEDURE get_inc_inf_info(entailment_name     IN     VARCHAR2,
                             enabled             OUT BOOLEAN,
                             prev_inf_start_time OUT TIMESTAMP);

  PROCEDURE merge_models(source_model           IN VARCHAR2,
                         destination_model      IN VARCHAR2,
                         rebuild_index          IN BOOLEAN DEFAULT TRUE,
                         drop_source_model      IN BOOLEAN DEFAULT FALSE,
                         options                IN VARCHAR2 DEFAULT NULL,
                         update_change_tracking IN BOOLEAN DEFAULT TRUE);
                        
  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_model(
      model_name       IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );

  /**
   * Note only rules index owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.gather_table_stats makes sense here.
   */
  PROCEDURE analyze_rules_index(
      index_name IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );


  PROCEDURE analyze_entailment(
      entailment_name  IN VARCHAR2,
      estimate_percent IN NUMBER   DEFAULT DBMS_STATS.AUTO_SAMPLE_SIZE,
      method_opt       IN VARCHAR2 DEFAULT 'FOR ALL COLUMNS SIZE AUTO',
      degree           IN NUMBER   DEFAULT NULL,
      cascade          IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_CASCADE,
      no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
      force            IN BOOLEAN  DEFAULT FALSE
     );


  -- 
  -- This method will swap the names of two models and also the names of
  -- the two underlying application tables that correspond to these two models.
  -- 
  -- The input to this method are two model names. This method requires that the
  -- current session user is the owner of both models.
  --
  -- Note that a commit will be executed at the very beginning and very end of
  -- this method call.
  PROCEDURE swap_names(model1  in VARCHAR2,
                       model2  in VARCHAR2);

  PROCEDURE rename_model(old_name  in VARCHAR2,
                         new_name  in VARCHAR2);
  PROCEDURE rename_entailment(old_name  in VARCHAR2,
                              new_name  in VARCHAR2);


  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  -- PROCEDURE delete_model_stats(
  --     model_name       IN VARCHAR2,
  --     cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
  --     cascade_columns  IN BOOLEAN  DEFAULT TRUE,
  --     cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
  --     no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
  --     force            IN BOOLEAN  DEFAULT FALSE
  --    );


  /**
   * Note only model owner (or sys dba) is allowed perform this action. Not all 
   * parameters of dbms_stats.delete_table_stats makes sense here.
   */
  -- PROCEDURE delete_rules_index_stats(
  --     index_name       IN VARCHAR2,
  --     cascade_parts    IN BOOLEAN  DEFAULT TRUE, 
  --     cascade_columns  IN BOOLEAN  DEFAULT TRUE,
  --     cascade_indexes  IN BOOLEAN  DEFAULT TRUE,
  --     no_invalidate    IN BOOLEAN  DEFAULT DBMS_STATS.AUTO_INVALIDATE,
  --     force            IN BOOLEAN  DEFAULT FALSE
  --    );
  -- 

  PROCEDURE add_sem_index (
    index_code      IN  VARCHAR2
  , tablespace_name IN  VARCHAR2 default NULL
  , compression_length IN  PLS_INTEGER default NULL
  );

  PROCEDURE drop_sem_index (
    index_code      IN   VARCHAR2
  );

  PROCEDURE alter_index_on_sem_graph (
    graph_name       IN  VARCHAR2
  , index_code       IN  VARCHAR2
  , command          IN  VARCHAR2
  , parallel         IN  PLS_INTEGER default NULL
  , online           IN  BOOLEAN default FALSE
  , tablespace_name  IN  VARCHAR2 default NULL
  , is_rules_index   IN  BOOLEAN default FALSE
  , use_compression  IN  BOOLEAN default NULL
  );

  PROCEDURE refresh_sem_network_index_info (
    options             IN VARCHAR2 default NULL
  );

  PROCEDURE alter_sem_graph (
    graph_name       IN  VARCHAR2
  , command          IN  VARCHAR2
  , tablespace_name  IN  VARCHAR2 default NULL
  , parallel         IN  PLS_INTEGER default NULL
  , is_rules_index   IN  BOOLEAN default FALSE
  );

  -- Helper functions for SPARQL FILTER --
  FUNCTION isV$CLOB(
    vname            IN VARCHAR2
  ) RETURN              BOOLEAN PARALLEL_ENABLE;
  pragma restrict_references (isV$CLOB,WNDS,RNDS,WNPS);

  FUNCTION getV$LitTypeFam(
    dTypeURI         IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (getV$LitTypeFam,WNDS,RNDS,WNPS);

  FUNCTION getV$TypeFam(
    vType            IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (getV$TypeFam,WNDS,RNDS,WNPS);

  FUNCTION langMatches(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (langMatches,WNDS,RNDS,WNPS);

  FUNCTION rdfTermComp(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (rdfTermComp,WNDS,RNDS,WNPS);

  FUNCTION rdfTermEqual(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (rdfTermEqual,WNDS,RNDS,WNPS);

  FUNCTION dateTimeComp(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (dateTimeComp,WNDS,RNDS,WNPS);

  FUNCTION dateTimeEqual(
    value_type1      IN VARCHAR2
  , vname_prefix1    IN VARCHAR2
  , vname_suffix1    IN VARCHAR2
  , literal_type1    IN VARCHAR2
  , language_type1   IN VARCHAR2
  , value_type2      IN VARCHAR2
  , vname_prefix2    IN VARCHAR2
  , vname_suffix2    IN VARCHAR2
  , literal_type2    IN VARCHAR2
  , language_type2   IN VARCHAR2
  ) RETURN              NUMBER PARALLEL_ENABLE;
  pragma restrict_references (dateTimeEqual,WNDS,RNDS,WNPS);

  FUNCTION getV$EBVVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              NUMBER DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$EBVVal,WNDS,RNDS,WNPS);

  FUNCTION getV$NumericVal ( 
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              NUMBER DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$NumericVal,WNDS,RNDS,WNPS);

  FUNCTION getV$StringVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$StringVal,WNDS,RNDS,WNPS);

  FUNCTION getV$DateTimeTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DateTimeTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$DateTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DateTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$TimeTZVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              TIMESTAMP WITH TIME ZONE DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$TimeTZVal,WNDS,RNDS,WNPS);

  FUNCTION getV$BooleanVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$BooleanVal,WNDS,RNDS,WNPS);

  FUNCTION getV$DatatypeVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$DatatypeVal,WNDS,RNDS,WNPS);

  FUNCTION getV$LangVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN               VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$LangVal,WNDS,RNDS,WNPS,RNPS);

  FUNCTION getV$STRVal (
    value_type       IN VARCHAR2
  , vname_prefix     IN VARCHAR2
  , vname_suffix     IN VARCHAR2
  , literal_type     IN VARCHAR2
  , language_type    IN VARCHAR2
  ) RETURN              VARCHAR2 DETERMINISTIC PARALLEL_ENABLE;
  pragma restrict_references (getV$STRVal,WNDS,RNDS,WNPS);
  -- End SPARQL FILTER functions --


END sdo_rdf ;
/
SHOW ERRORS;

