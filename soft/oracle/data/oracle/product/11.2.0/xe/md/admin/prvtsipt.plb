declare
begin
  begin
   execute immediate 
   ' drop type f81_nt_ind_type ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type f81_index_obj_array ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type f81_index_object ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type v81_nt_ind_type ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
    ' drop type v81_index_obj_array ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type v81_index_object ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type h81_nt_ind_type ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
    ' drop type h81_index_obj_array ';
   exception when others then  NULL;
  end;

  begin
   execute immediate 
     ' drop type h81_index_object ';
   exception when others then  NULL;
  end;

end;
/
create  type F81_INDEX_OBJECT wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
4e 85
ZWUadRLrQvBrxvqwEBWjnjobioMwg5n0dLhcuAN8X/7Sx1Lw4/6/m8Ayy8y4dCupwiFTrEzk
hpBzTHGUrHZqKl9ZLioy3eavqEqoa6+eOEo1/xxKNWCrQOzsPHSmYli2sw==

/
create type F81_index_obj_array as VARRAY (1000000) of F81_index_object;
/
create type F81_nt_ind_type as table of F81_index_object;
/
create type V81_INDEX_OBJECT wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
64 89
KXdN7gcGljXRAZGUJCLju8OaEBIwg5n0dLhc5wN8X/7Sx1Lw4/6/m8Ayy8y4dCupwiFTrEzk
hpBzTHGUrHZqKl9ZLioy3ea8SvNUotaEdoTm1pQVdtZuAHbWXyF0Kio7iKY+iYjD

/
create type V81_index_obj_array as VARRAY (1000000) of V81_index_object;
/
create type V81_nt_ind_type as table of V81_index_object;
/
create type H81_INDEX_OBJECT wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
7c 96
hHM88OECsBqvYYpWLWJxIXhsWYkwg5n0dLhcpUt8X/7Sx1Lw4/6/m8Ayy8y4dCupwiFTrEzk
hlXW5IQdbnNMcZSsdmoqX1kuKjLdBlRGz6TvS+M5B1dARjnVRq05rRJXOYjeBCcno4Kmpu53
LHA=

/
create type H81_index_obj_array as VARRAY (1000000) of H81_index_object;
/
create type H81_nt_ind_type as table of H81_index_object;
/
grant execute on F81_INDEX_OBJECT to public;
grant execute on H81_INDEX_OBJECT to public;
grant execute on V81_INDEX_OBJECT to public;
grant execute on V81_index_obj_array to public;
grant execute on H81_index_obj_array to public;
grant execute on F81_index_obj_array to public;
grant execute on F81_nt_ind_type to public;
grant execute on H81_nt_ind_type to public;
grant execute on V81_nt_ind_type to public;
