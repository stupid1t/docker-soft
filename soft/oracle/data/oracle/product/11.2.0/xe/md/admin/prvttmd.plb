declare
begin  
   begin
   execute immediate 
    'CREATE SEQUENCE  SDO_TOPO_TRANSACT_SUBSEQ NOCACHE ORDER ';
   execute immediate 
   ' CREATE SEQUENCE  SDO_TOPO_TRANSACT_SEQ NOCACHE ORDER ';
   exception when others then NULL;
  end;
end;
/
declare
begin
  begin
   execute immediate 
    ' drop table SDO_TOPO_TRANSACT_DATA ';
   exception when others then NULL;
  end;
  begin
   execute immediate 
    ' drop table SDO_TOPO_MAPS ';
   exception when others then NULL;
  end;
end;
/
CREATE GLOBAL TEMPORARY TABLE MDSYS.SDO_TOPO_MAPS (
   TOPOLOGY_ID    VARCHAR2(20) );
CREATE GLOBAL TEMPORARY TABLE MDSYS.SDO_TOPO_TRANSACT_DATA (
   TOPO_SEQUENCE  NUMBER,
   TOPOLOGY_ID    VARCHAR2(20),
   TOPO_ID        NUMBER,
   TOPO_TYPE      NUMBER,
   TOPO_OP        VARCHAR2(3),
   PARENT_ID      NUMBER);
GRANT select ON MDSYS.SDO_TOPO_TRANSACT_DATA TO PUBLIC;
CREATE OR REPLACE  VIEW SDO_TOPO_TRANSACT_DATA$ 
as select TOPOLOGY_ID,TOPO_ID, TOPO_TYPE, TOPO_OP, PARENT_ID
       from SDO_TOPO_TRANSACT_DATA;
GRANT select, insert ON MDSYS.SDO_TOPO_TRANSACT_DATA$ TO PUBLIC;
create or replace public synonym SDO_TOPO_TRANSACT_DATA$ for  
     MDSYS.SDO_TOPO_TRANSACT_DATA$;
declare
begin
  begin
   execute immediate
    ' DROP table  MDSYS.SDO_TOPO_RELATION_DATA ';
   exception when others then NULL;
  end;
end;
/
CREATE GLOBAL TEMPORARY TABLE MDSYS.SDO_TOPO_RELATION_DATA (
            TG_Layer_ID Number, 
            TG_ID Number, 
            Topo_ID Number,  
            Topo_type  Number,  
            Topo_attribute VARCHAR2(100));
GRANT select,insert,delete on  MDSYS.SDO_TOPO_RELATION_DATA to public;
/* Break this into an internal package which is executed as MDSYS
  and an external package which is executed as the USER.
  Grant public execute only to the external package so 
  we have less number of interfaces to worry about at the public
  execute level.  */
Create or replace package SDO_TOPO_METADATA_INT wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
7ca 233
5XevQqq8aS50CuzGFJNkz5lnhqkwg9eN2cBqynTpmDPqS1kXRfzf1UTD5tav720bhwi1qsxL
t3V6q9opC5eX1r7qBlpnHcAdq+kKz5GazlTv1uFAEXaZEQrl07JC9MUvnmnFUQv82+bOO4TT
+Dhuzqbx9g6xnPJxLDEZI1X0EqLUldbRqyrjcU3qWngGVBS4021XtvGnI7tliomiiHlT8JHS
7IyqbZ5jJq//adNhr8B/7h0sNXqWlqkekv1HOAGzNfC/ERrEa5+MEUpgWINkGa7OQ0ry6/Pj
DhKpzxvMUWldEyRIvBLFPbD+t2MjTStjfXZwuWw8wSYAoiCnYICZTCwRmrJzc1GzsNVARe7B
GnsskinvAW4VTHZ/iTLsTFW6leltXx/S+UlFmkA32hvIel1361R2eK0XvwfnAMa3XSNUhIsX
CG2lFSNpBaChne0c6qXRvy6fBCDxFcohKRBq1tLV0G76cIkc5nf66W4aqGLolcqxV81Lru/9
NERJMTBJ8WlIOUDelQalf4eqUZdc7meMKcLmsOIkPdmwrqbZ6FU=

/
show errors;
Create or replace package SDO_TOPO_METADATA wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
1ce 134
GaaOav3q8ZU1SRMOl133XY+eixwwgwL3LkhqZ+cCmDM+VOgJ6lUj6Ygn0ODWAmvc6ry1YhiG
oqF3nZNrsxIsKHXISrR/R+nqsaGx96SZe+FA/g2mAohtZH8D+/+yWejbbF83rkhavPj182Fq
K8uDEOaUzmeMHM2BYzckmjG9qz/H3Yg+V5tqtccSmhw5lcLDPkXkaB/koIs2/TXgzwsMtQDK
6HaJZDyaBLrxpMOVvgyeetsON15fd061RgxfsqUc5odcyvD1aL91GGoNWtvpkB6CM0v9A3lU
l/63Da4PMxEwLg==

/
show errors;
Create or replace package BODY SDO_TOPO_METADATA_INT wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
3751 1081
rausdZGD/4LIgoT79zsOnaub+WIwgz39BUgFe8BPc5TPV5mhOx7UoUtU2xPhGKIFg5fx7/p5
mxRX9K6upsbRfv7TDiydato+zMK335WTHXmRJ0CCbvjI0ncoGfjvX9ElpatDOKNvKSFiSVOl
hJ2l2RY+yE+/exFXDKxm8rYhCGSXtJ3Uk4gCSH/FhiVyZY5snkJGXt1y4q0ENDNkAHc15wFT
dQG98sm3mXRXrV/3KSLOfyz93VqOhyYgHzCObHy096kXwX3jqWs47MxUylWA7RzTNNKs9Tlx
xt57bHAWq2bqvPX7ah69+bPafzsHGH/cFnoloTEIFhBGty8bLMpFIZsCyHYRHl1O3S80pPWP
1uiTq4+4pzhdBzj0ti+2uJw/WWccmx3Vko8Xr7RySpikkbUvocYpfasAsmLy8jehPHKzdVO6
EZVXTrem/aAzykOIT7U+WjghGBg0R1GzUxbrOIgGyuypGzMLyVdPlQIi8f+ePihkHWBfm+BR
QGj5s7P3W0X7lLAftekuMwzKIfS92ZFAvWhQoa8JdmmqpRyU+elfYkrb1LNznmuV/gVDnM1z
sUQjVJpiyIRZOQ/ucCtwji4bV1/PMFFOy3ThrUpSaQ49sWP4fLW5uTxPsdmBsMab+f8eUe8o
c76TJCHxSO8dh0H8ChpI50heLacWB0PGaOpNPf1RqLlREBDwkomRbgv9Ta14ApH5kecq8sBu
UjkTKzMyvOTCtKzPP+RQO2o1uTGSBSctKVGCqOXRCXLKYeJE9Pf7dcCgVCRWnxPGZUEk0d9j
nTFHLEzFptUcWTRxfosKBUQvaEiFihe//k80b6+3iMlCiV9UNn3yiDIy3jKNHZ/06jL9NEZQ
7khnongcW97nD9bdiLgr2lbLXFFQEeAoolsCqFE320+PSdRryvAXt9uiejaWXxt3kyp2UZYE
yUKLpbgUhBixqIjLoQfokd3rZ4BqVpBQOqFaTXWkZz92hl86e7cFrbcskGpbC+inonAMRVDH
bGIdRvrm3Rb6qTz1TiA4NvFHdMGFkC6wV8c+tnCMihMaPoCORGTZXaHXl++nOKHDOKfcqhVn
5UL8LxqjfCR8p7HWZeqRC3o85ARxdfb1EsTR3l2mMksvrIsK2hThW4iNcnIIucFXxT0VmlBe
tdQZ61SflIyj14F2XiLnF9vl55TlVmzUh37sz/6nZ8zTBgoJ3vK/7NZMO7nesgG3VvAFmarh
PrEOGM8uLcCLtwt7IKaVEjuX8V6l7HGW29rj3+eB5imfcwENxw8IFYXQhgnCq2xqoOzbpEjx
FXa+HraE1djiD+lJyM8BQnzUtk8365N2mYpIPwNxiQo6aqzhGVYpfc0jOkM4bAcgnCj49nhm
gnsQXiqv6TyGIOzYNfvFRN607GSydUehjVrjne+BsH5rGzg/kP4bqEBIVk6Mdpy2EZ/CVtdx
F9N7aSI3Zo9yE/mhnZp4eeuRgcYosFtRA0vpmiHihD8hcfAScth4HlHNfGIAnRrBwsy5mQ3H
oxGBgmNntfiTmHPe2Y0zziZxJQn++UqsqqMVHQahKcpO9iFnVTiIGlXAP/+fyIQMMtLNP/4f
4WtynBOV5Lnzd8edVXmvSm6C4FnIIcqPWlUwHK6Yox3gHulGI1bXryts2va7ASghI7kFCsIQ
qA6LoiHfeHcsaHvVuhvBTQ8NLmLcHpioG1GwvkNnub4RTWCGFgSUqvZICdYhaqoPjqEJoT3p
HWkSCcAFhmAd4rr0UfjW0ABBIr2umaOXbUHfRGrF4rAjFhfWf/mphkllIUoGS3cQB9rQfU4o
wyZrXh7oUUlhL4OJVWrqRGJOxYZctmzDM9hOH+VzMSO1lBi1TOV4jBtOEd5EW3XdXnRnsbxc
gQcec2cqjP9OoOQUNBfVOsTetOznF+tQICnlx3EOI/+C64vxYy5aUqrRn0PPA0IpFPpla0tK
4gBJ5cvyBUdGwmugjbKR5Pm5mOs8/7qrQoN2sRJQHwVZs6NQwdIOliVcCUnefdXMAphqI0fF
KxMKeMw+vRP4PrJSzMKsoYEDxXUp1swFUq+1DCreXoD2k96EUIMsjTaJyr7EXEtU5yz1aW1F
pgPtd4beGEFvlwPm6ZMctqNZhf+N/UId7VDusG3FMg6rqyz+yFh2ZwDUv7GnaVYD3pYKXmHX
3rmJXEk9miNBrd5jwIJa/JM03yZCcCc5B0A7rozIfncSDYr2NvUTG1dTVmi3e9XfCzvqgWo4
KtHsS985NcwjEuR+lg5DH9rlK4hM1FEdm75iWZAKaHNR86Eu17dTTJ6d1bCxCXu04ItbRLek
Q6+NWJLIdGLxFJSN9w8xCT5OyZ7p0jM2lX3qv2ls3HPXkSdIvvked+vRk+Ox2+ziTocwf/oM
JElJaQBucf8JFroGTd1YLNmBs2mFMaYh4Mn8pVeWicpFhSmtQd7/eh3/wMCIRpGkIvPXP+5f
kBH82fue5GkMSZYdQHXQrF0Dmbc/nW1+Vu0p6oykJzjokGABP2VzHj941PpNBNVBRABC/P/q
F2kJOKxYAulJftBYZgOCnSIFnJwbC6++RimMOEZqZZ1fiOUG3bd4XCGGQN1DByyvbSXQnwpg
lZS9tdsiOjfFmDGETOpYg3+Wu8DPk50ZhMS7WJHetO8TT4pHLA7Vlcgk1wL098TJqV23WhG4
RxRJvL0O7TZ2gR3tLlFy2PX2nX6m0s779pWKGkB1pn3itrxdwdzstFlCwlTVySfTPkNzGJD4
jjCRnlyX0poGFkCNwZTEyWoQ+KUtEM7bIs+j/Rvc9ihl/FzN8GXXg/0RcEP0hlbZXjzr8UV/
aoQioqaWFwTgP8hDlGdvxWBSJko6DeY0dd9f0KvulICDzndyuAlads2yzEd/pIkM2pSysjGL
4rda/PHWrwvZyoI5AZklUtsHnJGK9JhnOT+ORZfIemIZUPpZhmSV4C6vGxoJZGIb6bc+rfnT
r+nXFTwVn/72AmBy8eaTyF03M0t81BX26i1NbGmocPda7GVu+mGxLW4W9zQ1y6fbdxoRhbiK
t/j3LWqWNsCW+IGJ1MyHGjGBLDlrtx9x8aEp0mMWlcx1XvKQ/FLiHJxrEdrbBE2la+yEYJNF
TFYfxNoalD8lpHoDrM1RTLtcbE6FceZWb82D8iujQtMoeMVvG4kYEdpHHv77p1LudcT7OuBH
ahU6wJMlC0Nik2Oj7da8cw20Vsi6rceHgbMjXgDjh6QS0hUMQ8tHGS4dq30w9EFnvkQeJc/M
QXtEI7rWq8sfpPVbHgVixZyrLqSl6PdMxCrAwnT0kiQTzrQmJxYJ0VWAXMy8YT3mvt7gB9l1
0iFDkoLYs1JmxiYqgB7XpFpOi/f3RvE4QPpVoyYZCwo9gnhkGXuOE1rvP6H34pGitW7NIoTV
ThiHtgIHxE4TOzAbfyUm3OnaNFWGwemcAP+/mNdz1x9pNbKJkgKPJg+YnvpFJDVf7I44Y9Al
dAivx982QCudoaHQUUSCdhMi8R90uzZzwaQj6xdlDLBwYOXRytoAR1v3YZjv1S4ziAZmBPZa
VnjcWOToXNpQYYilXMPIRCeOtVgalciRupHdMeRdVA8+/o5gwyX8UXji5AYNuGrEJNsZ4Wwc
295KDQEtar7canME0jM99hnKmIWAYUt0XeL5yphZZBk8kDgZ60h09THq+d2gViwmkifOR19a
3I+tX5FbRosyWskJ4t8tIoI+eVZXGiQOHjTMEXh55C+Yylb3+Kp0foIPecHEmJtIvBOM/dU5
gp8ilv6SyDAQsp+nyJquzlJxqsaIEMcWsK6RAzeAcmEO/OkvExFMr0vLVLOLPuvowYUD7xyx
MHFJYcYultt/2fqXM+MpLprW7jdVLQ66OBVLBHXxxocwJfCb5NiugogsGx3Gp+kML/Nu3Eeo
UUZOA14/ZMGxlf2D4oWpeUBPDx/HyGx9oMxgrp58irGWcv+a2JPfE+ADOMvgdKT3LF8I+ckx
tKnKMjm7dd2bSqDNOR4rHfsgPs53SmKmvNrxusskXkXjjMcbNg1QN/nW1c4cNMHEnea8TNia
W2MkOwadV9PmqBDHxlOhPiB9vN1MeAp02EMmdJMobVVuotUr2DyWGlyXK814/eCxdXp7pGN7
efZ16IVKc0CIH46as6SSzFkf3cQv18R+nR8yWQD3/ZVUaK7i+VRIpdC1+8WYNzE=

/
show errors;
Create or replace package BODY SDO_TOPO_METADATA wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
358 181
5piBa9UMPWPVQkfSs+kQav1DBkAwg43rAErWfC9EjzutlYk15j5hY6uKk+BEeoW0DE1TtWKf
fC+bhFqn4YhLmvhfDjUy1PEOcFWECkKlaKVpt4DwHPsFNqvGGPW4NvUIy41Pi7gX+qTUOJUB
KNQVlnsy5v6QbEsS243dr2TVymCj2Cm5P8WhYOdpad6KBLoeYSDQrlCvVKpo7gEIZZhhGr5G
9z06zTMO4HeQLeyYzRA3nXRggGuyMjXR8EXQuP0xxxkRQhpGYG8lZQiNyVZYWiVnQ4wX0E5R
yrqCxFTlZt6AMlH+Gqqda/ClLv+uS5f9YcqCPUKe8bxqZS9QaJCp7yxonjRqR3IGxkUeRTuT
o+J2rQC1eDqxLIyNBq0=

/
show errors;
grant execute on sdo_topo_metadata to public;
declare
begin
  begin   
   execute immediate 
     ' drop  table SDO_TOPO_DATA$ ';
   exception when others then NULL;
  end;
end;
/
CREATE GLOBAL TEMPORARY TABLE MDSYS.SDO_TOPO_DATA$ (
   TOPOLOGY          VARCHAR2(20),
   TG_LAYER_ID  NUMBER,
   TG_ID        NUMBER,
   TOPO_ID           NUMBER,
   TOPO_TYPE         NUMBER);
GRANT select, insert ON MDSYS.SDO_TOPO_DATA$ TO PUBLIC;
create or replace public synonym SDO_TOPO_DATA$ for  MDSYS.SDO_TOPO_DATA$;
CREATE OR REPLACE  TRIGGER SDO_TOPO_TRIG_INS1
INSTEAD OF INSERT ON SDO_TOPO_TRANSACT_DATA$
REFERENCING NEW AS n
FOR EACH ROW
declare
  user_name varchar2(32);
  topo_name varchar2(32);
  tname varchar2(100);
begin

   
   







   INSERT INTO SDO_TOPO_TRANSACT_DATA 
      values(SDO_TOPO_TRANSACT_SUBSEQ.nextval, :n.topology_id, :n.topo_id,
                :n.topo_type, :n.topo_op, :n.parent_id);

end;
/
show errors;
