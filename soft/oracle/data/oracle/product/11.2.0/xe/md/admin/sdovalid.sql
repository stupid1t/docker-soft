Rem
Rem $Header: sdo/admin/sdovalid.sql /main/4 2009/02/05 15:09:29 sravada Exp $
Rem
Rem sdovalid.sql
Rem
Rem Copyright (c) 2003, 2009, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdovalid.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     02/05/09 - fix how we check the valid state of SDO
Rem    sravada     10/18/04 - 
Rem    sravada     07/30/04 - change the validate procedure 
Rem    sravada     02/05/03 - sravada_catmdcnt_dif_3
Rem    sravada     02/05/03 - Created
Rem


CREATE OR REPLACE PROCEDURE  SYS.validate_sdo IS
cnt number;  
BEGIN
    select count(*) into cnt from dba_invalid_objects where status = 'INVALID'
           and OWNER='MDSYS';
          
     if cnt > 0 then
       dbms_registry.invalid('SDO');
       return;
     end if;
 
    dbms_registry.valid('SDO');
END;
/

show errors;

