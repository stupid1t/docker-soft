Rem
Rem $Header: sdo/admin/sdogmlsc.sql /main/2 2009/12/02 05:47:49 mhorhamm Exp $
Rem
Rem sdogmlsc.sql
Rem
Rem Copyright (c) 2005, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdogmlsc.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      This file is used to register the 3 schemas required for GML 212
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     05/12/05 - sravada_sdo_text_object
Rem    sravada     05/03/05 - Created
Rem

declare
begin
  begin
   execute immediate
'CREATE TABLE SDO_XML_SCHEMAS
(
   id               NUMBER PRIMARY KEY,
   description      VARCHAR2(300),
   xmlSchema        CLOB) ';
  exception when others then NULL;
 end;
end;
/
                                                                                
GRANT SELECT ON SDO_XML_SCHEMAS TO PUBLIC;
create or replace public synonym SDO_XML_SCHEMAS
for MDSYS.SDO_XML_SCHEMAS;

-- first delete all the GML related schemas
declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.opengis.net/cartographicText.xsd';

  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;

  url := 'http://www.opengis.net/gml/feature.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;

  url := 'http://www.opengis.net/gml/geometry.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;

  url := 'http://www.w3.org/1999/xlink/xlinks.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
end;
/
 
  -- register the schema
DECLARE
   schemaclob   CLOB;
   amt          NUMBER;
   buf          VARCHAR2(32767);
   pos          NUMBER;
BEGIN
 
   DELETE FROM SDO_XML_SCHEMAS WHERE id=1;
   INSERT INTO SDO_XML_SCHEMAS VALUES (1, 'GML:2.1.2  xlinks.xsd', empty_clob())
     RETURNING xmlSchema into schemaclob;
   SELECT xmlSchema into schemaclob from SDO_XML_SCHEMAS
     WHERE id = 1 FOR UPDATE;
 
   DBMS_LOB.OPEN(schemaclob, DBMS_LOB.LOB_READWRITE);
 
   buf := '<?xml version="1.0" encoding="UTF-8"?>
<schema targetNamespace="http://www.w3.org/1999/xlink" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2001/XMLSchema" version="2.1.2">
  <annotation>
    <appinfo>xlinks.xsd v2.1.2 2002-07</appinfo>
    <documentation xml:lang="en">
      This schema provides the XLink attributes for general use.
    </documentation>
  </annotation>
  <!-- ==============================================================
       global declarations
  =============================================================== -->
  <!-- locator attribute -->
  <attribute name="href" type="anyURI"/>
  <!-- semantic attributes -->
  <attribute name="role" type="anyURI"/>
  <attribute name="arcrole" type="anyURI"/>
  <attribute name="title" type="string"/>
  <!-- behavior attributes -->
  <attribute name="show">
    <annotation>
      <documentation>
        The show attribute is used to communicate the desired presentation 
        of the ending resource on traversal from the starting resource; its 
        value should be treated as follows: 
        new - load ending resource in a new window, frame, pane, or other 
              presentation context
        replace - load the resource in the same window, frame, pane, or 
                  other presentation context
        embed - load ending resource in place of the presentation of the 
                starting resource
        other - behavior is unconstrained; examine other markup in the 
                link for hints 
        none - behavior is unconstrained 
      </documentation>
    </annotation>
    <simpleType>
      <restriction base="string">
        <enumeration value="new"/>
        <enumeration value="replace"/>
        <enumeration value="embed"/>
        <enumeration value="other"/>
        <enumeration value="none"/>
      </restriction>
    </simpleType>
  </attribute>
  <attribute name="actuate">
    <annotation>
      <documentation>
        The actuate attribute is used to communicate the desired timing 
        of traversal from the starting resource to the ending resource; 
        its value should be treated as follows:
        onLoad - traverse to the ending resource immediately on loading 
                 the starting resource 
        onRequest - traverse from the starting resource to the ending 
                    resource only on a post-loading event triggered for 
                    this purpose 
        other - behavior is unconstrained; examine other markup in link 
                for hints 
        none - behavior is unconstrained
      </documentation>
    </annotation>
    <simpleType>
      <restriction base="string">
        <enumeration value="onLoad"/>
        <enumeration value="onRequest"/>
        <enumeration value="other"/>
        <enumeration value="none"/>
      </restriction>
    </simpleType>
  </attribute>
  <!-- traversal attributes -->
  <attribute name="label" type="string"/>
  <attribute name="from" type="string"/>
  <attribute name="to" type="string"/>
  <!-- ==============================================================
       Attributes grouped by XLink type, as specified by the allowed usage patterns 
       in sec. 4.1 of the W3C Recommendation (dated 2001-06-27)
  =================================================================== -->
  <attributeGroup name="simpleLink">
    <attribute name="type" type="string" use="optional" fixed="simple" form="qualified"/>
    <attribute ref="xlink:href" use="optional"/>
    <attribute ref="xlink:role" use="optional"/>
    <attribute ref="xlink:arcrole" use="optional"/>
    <attribute ref="xlink:title" use="optional"/>
    <attribute ref="xlink:show" use="optional"/>
    <attribute ref="xlink:actuate" use="optional"/>
  </attributeGroup>
  <attributeGroup name="extendedLink">
    <attribute name="type" type="string" use="required" fixed="extended" form="qualified"/>
    <attribute ref="xlink:role" use="optional"/>
    <attribute ref="xlink:title" use="optional"/>
  </attributeGroup>
  <attributeGroup name="locatorLink">
    <attribute name="type" type="string" use="required" fixed="locator" form="qualified"/>
    <attribute ref="xlink:href" use="required"/>
    <attribute ref="xlink:role" use="optional"/>
    <attribute ref="xlink:title" use="optional"/>
    <attribute ref="xlink:label" use="optional"/>
  </attributeGroup>
  <attributeGroup name="arcLink">
    <attribute name="type" type="string" use="required" fixed="arc" form="qualified"/>
    <attribute ref="xlink:arcrole" use="optional"/>
    <attribute ref="xlink:title" use="optional"/>
    <attribute ref="xlink:show" use="optional"/>
    <attribute ref="xlink:actuate" use="optional"/>
    <attribute ref="xlink:from" use="optional"/>
    <attribute ref="xlink:to" use="optional"/>
  </attributeGroup>
  <attributeGroup name="resourceLink">
    <attribute name="type" type="string" use="required" fixed="resource" form="qualified"/>
    <attribute ref="xlink:role" use="optional"/>
    <attribute ref="xlink:title" use="optional"/>
    <attribute ref="xlink:label" use="optional"/>
  </attributeGroup>
  <attributeGroup name="titleLink">
    <attribute name="type" type="string" use="required" fixed="title" form="qualified"/>
  </attributeGroup>
  <attributeGroup name="emptyLink">
    <attribute name="type" type="string" use="required" fixed="empty" form="qualified"/>
  </attributeGroup>
</schema> ';
   amt := length(buf);
   pos := 1;
 
   DBMS_LOB.WRITE(schemaclob, amt, pos, buf);
 
   DBMS_LOB.CLOSE(schemaclob);
 
   COMMIT;
 
END;
/
SHOW ERRORS;


declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.w3.org/1999/xlink/xlinks.xsd';
 
  -- First check whether the schema has been registered already.
  -- If it has, de-register it. This might cause an error to be raised if
  -- there are dependent tables or schemas. User then needs to manually
  -- remove/evolve the depdendent objects and run this script again.
 
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  select xmlSchema into loc from SDO_XML_SCHEMAS
   where id = 1; 
 
  -- register the schema
  dbms_xmlschema.registerSchema(url,
                 loc, FALSE, FALSE, FALSE, FALSE, FALSE, usr);
 
end;
/



DECLARE
   schemaclob   CLOB;
   amt          NUMBER;
   buf          VARCHAR2(32767);
   pos          NUMBER;
BEGIN
 
   DELETE FROM SDO_XML_SCHEMAS WHERE id=2;
   INSERT INTO SDO_XML_SCHEMAS VALUES (2, 'GML:2.1.2 geometry.xsd', empty_clob())
     RETURNING xmlSchema into schemaclob;
   SELECT xmlSchema into schemaclob from SDO_XML_SCHEMAS
     WHERE id = 2 FOR UPDATE;
 
   DBMS_LOB.OPEN(schemaclob, DBMS_LOB.LOB_READWRITE);
 
   buf := '<?xml version="1.0" encoding="UTF-8"?>
<schema targetNamespace="http://www.opengis.net/gml" 
 xmlns:xlink="http://www.w3.org/1999/xlink" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.w3.org/1999/xlink
         http://www.w3.org/1999/xlink/xlinks.xsd"
 xmlns:gml="http://www.opengis.net/gml" 
 xmlns="http://www.w3.org/2001/XMLSchema" 
  elementFormDefault="qualified" version="2.1.2">
  <annotation>
    <appinfo>geometry.xsd v2.1.2 2002-07</appinfo>
    <documentation xml:lang="en">
      GML Geometry schema. Copyright (c) 2001,2002 OGC, All Rights Reserved.
    </documentation>
  </annotation>
  <!-- bring in the XLink attributes -->
 
  <import namespace="http://www.w3.org/1999/xlink" schemaLocation="http://www.w3.org/1999/xlink/xlinks.xsd"/>

  <!-- ==============================================================
       global declarations
  =================================================================== -->
  <element name="_Geometry" type="gml:AbstractGeometryType" abstract="true"/>
  <element name="_GeometryCollection" type="gml:GeometryCollectionType" abstract="true" substitutionGroup="gml:_Geometry"/>
  <element name="geometryMember" type="gml:GeometryAssociationType"/>
  <element name="pointMember" type="gml:PointMemberType" substitutionGroup="gml:geometryMember"/>
  <element name="lineStringMember" type="gml:LineStringMemberType" substitutionGroup="gml:geometryMember"/>
  <element name="polygonMember" type="gml:PolygonMemberType" substitutionGroup="gml:geometryMember"/>
  <element name="outerBoundaryIs" type="gml:LinearRingMemberType"/>
  <element name="innerBoundaryIs" type="gml:LinearRingMemberType"/>
  <!-- primitive geometry elements -->
  <element name="Point" type="gml:PointType" substitutionGroup="gml:_Geometry"/>
  <element name="LineString" type="gml:LineStringType" substitutionGroup="gml:_Geometry"/>
  <element name="LinearRing" type="gml:LinearRingType" substitutionGroup="gml:_Geometry"/>
  <element name="Polygon" type="gml:PolygonType" substitutionGroup="gml:_Geometry"/>
  <element name="Box" type="gml:BoxType"/>
  <!-- aggregate geometry elements -->
  <element name="MultiGeometry" type="gml:GeometryCollectionType" substitutionGroup="gml:_Geometry"/>
  <element name="MultiPoint" type="gml:MultiPointType" substitutionGroup="gml:_Geometry"/>
  <element name="MultiLineString" type="gml:MultiLineStringType" substitutionGroup="gml:_Geometry"/>
  <element name="MultiPolygon" type="gml:MultiPolygonType" substitutionGroup="gml:_Geometry"/>
  <!-- coordinate elements -->
  <element name="coord" type="gml:CoordType"/>
  <element name="coordinates" type="gml:CoordinatesType"/>
  <!-- this attribute gives the location where an element is defined -->
  <attribute name="remoteSchema" type="anyURI"/>
  <!-- ==============================================================
       abstract supertypes
  =================================================================== -->
  <complexType name="AbstractGeometryType" abstract="true">
    <annotation>
      <documentation>
        All geometry elements are derived from this abstract supertype; 
        a geometry element may have an identifying attribute (gid). 
        It may be associated with a spatial reference system.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="anyType">
        <attribute name="gid" type="ID" use="optional"/>
        <attribute name="srsName" type="anyURI" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="AbstractGeometryCollectionBaseType" abstract="true">
    <annotation>
      <documentation>
        This abstract base type for geometry collections just makes the 
        srsName attribute mandatory.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:AbstractGeometryType">
        <attribute name="gid" type="ID" use="optional"/>
        <attribute name="srsName" type="anyURI" use="required"/>
      </restriction>
    </complexContent>
  </complexType>
  <attributeGroup name="AssociationAttributeGroup">
    <annotation>
      <documentation>
        These attributes can be attached to any element, thus allowing it 
        to act as a pointer. The remoteSchema attribute allows an element 
        that carries link attributes to indicate that the element is declared 
        in a remote schema rather than by the schema that constrains the 
        current document instance.  
      </documentation>
    </annotation>
    <attributeGroup ref="xlink:simpleLink"/>
    <attribute ref="gml:remoteSchema" use="optional"/>
  </attributeGroup>
  <complexType name="GeometryAssociationType">
    <annotation>
      <documentation>
        An instance of this type (e.g. a geometryMember) can either 
        enclose or point to a primitive geometry element. When serving 
        as a simple link that references a remote geometry instance, 
        the value of the gml:remoteSchema attribute can be used to 
        locate a schema fragment that constrains the target instance.
      </documentation>
    </annotation>
    <sequence minOccurs="0">
      <element ref="gml:_Geometry"/>
    </sequence>
    <!-- <attributeGroup ref="gml:AssociationAttributeGroup"/> -->
    <attributeGroup ref="xlink:simpleLink"/>
    <attribute ref="gml:remoteSchema" use="optional"/>
  </complexType>
  <complexType name="PointMemberType">
    <annotation>
      <documentation>Restricts the geometry member to being a Point instance.</documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:Point"/>
        </sequence>
        <attributeGroup ref="gml:AssociationAttributeGroup"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="LineStringMemberType">
    <annotation>
      <documentation>Restricts the geometry member to being a LineString instance.</documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:LineString"/>
        </sequence>
        <attributeGroup ref="gml:AssociationAttributeGroup"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="PolygonMemberType">
    <annotation>
      <documentation>Restricts the geometry member to being a Polygon instance.</documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:Polygon"/>
        </sequence>
        <attributeGroup ref="gml:AssociationAttributeGroup"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="LinearRingMemberType">
    <annotation>
      <documentation>Restricts the outer or inner boundary of a polygon instance 
			to being a LinearRing.</documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:LinearRing"/>
        </sequence>
        <attributeGroup ref="gml:AssociationAttributeGroup"/>
      </restriction>
    </complexContent>
  </complexType>
  <!-- ==============================================================
       primitive geometry types
  =================================================================== -->
  <complexType name="PointType">
    <annotation>
      <documentation>
        A Point is defined by a single coordinate tuple.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryType">
        <sequence>
          <choice>
            <element ref="gml:coord"/>
            <element ref="gml:coordinates"/>
          </choice>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="LineStringType">
    <annotation>
      <documentation>
        A LineString is defined by two or more coordinate tuples, with 
        linear interpolation between them. 
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryType">
        <sequence>
          <choice>
            <element ref="gml:coord" minOccurs="2" maxOccurs="unbounded"/>
            <element ref="gml:coordinates"/>
          </choice>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="LinearRingType">
    <annotation>
      <documentation>
        A LinearRing is defined by four or more coordinate tuples, with 
        linear interpolation between them; the first and last coordinates 
        must be coincident.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryType">
        <sequence>
          <choice>
            <element ref="gml:coord" minOccurs="4" maxOccurs="unbounded"/>
            <element ref="gml:coordinates"/>
          </choice>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="BoxType">
    <annotation>
      <documentation>
        The Box structure defines an extent using a pair of coordinate tuples.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryType">
        <sequence>
          <choice>
            <element ref="gml:coord" minOccurs="2" maxOccurs="2"/>
            <element ref="gml:coordinates"/>
          </choice>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="PolygonType">
    <annotation>
      <documentation>
        A Polygon is defined by an outer boundary and zero or more inner 
        boundaries which are in turn defined by LinearRings.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryType">
        <sequence>
          <element ref="gml:outerBoundaryIs"/>
          <element ref="gml:innerBoundaryIs" minOccurs="0" maxOccurs="unbounded"/>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <!-- ==============================================================
       aggregate geometry types
  =================================================================== -->
  <complexType name="GeometryCollectionType">
    <annotation>
      <documentation>
        A geometry collection must include one or more geometries, referenced 
        through geometryMember elements. User-defined geometry collections 
        that accept GML geometry classes as members must instantiate--or 
        derive from--this type.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractGeometryCollectionBaseType">
        <sequence>
          <element ref="gml:geometryMember" maxOccurs="unbounded"/>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="MultiPointType">
    <annotation>
      <documentation>
        A MultiPoint is defined by one or more Points, referenced through 
        pointMember elements.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryCollectionType">
        <sequence>
          <element ref="gml:pointMember" maxOccurs="unbounded"/>
        </sequence>
        <attribute name="gid" type="ID" use="optional"/>
        <attribute name="srsName" type="anyURI" use="required"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiLineStringType">
    <annotation>
      <documentation>
        A MultiLineString is defined by one or more LineStrings, referenced 
        through lineStringMember elements.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryCollectionType">
        <sequence>
          <element ref="gml:lineStringMember" maxOccurs="unbounded"/>
        </sequence>
        <attribute name="gid" type="ID" use="optional"/>
        <attribute name="srsName" type="anyURI" use="required"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiPolygonType">
    <annotation>
      <documentation>
        A MultiPolygon is defined by one or more Polygons, referenced through 
        polygonMember elements. 
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryCollectionType">
        <sequence>
          <element ref="gml:polygonMember" maxOccurs="unbounded"/>
        </sequence>
        <attribute name="gid" type="ID" use="optional"/>
        <attribute name="srsName" type="anyURI" use="required"/>
      </restriction>
    </complexContent>
  </complexType>
  <!-- ==============================================================
       There are two ways to represent coordinates: (1) as a sequence 
       of <coord> elements that encapsulate tuples, or (2) using a 
       single <coordinates> string.
  =================================================================== -->
  <complexType name="CoordType">
    <annotation>
      <documentation>
        Represents a coordinate tuple in one, two, or three dimensions.
      </documentation>
    </annotation>
    <sequence>
      <element name="X" type="decimal"/>
      <element name="Y" type="decimal" minOccurs="0"/>
      <element name="Z" type="decimal" minOccurs="0"/>
    </sequence>
  </complexType>
  <complexType name="CoordinatesType">
    <annotation>
      <documentation>
        Coordinates can be included in a single string, but there is no 
        facility for validating string content. The value of the cs attribute 
        is the separator for coordinate values, and the value of the ts 
        attribute gives the tuple separator (a single space by default); the 
        default values may be changed to reflect local usage.
      </documentation>
    </annotation>
    <simpleContent>
      <extension base="string">
        <attribute name="decimal" type="string" use="optional" default="."/>
        <attribute name="cs" type="string" use="optional" default=","/>
        <attribute name="ts" type="string" use="optional" default="&#x20;"/>
      </extension>
    </simpleContent>
  </complexType>
</schema> ';
   amt := length(buf);
   pos := 1;
 
   DBMS_LOB.WRITE(schemaclob, amt, pos, buf);
 
   DBMS_LOB.CLOSE(schemaclob);
 
   COMMIT;
 
END;
/
SHOW ERRORS;


declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.opengis.net/gml/geometry.xsd';
 
  -- First check whether the schema has been registered already.
  -- If it has, de-register it. This might cause an error to be raised if
  -- there are dependent tables or schemas. User then needs to manually
  -- remove/evolve the depdendent objects and run this script again.
 
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  select xmlSchema into loc from SDO_XML_SCHEMAS
   where id = 2; 
 
  -- register the schema
  dbms_xmlschema.registerSchema(url,
                 loc, FALSE, FALSE, FALSE, FALSE, FALSE, usr);
 
end;
/

DECLARE
   schemaclob   CLOB;
   amt          NUMBER;
   buf          VARCHAR2(32767);
   pos          NUMBER;
BEGIN
 
   DELETE FROM SDO_XML_SCHEMAS WHERE id=3;
   INSERT INTO SDO_XML_SCHEMAS VALUES (3,  'GML:2.1.2 feature.xsd', empty_clob())
     RETURNING xmlSchema into schemaclob;
   SELECT xmlSchema into schemaclob from SDO_XML_SCHEMAS
     WHERE id = 3 FOR UPDATE;
 
   DBMS_LOB.OPEN(schemaclob, DBMS_LOB.LOB_READWRITE);
 
   buf := '<?xml version="1.0" encoding="UTF-8"?>
<schema targetNamespace="http://www.opengis.net/gml"
 xmlns="http://www.w3.org/2001/XMLSchema"
 xmlns:gml="http://www.opengis.net/gml" 
 xmlns:xlink="http://www.w3.org/1999/xlink" 
  elementFormDefault="qualified" version="2.1.2">
  <annotation>
    <appinfo>feature.xsd v2.1.2 2002-07</appinfo>
    <documentation xml:lang="en">
      GML Feature schema. Copyright (c) 2002 OGC, All Rights Reserved.
    </documentation>
  </annotation>
  <!-- include constructs from the GML Geometry schema -->
  <include schemaLocation="http://www.opengis.net/gml/geometry.xsd"/>
  <!-- bring in the XLink namespace -->
 <import namespace="http://www.w3.org/1999/xlink" schemaLocation="http://www.w3.org/1999/xlink/xlinks.xsd"/>

  
  <!-- ==============================================================
       global declarations
  =================================================================== -->
  <element name="_Feature" type="gml:AbstractFeatureType" abstract="true"/>
  <element name="_FeatureCollection" type="gml:AbstractFeatureCollectionType" abstract="true" substitutionGroup="gml:_Feature"/>
  <element name="featureMember" type="gml:FeatureAssociationType"/>
  <!-- some basic geometric properties of features -->
  <element name="_geometryProperty" type="gml:GeometryAssociationType" abstract="true"/>
  <element name="geometryProperty" type="gml:GeometryAssociationType"/>
  <element name="boundedBy" type="gml:BoundingShapeType"/>
  <element name="pointProperty" type="gml:PointPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="polygonProperty" type="gml:PolygonPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="lineStringProperty" type="gml:LineStringPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="multiPointProperty" type="gml:MultiPointPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="multiLineStringProperty" type="gml:MultiLineStringPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="multiPolygonProperty" type="gml:MultiPolygonPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <element name="multiGeometryProperty" type="gml:MultiGeometryPropertyType" substitutionGroup="gml:_geometryProperty"/>
  <!-- common aliases for geometry properties -->
  <element name="location" type="gml:PointPropertyType" substitutionGroup="gml:pointProperty"/>
  <element name="centerOf" type="gml:PointPropertyType" substitutionGroup="gml:pointProperty"/>
  <element name="position" type="gml:PointPropertyType" substitutionGroup="gml:pointProperty"/>
  <element name="extentOf" type="gml:PolygonPropertyType" substitutionGroup="gml:polygonProperty"/>
  <element name="coverage" type="gml:PolygonPropertyType" substitutionGroup="gml:polygonProperty"/>
  <element name="edgeOf" type="gml:LineStringPropertyType" substitutionGroup="gml:lineStringProperty"/>
  <element name="centerLineOf" type="gml:LineStringPropertyType" substitutionGroup="gml:lineStringProperty"/>
  <element name="multiLocation" type="gml:MultiPointPropertyType" substitutionGroup="gml:multiPointProperty"/>
  <element name="multiCenterOf" type="gml:MultiPointPropertyType" substitutionGroup="gml:multiPointProperty"/>
  <element name="multiPosition" type="gml:MultiPointPropertyType" substitutionGroup="gml:multiPointProperty"/>
  <element name="multiCenterLineOf" type="gml:MultiLineStringPropertyType" substitutionGroup="gml:multiLineStringProperty"/>
  <element name="multiEdgeOf" type="gml:MultiLineStringPropertyType" substitutionGroup="gml:multiLineStringProperty"/>
  <element name="multiCoverage" type="gml:MultiPolygonPropertyType" substitutionGroup="gml:multiPolygonProperty"/>
  <element name="multiExtentOf" type="gml:MultiPolygonPropertyType" substitutionGroup="gml:multiPolygonProperty"/>
  <!-- common feature descriptors -->
  <element name="description" type="string"/>
  <element name="name" type="string"/>
  <!-- ==============================================================
       abstract supertypes
  =================================================================== -->
  <complexType name="AbstractFeatureType" abstract="true">
    <annotation>
      <documentation>
        An abstract feature provides a set of common properties. A concrete 
        feature type must derive from this type and specify additional 
        properties in an application schema. A feature may optionally 
        possess an identifying attribute (fid).
      </documentation>
    </annotation>
    <sequence>
      <element ref="gml:description" minOccurs="0"/>
      <element ref="gml:name" minOccurs="0"/>
      <element ref="gml:boundedBy" minOccurs="0"/>
      <!-- additional properties must be specified in an application schema -->
    </sequence>
    <attribute name="fid" type="ID" use="optional"/>
  </complexType>
  <complexType name="AbstractFeatureCollectionBaseType" abstract="true">
    <annotation>
      <documentation>
        This abstract base type just makes the boundedBy element mandatory 
        for a feature collection.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:AbstractFeatureType">
        <sequence>
          <element ref="gml:description" minOccurs="0"/>
          <element ref="gml:name" minOccurs="0"/>
          <element ref="gml:boundedBy"/>
        </sequence>
        <attribute name="fid" type="ID" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="AbstractFeatureCollectionType" abstract="true">
    <annotation>
      <documentation>
        A feature collection contains zero or more featureMember elements.
      </documentation>
    </annotation>
    <complexContent>
      <extension base="gml:AbstractFeatureCollectionBaseType">
        <sequence>
          <element ref="gml:featureMember" minOccurs="0" maxOccurs="unbounded"/>
        </sequence>
      </extension>
    </complexContent>
  </complexType>
  <complexType name="GeometryPropertyType">
		<annotation>
			<documentation>
        A simple geometry property encapsulates a geometry element.
        Alternatively, it can function as a pointer (simple-type link) 
        that refers to a remote geometry element.
      </documentation>
		</annotation>
		<sequence minOccurs="0">
			<element ref="gml:_Geometry"/>
		</sequence>
		<attributeGroup ref="xlink:simpleLink"/>
             <attribute ref="gml:remoteSchema" use="optional"/>
  </complexType>
  <complexType name="FeatureAssociationType">
    <annotation>
      <documentation>
        An instance of this type (e.g. a featureMember) can either 
        enclose or point to a feature (or feature collection); this 
        type can be restricted in an application schema to allow only 
        specified features as valid participants in the association. 
        When serving as a simple link that references a remote feature 
        instance, the value of the gml:remoteSchema attribute can be 
        used to locate a schema fragment that constrains the target 
        instance.
      </documentation>
    </annotation>
    <sequence minOccurs="0">
      <element ref="gml:_Feature"/>
    </sequence>
    <attributeGroup ref="xlink:simpleLink"/>
    <attribute ref="gml:remoteSchema" use="optional"/>
  </complexType>
  <complexType name="BoundingShapeType">
    <annotation>
      <documentation>
        Bounding shapes--a Box or a null element are currently allowed.
      </documentation>
    </annotation>
    <sequence>
      <choice>
        <element ref="gml:Box"/>
        <element name="null" type="gml:NullType"/>
      </choice>
    </sequence>
  </complexType>
  <!-- ==============================================================
       geometry properties
  =================================================================== -->
  <complexType name="PointPropertyType">
    <annotation>
      <documentation>
        Encapsulates a single point to represent position, location, or 
        centerOf properties.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:Point"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="PolygonPropertyType">
    <annotation>
      <documentation>
        Encapsulates a single polygon to represent coverage or extentOf 
        properties.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:Polygon"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="LineStringPropertyType">
    <annotation>
      <documentation>
        Encapsulates a single LineString to represent centerLineOf or 
        edgeOf properties.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:LineString"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiPointPropertyType">
    <annotation>
      <documentation>
        Encapsulates a MultiPoint element to represent the following 
        discontiguous geometric properties: multiLocation, multiPosition, 
        multiCenterOf.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:MultiPoint"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiLineStringPropertyType">
    <annotation>
      <documentation>
        Encapsulates a MultiLineString element to represent the following 
        discontiguous geometric properties: multiEdgeOf, multiCenterLineOf.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:MultiLineString"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiPolygonPropertyType">
    <annotation>
      <documentation>
        Encapsulates a MultiPolygon to represent the following discontiguous 
        geometric properties: multiCoverage, multiExtentOf.
      </documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:MultiPolygon"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <complexType name="MultiGeometryPropertyType">
    <annotation>
      <documentation>Encapsulates a MultiGeometry element.</documentation>
    </annotation>
    <complexContent>
      <restriction base="gml:GeometryAssociationType">
        <sequence minOccurs="0">
          <element ref="gml:MultiGeometry"/>
        </sequence>
        <attributeGroup ref="xlink:simpleLink"/>
        <attribute ref="gml:remoteSchema" use="optional"/>
      </restriction>
    </complexContent>
  </complexType>
  <simpleType name="NullType">
    <annotation>
      <documentation>
        If a bounding shape is not provided for a feature collection, 
        explain why. Allowable values are:
        innapplicable - the features do not have geometry
        unknown - the boundingBox cannot be computed
        unavailable - there may be a boundingBox but it is not divulged
        missing - there are no features
      </documentation>
    </annotation>
    <restriction base="string">
      <enumeration value="inapplicable"/>
      <enumeration value="unknown"/>
      <enumeration value="unavailable"/>
      <enumeration value="missing"/>
    </restriction>
  </simpleType>
</schema> ';
   amt := length(buf);
   pos := 1;
 
   DBMS_LOB.WRITE(schemaclob, amt, pos, buf);
 
   DBMS_LOB.CLOSE(schemaclob);
 
   COMMIT;
 
END;
/
SHOW ERRORS;


declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.opengis.net/gml/feature.xsd';
 
  -- First check whether the schema has been registered already.
  -- If it has, de-register it. This might cause an error to be raised if
  -- there are dependent tables or schemas. User then needs to manually
  -- remove/evolve the depdendent objects and run this script again.
 
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  select xmlSchema into loc from SDO_XML_SCHEMAS
   where id = 3; 
 
  -- register the schema
  dbms_xmlschema.registerSchema(url,
                 loc, FALSE, FALSE, FALSE, FALSE, FALSE, usr);
 
end;
/

commit;

-- register the schema
DECLARE
  schemaclob CLOB;
  amt        NUMBER;
  buf        VARCHAR2(32767);
  pos        NUMBER;
BEGIN
  DELETE FROM SDO_XML_SCHEMAS WHERE id=4;
  INSERT INTO SDO_XML_SCHEMAS VALUES (4, 'EPSG  sdoepsggrid.xsd', empty_clob())
    RETURNING xmlSchema into schemaclob;
  SELECT xmlSchema into schemaclob from SDO_XML_SCHEMAS
    WHERE id = 4 FOR UPDATE;

  DBMS_LOB.OPEN(schemaclob, DBMS_LOB.LOB_READWRITE);

  buf := '<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 4 U (http://www.xmlspy.com) by MIKE HORHAMMER (ORACLE CORPORATION) -->
<xs:schema targetNamespace="http://www.oracle.com/2004/spatial/epsg/gridfile/schema" elementFormDefault="qualified" attributeFormDefault="unqualified" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.oracle.com/2004/spatial/epsg/gridfile/schema">
	<xs:element name="GridFile">
		<xs:annotation>
			<xs:documentation>Root element describing the grid or set of grids used for a particular transformation</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="GridFileType">
					<xs:attributeGroup ref="DateAttributeGroup"/>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="GridFileType">
		<xs:annotation>
			<xs:documentation>Root element describing the grid or set of grids used for a particular transformation</xs:documentation>
		</xs:annotation>
		<xs:all>
			<xs:element name="Grids">
				<xs:annotation>
					<xs:documentation>This represents a flat list of grids. Hierarchies can be imposed on the grids in a separate structure within each Grid.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="Grid" maxOccurs="unbounded">
							<xs:annotation>
								<xs:documentation>Represents a single grid, of which a grid file might have several</xs:documentation>
							</xs:annotation>
							<xs:complexType>
								<xs:complexContent>
									<xs:extension base="GridType">
										<xs:attribute name="gridName" type="xs:string" use="optional"/>
										<xs:attribute name="id" type="xs:ID" use="optional"/>
										<xs:attributeGroup ref="DateAttributeGroup"/>
									</xs:extension>
								</xs:complexContent>
							</xs:complexType>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:all>
	</xs:complexType>
	<xs:complexType name="CoordinatesType">
		<xs:annotation>
			<xs:documentation>Combines several coordinates (usually 2) to one vector</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Coordinate" type="SingleCoordinateType" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Coordinate (based on source coordinate reference system)</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GridType">
		<xs:annotation>
			<xs:documentation>Represents a single grid, of which a grid file might have several</xs:documentation>
		</xs:annotation>
		<xs:all>
			<xs:element name="GridProperties" type="GridPropertiesType">
				<xs:annotation>
					<xs:documentation>Grid position, size, and resolution/density</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="GridData" type="GridDataType">
				<xs:annotation>
					<xs:documentation>The grid offset data</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="HierarchyParticipation">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="Parent" type="ParentType" minOccurs="0" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:all>
	</xs:complexType>
	<xs:complexType name="GridNodeType">
		<xs:annotation>
			<xs:documentation>One single node in the grid, describing the offset at on point</xs:documentation>
		</xs:annotation>
		<xs:all>
			<xs:element name="Offset" type="CoordinatesType">
				<xs:annotation>
					<xs:documentation>Coordinate offset</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OffsetPrecision" type="CoordinatesType">
				<xs:annotation>
					<xs:documentation>This represents the precision of the offset. The precision is represented as the maximum absolute error, given in the same unit of measure as the offset itself. If the local precision within the grid is unknown, the global maximum possible error shall be quoted. Data with unbounded (unknown) possible error would be worthless, thus some quote of precision is mandatory. If the original data source does not explicitly specify precision, but is reputed to be of high quality, an absolute error of 0 (perfect precision) shall be quoted.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:all>
	</xs:complexType>
	<xs:complexType name="GridPropertiesType">
		<xs:annotation>
			<xs:documentation>Grid position, size, and resolution/density</xs:documentation>
		</xs:annotation>
		<xs:all>
			<xs:element name="MinCoords" type="CoordinatesType">
				<xs:annotation>
					<xs:documentation>Lower left corner of the grid (coordinates based on source coordinate reference system)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MaxCoords" type="CoordinatesType">
				<xs:annotation>
					<xs:documentation>Upper right corner of the grid (coordinates based on source coordinate reference system)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CoordSpacing" type="CoordinatesType">
				<xs:annotation>
					<xs:documentation>Grid resolution/density (unit of measure based on source coordinate reference system)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CoordinateUnitOfMeasure" type="UnitOfMeasureType"/>
		</xs:all>
	</xs:complexType>
	<xs:complexType name="GridLineType">
		<xs:annotation>
			<xs:documentation>One single horizontal line within the grid</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="GridNode" type="GridNodeType" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>One single node in the grid, describing the offset at one point</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="SingleCoordinateType">
		<xs:annotation>
			<xs:documentation>Represents a single coordinate; the unit depends on the elsewhere specified source coordinate reference system</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:simpleType name="crsType">
		<xs:annotation>
			<xs:documentation>Represents a coordinate reference system (identified by EPSG id)</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:long"/>
	</xs:simpleType>
	<xs:simpleType name="tfmType">
		<xs:annotation>
			<xs:documentation>Represents a transformation (identified by EPSG id)</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:long"/>
	</xs:simpleType>
	<xs:complexType name="GridApplicationType">
		<xs:annotation>
			<xs:documentation>Defines a single transformation, with source and target CRS, using this grid file</xs:documentation>
		</xs:annotation>
		<xs:all>
			<xs:element name="sourceCrs" type="crsType">
				<xs:annotation>
					<xs:documentation>The source coordinate reference system</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Transformation" type="tfmType">
				<xs:annotation>
					<xs:documentation>The transformation using this grid file</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="targetCrs" type="crsType">
				<xs:annotation>
					<xs:documentation>The target coordinate reference system</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:all>
	</xs:complexType>
	<xs:complexType name="GridApplicationsType">
		<xs:annotation>
			<xs:documentation>Defines the set of transformations using this grid file</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="GridApplication" type="GridApplicationType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ParentType">
		<xs:annotation>
			<xs:documentation>Specifies a hierarchy that the grid is member of, and its parent within that hierarchy</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="HierarchyType" type="xs:anyURI">
				<xs:annotation>
					<xs:documentation>Specifies the hierarchy URI</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ParentNode" type="xs:IDREF" nillable="true">
				<xs:annotation>
					<xs:documentation>Specifies the parent node IDREF</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:attributeGroup name="DateAttributeGroup">
		<xs:annotation>
			<xs:documentation>Defines creation and update date and time</xs:documentation>
		</xs:annotation>
		<xs:attribute name="creation" type="xs:dateTime" use="required"/>
		<xs:attribute name="update" type="xs:dateTime" use="required"/>
	</xs:attributeGroup>
	<xs:complexType name="GridDataType">
		<xs:sequence>
			<xs:element name="GridRow" type="GridLineType" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>One single (horizontal) row within the grid (all rows have the same number of nodes)</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="UnitOfMeasureType">
		<xs:sequence>
			<xs:element name="UnitOfMeasureType" nillable="false">
				<xs:annotation>
					<xs:documentation>The unit type (length or angle)</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="length"/>
						<xs:enumeration value="angle"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="UnitOfMeasureId" type="xs:long" nillable="false">
				<xs:annotation>
					<xs:documentation>The EPSG unit ID</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="UnitOfMeasureFactor" type="xs:double" nillable="false">
				<xs:annotation>
					<xs:documentation>How many base units are equal to one of these units? The base unit of length is meter, the base unit of angle is radians.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>';
  amt := length(buf);
  pos := 1;

  DBMS_LOB.WRITE(schemaclob, amt, pos, buf);

  DBMS_LOB.CLOSE(schemaclob);

  COMMIT;
END;
/

SHOW ERRORS;

declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.oracle.com/2004/spatial/epsg/gridfile/schema/sdoepsggrid.xsd';

  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if(cnt <> 0) then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;

  select xmlSchema into loc from SDO_XML_SCHEMAS where id = 4;

  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => url,
    SCHEMADOC => loc,
    LOCAL     => FALSE,
    OWNER     => 'MDSYS');
end;
/

commit;

-- register the schema
DECLARE
  schemaclob CLOB;
  amt        NUMBER;
  buf        VARCHAR2(32767);
  pos        NUMBER;
BEGIN
  DELETE FROM SDO_XML_SCHEMAS WHERE id=5;
  INSERT INTO SDO_XML_SCHEMAS VALUES (5, 'EPSG  sdo3d.xsd', empty_clob())
    RETURNING xmlSchema into schemaclob;
  SELECT xmlSchema into schemaclob from SDO_XML_SCHEMAS
    WHERE id = 5 FOR UPDATE;

  DBMS_LOB.OPEN(schemaclob, DBMS_LOB.LOB_READWRITE);

  buf := '<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 4 U (http://www.xmlspy.com) by MIKE HORHAMMER (ORACLE CORPORATION) -->
<!-- 2009-07-27 11:17 -->
<xs:schema elementFormDefault="qualified" attributeFormDefault="unqualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:element name="Style3d" type="Style3dType">
		<xs:annotation>
			<xs:documentation>Declares a 3D style, with color, texture, and model style</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="Theme3d" type="Theme3dType">
		<xs:annotation>
			<xs:documentation>Declares a theme, with default style(s) and a reference to the theme with the next (lower) LOD level</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="Scene3d" type="Scene3dType">
		<xs:annotation>
			<xs:documentation>Declares a scene, with multiple themes</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="ViewFrame3d" type="ViewFrame3dType">
		<xs:annotation>
			<xs:documentation>Declares a view frame, specifying a scene and a view point</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="Animation3d" type="Animation3dType"/>
	<xs:complexType name="Animation3dType">
		<xs:sequence>
			<xs:element name="SceneName" type="xs:string"/>
			<xs:element ref="ViewPoint3d" maxOccurs="unbounded"/>
			<xs:element ref="DefaultStyle" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="closedLoop" type="xs:boolean" use="optional" default="false"/>
	</xs:complexType>
	<xs:element name="LightSource3d"/>
	<xs:element name="Vis3DConfig" type="Vis3DConfigType"/>
	<xs:complexType name="Style3dType">
		<xs:annotation>
			<xs:documentation>Version 08-07-15</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Color" type="SharedValueType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Stored in DB as VARCHAR2 or SDO_ORDINATE_ARRAY</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Texture" type="TextureType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>This describes a texture for a geometry. For now, it only represents a single texture, as opposed to multiple textures being superimposed. Also, alpha maps and bump maps are not yet mentioned. They will be added, subsequently. Another aspect is that this schema currently only maps a single texture image per geometry. To map a separate facade to each wall of a building, the facades simply get aggregated into a single bitmap file. The texture coordinates link facades to walls.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ModelStyle" type="FeatureReferenceType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Model object is applied to an oriented point. The model object has SRID null (euclidean). It is applied to the oriented point, such that the euclidean origin coincides with the point, its z-axis coincides with the orientation vector of the point, and scaling is performed, such that the orientation vector represents one unit in the model object euclidean CRS.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Normals" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="GenerateNormals" type="xs:string" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="TextureType">
		<xs:annotation>
			<xs:documentation>Version 08-07-15</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="TextureBLOB" type="SharedValueType">
				<xs:annotation>
					<xs:documentation>Stored in DB as BLOB</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TextureCoordinates" type="SharedValueType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Stored in DB as SDO_ORDINATE_ARRAY. Label Strings are not required, in this xsd, since we assume a texture coordinate array for the entire geometry. The database can supply tools to update texture coordinate arrays, using label strings.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Theme3dType">
		<xs:sequence>
			<xs:element name="LOD">
				<xs:annotation>
					<xs:documentation>This declares Level Of Detail information, as well as a reference to the next less detailed LOD theme within a chain.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="ThemeLOD" type="xs:double"/>
						<xs:element name="Generalization" type="FeatureReferenceType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>There may be a chain of themes with an increasingly detailed representation of the same concept. In a more detailed theme, there is at least one feature for each feature in the less detailed theme. A simple city block in theme 1 may split into several low-detail buildings in theme 2 (separate features). Each of these may map to a single high-detail representation in theme 3. The link is provided by a foreign key from the more detailed theme to the less detailed one. The least detailed theme has no such foreign key.</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element ref="DefaultStyle"/>
			<xs:element name="Tiling" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="TileIdColumn" type="xs:string"/>
						<xs:element name="TileWidth" type="xs:double"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="hidden_info" minOccurs="0">
				<xs:annotation>
					<xs:documentation>This refers to optional attribute columns</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="field" minOccurs="0" maxOccurs="unbounded">
							<xs:complexType>
								<xs:attribute name="column" type="xs:string" use="required"/>
								<xs:attribute name="name" type="xs:string" use="required"/>
							</xs:complexType>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="ExternalRepresentations" minOccurs="0">
				<xs:annotation>
					<xs:documentation>This describes optional external representations, such as CityGML</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="GML" type="SharedValueType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>This refers to an optional GML column / foreign key</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="CityGML" type="SharedValueType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>This refers to an optional CityGML column / foreign key</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="KML" type="SharedValueType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>This refers to an optional KML column / foreign key</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="X3D" type="SharedValueType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>This refers to an optional X3D column / foreign key</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="BIM" type="SharedValueType" minOccurs="0">
							<xs:annotation>
								<xs:documentation>This refers to an optional BIM column / foreign key</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="srid" type="xs:integer" use="optional"/>
	</xs:complexType>
	<xs:complexType name="SharedValueType">
		<xs:sequence>
			<xs:element ref="ForeignKey" minOccurs="0"/>
			<xs:element name="ValueColumn" type="xs:string">
				<xs:annotation>
					<xs:documentation>Name of value column in target table</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ForeignKey" type="xs:string">
		<xs:annotation>
			<xs:documentation>Foreign key for joining consumer table with target table</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:complexType name="Vector3dType">
		<xs:attribute name="x" type="xs:double" use="required"/>
		<xs:attribute name="y" type="xs:double" use="required"/>
		<xs:attribute name="z" type="xs:double" use="required"/>
	</xs:complexType>
	<xs:complexType name="ViewPoint3dType">
		<xs:sequence>
			<xs:element name="Eye" type="Vector3dType">
				<xs:annotation>
					<xs:documentation>The location of the eye</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Center" type="Vector3dType">
				<xs:annotation>
					<xs:documentation>The point where the eye is looking</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Up" type="Vector3dType">
				<xs:annotation>
					<xs:documentation>The up vector specifying the frustum''s up direction</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Frustum3d" type="Frustum3dType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The frustum establishes a view model with the eye at the apex of a symmetric view frustum</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="srid" type="xs:integer" use="optional"/>
		<xs:attribute name="fadeIn" type="xs:integer" use="optional" default="1">
			<xs:annotation>
				<xs:documentation>Fade in in ms</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="pause" type="xs:integer" use="optional" default="1">
			<xs:annotation>
				<xs:documentation>Pause in ms</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="fadeOut" type="xs:integer" use="optional" default="1">
			<xs:annotation>
				<xs:documentation>Fade out in ms</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>
	<xs:element name="ViewPoint3d" type="ViewPoint3dType">
		<xs:annotation>
			<xs:documentation>Declares a view point</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:complexType name="ViewFrame3dType">
		<xs:sequence>
			<xs:element name="SceneName" type="xs:string"/>
			<xs:element ref="ViewPoint3d"/>
			<xs:element ref="DefaultStyle" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Frustum3dType">
		<xs:annotation>
			<xs:documentation>The frustum establishes a view model with the eye at the apex of a symmetric view frustum</xs:documentation>
		</xs:annotation>
		<xs:attribute name="left" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The vertical line on the left edge of the near clipping plane mapped to the left edge of the graphics window</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="right" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The vertical line on the right edge of the near clipping plane mapped to the right edge of the graphics window</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="bottom" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The horizontal line on the bottom edge of the near clipping plane mapped to the bottom edge of the graphics window</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="top" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The horizontal line on the top edge of the near</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="near" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The distance to the frustum''s near clipping plane. This value must be positive, (the value -near is the location of the near clip plane).</xs:documentation>
			</xs:annotation>
		</xs:attribute>
		<xs:attribute name="far" type="xs:double" use="required">
			<xs:annotation>
				<xs:documentation>The distance to the frustum''s far clipping plane. This value must be positive, and must be greater than near.</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>
	<xs:element name="DefaultStyle" type="Style3dType">
		<xs:annotation>
			<xs:documentation>This refers to style descriptions, such as color, texture, model style, etc.</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:complexType name="Scene3dType">
		<xs:sequence>
			<xs:element name="Theme" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>A scene may have several themes. There may be a chain of increasingly detailed themes, representing the same concept. Only the most detailed admissable theme has to be referenced, here. Any less-detailed themes will automatically be rendered, as appropriate. Any more detailed themes are inadmissable for rendering in this scene.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="display" type="xs:boolean" use="required"/>
							<xs:attribute name="pickable" type="xs:boolean" use="required"/>
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="ExternalTheme" minOccurs="0" maxOccurs="unbounded">
				<xs:complexType>
					<xs:choice>
						<xs:element name="MapViewerTiles">
							<xs:complexType>
								<xs:simpleContent>
									<xs:extension base="xs:string">
										<xs:attribute name="url" type="xs:anyURI" use="required"/>
										<xs:attribute name="datasource" type="xs:string" use="required"/>
										<xs:attribute name="tile_layer_name" type="xs:string" use="required"/>
									</xs:extension>
								</xs:simpleContent>
							</xs:complexType>
						</xs:element>
						<xs:element name="DigitalGlobeTiles"/>
					</xs:choice>
				</xs:complexType>
			</xs:element>
			<xs:element ref="DefaultStyle" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="srid" type="xs:integer" use="optional"/>
	</xs:complexType>
	<xs:complexType name="FeatureReferenceType">
		<xs:sequence>
			<xs:element ref="ForeignKey"/>
			<xs:element name="TargetTheme" type="xs:string"/>
			<xs:element name="ActivationDistance">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:double">
							<xs:attribute name="uom_id" type="xs:int" use="required"/>
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Vis3DConfigType">
		<xs:sequence>
			<xs:element name="DefaultTexture" type="xs:string"/>
			<xs:element name="Logos" type="LogosType"/>
			<xs:element name="Icons" type="IconsType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="IconsType">
		<xs:sequence>
			<xs:element name="TurnIcon" type="xs:string"/>
			<xs:element name="RotateIcon" type="xs:string"/>
			<xs:element name="ZoomIcon" type="xs:string"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LogosType">
		<xs:sequence>
			<xs:element name="Logo" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>';
  amt := length(buf);
  pos := 1;

  DBMS_LOB.WRITE(schemaclob, amt, pos, buf);

  DBMS_LOB.CLOSE(schemaclob);

  COMMIT;
END;
/

SHOW ERRORS;

declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.oracle.com/2009/spatial/vis3d/schema/sdo3d.xsd';

  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if(cnt <> 0) then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;

  select xmlSchema into loc from SDO_XML_SCHEMAS where id = 5;

  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => url,
    SCHEMADOC => loc,
    LOCAL     => FALSE,
    OWNER     => 'MDSYS');
end;
/

commit;
