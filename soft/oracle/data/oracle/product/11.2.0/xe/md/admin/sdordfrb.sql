Rem
Rem $Header: sdo/admin/sdordfrb.sql /st_sdo_11.2.0/1 2010/04/27 08:25:30 vkolovsk Exp $
Rem
Rem sdordfrb.sql
Rem
Rem Copyright (c) 2005, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdordfrb.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    vkolovsk    04/23/10 - XbranchMerge vkolovsk_bug-9646520 from main
Rem    alwu        03/16/09 - add skos rulebase
Rem    sdas        05/11/07 - debug ORA-13199: RDF: Error in
Rem                           load_predefined_rulebases()
Rem    alwu        02/05/07 - add default OWL rulebases
Rem    geadon      05/20/05 - use internal CREATE_RULEBASE 
Rem    geadon      05/04/05 - new rulebase interface 
Rem    geadon      04/29/05 - do not use synonyms 
Rem    geadon      04/26/05 - rename APIs to conform to SDO standards
Rem    nalexand    04/26/05 - create procedure to be called in create_rdf_network() in sdordfb.sql
Rem    sravada     04/11/05 - remove echo on 
Rem    geadon      04/04/05 - use new RDF_APIS names 
Rem    geadon      03/07/05 - geadon_rdf_query
Rem    geadon      03/07/05 - Created
Rem

-- 
-- RDF-AXIOMS are complete w.r.t. the RDF rulebase (that is, no
-- additional triples will be inferred when the RDF rulebase is applied
-- to RDF-AXIOMS). However, RDF-AXIOMS union RDFS-AXIOMS are not
-- complete w.r.t. the RDFS rulebase.
-- 
-- The RDFS-AXIOMS are partitioned into three rules (-DOMAIN, -RANGE,
-- and -MISC) to ensure that the axioms do not exceed 4000 characters
-- when expanded.
-- 
-- Note that the RDF/S containerMembershipProperty axioms (e.g.,
-- rdf:_N rdfs:subPropertyOf rdfs:containerMembershipProperty) are 
-- generated as a special case by the InferenceEngine.
-- 

CREATE OR REPLACE PROCEDURE load_predefined_rulebases IS
BEGIN

  mdsys.rdf_apis_internal.create_rulebase('RDF');
  mdsys.rdf_apis_internal.create_rulebase('RDFS');
  mdsys.rdf_apis_internal.create_rulebase('RDFS++');
  mdsys.rdf_apis_internal.create_rulebase('OWLSIF');
  mdsys.rdf_apis_internal.create_rulebase('OWLPRIME');
  mdsys.rdf_apis_internal.create_rulebase('SKOSCORE');
  mdsys.rdf_apis_internal.create_rulebase('OWL2RL');
  EXECUTE IMMEDIATE 'GRANT SELECT ON RDFR_RDF  TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON RDFR_RDFS TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "SEMR_RDFS++" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "RDFR_RDFS++" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "SEMR_OWLSIF" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "RDFR_OWLSIF" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "SEMR_OWLPRIME" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "RDFR_OWLPRIME" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "SEMR_SKOSCORE" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "RDFR_SKOSCORE" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "RDFR_OWL2RL" TO PUBLIC';
  EXECUTE IMMEDIATE 'GRANT SELECT ON "SEMR_OWL2RL" TO PUBLIC';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDF(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDF-AXIOMS',
        '',
        '(rdf:type rdf:type rdf:Property)
         (rdf:subject rdf:type rdf:Property)
         (rdf:predicate rdf:type rdf:Property)
         (rdf:object rdf:type rdf:Property)
         (rdf:first rdf:type rdf:Property)
         (rdf:rest rdf:type rdf:Property)
         (rdf:value rdf:type rdf:Property)
         (rdf:nil rdf:type rdf:List)';

  -- artificially exercise the RDFAliases parameter
  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDF(rule_name, antecedents, consequents, aliases)
   values(:1, :2, :3, :4)'
  USING 'RDF1',
        '(?s ?p ?o)',
        '(?p :type :Property)',
        RDF_Aliases(RDF_Alias('', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'));

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS select * from MDSYS.RDFR_RDF';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS-AXIOMS-DOMAIN',
        '',
        '(rdf:type rdfs:domain rdfs:Resource)
         (rdfs:domain rdfs:domain rdf:Property)
         (rdfs:range rdfs:domain rdf:Property)
         (rdfs:subPropertyOf rdfs:domain rdf:Property)
         (rdfs:subClassOf rdfs:domain rdfs:Class)
         (rdf:subject rdfs:domain rdf:Statement)
         (rdf:predicate rdfs:domain rdf:Statement)
         (rdf:object rdfs:domain rdf:Statement)
         (rdfs:member rdfs:domain rdfs:Resource) 
         (rdf:first rdfs:domain rdf:List)
         (rdf:rest rdfs:domain rdf:List)
         (rdfs:seeAlso rdfs:domain rdfs:Resource)
         (rdfs:isDefinedBy rdfs:domain rdfs:Resource)
         (rdfs:comment rdfs:domain rdfs:Resource)
         (rdfs:label rdfs:domain rdfs:Resource)
         (rdf:value rdfs:domain rdfs:Resource)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS-AXIOMS-RANGE',
        '',
        '(rdf:type rdfs:range rdfs:Class)
         (rdfs:domain rdfs:range rdfs:Class)
         (rdfs:range rdfs:range rdfs:Class)
         (rdfs:subPropertyOf rdfs:range rdf:Property)
         (rdfs:subClassOf rdfs:range rdfs:Class)
         (rdf:subject rdfs:range rdfs:Resource)
         (rdf:predicate rdfs:range rdfs:Resource)
         (rdf:object rdfs:range rdfs:Resource)
         (rdfs:member rdfs:range rdfs:Resource)
         (rdf:first rdfs:range rdfs:Resource)
         (rdf:rest rdfs:range rdf:List)
         (rdfs:seeAlso rdfs:range rdfs:Resource)
         (rdfs:isDefinedBy rdfs:range rdfs:Resource)
         (rdfs:comment rdfs:range rdfs:Literal)
         (rdfs:label rdfs:range rdfs:Literal)
         (rdf:value rdfs:range rdfs:Resource)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS-AXIOMS-MISC',
        '',
        '(rdf:Alt rdfs:subClassOf rdfs:Container)
         (rdf:Bag rdfs:subClassOf rdfs:Container)
         (rdf:Seq rdfs:subClassOf rdfs:Container)
         (rdfs:ContainerMembershipProperty rdfs:subClassOf rdf:Property)
         (rdfs:isDefinedBy rdfs:subPropertyOf rdfs:seeAlso)
         (rdf:XMLLiteral rdf:type rdfs:Datatype)
         (rdf:XMLLiteral rdfs:subClassOf rdfs:Literal) 
         (rdfs:Datatype rdfs:subClassOf rdfs:Class)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS2',
        '(?s ?p ?o) (?p rdfs:domain ?d)',
        '(?s rdf:type ?d)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS3',
        '(?s ?p ?o) (?p rdfs:range  ?r)',
        '(?o rdf:type ?r)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS4A',
        '(?s ?p ?o)',
        '(?s rdf:type rdfs:Resource)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS4B',
        '(?s ?p ?o)',
        '(?o rdf:type rdfs:Resource)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS5',
        '(?f rdfs:subPropertyOf ?g) (?g rdfs:subPropertyOf ?h)',
        '(?f rdfs:subPropertyOf ?h)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS6',
        '(?p rdf:type rdf:Property)',
        '(?p rdfs:subPropertyOf ?p)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS7',
        '(?s ?f ?o) (?f rdfs:subPropertyOf ?g)',
        '(?s ?g ?o)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS8',
        '(?c rdf:type rdfs:Class)',
        '(?c rdfs:subClassOf rdfs:Resource)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS9',
        '(?x rdf:type ?a) (?a rdfs:subClassOf ?b)',
        '(?x rdf:type ?b)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS10',
        '(?c rdf:type rdfs:Class)',
        '(?c rdfs:subClassOf ?c)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1 ,:2 ,:3)'
  USING 'RDFS11',
        '(?a rdfs:subClassOf ?b) (?b rdfs:subClassOf ?c)',
        '(?a rdfs:subClassOf ?c)';

  EXECUTE IMMEDIATE
  'insert into MDSYS.RDFR_RDFS(rule_name, antecedents, consequents)
   values(:1, :2, :3)'
  USING 'RDFS12',
        '(?p rdf:type rdfs:ContainerMembershipProperty)',
        '(?p rdfs:subPropertyOf rdfs:member)';

  commit;

   EXCEPTION 
	WHEN OTHERS THEN
		MDERR.RAISE_MD_ERROR('MD', 'SDO', -13199, 'RDF: Error in load_predefined_rulebases(): SQLERRM=' || SQLERRM);
END load_predefined_rulebases;
/
show errors;
