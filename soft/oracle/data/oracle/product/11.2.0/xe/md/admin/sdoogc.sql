Rem
Rem $Header: sdoogc.sql 05-apr-2007.11:05:07 sravada Exp $
Rem
Rem sdoogc.sql
Rem
Rem Copyright (c) 2004, 2007, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdoogc.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     04/05/07 - fix length/area
Rem    mhorhamm    11/22/05 - Rename 2 public synonyms 
Rem    mhorhamm    09/06/05 - Fix ST_GEOMCOLLECTION reference 
Rem    mhorhamm    09/02/05 - Fix 4555902 
Rem    sravada     12/06/04 - 
Rem    mhorhamm    09/27/04 - Move some of the logic to sdogmmh, sdogmmb 
Rem    mhorhamm    09/23/04 - mhorhamm_epsg_040823
Rem    mhorhamm    09/21/04 - Add more OGC functions 
Rem    mhorhamm    09/13/04 - continue
Rem    mhorhamm    09/03/04 - Continue with OGC functions 
Rem    mhorhamm    09/02/04 - Add more OGC functions 
Rem    mhorhamm    08/31/04 - Created
Rem

CREATE OR REPLACE FUNCTION OGC_PolygonFromText(
  wkt   IN VARCHAR2,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_POLYGON IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKT(wkt, srid) AS ST_POLYGON);
END OGC_PolygonFromText;
/
show errors;

create or replace public synonym PolygonFromText for MDSYS.OGC_PolygonFromText;
grant execute on OGC_PolygonFromText to public;


CREATE OR REPLACE FUNCTION OGC_LineStringFromText(
  wkt   IN VARCHAR2,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_LineString IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKT(wkt, srid) AS ST_LineString);
END OGC_LineStringFromText;
/
show errors;

create or replace public synonym LineStringFromText for MDSYS.OGC_LineStringFromText;
grant execute on OGC_LineStringFromText to public;

CREATE OR REPLACE FUNCTION OGC_MultiPolygonFromText(
  wkt   IN VARCHAR2,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_MULTIPOLYGON IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKT(wkt, srid) AS ST_MULTIPOLYGON);
END OGC_MultiPolygonFromText;
/
show errors;

create or replace public synonym MultiPolygonFromText for MDSYS.OGC_MultiPolygonFromText;
grant execute on OGC_MultiPolygonFromText to public;

CREATE OR REPLACE FUNCTION OGC_MultiLineStringFromText(
  wkt   IN VARCHAR2,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_MultiLineString IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKT(wkt, srid) AS ST_MultiLineString);
END OGC_MultiLineStringFromText;
/
show errors;

create or replace public synonym MultiLineStringFromText for MDSYS.OGC_MultiLineStringFromText;
grant execute on OGC_MultiLineStringFromText to public;

CREATE OR REPLACE FUNCTION OGC_PointFromText(
  wkt   IN VARCHAR2,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_Point IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKT(wkt, srid) AS ST_Point);
END OGC_PointFromText;
/
show errors;

create or replace public synonym PointFromText for MDSYS.OGC_PointFromText;
grant execute on OGC_PointFromText to public;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION OGC_PolygonFromWKB(
  wkb   IN BLOB,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_POLYGON IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKB(wkb, srid) AS ST_POLYGON);
END OGC_PolygonFromWKB;
/
show errors;

create or replace public synonym PolygonFromWKB for MDSYS.OGC_PolygonFromWKB;
grant execute on OGC_PolygonFromWKB to public;

CREATE OR REPLACE FUNCTION OGC_LineStringFromWKB(
  wkb   IN BLOB,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_LineString IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKB(wkb, srid) AS ST_LineString);
END OGC_LineStringFromWKB;
/
show errors;

create or replace public synonym LineStringFromWKB for MDSYS.OGC_LineStringFromWKB;
grant execute on OGC_LineStringFromWKB to public;

CREATE OR REPLACE FUNCTION OGC_MultiPolygonFromWKB(
  wkb   IN BLOB,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_MULTIPOLYGON IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKB(wkb, srid) AS ST_MULTIPOLYGON);
END OGC_MultiPolygonFromWKB;
/
show errors;

create or replace public synonym MultiPolygonFromWKB for MDSYS.OGC_MultiPolygonFromWKB;
grant execute on OGC_MultiPolygonFromWKB to public;

CREATE OR REPLACE FUNCTION OGC_MultiLineStringFromWKB(
  wkb   IN BLOB,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_MultiLineString IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKB(wkb, srid) AS ST_MultiLineString);
END OGC_MultiLineStringFromWKB;
/
show errors;

create or replace public synonym MultiLineStringFromWKB for MDSYS.OGC_MultiLineStringFromWKB;
grant execute on OGC_MultiLineStringFromWKB to public;

CREATE OR REPLACE FUNCTION OGC_PointFromWKB(
  wkb   IN BLOB,
  srid  IN INTEGER DEFAULT NULL)
    RETURN ST_Point IS
BEGIN
  RETURN TREAT(ST_GEOMETRY.FROM_WKB(wkb, srid) AS ST_Point);
END OGC_PointFromWKB;
/
show errors;

create or replace public synonym PointFromWKB for MDSYS.OGC_PointFromWKB;
grant execute on OGC_PointFromWKB to public;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION OGC_Dimension(
  g ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g.ST_CoordDim();
END OGC_Dimension;
/
show errors;

create or replace public synonym Dimension for MDSYS.OGC_Dimension;
grant execute on OGC_Dimension to public;

CREATE OR REPLACE FUNCTION OGC_AsText(
  g ST_Geometry)
    RETURN VARCHAR2 IS
BEGIN
  RETURN g.GET_WKT();
END OGC_AsText;
/
show errors;

create or replace public synonym AsText for MDSYS.OGC_AsText;
grant execute on OGC_AsText to public;

CREATE OR REPLACE FUNCTION OGC_AsBinary(
  g ST_Geometry)
    RETURN BLOB IS
BEGIN
  RETURN g.GET_WKB();
END OGC_AsBinary;
/
show errors;

create or replace public synonym AsBinary for MDSYS.OGC_AsBinary;
grant execute on OGC_AsBinary to public;

CREATE OR REPLACE FUNCTION OGC_SRID(
  g ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g.ST_SRID();
END OGC_SRID;
/
show errors;

create or replace public synonym SRID for MDSYS.OGC_SRID;
grant execute on OGC_SRID to public;

CREATE OR REPLACE FUNCTION OGC_X(
  p ST_Point)
    RETURN NUMBER IS
BEGIN
  RETURN p.ST_X();
END OGC_X;
/
show errors;

create or replace public synonym OGC_X for MDSYS.OGC_X;
grant execute on OGC_X to public;

CREATE OR REPLACE FUNCTION OGC_Y(
  p ST_Point)
    RETURN NUMBER IS
BEGIN
  RETURN p.ST_Y();
END OGC_Y;
/
show errors;

create or replace public synonym OGC_Y for MDSYS.OGC_Y;
grant execute on OGC_Y to public;

CREATE OR REPLACE FUNCTION OGC_NumInteriorRings(
  p ST_Polygon)
    RETURN Integer IS
BEGIN
  RETURN p.ST_InteriorRingsP().LAST;
END OGC_NumInteriorRings;
/
show errors;

create or replace public synonym NumInteriorRings for MDSYS.OGC_NumInteriorRings;
grant execute on OGC_NumInteriorRings to public;

CREATE OR REPLACE FUNCTION OGC_InteriorRingN(
  p ST_Polygon,
  n Integer)
    RETURN ST_LineString IS
  arr ST_LineString_Array;
BEGIN
  arr := p.ST_InteriorRingsP();
  RETURN arr(n);
END OGC_InteriorRingN;
/
show errors;

create or replace public synonym InteriorRingN for MDSYS.OGC_InteriorRingN;
grant execute on OGC_InteriorRingN to public;

CREATE OR REPLACE FUNCTION OGC_ExteriorRing(
  p ST_Polygon)
    RETURN ST_LineString IS
BEGIN
  RETURN p.ST_ExteriorRing();
END OGC_ExteriorRing;
/
show errors;

create or replace public synonym ExteriorRing for MDSYS.OGC_ExteriorRing;
grant execute on OGC_ExteriorRing to public;

CREATE OR REPLACE FUNCTION OGC_NumGeometries(
  g ST_GeomCollection)
    RETURN Integer IS
BEGIN
  RETURN g.ST_Geometries().LAST;
END OGC_NumGeometries;
/
show errors;

create or replace public synonym NumGeometries for MDSYS.OGC_NumGeometries;
grant execute on OGC_NumGeometries to public;

CREATE OR REPLACE FUNCTION OGC_GeometryN(
  g ST_GeomCollection,
  n Integer)
    RETURN ST_Geometry IS
  arr ST_GEOMETRY_ARRAY;
BEGIN
  arr := g.ST_Geometries();
  RETURN arr(n);
END OGC_GeometryN;
/
show errors;

create or replace public synonym GeometryN for MDSYS.OGC_GeometryN;
grant execute on OGC_GeometryN to public;

CREATE OR REPLACE FUNCTION OGC_Disjoint(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g1.ST_Disjoint(g2);
END OGC_Disjoint;
/
show errors;

create or replace public synonym Disjoint for MDSYS.OGC_Disjoint;
grant execute on OGC_Disjoint to public;

CREATE OR REPLACE FUNCTION OGC_Touch(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer IS
  result VARCHAR2(128);
BEGIN
  RETURN g1.ST_Touch(g2);
END OGC_Touch;
/
show errors;

create or replace public synonym Touch for MDSYS.OGC_Touch;
grant execute on OGC_Touch to public;

CREATE OR REPLACE FUNCTION OGC_Within(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g1.ST_Within(g2);
END OGC_Within;
/
show errors;

create or replace public synonym Within for MDSYS.OGC_Within;
grant execute on OGC_Within to public;

CREATE OR REPLACE FUNCTION OGC_Overlap(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g1.ST_Overlap(g2);
END OGC_Overlap;
/
show errors;

create or replace public synonym Overlap for MDSYS.OGC_Overlap;
grant execute on OGC_Overlap to public;

CREATE OR REPLACE FUNCTION OGC_Contains(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g1.ST_Contains(g2);
END OGC_Contains;
/
show errors;

create or replace public synonym Ogc_Contains for MDSYS.OGC_Contains;
grant execute on OGC_Contains to public;

CREATE OR REPLACE FUNCTION OGC_Intersection(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g1.ST_Intersection(g2);
END OGC_Intersection;
/
show errors;

create or replace public synonym Intersection for MDSYS.OGC_Intersection;
grant execute on OGC_Intersection to public;

CREATE OR REPLACE FUNCTION OGC_Difference(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g1.ST_Difference(g2);
END OGC_Difference;
/
show errors;

create or replace public synonym Difference for MDSYS.OGC_Difference;
grant execute on OGC_Difference to public;

CREATE OR REPLACE FUNCTION OGC_Union(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g1.ST_Union(g2);
END OGC_Union;
/
show errors;

create or replace public synonym Ogc_Union for MDSYS.OGC_Union;
grant execute on OGC_Union to public;

CREATE OR REPLACE FUNCTION OGC_ConvexHull(
  g ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g.ST_ConvexHull();
END OGC_ConvexHull;
/
show errors;

create or replace public synonym ConvexHull for MDSYS.OGC_ConvexHull;
grant execute on OGC_ConvexHull to public;

CREATE OR REPLACE FUNCTION OGC_Centroid(
  g ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g.ST_Centroid();
END OGC_Centroid;
/
show errors;

create or replace public synonym Centroid for MDSYS.OGC_Centroid;
grant execute on OGC_Centroid to public;

CREATE OR REPLACE FUNCTION OGC_GeometryType(
  g ST_Geometry)
    RETURN VARCHAR2 IS
BEGIN
  RETURN g.ST_GeometryType();
END OGC_GeometryType;
/
show errors;

create or replace public synonym GeometryType for MDSYS.OGC_GeometryType;
grant execute on OGC_GeometryType to public;

CREATE OR REPLACE FUNCTION OGC_StartPoint(
  c ST_Curve)
    RETURN ST_Point IS
BEGIN
  RETURN c.ST_StartPoint();
END OGC_StartPoint;
/
show errors;

create or replace public synonym StartPoint for MDSYS.OGC_StartPoint;
grant execute on OGC_StartPoint to public;

CREATE OR REPLACE FUNCTION OGC_EndPoint(
  c ST_Curve)
    RETURN ST_Point IS
BEGIN
  RETURN c.ST_EndPoint();
END OGC_EndPoint;
/
show errors;

create or replace public synonym EndPoint for MDSYS.OGC_EndPoint;
grant execute on OGC_EndPoint to public;

CREATE OR REPLACE FUNCTION OGC_Boundary(
  g ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g.ST_Boundary();
END OGC_Boundary;
/
show errors;

create or replace public synonym Boundary for MDSYS.OGC_Boundary;
grant execute on OGC_Boundary to public;

CREATE OR REPLACE FUNCTION OGC_Envelope(
  g ST_Geometry)
    RETURN ST_Geometry IS
BEGIN
  RETURN g.ST_Envelope();
END OGC_Envelope;
/
show errors;

create or replace public synonym Envelope for MDSYS.OGC_Envelope;
grant execute on OGC_Envelope to public;

CREATE OR REPLACE FUNCTION OGC_IsEmpty(
  g ST_Geometry)
    RETURN Integer IS
BEGIN
  RETURN g.ST_IsEmpty();
END OGC_IsEmpty;
/
show errors;

create or replace public synonym IsEmpty for MDSYS.OGC_IsEmpty;
grant execute on OGC_IsEmpty to public;

CREATE OR REPLACE FUNCTION OGC_NumPoints(
  c ST_Curve)
    RETURN Integer IS
BEGIN
  RETURN c.ST_NumPoints();
END OGC_NumPoints;
/
show errors;

create or replace public synonym NumPoints for MDSYS.OGC_NumPoints;
grant execute on OGC_NumPoints to public;

CREATE OR REPLACE FUNCTION OGC_PointN(
  c ST_Curve,
  n Integer)
    RETURN ST_Point IS
BEGIN
  RETURN c.ST_PointN(n);
END OGC_PointN;
/
show errors;

create or replace public synonym PointN for MDSYS.OGC_PointN;
grant execute on OGC_PointN to public;

CREATE OR REPLACE FUNCTION OGC_IsClosed(
  g ST_Geometry)
    RETURN Integer DETERMINISTIC IS
BEGIN
  IF(UPPER(OGC_GeometryType(g)) IN ('LINESTRING', 'ST_LINESTRING', 'ST_CIRCULARSTRING', 'ST_COMPOUNDCURVE')) THEN
    RETURN TREAT(g AS ST_Curve).ST_IsClosed();
  END IF;
  IF(UPPER(OGC_GeometryType(g)) IN ('MULTILINESTRING', 'ST_MULTILINESTRING', 'ST_MULTICURVE')) THEN
    RETURN TREAT(g AS ST_MultiCurve).ST_IsClosed();
  END IF;
  RETURN NULL;
END OGC_IsClosed;
/
show errors;

create or replace public synonym IsClosed for MDSYS.OGC_IsClosed;
grant execute on OGC_IsClosed to public;

CREATE OR REPLACE FUNCTION OGC_PointOnSurface(
  s ST_Geometry)
    RETURN ST_Point DETERMINISTIC IS
BEGIN
  IF(UPPER(OGC_GeometryType(s)) IN ('POLYGON')) THEN
    RETURN TREAT(s AS ST_Surface).ST_PointOnSurface();
  END IF;
  IF(UPPER(OGC_GeometryType(s)) IN ('MULTIPOLYGON')) THEN
    RETURN TREAT(s AS ST_MultiSurface).ST_PointOnSurface();
  END IF;
  RETURN NULL;
END OGC_PointOnSurface;
/
show errors;

create or replace public synonym PointOnSurface for MDSYS.OGC_PointOnSurface;
grant execute on OGC_PointOnSurface to public;

CREATE OR REPLACE FUNCTION OGC_Area(
  s ST_Geometry)
    RETURN NUMBER DETERMINISTIC IS
BEGIN
  IF(UPPER(OGC_GeometryType(s)) IN ('POLYGON', 'ST_POLYGON')) THEN
    RETURN TREAT(s AS ST_Surface).ST_Area();
  END IF;
  IF(UPPER(OGC_GeometryType(s)) IN ('MULTIPOLYGON', 'ST_MULTIPOLYGON')) THEN
    RETURN TREAT(s AS ST_MultiSurface).ST_Area();
  END IF;
  RETURN NULL;
END OGC_Area;
/
show errors;

create or replace public synonym Area for MDSYS.OGC_Area;
grant execute on OGC_Area to public;

CREATE OR REPLACE FUNCTION OGC_Buffer(
  g ST_Geometry,
  d NUMBER)
    RETURN ST_Geometry DETERMINISTIC IS
BEGIN
  RETURN g.ST_Buffer(d);
END OGC_Buffer;
/
show errors;

create or replace public synonym Buffer for MDSYS.OGC_Buffer;
grant execute on OGC_Buffer to public;

CREATE OR REPLACE FUNCTION OGC_Equals(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN g1.ST_Equals(g2);
END OGC_Equals;
/
show errors;

create or replace public synonym Equals for MDSYS.OGC_Equals;
grant execute on OGC_Equals to public;

CREATE OR REPLACE FUNCTION OGC_SymmetricDifference(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN ST_Geometry DETERMINISTIC IS
BEGIN
  RETURN g1.ST_SymmetricDifference(g2);
END OGC_SymmetricDifference;
/
show errors;

create or replace public synonym SymmetricDifference for MDSYS.OGC_SymmetricDifference;
grant execute on OGC_SymmetricDifference to public;

CREATE OR REPLACE FUNCTION OGC_Distance(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN NUMBER DETERMINISTIC IS
BEGIN
  RETURN g1.ST_Distance(g2);
END OGC_Distance;
/
show errors;

create or replace public synonym Distance for MDSYS.OGC_Distance;
grant execute on OGC_Distance to public;

CREATE OR REPLACE FUNCTION OGC_Length(
  g ST_Geometry) RETURN NUMBER DETERMINISTIC IS
BEGIN
  IF(UPPER(OGC_GeometryType(g)) IN ('LINESTRING', 'ST_CIRCULARSTRING', 'ST_COMPOUNDCURVE', 'ST_LINESTRING')) THEN
    RETURN TREAT(g AS ST_Curve).ST_Length();
  END IF;
  IF(UPPER(OGC_GeometryType(g)) IN ('MULTILINESTRING', 'ST_MULTILINESTRING')) THEN
    RETURN TREAT(g AS ST_MultiCurve).ST_Length();
  END IF;
  RETURN NULL;
END OGC_Length;
/
show errors;

create or replace public synonym OGC_Length for MDSYS.OGC_Length;
grant execute on OGC_Length to public;

CREATE OR REPLACE FUNCTION OGC_IsSimple(
  g ST_Geometry)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN g.ST_IsSimple();
END OGC_IsSimple;
/
show errors;

create or replace public synonym IsSimple for MDSYS.OGC_IsSimple;
grant execute on OGC_IsSimple to public;

CREATE OR REPLACE FUNCTION OGC_IsRing(
  c ST_Curve)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN c.ST_IsRing();
END OGC_IsRing;
/
show errors;

create or replace public synonym IsRing for MDSYS.OGC_IsRing;
grant execute on OGC_IsRing to public;

CREATE OR REPLACE FUNCTION OGC_Intersects(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN g1.ST_Intersects(g2);
END OGC_Intersects;
/
show errors;

create or replace public synonym Intersects for MDSYS.OGC_Intersects;
grant execute on OGC_Intersects to public;

CREATE OR REPLACE FUNCTION OGC_Relate(
  g1            ST_Geometry,
  g2            ST_Geometry,
  PatternMatrix VARCHAR2)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN g1.ST_Relate(g2, PatternMatrix);
END OGC_Relate;
/
show errors;

create or replace public synonym Relate for MDSYS.OGC_Relate;
grant execute on OGC_Relate to public;

CREATE OR REPLACE FUNCTION OGC_Cross(
  g1 ST_Geometry,
  g2 ST_Geometry)
    RETURN Integer DETERMINISTIC IS
BEGIN
  RETURN g1.ST_Cross(g2);
END OGC_Cross;
/
show errors;

create or replace public synonym Cross for MDSYS.OGC_Cross;
grant execute on OGC_Cross to public;

grant execute on OGC_PolygonFromText to public;
grant execute on OGC_LineStringFromText to public;
grant execute on OGC_MultiPolygonFromText to public;
grant execute on OGC_MultiLineStringFromText to public;
grant execute on OGC_PointFromText to public;
grant execute on OGC_PolygonFromWKB to public;
grant execute on OGC_LineStringFromWKB to public;
grant execute on OGC_MultiPolygonFromWKB to public;
grant execute on OGC_MultiLineStringFromWKB to public;
grant execute on OGC_PointFromWKB to public;
grant execute on OGC_Dimension to public;
grant execute on OGC_AsText to public;
grant execute on OGC_AsBinary to public;
grant execute on OGC_SRID to public;
grant execute on OGC_X to public;
grant execute on OGC_Y to public;
grant execute on OGC_NumInteriorRings to public;
grant execute on OGC_InteriorRingN to public;
grant execute on OGC_ExteriorRing to public;
grant execute on OGC_NumGeometries to public;
grant execute on OGC_GeometryN to public;
grant execute on OGC_Disjoint to public;
grant execute on OGC_Touch to public;
grant execute on OGC_Within to public;
grant execute on OGC_Overlap to public;
grant execute on OGC_Contains to public;
grant execute on OGC_Intersection to public;
grant execute on OGC_Difference to public;
grant execute on OGC_Union to public;
grant execute on OGC_ConvexHull to public;
grant execute on OGC_Centroid to public;
grant execute on OGC_GeometryType to public;
grant execute on OGC_StartPoint to public;
grant execute on OGC_EndPoint to public;
grant execute on OGC_Boundary to public;
grant execute on OGC_Envelope to public;
grant execute on OGC_IsEmpty to public;
grant execute on OGC_NumPoints to public;
grant execute on OGC_PointN to public;
grant execute on OGC_IsClosed to public;
grant execute on OGC_PointOnSurface to public;
grant execute on OGC_Area to public;
grant execute on OGC_Buffer to public;
grant execute on OGC_Equals to public;
grant execute on OGC_SymmetricDifference to public;
grant execute on OGC_Distance to public;
grant execute on OGC_Length to public;
grant execute on OGC_IsSimple to public;
grant execute on OGC_IsRing to public;
grant execute on OGC_Intersects to public;
grant execute on OGC_Relate to public;
grant execute on OGC_Cross to public;


