Rem
Rem $Header: sdoe101.sql 26-jun-2007.07:12:55 sravada Exp $
Rem
Rem sdoe101.sql
Rem
Rem Copyright (c) 2004, 2007, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdoe101.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     06/24/07   - downgrade sdo_topo_geom for 10.1.0.5 issue
Rem    bgouslin    01/07/07   - New version to fix Windoze format issue caused
Rem                             by some ADE bug
Rem    sravada     02/22/06  -  move some drops in here from sdoeoper.sql
Rem    rkothuri    01/30/06  - drop sdo_statistics force 
Rem    bgouslin    01/17/06  - changes for demos to demo move 
Rem    zzhang      11/18/05  - include sdoe102.sql
Rem    rkothuri    05/31/05  - fix lrgs 1860316 
Rem    geadon      05/26/05  - Drop sdo_drop_user_before trigger 
Rem    geadon      05/23/05  - lrg 1858395: fix sdo_rdf downgrade 
Rem    nalexand    05/23/05  - add sdo_rdf_internal package 
Rem    rkothuri    05/10/05 -  drop current_user_sdo_index_metadata 
Rem    geadon      04/28/05 - drop SDO_RDF_MATCH synonym 
Rem    geadon      04/27/05 - rename RDF_APIS to SDO_RDF_INFERENCE 
Rem    sravada     04/04/05 - remove RDF stuff 
Rem    sravada     03/14/05 - lrg 1835517 
Rem    sravada     12/06/04  - 
Rem    sravada     11/10/04  - remove OGIS triggers 
Rem    sravada     10/21/04 -  bug 3936644 
Rem    sravada     09/20/04 - remove epsg tables on downgrade 
Rem    sravada     08/30/04 - add downgrade steps 
Rem    sravada     05/20/04 - sravada_fix_install
Rem    sravada     05/19/04 - Created
Rem

@@sdoe102.sql

ALTER SESSION SET CURRENT_SCHEMA = MDSYS;
 
EXECUTE dbms_registry.downgrading('SDO');


declare
begin
  begin
    execute immediate
   ' alter type SDO_Topo_Geometry ' ||
   'drop member function Get_TGL_Objects ' ||
   ' return SDO_TGL_OBJECT_ARRAY DETERMINISTIC CASCADE ';
    exception when others then
    return;
   end;
end;
/


drop public synonym PolygonFromText;
drop FUNCTION OGC_PolygonFromText;
drop public synonym LineStringFromText;
drop FUNCTION OGC_LineStringFromText;
drop public synonym MultiPolygonFromText;
drop FUNCTION OGC_MultiPolygonFromText;
drop public synonym MultiLineStringFromText;
drop FUNCTION OGC_MultiLineStringFromText;
drop public synonym PointFromText;
drop FUNCTION OGC_PointFromText;
drop public synonym PolygonFromWKB;
drop FUNCTION OGC_PolygonFromWKB;
drop public synonym LineStringFromWKB;
drop FUNCTION OGC_LineStringFromWKB;
drop public synonym MultiPolygonFromWKB;
drop FUNCTION OGC_MultiPolygonFromWKB;
drop public synonym MultiLineStringFromWKB;
drop FUNCTION OGC_MultiLineStringFromWKB;
drop public synonym PointFromWKB;
drop FUNCTION OGC_PointFromWKB;
drop public synonym Dimension;
drop FUNCTION OGC_Dimension;
drop public synonym AsText;
drop FUNCTION OGC_AsText;
drop public synonym AsBinary;
drop FUNCTION OGC_AsBinary;
drop public synonym SRID;
drop FUNCTION OGC_SRID;
drop public synonym X;
drop public synonym OGC_X;
drop FUNCTION OGC_X;
drop public synonym Y;
drop public synonym OGC_Y;
drop FUNCTION OGC_Y;
drop public synonym NumInteriorRings;
drop FUNCTION OGC_NumInteriorRings;
drop public synonym InteriorRingN;
drop FUNCTION OGC_InteriorRingN;
drop public synonym ExteriorRing;
drop FUNCTION OGC_ExteriorRing;
drop public synonym NumGeometries;
drop FUNCTION OGC_NumGeometries;
drop public synonym GeometryN;
drop FUNCTION OGC_GeometryN;
drop public synonym Disjoint;
drop FUNCTION OGC_Disjoint;
drop public synonym Touch;
drop FUNCTION OGC_Touch;
drop public synonym Within;
drop FUNCTION OGC_Within;
drop public synonym Overlap;
drop FUNCTION OGC_Overlap;
drop public synonym Ogc_Contains;
drop FUNCTION OGC_Contains;
drop public synonym Intersection;
drop FUNCTION OGC_Intersection;
drop public synonym Difference;
drop FUNCTION OGC_Difference;
drop public synonym Ogc_Union;
drop FUNCTION OGC_Union;
drop public synonym ConvexHull;
drop FUNCTION OGC_ConvexHull;
drop public synonym Centroid;
drop FUNCTION OGC_Centroid;
drop public synonym GeometryType;
drop FUNCTION OGC_GeometryType;
drop public synonym StartPoint;
drop FUNCTION OGC_StartPoint;
drop public synonym EndPoint;
drop FUNCTION OGC_EndPoint;
drop public synonym Boundary;
drop FUNCTION OGC_Boundary;
drop public synonym Envelope;
drop FUNCTION OGC_Envelope;
drop public synonym IsEmpty;
drop FUNCTION OGC_IsEmpty;
drop public synonym NumPoints;
drop FUNCTION OGC_NumPoints;
drop public synonym PointN;
drop FUNCTION OGC_PointN;
drop public synonym IsClosed;
drop FUNCTION OGC_IsClosed;
drop public synonym PointOnSurface;
drop FUNCTION OGC_PointOnSurface;
drop public synonym Area;
drop FUNCTION OGC_Area;
drop public synonym Buffer;
drop FUNCTION OGC_Buffer;
drop public synonym Equals;
drop FUNCTION OGC_Equals;
drop public synonym SymmetricDifference;
drop FUNCTION OGC_SymmetricDifference;
drop public synonym Distance;
drop FUNCTION OGC_Distance;
drop public synonym OGC_Length;
drop FUNCTION OGC_Length;
drop public synonym IsSimple;
drop FUNCTION OGC_IsSimple;
drop public synonym IsRing;
drop FUNCTION OGC_IsRing;
drop public synonym Intersects;
drop FUNCTION OGC_Intersects;
drop public synonym Cross;
drop FUNCTION OGC_Cross;
drop public synonym Relate;
drop FUNCTION OGC_Relate;

DROP TYPE ST_MultiPolygon;
DROP TYPE ST_POLYGON_ARRAY;
DROP TYPE ST_POLYGON;
DROP TYPE ST_MULTILINESTRING;
DROP TYPE ST_LINESTRING_ARRAY;
DROP TYPE ST_LINESTRING;
DROP TYPE ST_MULTIPOINT;
DROP TYPE ST_CIRCULARSTRING;
DROP TYPE ST_CURVEPOLYGON;
DROP TYPE ST_COMPOUNDCURVE;
DROP TYPE ST_MULTICURVE;
DROP TYPE ST_CURVE_ARRAY;
DROP TYPE ST_CURVE;
DROP TYPE ST_Point_Array;
DROP TYPE ST_MULTISURFACE;
DROP TYPE ST_SURFACE_ARRAY;
DROP TYPE ST_SURFACE;
DROP TYPE ST_Point;
DROP TYPE ST_DOUBLE_PRECISION_ARRAY;
DROP TYPE ST_GEOMCOLLECTION;
DROP TYPE ST_GEOMETRY_ARRAY;
DROP TYPE ST_GEOMETRY;
                                                                                
drop type body SDO_GEOMETRY ;

declare
begin
   begin
     execute immediate ' PURGE TABLESPACE SYSTEM USER MDSYS  ';
     execute immediate ' PURGE TABLESPACE SYSAUX USER MDSYS  ';
           exception when others then NULL;
   end;
end;
/
                                                                                
                                                                                
ALTER TYPE SDO_GEOMETRY
DROP MEMBER FUNCTION GET_WKB RETURN BLOB DETERMINISTIC,
DROP MEMBER FUNCTION GET_WKT RETURN CLOB DETERMINISTIC,
DROP MEMBER FUNCTION ST_CoordDim RETURN SMALLINT DETERMINISTIC,
DROP MEMBER FUNCTION ST_IsValid RETURN INTEGER DETERMINISTIC,
DROP CONSTRUCTOR FUNCTION SDO_GEOMETRY(wkt IN CLOB, srid IN INTEGER DEFAULT NULL) RETURN SELF AS RESULT,
DROP CONSTRUCTOR FUNCTION SDO_GEOMETRY(wkt IN VARCHAR2, srid IN INTEGER DEFAULT NULL) RETURN SELF AS RESULT,
DROP CONSTRUCTOR FUNCTION SDO_GEOMETRY(wkb IN BLOB, srid IN INTEGER DEFAULT NULL) RETURN SELF AS RESULT
CASCADE;
                                                                                


drop table MDSYS.SDO_PROJECTIONS_OLD_SNAPSHOT;
drop table MDSYS.SDO_ELLIPSOIDS_OLD_SNAPSHOT;
drop table MDSYS.SDO_DATUMS_OLD_SNAPSHOT;

drop table MDSYS.SDO_CS_SRS;
drop table MDSYS.SDO_COORD_OP_PARAM_VALS;
drop table MDSYS.SDO_COORD_OP_PARAM_USE;
drop table MDSYS.SDO_COORD_OP_PARAMS;
drop table MDSYS.SDO_COORD_OP_PATHS;
drop table MDSYS.SDO_PREFERRED_OPS_USER;
drop table MDSYS.SDO_PREFERRED_OPS_SYSTEM;

drop table MDSYS.SDO_coord_axes;
drop table MDSYS.SDO_coord_axis_names;

ALTER TABLE MDSYS.SDO_UNITS_OF_MEASURE DROP 
        CONSTRAINT ELLIPSOID_FOREIGN_LEGACY ;
ALTER TABLE MDSYS.SDO_UNITS_OF_MEASURE  DROP
 CONSTRAINT UNIT_OF_MEASURE_FOREIGN_UOM;


ALTER TABLE MDSYS.SDO_PRIME_MERIDIANS DROP
CONSTRAINT PRIME_MERIDIAN_FOREIGN_UOM ;


ALTER TABLE MDSYS.SDO_DATUMS DROP 
 CONSTRAINT DATUM_FOREIGN_ELLIPSOID ;
ALTER TABLE MDSYS.SDO_DATUMS DROP 
CONSTRAINT DATUM_FOREIGN_MERIDIAN;

ALTER TABLE MDSYS.SDO_COORD_REF_SYS DROP
CONSTRAINT COORD_REF_SYS_FOREIGN_CS;
ALTER TABLE MDSYS.SDO_COORD_REF_SYS DROP
CONSTRAINT COORD_REF_SYS_FOREIGN_DATUM;

ALTER TABLE MDSYS.SDO_COORD_OPS DROP
CONSTRAINT COORD_OPERATION_FOREIGN_SOURCE ;
ALTER TABLE MDSYS.SDO_COORD_OPS DROP
CONSTRAINT COORD_OPERATION_FOREIGN_TARGET ;
ALTER TABLE MDSYS.SDO_COORD_OPS DROP
CONSTRAINT COORD_OPERATION_FOREIGN_METHOD;


drop table MDSYS.SDO_ELLIPSOIDS;
drop table MDSYS.SDO_DATUMS;

drop table MDSYS.SDO_UNITS_OF_MEASURE ;

drop table MDSYS.SDO_PRIME_MERIDIANS;
drop table MDSYS.SDO_COORD_SYS;
drop table MDSYS.SDO_COORD_REF_SYS;
drop table MDSYS.SDO_COORD_OP_METHODS;
drop table MDSYS.SDO_COORD_OPS;

drop view MDSYS.MY_SDO_INDEX_METADATA;
drop view MDSYS.SDO_AVAILABLE_NON_ELEM_OPS;
drop view MDSYS.SDO_AVAILABLE_ELEM_OPS;
drop view MDSYS.SDO_AVAILABLE_OPS;
drop view MDSYS.CS_SRS;
drop view MDSYS.SDO_ANGLE_UNITS;
drop view MDSYS.SDO_AREA_UNITS;
drop view MDSYS.SDO_DIST_UNITS;
drop view MDSYS.SDO_ELLIPSOIDS_OLD_FORMAT;
drop view MDSYS.SDO_PROJECTIONS_OLD_FORMAT;
drop view MDSYS.SDO_DATUMS_OLD_FORMAT;
drop view MDSYS.SDO_COORD_REF_SYSTEM;
drop view MDSYS.SDO_DATUM_ENGINEERING;
drop view MDSYS.SDO_DATUM_GEODETIC;
drop view MDSYS.SDO_DATUM_VERTICAL;
drop view MDSYS.SDO_CRS_COMPOUND;
drop view MDSYS.SDO_CRS_ENGINEERING;
drop view MDSYS.SDO_CRS_GEOCENTRIC;
drop view MDSYS.SDO_CRS_GEOGRAPHIC2D;
drop view MDSYS.SDO_CRS_GEOGRAPHIC3D;
drop view MDSYS.SDO_CRS_PROJECTED;
drop view MDSYS.SDO_CRS_VERTICAL;
drop view MDSYS.GEODETIC_SRIDS;

declare
begin
  begin
   execute immediate ' drop type ST_COMPOUNDCURVE ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_CIRCULARSTRING ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_MultiPolygon ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_MULTILINESTRING ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_MULTISURFACE ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_MULTICURVE ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_MULTIPOINT ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_GEOMCOLLECTION ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_POLYGON_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_POLYGON ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_LINESTRING_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_LINESTRING ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_CURVEPOLYGON ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_SURFACE_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_SURFACE ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_CURVE_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_CURVE ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_Point_Array ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_Point ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_DOUBLE_PRECISION_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_GEOMETRY_ARRAY ';
   exception when others then NULL;
  end;
  begin
   execute immediate ' drop type ST_GEOMETRY ';
   exception when others then NULL;
  end;
end;
/


declare
begin
                                                                                
  begin
    execute immediate 'alter type sdo_index_method_10i ' ||
    ' drop STATIC FUNCTION execute_index_ptn_drop(ia in SYS.ODCIIndexInfo) '||
           ' RETURN number ';
    exception when others then NULL;
  end;
  begin
                                                                                
    execute immediate 'alter type sdo_index_method_10i ' ||
    ' drop STATIC FUNCTION index_update (ia sys.odciindexinfo, rid varchar2, '||
    ' oldval mdsys.sdo_geometry, newval mdsys.sdo_geometry, ' ||
    ' env sys.ODCIEnv) RETURN NUMBER ';
    exception when others then NULL;
  end;
  begin
    execute immediate 'alter type sdo_index_method_10i ' ||
    ' drop STATIC FUNCTION insert_delete(ia sys.odciindexinfo, rid varchar2,' ||
    ' val mdsys.sdo_geometry, upd_type varchar2, env sys.ODCIEnv) ' ||
    ' RETURN number  ';
    exception when others then NULL;
  end;
end;
/

-- first delete all the GML related schemas
declare
  usr varchar2(30);
  url varchar2(200);
  loc clob;
  cnt number;
begin
  usr := 'MDSYS';
  url := 'http://www.opengis.net/cartographicText.xsd';
                                                                                
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;
  if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  url := 'http://www.opengis.net/gml/feature.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  url := 'http://www.opengis.net/gml/geometry.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
 
  url := 'http://www.w3.org/1999/xlink/xlinks.xsd';
  execute immediate
    'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
    into cnt using usr, url;   if cnt <> 0 then
    dbms_xmlschema.deleteSchema(url, dbms_xmlschema.DELETE_CASCADE);
  end if;
end;
/
  
declare
begin
  begin
    execute immediate  ' drop public synonym SDO_XML_SCHEMAS';
      exception when others then NULL;
  end;

  begin
     execute immediate ' drop table SDO_XML_SCHEMAS ';
      exception when others then NULL;
  end;
end;
/


declare
begin
   begin
   DELETE FROM SDO_GEOR_XMLSCHEMA_TABLE WHERE id=1;
      exception when others then NULL;
  end;
end;
/

declare
begin
   begin
   execute immediate 'drop trigger MDSYS.OGIS_CRS_INSERT_TRIGGER ';
      exception when others then NULL;
  end;
   begin
   execute immediate 'drop trigger MDSYS.OGIS_CRS_DELETE_TRIGGER ';
      exception when others then NULL;
  end;
   begin
   execute immediate 'drop table MDSYS.OGIS_GEOMETRY_COLUMNS ';
      exception when others then NULL;
  end;
   begin
   execute immediate 'drop table MDSYS.OGIS_SPATIAL_REFERENCE_SYSTEMS ';
      exception when others then NULL;
  end;
end;
/
                                                                                 
declare
begin
   begin
     execute immediate 'drop package MDSYS.sdo_geor_lizardtech ';
      exception when others then NULL;
   end;
   begin
     execute immediate 'drop library MDSYS.sdo_geor_lizardtech_lib ';
      exception when others then NULL;
   end;
end;
/

declare
begin
   begin
     execute immediate ' drop package MDSYS.sdo_net_mem ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop public synonym sdo_net_mem ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_link_i ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_network_i ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_network_manager_i ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_node_i ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_path_i ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_network_t ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop PUBLIC SYNONYM sdo_network_t  ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop type MDSYS.sdo_statistics';
      exception when others then NULL;
   end;

end;
/

declare
begin
   begin
     execute immediate ' drop type  tfm_plan  ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop PUBLIC SYNONYM   tfm_plan  ';
      exception when others then NULL;
   end;
   begin
     execute immediate ' drop trigger  SDO_CRS_INSERT_TRIGGER  ';
      exception when others then NULL;
   end;
                                                                                
end;
/

-- RDF section
declare
begin
   begin
    execute immediate ' drop public synonym sdo_rdf_triple_s ';
          exception when others then NULL;
   end;
   begin
    execute immediate 'drop public synonym sdo_rdf_triple ';
          exception when others then NULL;
   end;
   begin
    execute immediate 'drop public synonym sdo_rdf ';
          exception when others then NULL;
   end;
   begin
    execute immediate 'drop package sdo_rdf ';
          exception when others then NULL;
   end;
   begin
    execute immediate 'drop public synonym sdo_rdf_internal ';
          exception when others then NULL;
   end;
   begin
    execute immediate 'drop package sdo_rdf_internal ';
          exception when others then NULL;
   end;
   begin
    execute immediate ' drop type body sdo_rdf_triple_s ';
          exception when others then NULL;
   end;
end;
/

declare
begin
   begin
      execute immediate ' drop table RDF_RULEBASE_MEMBER$ ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop table RDF_RULE$ ';
        exception when others then NULL;
   end;
    
   begin
      execute immediate ' drop table RDF_RULEBASE$ ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop table RDF_PRECOMP_DEP$ ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop table RDF_PRECOMP$ ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop table RDF_PRECOMP$ ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop package RDF_APIS ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop package RDF_APIS_INTERNAL ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop package RDF_APIS_USER ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop procedure load_predefined_rulebases ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop type RDF_MATCH_impl_t ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop function RDF_MATCH ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop public synonym SDO_RDF_MATCH ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop public synonym RDF_APIS_INTERNAL ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop public synonym SDO_RDF_INFERENCE ';
        exception when others then NULL;
   end;

   begin
      execute immediate ' drop trigger sdo_drop_user_before ';
        exception when others then NULL;
   end;
     
end;
/

DECLARE
    usr VARCHAR2(30);
    url VARCHAR2(200);
    cnt NUMBER;
  BEGIN
    usr := 'MDSYS';
    url := 'http://www.oracle.com/2004/spatial/epsg/gridfile/schema/sdoepsggrid.xsd';

    EXECUTE IMMEDIATE
      'SELECT count(*) FROM dba_xml_schemas WHERE owner=:1 AND schema_url=:2'
      INTO cnt using usr, url;
    IF (cnt <> 0) THEN
      DBMS_XMLSCHEMA.deleteSchema(url, DBMS_XMLSCHEMA.DELETE_CASCADE);
    end if;
END;
/

drop type VECTOR_2D;
drop type NUM_ARRAY;
drop type STRING_ARRAY;

alter session set current_schema=SYS;
call dbms_java.grant_permission('SYSTEM', 'java.io.FilePermission',
                                 '<<ALL FILES>>', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission',
                                'md/lib/*', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);
call dbms_java.grant_permission('ORDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);
                                                                                
call dbms_java.dropjava('-force -synonym -schema MDSYS  md/lib/sdordf.jar');

call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission',
                                 'md/lib/*','read');
call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission',
                                 'sdo/demo/georaster/jlibs/*','read');
                                                                                
commit;
/* drop all the trusted libraries */
                                                                                
Alter session set current_schema=MDSYS;




@@sdocmprg.sql
commit;
 
EXECUTE dbms_registry.downgraded('SDO', '10.1.0');
 
ALTER SESSION SET CURRENT_SCHEMA = SYS;


