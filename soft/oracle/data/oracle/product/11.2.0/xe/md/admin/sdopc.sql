Rem
Rem $Header: sdo/admin/sdopc.sql /main/12 2009/02/17 18:35:58 sravada Exp $
Rem
Rem sdotin.sql
Rem
Rem Copyright (c) 2006, 2009, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdotin.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     02/04/09 - add paramter to get pt_id/blk_id in to_geometry
Rem    sravada     02/14/08 - upgrade diffs
Rem    rkothuri    08/04/06 - change clippc i/f
Rem    rkothuri    05/23/06 - add tin read and clip functions 
Rem    rkothuri    05/03/06 - add synonym 
Rem    rkothuri    01/31/06 - TIN functionality 
Rem    rkothuri    01/31/06 - TIN functionality 
Rem    rkothuri    01/31/06 - Created
Rem

CREATE OR REPLACE PACKAGE prvtpc authid current_user AS
END prvtpc; 
/
show errors;

CREATE OR REPLACE PACKAGE BODY prvtpc AS

END prvtpc; 
/
show errors;


CREATE OR REPLACE PACKAGE sdo_pc_pkg authid current_user AS
  -- all tables as "schema.table" for simpler interface.
  FUNCTION INIT(basetable varchar2, basecol varchar2,
       blktable             VARCHAR2,
       ptn_params varchar2,
       pc_extent            sdo_geometry default null,
       pc_tol               NUMBER default 0.0000000000005,
       pc_tot_dimensions    NUMBER default 2,
       pc_domain            sdo_orgscl_type default null,
       pc_val_attr_tables   SDO_STRING_ARRAY default null,
       pc_other_attrs             SYS.XMLTYPE default null)
    RETURN SDO_PC;

  PROCEDURE CREATE_PC(inp sdo_pc, inptable varchar2,
	              clstpcdatatbl varchar2 default null) ;

  -- works as read if qry params are null
  FUNCTION CLIP_PC(inp sdo_pc,
                   ind_dim_qry sdo_geometry, 
                   other_dim_qry sdo_mbr, 
		   qry_min_res number, qry_max_res number, 
                   blkno number default null)
    RETURN MDSYS.SDO_PC_BLK_TYPE PIPELINED;

  PROCEDURE DROP_DEPENDENCIES(basetable varchar2, col varchar2);

  FUNCTION TO_GEOMETRY(pts BLOB, num_pts NUMBER, 
                       pc_tot_dim NUMBER, srid NUMBER default null,
                       blk_domain sdo_orgscl_type default null,
                       get_ids  NUMBER default NULL)     
    RETURN MDSYS.SDO_GEOMETRY DETERMINISTIC;
                                                                                 
  FUNCTION GET_PT_IDs(pts BLOB, num_pts NUMBER, 
                      pc_tot_dim NUMBER, 
                      blk_domain sdo_orgscl_type default null)     
    RETURN SDO_NUMBER_ARRAY DETERMINISTIC;
                                                                                 

/*
PROCEDURE POPULATE_FROM_CLSTPCDATA(inp sdo_pc, clstpcdatatbl varchar2);                                                                          
  
  PROCEDURE WRITE_CLSTPCDATA(inp sdo_pc, clstpcdatatbl varchar2);
*/


/*
  -- works as read if qry is null
  FUNCTION CLIP_PCAttrs(inp sdo_pc, qry sdo_geometry, blkno number default null)
    RETURN ANYDATASET;
  FUNCTION CLIP_PC_PtIds(inp sdo_pc, qry sdo_geometry, blkno number default null)
    RETURN ANYDATASET;
*/

END sdo_pc_pkg;
/
show errors;
GRANT EXECUTE ON sdo_pc_pkg TO public; 
create or replace public synonym sdo_pc_pkg for mdsys.sdo_pc_pkg;

--GRANT EXECUTE ON sdo_tin_table TO public; 


