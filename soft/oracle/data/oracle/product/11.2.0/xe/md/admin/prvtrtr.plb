create or replace type sdoridlist as varray(1048576) of varchar2(24)
/
show errors;
GRANT EXECUTE ON MDSYS.sdoridlist to PUBLIC WITH GRANT OPTION;
declare
begin
  begin
  execute immediate
    'revoke execute on rtree_index_method from public';
  exception when others then NULL;
  end;
end;
/
declare
begin
  begin
  execute immediate
    'revoke execute on rtree_idx from public';
  exception when others then NULL;
  end;
end;
/
declare
begin
  begin
  execute immediate 
    'drop type rtree_index_method force ';
  exception when others then NULL;
  end;
end;
/
declare
begin
  begin
  execute immediate 
    'drop type rtreejoin_imp_t force ';
  exception when others then NULL;
  end;
end;
/
declare
begin
  begin
  execute immediate 
    'drop package rtree_idx';
  exception when others then NULL;
  end;
end;
/
CREATE OR REPLACE PACKAGE rtree_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
102 f7
jPkObJAz3IQH0sF3NsKjkjlnX44wgxBKLcusZy+iee7VYXdGVLQJYsRtcUYAwr8HaUzvAmIt
pU7pmQtr3Cr6RzRZ0ItGgEBDYUfHRYe5qX8hp92iXKfIURdE0q1npJU2HjbYL9A+M0Y3OPbz
5F3+Sl8QRCsn6qni4/s9IU0H4KS1yPbe3W97I2Z6vndckrDRz/kJcsJ0VED+8D27IoiXqOWp
2+LgYsdv4bKXblbwQ4v7o3p6UQ==

/
SHOW ERRORS;
CREATE OR REPLACE TYPE rtree_index_method wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
719 268
h9w4EWhGeFZVxd24Aze9Eyu5/i4wgztcACCGfHTtKvmUdqTdU2t9PKH3fW2eusA+xSy3pzkR
Bxn76Qf4OoCY4gVx4Go+Ufro1/4cOlns/rIVHgUe7shBqiQc+eicagXrT4Bp97E8qUqifVeV
qTbFqQ9VjItWcmWpDo0M7L9nJxOa0W3f2xg7QGV71zzapWAFD4U8fwkmIUBop0XCN6+AQEso
JAV4GTUFpQl8koSF93ZTVGRGI3Roe9baHqKkhEg9pjHGDA+moYj96SM7feipfqiAj6RIeOWs
g6CIr87DZE2m9RP75uKcMQZUgPcuKXxUqIZsZc8B4uLmtsXULUghGPy46CxJbsHwddI3oea2
aIZ2H8jb1FlpdjI6mfm0rfbdXzQDT66a2TE7bsVeXGVl71z/UEnf1QgxRUhQ0SlagG7P6/o6
aSOVFV012gprTHbteuDmWVXrw+9k7Vjv0pg7CNfIsS/svt4on1oS41r4vkI3KW37wNXknM4t
DdH6rOSqgNKn0iKqBg+V74RmwIsvavqfrG+Y1EggT+CchSOQ2psFfBSFd0CMJKw2g5o1te7w
d6BXVauM2EWbNafUR7mR2ZgstabbVIzs

/
SHOW ERRORS;
create or replace library ORDMD_RTREE_LIBS wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
16
2c 61
Nq8Gnj+8C0LYUxmmU3u6TXp8etEwg04I9Z7AdBjDpSjACDL0KJ69wDLw/gj1nivnvZ6yy1Iy
zLh0K+fLUnQI9WHJpqakZHRX

/
create or replace type body rtree_index_method wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
1135 467
M/ajoW4WHPVAQA9J/hNRYQdPo5Uwg+3qLiAGfC9V9k7V2Iem+vxrxjszcH4dh0TaDZDhsOc1
Kzt3eeoh1c1ymi2QNgm8DINaFNuI0HMZE1hZilbuTWS0dDsYXEEXrpbZ5A8z2pYcD5wW0xvm
54gM2A4RyPInAvFaf8VAaJ7evhwvly7qdBFWX+nFDYVHR/ADaXNPbqN4aIT/DFchxCanWuiG
IrEWzrtOa8iNoUP8+NCkQUXXa0hbudpThmBGkc8jBSmBuVGLCbmoXCW0/bcJtNgIOXHCeXa/
zKD9O7/cKkmnjOLKgf/kU8gdAszm0rQOW1Ep4326X1HVV+TmKA2t5P5xIaHKOQg+fjXxtCBM
8X+AsZ2ck2mRLZfB6T/NLJVRZPRBqGMxdQGDJ/+CBE5WUfRL7zZaYjBOHUtsuERBLsR21Fov
/h3sbLaJVGP7ZNwmlfb0FfOlFp7Y9qgHlN0jGTw2pgcAg4zi1ThN53TSy07RGu6DuYZMmWHJ
c3Py2sv6SSgUYAuLz4jowlIPhmKN/5QpKUNiD5RibfVuWq/h+/Zc8s89gVPQc8KQCraGf48R
oRuUA67I3lgkwTvBJQeRns7l9h5lpHehCjbNDLCkdE8Tntk9Hk9k6FPtFkCT03al6zqtbZDg
gf4Bf7WSBH7QDiE4XcfrNA3FvHPisrKAjIYWsuE6rC4gem7YQPRMkqcpo5TqwXrSiFwsLTXX
99LvfxZTlnNRUCHYmq/bnccQCw2LDThdcwHFCAo2dZwHKeS+heoPpIz70Ydq4goBrdPHraCM
O0ITdI8bLBPNlz1bDCmof+439i50Devy6GXIzy/7tu+V3L19kO648uFsgTSix5/8oBLMrpiu
vkGQeyjjg+eiUwKBcvr+9MiI0rm7tsR+pwxYsfmWSEGaGEISeVherW7Qxpv6EGHyS7YITVXt
3dwNjJ/cqHb1g5oK2lXygTP9jaPuCvKgtrbOVYCEgo2MmcK4R+1dBZNXj6dKRQkK9CbVcAOJ
5rJ48GReBLgsidejI9495I0fbcY8UfDG1rgZNc8NF9CIB9nJr/UwajrAufN72wDwje5VDFi6
ep28Qw8VaGwQ83z7u/iYILX7ALUUPg==

/
SHOW ERRORS;
/
CREATE OR REPLACE PACKAGE BODY rtree_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
1f6 168
Cf6UZmrEXk1cJ8M5WZ0CRAirEDwwgwLIr0jWfHRAvwKwpS3rUHdSdoOocMQNOdxDNoNBU9gH
hwqkM86ZL6MaeaNm6Twg+G2quy6BSGE2Y5W2cSRkwceqOs7hhiWOQNlBdzDh3hPxjoeacExG
jn6jJnq6+28aC5q5f8M7tKnavhdtlNB72dTJDl9QiDufYE5W0vk/fvCQJmuvN49QMy4mquqZ
pD8rhOwUvaG//Wbb0K82FjorAd+vSNs5f8qHUaNWoM8mjaABobA1i6dLOYHyd665/Zvabb8F
WirwBQTtF7OjOWpXlfGJtYDgUhr53Ox+chr9J3zEGGoA6fVHt3DnFdq11sYG7v1UIr/h

/
COMMIT;
CREATE OR REPLACE  PUBLIC SYNONYM sdo_rtree_admin FOR mdsys.sdo_rtree_admin
/
CREATE OR REPLACE PACKAGE sdo_rtree_admin wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
8ce 2c5
5ZHFQ3PcqBoqToUrpQGv5dUQwDEwg9dcLUgFfC/NJ6qV8H/K9YX89Btyb/MYw9CAsbnmjlpA
y3k3oLXQQV1T8XaVKwsoQ903NL/Pu1WQw9VzDt/nnRyqi8Gacq6uOh6G/iAkZ2zRO/uQ41DH
3PO9w25ywaYeXSoFjzh5pO9VeXzDcADDG7mGxhHm4ivDZ8fPtZJx027i8pUHG7gKWpwYuZlL
yZwquw77OYw7gTm/wHxASi4ODoTgyhvbaGDBnr506MgSnZ8nHSJc1RoCkcpdolXH2TcWu2S2
0j9/xC9lwiODq5cHR0Wg/+f4MB8y9I2b5RX7HvOMWwpj1CdDQHx8YQ253Qjgb/FvHq8S7HvQ
/QX33igMvjuhecVsNff6J/9G9ok5PRbmh23VjwN4zYfZ3MKW1AB7C/Fp5CAue5+Jolf9Opp0
J0YueH1fLjtaOlo062MTdfUGs2OlztO18XZ+idGChSu/sHgn1IzatztImznwG/PmpPxdjgvo
L1cPAGSwTcRtAKMR1swPf8uMjp6pmUuLPYq4beKaywMx2JwNK9thAQUMYoUN8kpEhmjhYJc6
v0g56CzbGHrEtawQO15Gn2baAsEVQ5rK7Zo/Tkn7JxV5+V0wmubRKMahfeC91garlcUeyBlB
hy8jXtUAGZGQCFvLH2CwjPV2iKv9dtLoseyXwzyWJOT7WMSBng==

/
SHOW ERRORS;
/
CREATE OR REPLACE PACKAGE BODY sdo_rtree_admin wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
1804 7b7
iDHDBI4q8/EdZu7+U7vKfh6WklQwg1VUedCreC+56s6UKxqeEWFJjyXl14EiVpg52AUf4BOZ
0tkYdrpEKaa9WgXDOgikBZhhnExwu9HlURvzOACWHkKLWAh19B5Vvu9fOS0kgPhD3zGkn97s
VqcePTm+2DGiEVfwetwG57Kh3X950Lh2ZtNrUBfHn5XkV71bdzXgvg+HW1vGquacVb2VEpwj
C5wzCvXjJAx6IoQVXIoSRqIiQa/mjdG2GI+WQcCirZVX94Oh4zpXM6TIgGrdpGsRq8t8zalb
GDqJ7LLKFRq6etbOZnjIKPf94NIn7UcvOguBMFq/ZBoWyh38Ja0YJiyS9CNs2D5Ju70s75ZK
VzC3mbacrNWlR4MxwuEMEBXUEAa+eujjUnuBMD7mvdiv/qxhprH6qP/1DPKIRUpAOlG6Gz3H
TZbFDN2ym7C/8QFmM+fOZrNNx+t+b1of6ZbvcfR+DSaBDREq3e/TAbyELF4j6UwVuOF3Nwd1
YbprmsBpDLyMQ+S0mLkPtXRXjqYnslWp7qdxlfeqXt1aRuKfffChjx2e2Nc0f8tMoQaZYyuR
QOf3mNYOEUDPZl4O4O19AtOa38RLf77TKWp25eNL5RVeXP6lCFINcaq0zU9UMH2RQ4hxirvG
dHkRyxHwpoDvIp9WRm5mo43LVvBo+2dYhlgT590Yn541uuLNghQMXHW9FMf/sx3PEAPShfbT
IeF955y+LbpitfZIjqPbginMst5iQIm42OnTbaRJU6jEN+/5eifPsuB2sxXe36vldlQb+jSm
1Q5CznW2ctgincZ+pymaB/XHzk/EdYYzBm6Mrl673CfC71xi31hXl/KTDSsF8bUbITNLJGME
Lgb6Ibit3BF2Tl2VwYMtXICl5qNlHMB3hbEF7Ui1Lp/hmaGa03N3HhfTlgayd2mKXtXIRtsT
JTlQlmsw5w+TbvSRpqxK8J6fvO1E01s3JUYWHj4YrTnAR566L20GTMlqe9OorKSr+jIcvRel
nKPu/qr304K+FluEd0iufQCu3T2UXa+YDBS1Bd+GZJjL/DJImKQWqJCNJ4KQjtnl1s4EsYic
l2hpA+sTD7KIo5FOweHcvaCqznXiH8uj9OCAE5V7VcRKYFaBNgOU2z7GCl+UqUKZf3Zw0IxD
J6pUeRzLPC7evqOwE3MnmDf+diLap5HEVRxwT33ZnOGtugnpSBStViJ66BFaOLxMeiIbYEvc
KG1DBktpi7IB/j9faxtfJTNkLkWExH15KL5Lee8vCPSBvOwf9zppWG9/vUg61shs0Qt1m9WN
FaKRavIXN8qNyRRuNlc4u+EB5V7sk9S2dkMyN0DC/PvrPtbMnVBJDZDiYghh4iOeBjCsZS8H
dRILzfeiejwCjxEz2q0xTo6GiO7hJO2cu4x1KIbe36EfxcNAQp9RPLnpEtkoPsMt0Eoz0SfG
NPuDyzQPPMIG2iWOPhbl/MFOvWCdVVGHxAViiYA4BafRNmkN5KO7pVvu/GAS+qFku1mTmZf3
UD9ncN+wHltq1xolYWivgxAwLe8dlqqrB5/zIvQQxWL80zHDeKE9gEYBfR7RRhe0E+a7BC5J
ItLI/GiLa3VWg69YYF+bigHYArWH/lna84oCsOgjxcQ7t7ZY0Fj+rDjtm4XHL0VwGxnsNs4c
WzOP6JPR7eb4i6K5MxViZP92nVn+KJqLBKp37NGyCERbb18tgJv6DaZIlVdcprE+ODtiShQx
DK7/ozPSIHLqnofjenW1SB81YkpYdfbeCuXxneh3s09pz080mc5xo3G8JHYmn/8JSPuwgcGq
JZxz7AdB3VOeGCquBpMuvmXG7El8PGJHYTwwow9yL6s+PVE8CEPzo7ATMLgYQTQhGIZhSJrl
HLMuXsz0ALW33TPr6jTGKtnqoJIpKjyINt9vg9EPxYgAWHRNKwhttoV2fOIs8X2LmKr7z2Y0
sQ==

/
SHOW ERRORS;
GRANT EXECUTE ON sdo_rtree_admin TO PUBLIC;
/
CREATE OR REPLACE OPERATOR rtree_filter 
  BINDING (mdsys.sdo_mbr, mdsys.sdo_mbr, varchar2) 
RETURN VARCHAR2 
  WITH INDEX CONTEXT, SCAN CONTEXT rtree_index_method
  USING sdo_rtree_admin.filter;
GRANT EXECUTE ON rtree_filter TO PUBLIC;
CREATE OR REPLACE OPERATOR rtree_nn 
  BINDING (mdsys.sdo_mbr, mdsys.sdo_mbr, varchar2) 
RETURN VARCHAR2 
  WITH INDEX CONTEXT, SCAN CONTEXT rtree_index_method
  USING sdo_rtree_admin.nn;
GRANT EXECUTE ON rtree_nn TO PUBLIC;
CREATE OR REPLACE INDEXTYPE rtree_index FOR
  rtree_filter(mdsys.sdo_mbr, mdsys.sdo_mbr, varchar2),
  rtree_nn(mdsys.sdo_mbr, mdsys.sdo_mbr, varchar2)
USING rtree_index_method
WITH LOCAL RANGE PARTITION;
GRANT EXECUTE ON rtree_index TO PUBLIC;
COMMIT;
CREATE OR REPLACE TYPE RtreeJoin_Imp_t wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
4fc 1e2
QtAu+MVyjxn8ImGrtx1xlU9OqNwwg0PxAK5qfC9AkylkpXValNOU/bcLD8PE9KRVkN7aQvzd
3QdptZ1XW8wqXA41S+76MzoQECwou3tpMDAFu+qW0HL6AO3o1iX3nP/FtvOvxxg2TSEmaTvd
pdh1Pg1AkObtPrmXvm7jKtqM/H2ymI9cS9Qt4qvIpsA1QMTFoiuF50ue/0mxn/TBOlvp2vLn
DX4+j3H0VQJD0jvAIh5Im4D/urR7D1uzh9nxzuB6QWqAnFChzJtHxmHh5Hdh7Dxuumb3W/DJ
JlJ/d5MGoqR16x+X5jsLauA8IWrBCZP8L9iC7DhRYKPEdcrFJCMC3fOu/MjZb5s/kRwqS5Nv
Ni6QTICABn9+EVB1bR3zLOG5zYVAFDn/l+k6jHdZtudKJpBjMkgq/TfiF/BXKxDctaZFJg4u
QB7VDmDUPNaJE3OcfHNAjBxPNeaa7yHVu2QZ+w6YxVQ=

/
show errors;
CREATE OR REPLACE TYPE BODY RtreeJoin_Imp_t wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
9f4 374
0pZVQ18uulIZQ2wEp86R4i3AkFAwgw0rDCAFfHQZMvmU5q69cDZU4gD6ZZBNA2yEUbFdkXRM
QHlUBajXtcYU1FJWJCV+HkbdyM7T+ruML2in3dOQSFitir+YmMG7rg3P5FwsdaDzx9kQVsCJ
rAGN0xYN4HuU00Lx1l4EbpM3/ejVkTmy3ZCn5afpkwtHogF1VYODZ4thKxN1zEdtDJsoWcW7
xKUfoY0VYOVZ2pr+SK1zwHG7PXmOTKwIyeXF03xJmqwF0H3Vn+yuaOMqeNlQlg1UBdOisaxj
yAjFgB9WcoTH6pNMq1pjj+fqklM8YmS47PiHiN2XbuWl4eZsN43Dswg/Jf57ncpwViu9N7aY
i8a8ZPkDXyZ4sErOMvzyaCssBwKVFwV8Ojo72eHDI/3wUBRZBnkEZS5K6t6zF9vp0V+zDlzc
rBmjcakw/0XdkDeBUMJuClyV6t02ocBYBEhhWROgWvYnh82RMxFkMPuKiwjZ+cbr2e5O+biE
JRL09hug5rTKvfJIbqisLX480mHMxtf3VyUxmM8+Us1LNV70PFBOFic3EWt7rCXHrdWcVXOg
qO6MAn6l9KytSM/cJW/aF7sVWg2rETkiYhodT8u0XiKtTZTirgbIptYwED/SrED8fMDQV9ZQ
HeCB4P7Sc6D0x3nQoVnvij9EL7YH0M85FjyCmUKLQnp7lZd9Ev00YWS8m9FKRBq43z0Nmcpe
SXUH1n86jzr1b4mIAHyJZ2g2shxjP5MXT3jlffzmBIbqKDCqvJ25yyia+T+8TGNVc13PLAW0
AHSIn9h4evBSz24nnOMbwHlrAmNb+izz1mbzhR++uN+YkiWq+wjuwU7ZubXWV7MjktUfOis0
+LJ/xVxL

/
SHOW ERRORS;
BEGIN
  BEGIN
    EXECUTE IMMEDIATE 'REVOKE EXECUTE ON RtreeJoinFunc FROM PUBLIC';
    EXECUTE IMMEDIATE 'REVOKE EXECUTE ON MDSYS.RtreeJoinFunc FROM PUBLIC';
    EXCEPTION WHEN OTHERS THEN NULL;
  END;
END;
/
CREATE OR REPLACE FUNCTION RtreeJoinFunc wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
199 144
RCSOpq+cFpgF0Z9BMxahatCrSQQwg/DIf64df3QCk2SdFQv3xRRIAZtsvAbxx2bGPmiJ+zJ9
CRQ2tU9xBwlvw648+ORB2HhvUtTMZ50iKAdVxgghP08Z6kyxr2X26o6bbgoltDx2Msc43c5T
Swd9pAMwIwuIhiwZtdkLZkQ+62dFNw9CFeTgx2a0jEUXd/mXBgM9boX72UTWsPMMS2+UFT15
Er4bbHLL8Eh/BMtzskoSVGLuK8PK+FQqvMJykS4uTQThAvKshMpUeK/OaLsJXXwdl88zSdyO
VIWE+la6aV5RTJEmemMnrr8knmE4NQ==

/
SHOW ERRORS;
GRANT EXECUTE ON RtreeJoinFunc TO PUBLIC;
CREATE OR REPLACE  PUBLIC SYNONYM RtreeJoinFunc FOR MDSYS.RtreeJoinFunc;
SHOW ERRORS;
CREATE OR REPLACE PROCEDURE reset_inprog_index wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
7
161 11b
/y2Sk78OMa1Yuol28mrfzha8VeUwg43QmMvWfHTp2v7VVhvIdUWfwLaZZrM2WFDG2+HlBJIv
AY9O/QKAKbRW99DNE25u8MqTZTWxNBO8JyxkuULH6lV0a6324Gmr3IVm6RmVus67FmIpCd56
JXsbm3/lpkVU4PBYZIJ3ofS9NwQMMnBd1QSngJMtAVGXLRV4EOC4ZJi8EsSBLzAwK99EOKAy
LRxdhUj/AMxnV3PPJ3JmGXxAk8PFNzxWyu+QtPVBc0XbnYfZPBhKCrM/Tvs1acNH

/
SHOW ERRORS;
COMMIT;
