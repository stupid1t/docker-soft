Rem
Rem $Header: sdounloadj.sql 22-feb-2008.10:53:31 alwu Exp $
Rem
Rem sdounloadj.sql
Rem
Rem Copyright (c) 2007, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdounloadj.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    alwu        02/22/08 - comment out dropjava for sdordf.jar
Rem    sravada     05/13/07 - Created
Rem


ALTER SESSION SET CURRENT_SCHEMA = SYS;

-- drop sdonm.jar
call dbms_java.grant_permission('SYSTEM', 'java.io.FilePermission',
                                 '<<ALL FILES>>', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.io.FilePermission',
                                'md/jlib/*', 'read');
call dbms_java.grant_permission('MDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);
call dbms_java.grant_permission('ORDSYS', 'SYS:java.lang.RuntimePermission',
                                'getClassLoader', null);


call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdonm.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdoapi.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdotopo.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdoutl.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdogr.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdogcdr.jar');
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/routepartition.jar');


-- drop sdotype.jar
call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdotype.jar');

-- drop RDF jar. 
-- Seems that this script is only invoked as part of loce111.sql 
-- RDF/OWL cannot drop this jar because if this jar is dropped, then in the
-- case that downgrade is followed by upgrade, we no longer have this jar.
-- call dbms_java.dropjava('-force -synonym -schema MDSYS md/jlib/sdordf.jar');

call dbms_java.revoke_permission('MDSYS','SYS:java.io.FilePermission',
                                 'md/jlib/*','read');



