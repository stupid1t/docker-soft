Rem
Rem $Header: sdotopoh.sql 29-apr-2008.14:30:27 sravada Exp $
Rem
Rem sdotopoh.sql
Rem
Rem Copyright (c) 2002, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      sdotopoh.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     04/29/08 - bug 6875753
Rem    sravada     03/15/07 - add versionEnabled func
Rem    sravada     12/15/06 - add sdo_topo_crt_rlids_view
Rem    sravada     01/09/06 - support user specified tg_l_id, topology_id 
Rem    sravada     09/15/04 - exp/imp support 
Rem    sravada     08/12/04 - add topo_elem int for operator 
Rem    sravada     04/28/04 - add digits_right_of_decimal 
Rem    ningan      03/25/04 - modify get_feature_properties
Rem    ningan      03/04/04 - modify get_topo_objects interface
Rem    ningan      01/12/04 - modify sdo_topo.relate interface to reduce
Rem                           OCI overhead 
Rem    ningan      12/24/03 - add sdo_topo.relate taking sdo_geom and topo_geom
Rem                           as input
Rem    ningan      12/24/03 - add sdo_topo.relate taking topo_geom and 
Rem                           properties of another topo_geom as input 
Rem    ningan      11/21/03 - improve performance of sdo_topo.relate(...) 
Rem    sravada     11/17/03 - add topo filter 
Rem    ningan      10/29/03 - add sdo_topo.relate(...)
Rem    ningan      10/17/03 - add sdo_topo.get_feature_properties(...)
Rem    fjlee       10/20/03 - Move topo map funcs to sdo_topo_map package 
Rem    fjlee       10/15/03 - remove topo_map parameter in edit methods 
Rem    fjlee       09/24/03 - Add new topomap functions 
Rem    fjlee       09/19/03 - Fix parameter to get_topo_name 
Rem    sravada     09/10/03 - add internal_tgids 
Rem    sravada     08/15/03 - add support for hierarchy model 
Rem    fjlee       08/25/03 - Add move isolated node support 
Rem    fjlee       08/07/03 - Add move edge support 
Rem    sravada     08/04/03 - fix parameters 
Rem    fjlee       07/28/03 - Support move node operation
Rem    sravada     07/09/03 - add storage parameters
Rem    fjlee       07/07/03 - Add commit_db, rollback_db, and others
Rem    sravada     06/27/03 - 
Rem    sravada     06/19/03 - 
Rem    sravada     06/13/03 - add owner
Rem    sravada     06/06/03 - add constructors with add/delete topo_ids 
Rem    fjlee       05/22/03 - Add TopoMap wrappers
Rem    sravada     04/25/03 - update get_topo_objects
Rem    sravada     04/23/03 - fix get_face_boundary
Rem    sravada     04/15/03 - add insert_relation func for constructor use
Rem    sravada     10/31/02 - change feature to topo_geometry
Rem    sravada     09/12/02 - add get topo_object_array for feature
Rem    sravada     08/19/02 - add get edge_id list for a face
Rem    sravada     08/12/02 - change to full name form
Rem    sravada     08/05/02 - add support for bulk-load
Rem    sravada     07/29/02 - add layer feature type
Rem    sravada     07/22/02 - add tolerance
Rem    sravada     07/19/02 - sravada_topo_metadata_views
Rem    sravada     07/17/02 - Created
Rem



Rem create topology, drop topology, add_topo_geometry, delete_topo_geometry

Create or replace package SDO_TOPO AUTHID current_user AS

 -- In memory representation of USER_SDO_TOPO_INFO
 TYPE topo_metadata IS RECORD 
 (
   owner            VARCHAR2(32),
   topology         VARCHAR2(20),
   topology_id      NUMBER,
   tolerance        NUMBER,
   srid             NUMBER,
   table_schema     VARCHAR2(64),
   table_name       VARCHAR2(62),
   column_name      VARCHAR2(32),
   tg_layer_id      NUMBER,
   tg_layer_type    VARCHAR2(10),
   tg_layer_level   NUMBER,
   child_layer_id   NUMBER
 );

 PROCEDURE create_topology (Topology  IN varchar2, tolerance IN NUMBER,
                        SRID IN NUMBER default NULL, 
                        node_table_storage in varchar2 DEFAULT NULL, 
                        edge_table_storage in varchar2 DEFAULT NULL, 
                        face_table_storage in varchar2 DEFAULT NULL,
                        history_table_storage in varchar2 DEFAULT NULL,
                        Digits_Right_Of_Decimal in number default 16,
                        Topology_ID in number default NULL );

 PROCEDURE drop_topology (Topology  IN varchar2);

 PROCEDURE add_topo_geometry_layer (Topology             IN varchar2,
                        Table_Name           IN varchar2,
                        Column_Name          IN varchar2,
                        Topo_Geometry_Layer_Type   IN varchar2,
                        Relation_table_storage in varchar2 DEFAULT NULL,
                        Child_Layer_id in number DEFAULT NULL,
                        Layer_ID in number default NULL);

 PROCEDURE delete_topo_geometry_layer (Topology     IN varchar2,
                           Table_Name   IN varchar2,
                           Column_Name  IN varchar2);

 PROCEDURE initialize_metadata (Topology IN varchar2);

 PROCEDURE prepare_for_export (Topology IN varchar2);

 PROCEDURE initialize_after_import (Topology IN varchar2,   
                      tg_layer_owner IN varchar2 default NULL);

/*
 --
 -- For a given sdo_topo_geometry, this procedure gets its boundary and
 -- interior.
 --
 PROCEDURE get_feature_properties 
 ( 
   mtdt      IN  TOPO_METADATA,
   tg_id     IN  NUMBER,                                --- feature id
   tg_type   IN  NUMBER,                                --- feature type
   nd_bdy    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- boundary nodes
   nd_int    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- interior nodes
   eg_bdy    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- boundary edges
   eg_int    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- interior edges 
   fc_int    OUT NOCOPY SDO_TOPO_NSTD_TBL               --- interior faces
 );

 PROCEDURE get_topo_properties 
 ( 
   Topology   IN VARCHAR2,
   topo_elem_array   IN   SDO_TOPO_OBJECT_ARRAY,
   tg_type   IN  NUMBER,                                --- feature type
   nd_bdy    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- boundary nodes
   nd_int    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- interior nodes
   eg_bdy    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- boundary edges
   eg_int    OUT NOCOPY SDO_TOPO_NSTD_TBL,              --- interior edges 
   fc_int    OUT NOCOPY SDO_TOPO_NSTD_TBL               --- interior faces
 );
*/

 FUNCTION get_interacting_tgids
  (
   base_layer_id  IN   NUMBER,             --- base layer id
   q_feature      IN   SDO_TOPO_GEOMETRY,  --- the query feature
   mask           IN   VARCHAR2            --- the query
  ) 
 RETURN SDO_LIST_TYPE DETERMINISTIC;

 FUNCTION get_interacting_tgids
  ( 
   tp_id            IN NUMBER,             -- topology id
   base_layer_id    IN   NUMBER,             --- base layer id
   topo_elem_array  IN   SDO_TOPO_OBJECT_ARRAY,  --- the query topo elements
   mask             IN   VARCHAR2            --- the query
  ) 
 RETURN SDO_LIST_TYPE DETERMINISTIC;

 FUNCTION relate (
  geom1 IN SDO_TOPO_GEOMETRY, geom2 IN SDO_TOPO_GEOMETRY, mask  IN VARCHAR2  
 ) RETURN VARCHAR2 DETERMINISTIC;

 FUNCTION relate (
  geom IN SDO_TOPO_GEOMETRY, topo_elem_array IN SDO_TOPO_OBJECT_ARRAY, 
                            mask  IN VARCHAR2  
 ) RETURN VARCHAR2 DETERMINISTIC;

/*
 -- an overloading version of relate
 FUNCTION relate(
   geom1        IN  SDO_TOPO_GEOMETRY,
   nd_bdy_2     IN  SDO_TOPO_NSTD_TBL,
   nd_int_2     IN  SDO_TOPO_NSTD_TBL,
   eg_bdy_2     IN  SDO_TOPO_NSTD_TBL,
   eg_int_2     IN  SDO_TOPO_NSTD_TBL,
   fc_int_2     IN  SDO_TOPO_NSTD_TBL,    
   mask         IN  VARCHAR2
 ) RETURN VARCHAR2 DETERMINISTIC;

 -- an overloading version of relate
 FUNCTION relate
 (
   mtdt      IN TOPO_METADATA,
   tg_id     IN NUMBER,
   tg_type   IN NUMBER,
   nd_bdy    IN SDO_TOPO_NSTD_TBL,              --- geom2: boundary nodes
   nd_int    IN SDO_TOPO_NSTD_TBL,              ---        interior nodes
   eg_bdy    IN SDO_TOPO_NSTD_TBL,              ---        boundary edges
   eg_int    IN SDO_TOPO_NSTD_TBL,              ---        interior edges 
   fc_int    IN SDO_TOPO_NSTD_TBL,              ---        interior faces       
   mask      IN VARCHAR2
 ) RETURN VARCHAR2 DETERMINISTIC;

 -- another overloading version of relate
 FUNCTION relate (   
   tsname    IN VARCHAR2,             tabname   IN VARCHAR2, 
   colname   IN VARCHAR2,             rowid     IN VARCHAR2,  
   nd_bdy    IN SDO_TOPO_NSTD_TBL,    nd_int    IN SDO_TOPO_NSTD_TBL, 
   eg_bdy    IN SDO_TOPO_NSTD_TBL,    eg_int    IN SDO_TOPO_NSTD_TBL,  
   fc_int    IN SDO_TOPO_NSTD_TBL,    mask      IN VARCHAR2
 ) RETURN VARCHAR2 DETERMINISTIC; 

*/ 
 -- another overloaded version of relate
  FUNCTION relate (
   feature1 IN sdo_topo_geometry, feature2 IN sdo_geometry, mask IN VARCHAR2
  ) RETURN VARCHAR2 DETERMINISTIC;

 FUNCTION get_face_boundary (Topology IN varchar2, face_id IN number,
                             All_Edges IN varchar2 DEFAULT 'FALSE')
 RETURN SDO_LIST_TYPE DETERMINISTIC;

 FUNCTION get_topo_objects(Topology IN varchar2, 
                       topo_geometry_layer_id IN number,
                       topo_geometry_id IN number )
 RETURN SDO_TOPO_OBJECT_ARRAY DETERMINISTIC;


 FUNCTION get_topo_objects(Topology IN varchar2, geometry IN SDO_GEOMETRY)
 RETURN SDO_TOPO_OBJECT_ARRAY DETERMINISTIC;

 FUNCTION internal_get_tgids(Topology IN varchar2, geometry IN SDO_GEOMETRY,
                           in_tg_layer_id IN NUMBER DEFAULT NULL,
                            operator IN varchar2)
 RETURN SDO_LIST_TYPE DETERMINISTIC;

 FUNCTION internal_get_tgids(Topology IN varchar2, 
                             q_tg_layer_id IN NUMBER, q_tg_id in number,
                             in_tg_layer_id IN NUMBER DEFAULT NULL,
                             operator IN varchar2)
 RETURN SDO_LIST_TYPE DETERMINISTIC;

 Function tg_insert_relation (topology varchar2, tg_layer_id number,
                              topo_ids SDO_TOPO_OBJECT_ARRAY, gtype number,
                              input_tg_id number, input_tg_attribute varchar2)
 return NUMBER;

 Function tg_insert_relation (topology varchar2, tg_layer_id number,
                              topo_ids SDO_TGL_OBJECT_ARRAY, gtype number,
                              input_tg_id number, input_tg_attribute varchar2,
                              child_lid number)
 return NUMBER;
 
 PROCEDURE sdo_topo_crt_rlids_view(Topology in varchar2, topo_id number);

 PROCEDURE lockRow_UniverseFace(topologyid in number);

END SDO_TOPO;
/

show errors;



