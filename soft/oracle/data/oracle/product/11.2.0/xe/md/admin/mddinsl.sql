Rem
Rem $Header: sdo/admin/mddinsl.sql /main/2 2009/03/11 13:28:43 sravada Exp $
Rem
Rem mddinsl.sql
Rem
Rem Copyright (c) 2005, 2009, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME
Rem      mddinsl.sql - Deinstalls Locator
Rem
Rem    DESCRIPTION
Rem      This deinstalls everything under MDSYS
Rem      Do not call this directly
Rem
Rem    NOTES
Rem      Must be connected as SYSDBA user
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     05/25/05 - sravada_cleanup_label_difs_2
Rem    sravada     05/25/05 - Created
Rem

REM MUST connect as a SYSDBA user

alter session set current_schema=MDSYS;

REM Deinstall Cartridge Components
@@deinssdo.sql
 

 
alter session set current_schema="SYS";


