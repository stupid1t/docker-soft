create  or replace library ORDMD_IDX_LIBS wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
16
2a 61
rR3BRZxFc66Bkn6obFpnyQE5UqUwg04I9Z7AdBjDpSjACDL0/vX04/4I9Z4r572esstSMsy4
dCvny1J0CPVhyaamHWmZEw==

/
declare
begin
  begin
  execute immediate 
'create type sdo_index_method_10i 
OID ''A6AF3DC91710108FE034080020F7892A''
AUTHID current_user  AS OBJECT
(
  scan_ctx raw(4),
  STATIC function ODCIGetInterfaces(ifclist OUT sys.ODCIObjectList)
         return number,
  STATIC function ODCIIndexCreate (ia sys.odciindexinfo, parms varchar2,
                                   env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexDrop  (ia sys.odciindexinfo, env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexTruncate (ia sys.odciindexinfo, env sys.ODCIEnv)
          return number,
  STATIC function ODCIIndexCoalescePartition(ia sys.odciindexinfo, 
                                             env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexExchangePartition(ia sys.odciindexinfo,
                                             ia1 sys.odciindexinfo,
                                             env sys.ODCIEnv)
          return number,
  STATIC function ODCIIndexMergePartition(ia sys.odciindexinfo,
                                          part_name1 sys.odcipartinfo,
                                          part_name2 sys.odcipartinfo,
                                          params varchar2,
                                          env sys.ODCIEnv)
          return number, 
  STATIC function ODCIIndexSplitPartition(ia sys.odciindexinfo,
                                          part_name1 sys.odcipartinfo,
                                          part_name2 sys.odcipartinfo,
                                          params varchar2,
                                          env sys.ODCIEnv)
          return number,
  STATIC function ODCIIndexInsert (ia sys.odciindexinfo, rid varchar2,
                                   newval mdsys.sdo_geometry,
                                   env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexDelete (ia sys.odciindexinfo, rid varchar2,
                                   oldval mdsys.sdo_geometry, 
                                   env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexUpdate (ia sys.odciindexinfo, rid varchar2,
                                   oldval mdsys.sdo_geometry,
                                   newval mdsys.sdo_geometry,
                                   env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexAlter (ia sys.odciindexinfo,
                                  params IN OUT varchar2,
                                  alter_option number,
                                  env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_geometry,
                                  params varchar2,
                                  env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_geometry, rid ROWID,
                                  params varchar2,
                                  idx_tab1 varchar2, idx_tab2 varchar2,
                                  sdo_level number, sdo_ntiles number,
                                  win_ndim number, 
                                  env sys.ODCIEnv)
         return number,
  STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_geometry,
                                  env sys.ODCIEnv)
         return number,
  member function ODCIIndexFetch (nrows number,  
                                  rids OUT sys.odciridlist,
                                  env sys.ODCIEnv)
        return number
        IS LANGUAGE C 
        name "md_idx_fetch"
        library ORDMD_IDX_LIBS
        with context
        parameters (
           context,
           self,
           self INDICATOR STRUCT,
           nrows,
           nrows INDICATOR, 
           rids,
           rids INDICATOR,
           env, 
           env INDICATOR STRUCT,
           return OCINumber
      ),
  member function ODCIIndexClose (env sys.ODCIEnv)
   return number 
   IS LANGUAGE C
   name "md_idx_close"
   library ORDMD_IDX_LIBS
   with context
   parameters (
     context,
     self,
     self INDICATOR STRUCT,
     env, 
     env INDICATOR STRUCT,
     return OCINumber
   ),
  STATIC function ODCIIndexRewrite(ia1 sys.ODCIIndexInfo,
                                   ia2 sys.ODCIIndexInfo,
                                   cor1 VARCHAR2, cor2 VARCHAR2,
                                   op sys.ODCIPredInfo, qi sys.ODCIQueryInfo,
                                   strt VARCHAR2, stop VARCHAR2,
                                   params varchar2, rstr OUT varchar2,
                                   env sys.ODCIEnv)
      return number,
  STATIC function ODCIIndexGetMetadata(ia sys.odciindexinfo, 
                                       expversion VARCHAR2, 
                                       newblock OUT PLS_INTEGER,
                                     env sys.ODCIEnv)
      return varchar2,
   static function ODCIIndexUtilGetTableNames(ia        IN  sys.odciindexinfo,
                                              read_only IN  PLS_INTEGER,
                                              version   IN  varchar2,
                                              context   OUT PLS_INTEGER)
            return boolean,
   static procedure ODCIIndexUtilCleanup(context IN PLS_INTEGER),

 STATIC function ODCIIndexInsert (ia sys.odciindexinfo, rid varchar2,
                                   newval mdsys.sdo_topo_geometry,
                                   env sys.ODCIEnv)
         return number,
 STATIC function ODCIIndexDelete (ia sys.odciindexinfo, rid varchar2,
                                   oldval mdsys.sdo_topo_geometry,
                                   env sys.ODCIEnv)
         return number,
 STATIC function ODCIIndexUpdate (ia sys.odciindexinfo, rid varchar2,
                                   oldval mdsys.sdo_topo_geometry,
                                   newval mdsys.sdo_topo_geometry,
                                   env sys.ODCIEnv)
         return number,
 STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_topo_geometry,
                                  params varchar2,
                                  env sys.ODCIEnv)
         return number,
 STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_topo_geometry,
                                  env sys.ODCIEnv)
         return number,
 STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,
                                  ia sys.odciindexinfo,
                                  op sys.OdciPredInfo,
                                  qi sys.OdciQueryInfo,
                                  strt varchar2, stop varchar2,
                                  win_obj mdsys.sdo_topo_object_array,
                                  env sys.ODCIEnv)
   RETURN number ) ';
     exception when others then NULL;
  end;
end;
/
show errors;
declare 
begin

  begin
    execute immediate 'alter type sdo_index_method_10i ' ||
    ' add STATIC FUNCTION execute_index_ptn_drop(ia in SYS.ODCIIndexInfo) '||
           ' RETURN number ';
    exception when others then NULL;
  end; 
  begin 

    execute immediate 'alter type sdo_index_method_10i ' ||
    ' add STATIC FUNCTION index_update (ia sys.odciindexinfo, rid varchar2, '||
    ' oldval mdsys.sdo_geometry, newval mdsys.sdo_geometry, ' ||
    ' env sys.ODCIEnv) RETURN NUMBER ';
    exception when others then NULL;
  end; 
  begin
    execute immediate 'alter type sdo_index_method_10i ' ||
    ' add STATIC FUNCTION insert_delete(ia sys.odciindexinfo, rid varchar2,' ||
    ' val mdsys.sdo_geometry, upd_type varchar2, env sys.ODCIEnv) ' ||
    ' RETURN number  ';
    exception when others then NULL;
  end; 

  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexInsert (ia sys.odciindexinfo, ' ||
    ' rid varchar2, newval mdsys.st_geometry, env sys.ODCIEnv) ' ||
    ' return number ';
    exception when others then NULL;
  end; 
  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexDelete (ia sys.odciindexinfo, ' ||
    '  rid varchar2, oldval mdsys.st_geometry, env sys.ODCIEnv) ' ||
    '     return number ';
    exception when others then NULL;
  end; 
  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexUpdate (ia sys.odciindexinfo, ' ||
    '  rid varchar2, oldval mdsys.st_geometry, newval mdsys.st_geometry, ' ||
    ' env sys.ODCIEnv)  return number ';
    exception when others then NULL;
  end; 
  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,'||
    ' ia sys.odciindexinfo,op sys.OdciPredInfo, qi sys.OdciQueryInfo, '||
    ' strt varchar2, stop varchar2,win_obj mdsys.st_geometry, ' ||
    ' env sys.ODCIEnv)  return number ';
    exception when others then NULL;
  end; 

  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexStart (SCTX IN OUT sdo_index_method_10i,'||
    ' ia sys.odciindexinfo,op sys.OdciPredInfo, qi sys.OdciQueryInfo, '||
    ' strt varchar2, stop varchar2,win_obj mdsys.st_geometry, ' ||
    ' params varchar2, env sys.ODCIEnv)  return number ';
    exception when others then NULL;
  end; 

  begin
    execute immediate ' alter type sdo_index_method_10i ' ||
    ' add STATIC function ODCIIndexRewrite(ia1 sys.ODCIIndexInfo, '||
    ' ia2 sys.ODCIIndexInfo, cor1 VARCHAR2, cor2 VARCHAR2, ' ||
    ' op sys.ODCIPredInfo, qi sys.ODCIQueryInfo, strt VARCHAR2, ' ||
    ' stop VARCHAR2, rstr OUT varchar2, env sys.ODCIEnv) return number ';
    exception when others then NULL;
  end; 

end;
/
CREATE OR REPLACE PACKAGE mdprvt_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
900 333
+o2WMAbHQgoycO5aq3OhynYt7Mkwg5ArACAFfC/NJ7WeSr0vjhGxWUmbVrao0HRCwF3RccL1
FmmO6bNPD/KbB9hQ8Wli382//rkZEGkcXZl3Fj80teSBwb+S+fnqZ2ie7ta3TeyprK0bbXQo
zLmzrFdnyBLnD09GG0swrSQimS3tLXcaQo3eIhLos1KYMedriOX3/rqatFF+BF5VMERfREAt
gV5h7bV3fdhLxRyGwU1x/EqF7ldpuavx2KQB1IoYJ89QyntNXgn2CbQng75eha0RzgnOv5iI
Tqik0XXkc1J2cWdI4b0nfgI4q6mu5S28nCFJsr9m80YgjVKb6VNAAnh/3uDYf1/03HoXNxv8
y1HoGd6ABwBMICLnkJKSVSLsGyO/4MvRzn1S8yelqYuEUtKTX9V8T5P9X21nmXmv3vsWkA2+
6tqQFftHEI8+pE104tnP5et0d1t5qHbBBl1UUO2KXnvrVOybH2s3HeCDjOL+etXrpkedG8D8
UbVMDiVvPos8RIw6Vx1o+zI7ow5vtpG4SpVFz2a8GZEPRrn1beUsTfSxpiDvMZgjgCjALj4q
9ny9umAGkxYBRza/KPxOOcqIL2bR9TTot9lTV6seqYQgiwnUAohluNvI/PpNco4WSQ3ZT9ld
HOEeP0T/ofU5a9PV7M4NrSpD0zr7hDQJcqZODWvIDXF/Kfc3E1PK01glYT7mjSom4af4joF4
cUcAHwANQXd2fHKTwlzOSxG5TLexmO9wotxVumpDkqm1RSKXmaDn7Y6qSJqLZ5Vvx4MrOriF
BYcNck/5+5Iotu4=

/
CREATE OR REPLACE PACKAGE prvt_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
10f8 268
ySysld9E4fYTt2GlV8FeSc3h9Mwwg83qAK5qfC8Bi+poVBrn+/awChDeO5LvtBd33IdUtC/m
40/QpMN8N46oRqKMo8yJbrvk5EFOBhqELdRsr3Nq6wtFmBA5XzQJ06LSQVawBIUdQF6pz6Cz
niv/rA0DbK0dl6z3OP3/1vUmouNzYTfAMiolWAcmE69L0Vh4fZWec1wd8kjxj7KJ8kkCCSsl
G1tAjWsUQ/IfiJ7mT5xlhZf69UJr7G44uvAMS7NWhU89gUFXkr/DfVCMPBq2k3ER7LtJ2YDi
arw+aVX4dCwcsSROivWke5Mk3LEQtdT2lCom4YyUIfAumw3BEz12jEeA1FtL9OQYpTVIEwtf
tTyFXsTN2y57Mgz5eB2esXnaYZdhSOhLyj91k/R4ATY/bZwT0/meCF7NpkfH8bN1eZdlX2qy
Gk2rH+H6UWuSDTnxo1/sgjg7xgLOS0vg6eCQGCHJPE8KnBroTKG3TjwBd5Cn2hptEHwqy0Dk
v3ImDRUp+1C8ck+ticx7uzb6fb3msJSwHuOjDRiRnrFFnDhh3WftxbtOLJxkSJnLkx3Wc7Yz
hoM/vfyXA1n4idhHx7zuwjROtx86jRQc

/
show errors;
CREATE OR REPLACE PACKAGE sdo_tpidx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
2e8 140
4Pf1OZR9/rHGPb2s4NT6bWxE32Iwg5X3Ldwdf3RAWLs+emEJld2D+vBVCpWQx8BujHgBKRU2
03R6IimhoKSe03wht2NdA50kHHx64NmlnzA+u2RyByjditqo5zFfPQzcfqkm3qZhk9YMwy9s
GZx6L3tq5i8libnvRFiWosRSRCsSXxE4BzNL5aW/SZ8h4FsApBIUGJWGLwzZLRfEKUkJOinQ
FB4wCeKBm5smLRdeUGggtDzq4G08LAi11Lz3elLGxGgxZF1yud6RDA3go3aAMi2QJw63Hptt
lDJIF1M4bHU9OWtzkrm2bnAmjw==

/
show errors;
CREATE OR REPLACE PACKAGE BODY mdprvt_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
61b5 1838
MMCgtUyTOTXLyNbtDlCZ1Al9NVwwg81xEiAFYNOdqs2uLbK5ZwgOBKfTjnky0Wdt9hMW6cUU
Q7QAaPPllRQVMcDK1d6jzmqaim34j9E+JBPhXOGTccu/FxcXyRcHnVXefPk+V9R9fLJysz0G
hpJO6HHRvA7KmlXpFpas4ZwHzlfyL6uwvXFJRPLJt4RuayzLUHDDtumh43LYomrz5Sa402aR
YyMqZrK8WGNEYOReXvCurNh4pb1YWywlfVeQWawrFSJjjJSnbqOw/RieiGzkrqUFV/K4DGOY
HAG3c9CebjWwidwWm1H1hpOnsIhRwj5JU+cVDucjimUQE92EsDVg/CwzrczTWbhMfYwWSVFq
fxwfGunOpOhjm8+aFlb/sFSnlc6OxpvTzlZRBMHCkdNuiCGiCcz7DTWa3NZTzKq4BAsDyKV+
py4DEdORwcJThTLCUqtyG/QIfiuRBJxJV3siPkcJj7xZJWkBT1XnjL3cmxYurFfF/9eYgDXx
/6uNAd6DFtkI/QKQVjC6GknO2IDnSlGCdiX70q9lyIca0vdaSU85Cy52zPNEUvRynm4WeRtp
UBZuSm0WiBdXhY2rG8wVvYH39m9ls9Z6icJXV8aTQtjVOR58oJ82qv2EH5FEpW4bng9R5wfJ
bCDJQLSTbLBX2Q3FVAh/QPNWUS2T3HQX6LiQ258CmUlSQgSOhrdl0D9I9ecazEEuto2ABMHA
wIXNFNsLEby/BDdMB+0bAxFLJM/NAyQQXZASpY0wIuFklEShQo5bxmlyzKfcjjwCk4Ir7vkF
tm4462mz+wlkDhBqqSfhwDWQxiEMxlduNUzp9+vJIzrONtbvXpBAlQ/Kw12lrnucGXDOkK7P
doFZmIk2T+4IdG7qIVhwfaTkm1EUxpP7Mm8mA6qFhMTEoOHOlaTrJLOYgYGUm0ggzD9BuwIH
tzR0wAmxHNx1HRt+wLn4fRbjx4rZIzOOtRUS8qvk2pxd1wZFceJvPZzAKp3lZ9wcqiU9VFmk
x2iCu8GzkavWkJ30iCTDMLzQZ2VxFAt6bKP/zmbXSRZiqfR2r8QWQUZX2TPCypdSrb0MU1oD
QXi99GHn+e352a8K7h67axajVcMeskU/B+Ll189/mkezmDqbwqMw9Vp1ZDdFhUQW7H/S2xB+
KjO4AGLTQ8C/FwdZzK/uTD2Ea6k+O6gB1Ad7UTQseXJUbHIcbUpp0MIcQxahoVovxiKNeAYG
vgyVafNknEars7NVkx30edcxavDjg/NZoIJuHgIArr5FA+h4xoVk1ZjtbOyenv0CF/UjZvMv
Ja3hczG77wZE6k/RlvgoBpUbcLcTxthPkw08qai7+7tvtRnq+QPTcpkIzHxt9BHDCYwcDfGL
/ktDa4DNfLM6Yfk8VaTIQcy7DEB2w5SOlp76u1tLtxopiSjFJYCU7AWaHVgJmAhTXOaGqohy
D9ROL4bzzc/t8v+wQcYYxVE9wF7xfe41OAIcVLge1dAXXktifr2Wd1BrD7+DMzdNP+Gkyubv
KwzRUveQtioG81vY2E7gCWn8e4ow7bVzxKOKS16aIyr1ORT+44C5fMEKaBW3mOYIdYlo3Cno
g1BnMhAQsoSB2ZKPB/6U9PQ1oZFsnT9NPI3Tcoe1ReU8FA6N7GgmddgH5fxtg8EKRAnFmkeq
vti5K62NHauoxuE1dAZW0PELe33vTGr3hJVj+qDerUxqobKc6PCxHh4qo+JQiu99OsCBHOka
BxFmjg/lNFHSpbYfA1bMhvyMk2XoX+HZSTXAdIQsaSYa7ldRhqtsgCZMMYxwS2I+3Nz2Cc4u
iqkIZgnpL2nClHTzFzvSanOxmvRQgvoM/i4E6PtleQKNKGJDkVI7UCgzjZtrY25dnjlKwP0Q
BC45NBcyr2nV466GN5MOXhQhgipLiSn1xmsN1WloXVkj0Htuuy4D61FGvhIoihHktqF+NrZp
rAB7UeA3kieaW2o3lonJgabEJPK2gP5Cacm6/LBzecFE5EMGas+BvObrOiMDaFZk6hg6it2O
CEI/vnEOE20oPT000E0Pj6pQxpLzrQ98pGyjDLdPEqB/efKEmJBRdnpnpx0TfE/Ddv4pAHIH
mbCX8crJSqBYaCca3r9TIfHrUxcauw4Fc0VuYxHezIZYe7AKADgUBS/QB6JxgIb2KXAmJCmC
eLMXPJqwYFWwQAUf1DGwPOoDUOxbXuisX3HjB+zeLWE6cXhZnUiD7CIebkOUXjJcles8Wx2x
EDg7UIcuwnlqgWr548kXYRN6xOzaAvyC152H4mhOfS4yk0xQk8M2SkGW6M91G5SZF07UeC/m
Sy/s/QZIO+mX4PByn0MFygzKe4W1/7EBTXiPJhZwytgZXB4yRBvO84sIOmyCr/w5TEfqCh6y
kmPmlLuFUZV8Qv2nVBOTi0+zfL91LiU+blgGyFgGyOwGyOwGaXUGyOwGuAxeiqA1zK6YPidK
VYDN1H3Ywjj9GHK/K6MTbKvMajrFYoB4bu7vBP+5gliykP+Td1tZXjtOOEWn4S+/HhP1z1Tu
N/5AObk0LzC5gRE0W1zzkGMqgaLPWwJxAhCa6oSsSt9lO7JGWtUhb0ae9D5LKxEUnmzb4M8P
0Ha5feLKcVNTvGbhSdlNsPE05CVx6rEnA2HRcQTowaABp69Q2DvQkelFDFd9MG74qbxDOiPx
2sqI97w6ksin8pUG4ibMmc3obZnT4s7mMWlOTKC3Ej3rtCwgu+08OrMsXQIi9Om4bbYjzw2D
5hoQXG78yCiS/rdLJb/Epw2xFITiReL45WhLXENhfMQrjQWXGbwJjYSYK2ScctQLbsGJ3gzh
yaNAA/rDNZW0ZX8hH78EsIZPaXvVUCH/PiFDBBe8Uf003i35H6GAADebI6oPGyIbJ281vh2G
2O1cjaGJUqvauchTS4Va8lmhZ3cqvClTkHLL16LQXQNDHFzqW4B09gu7237xfXg+i2u59PHU
AgplhcKPbiS6ag1Xx5OO355wbw1sDl+pXj+DEfpH0Hve3nQZhd3ezz6zMNqNqYQNXogvPluN
BsNhipakp2LeQmeKXff2qQgtsU3KLX6g8gazRhG+AZFnYaMm9eSnbRjuU0WjFDBPuRweMGSf
o/ROwjz4OyErsqZh12fl3Uufo0RWPJNkrKj2XjK9xfZ442JNXZtT136OyoOnHdsI3qTNl4lZ
CsOReqxFieOEg3uDQw4ty8UtdyDS/phALgN+opwR9ZFxtzON5gqMQ4PaYkvH3MVKGusA8k5j
D+2F8+OtnCirzgZiLrkwOxaTyFMNe3YvfQvCMJRncbKp2bFo8hsWX8Dgq+kdquVFWUUiXbcQ
D0camPLsbOOGwog19incmTOubX4eUJXLHcOsJ1tjuLXDOaMw4eD6pSE5KHcK2u7tpaJ9hWw1
MrCJc5Q4rlU1g3T/gVbi7YJahrMxtiOcDdX8OBWkiAshODcsspYWx2nb0T3MTQc3fUwAnp8/
OsryOVcSNLGrtfyRMMoal5OJqJxDkMI60TK/mOQfwa/+Q1cY53ltYFeqL/BObnV0372jcydS
TgWyDf+Vm+VuZLFOmiNB47HQJV/U9XA+HaDEgBxV/bCCFvQ4zajAXCPRKtaF/eH+5xX4XxS6
I2k3lqzp4n61+bvkQ7uv2U/+Dz8P+cZej9P1fPQN6/t0r/oYRr36JPf9JPH+orqWcKuMcnFR
eYi34tGqhE7Z0EuPOvVcO5wf5zwiHjqV6KOaUP6LlpcfEbziqYubkrEnNYff1e3xJqRiQgvj
uodoGWau0Iw1zy9djqvR3hN4m9+I+HSNx8bsnrhCke/WKwvPffjQKzGKf9XVJQRIGwuUX9t/
eOEBatJATaWdP4mD9hbIEEYWE4t1GJqic4CYpGoeEhUgCCqDKFslJLg+FtRW8llTqf/Mtj0t
DaXjqhQGW/9Q5z1HswLRN+0jbfwG8RFXqnP6S3dPwfbg5/U4vdzbA6/yIlXD9pDOulSWUQ/l
/nAUGq7PsVlOzKeWgI/Wl20ePeiSwilCHCbioxAzFl9Ietll0zBLWNsdGTtWVuoT9vAgGHgP
z+wPZUGYV8p5wvL8/00qoMNLz0ksOotnhjhOa6OTMQGHnKZSWkWR0AGT8qVWbD7vSw4JXcwj
8gIQugHmmE3/rzIyQl6T/Uxnrng0ukaGybp2EKI4NaCvHbrp5ctecjBSEDopJ2wcfI0jXSqT
zLbNgs7eWOgkcpYkXHhNsSULg+6eBFS8/fed/Fcmk9wGHc5Ay/NIyKasokFCD/pglI8umwZ8
/7ALLEAJTxElPtyrmKB0aeOnHtgAL9B6Hu0PiaLmTPdtSmz/IbuMybNN7gnhTZXT7sJVnzIo
Snw2OmBb0sTkPBALM5AL8O0LYVULYXMLYtqFdhKFipiFbviF7Nr6dhL6cZj6ozO3Ms0Wx80+
A7vFx7vVhDxVCzNzC7mkkUoAkUpMkUoQkUqQkTPtkUpVkf9zkWLal3YSl26Yl274l2ja2XES
2WiY2Wj42WjaR34SR6ANu+fNhXjYRUL576Ft7+dU5tfz8IYhxkCo0EWQ43vrscdYv1IQMg1o
X8Odk/hskdv+ekYI7Jkj0wNReZzafDfjqaL3dArQFvC7XsYZZO+OMaClkigg9JXmNKgzbyVV
biUL0xqckZtw3ruHpKjsDGNgbYG3/J7lQbNfb8S7OAwsHm/ehYrVplRsFrkG6+ONYhVr8Qo4
O8C7QI20sSaNS6Oy2JjGqNcVFqiSELE4LyiLct90Mm2/qznWcsYcObXLalC4LLpijvmOr51f
apxtlhIT17Jb1JbqDKxDVPmHiO7E6BMdqOv7wtgFeziSo3Hpz/MArA5jAtoRC4Rs3nTUehle
3+9m3bAgoduNrprx8zuDqxrj0YbwQceAfQulsZAqIbQIlES3IuENCF09TlEqLYGl3H6aRLyZ
+N4uorouevCjDUhZ5bFiwrC/rjME9pBJeMsy+0MkXvaBwoq+q3cfEnI9xbrmSLGDEHei0VoQ
60tGJIIZrHhxg+8joHluN/9KVWMA6IRO3HA9E8mvBRkQm+KLJnzy5Pbjk6825LNmp9h6pzCz
uQvGWSfiaqrIsMPC7/nr3NHyFUOI4O9Y1EKnZ4o6q7c+sDuDRdDcFjoKSTy9Ymw8pCvyjtks
zRR0P0EZ8ySFm5HTCL7JSEHsz9HHj71ySQPnABKSfNAKi7qQOAf7cJKeKwl7gLy1PTtjwztQ
gnfsow5JdyP3zmEpa5iTp/HgsJZINRmvY/FU/mL7Oftjt19XAMc1cjAKHeA0OAU7gHWdromh
n3NXgEOTnzZjo2I6MamgA4MxqkKlOQUIcY6t/AFdzxgWa1AzksjR1OlKhaATr1FGeQJJ85Jt
CkShAY0fLOVttY0T19eZYZwie0fnBFbXFaFa31zW3cuI4fl5w27I1laZTV9vUPA8U8r9RNsO
TgUGzUTuJtBXFzghy8pgi7F/3EpttppXa195xfe4V1maQWXON3H7Ewc+FNSzZki0xI13SpJK
2/uZc8uPxAJQSnNhL4Pumm/m2uE0Y8HobqNaLuTEY0LaNmijsKT7SGwNap1i1wXzj9zmtja5
FCFSj/EvMnCvupIL5q87Gp8UV8gVq8Ka+/u/dGOy+0hsDR3dOxqf5MQuhjsvuJjefoARW6y6
581Upaf7ZWuP/9QuIVNMoit8U15DWXpTA1K207kUIczuYqdIURXN/DhEArqhzXW4F5ouVFPw
UBh0W+bZ9GnP9m+Q3PVWV+XWHUJ+DNRyIJvTHLo9x0mQexBSVDVzGJAiuGfcSD0LoZTxFL7C
uTeO+KkBNElJf0KD0KR4CzX/tC2Cn9JZ1T/U9A5SOBGjaSHfdvC2oYbUJaBR3kUIIUii8Jle
60DlUu4ncYXs337wtkyG27FX89/cX/CZAYbbMaDKeaMt39ygLpkZ6ERS60C+UoxIg7h4eYUT
34rwuRDks0pC3ZH9MGhfoyaZqf2yilND767T5UhIVkkWKnJPASIYiVSMAe9R/7hKmSOOwvuh
NPZpeSjVOxoNP6Dpa2gQ/cEnMrnFxHnFH15DN2OKbACp/WtKACrlyqwUaA27jhdVzCixSC/a
67hdTeGBBHScJjWBReTPUlkC5PV2e9xhm90PLS6cWfubj3tRV7dpblgqkgW1+2RR7pw=

/
show errors;
CREATE OR REPLACE PACKAGE BODY prvt_idx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
3192 bc9
Dp/R4bJEn1n20jn53CRvtQgcOBUwg82juiAF344Z9tLV4+CpVlm9iqAEPL4+ygygjFwf6hrs
XaGlqwuZUHvphvUSfdCa7pCJkcbc67KTCjm8yMF2QR7GhEVOFj+U13zsifpEkqUI60W+ReT9
6dtOL+w86Vnrwmfpf3XuZYDeN5tGccjv8Ymy/a+gRefz+PU0ONPbtK4c9+pfmy7DlbDMMnZR
Ia0g4CNKBBdY296jFM52OYNuRvP7F5Ne2yuP9bJWwKMb1xF1QGfMWQMRQBh7T4/Fpvme60Vl
bWuJ3Y82ciUVhiVONuo9zJRjNnhCJd2cwqvOQgYHhWQRAi0sHEpBHCQSHGYm8HKn0Z7Sny2j
TGSzuwyg5KAwv5hUlpazD+oeJGQgsX0ziyeBW0WjTNG1M9Z/F4kYzusW77QXC1Na1AVMWSy+
BywN1im/tT8tqsTg279bu11wQF4PW/R4ezIfJJ92taRCtYTQ+a73M5gkLASTnVIHYaODFGwh
xeDkTbq6fw5DYzYBzH8Kk4AsHrNml38jm/nxDTPXJzJCMgOH2BnRTxGsVpDDzDI3l8U3UHwN
OEZ9KQOJQVFTVhYisCO4HFzKpwYYDjd50BrHfpHRHnSplaGrgKDYL2tcpL+TSpjvBVhU53sd
QJVY8a9/zIPFWFcBwo3IgJdG2KoyiCKCJmCXVzLgq1+/2aOSffErptt0MfzwpIV1aw+uqVfZ
ojiwXhQ7G88hZ9KOH0GGaQU1JfNrWpm2uGYPvp/TIXxwlcmhyovjwaUi0202IYtPmKk/TxYl
tQEvzDY/8ei5djxMhIOTfsaJ968ZIPeQskvCKe7Hxyg9u7cO0ClZ/gKIRa56LFaJD3aVZGhd
5Nk4gjXxt9Y/qgJ6af1MlNLEPSg0nD9OvGuaYfYcksMzWs+Gytxx/5aLoFpt3q1a7+jiC2/9
KDGzN/xSMwKyuvyfVEaLz7z7JpBC+wJSWCpSkx+Op2MwQNUAWSGZzzh0iARWsxlsyYN1eSel
9QRTEkQFdiV942RHKsErIDEWRpfRK/ahaPMPFWiwWqFiQvxWYoqsEOIHCgMqvtsh7JFTLtiJ
3q0cMIm/dQXQzk3AIVnLO8jdsUFNdF28n5/syQTP8IcECPtP+pfA0ZRDZetPYAuukAA4LlfP
l+xchNYhcEYZNfbtNzvVL71eoc8YToXivIdln03L2l6l/0lhuGra7XUgixe3TKQj5vNRqLX9
r089sny1X/cXH19DJ0yCgzC+pyvpfaqmKF7yw/Y46IiWhKYV3tKUHcYYzIWOCCOCVHACspSU
efF7cpa0AmrJS6VHZuMFmqUedApYor1BYDIb67xbgaLzL+xeXjFyQHKqp9wMNdhcm8Y8fE08
goJENcnOwRc578g+cq80I4RY3oCPms5carsMe9xbqTVpb/vmY76VMf/F08EcXysGEbnaWPDZ
spwdXOCqBZ+YwTaDRUPapes9sJWYCgzSOAhSCD++hz80zJwwe+NRcZnm0FNZWz8axJNXympz
3j8/xmogHffL95DcGlshAVqBOV+Cdl6i2UBEP2ncP9ZkcF6FKue0YAn82FgrUP0OFtmrNYCK
mXcDXnE4w0GUyEFRIbeh3OzjL3Rc0cDWHj2XnN3evbT6iKFiU/MAUSXPyxvxsVjVShC9UGDe
dKxn7HLe9//9nQfWd7bqRE2xTA1QrKhtbH59I5H717aFdiwQ+VaQkjqCm6rSXKymEQatgNNy
m9WRdEZLZp8YSJ3PK6GFlveE9U4zckP46ePiJMKgcQ87ZOLqvN2Ejb4irCFEhL56O0pox0ci
5YpT2aQbsh0ySgPf6jrlD8PtKScubYnC33yDSn1NmR1vqYJMrfUS7Q2A/FfURtKGUwJv5D17
Q2rfPjhXNkXq5MtgFcLhze5AIpRhemoAn6oAkZUK2+12e7gU2oxNEAx9DUiHBytod/uDzS3O
my8iMBOzXqghfOo2z5JsE1zVMIeanpC8Lrjy0iq+2gRnPvOj9ALbhyMGJAgdKxfouaPDWW8F
+a0BjxWcdwugodCgSzYtbmCWh/I7o8eAP6WsOpMms2PnbOm/i13chzA2+m/sr2A6N3XM6Jrz
RH/BDVXrAIXjo3noRCE1O6CLYdL5g0TkdkQoRQDWfnT4F90cgn+rHCS3XkQi9mFZTpWrhakd
XmxR/bv9Hf2QLTTYfJz/m68iHUMfZ+j4e2pnveQK2zDqpMF1GiqGsiPa8ejjdFVttqem3A8t
iMKYJJTFL08Z4hll2yLkEFIDNKIIgZgF/tVKPM1WL/f8N8GxMolj645JOWsFjd6K2nZEN4YG
4q/7Ec9Xwl8riQ/rkXf2A4Vs6GbALiUz5DbUoXPLee8FPh1mFOK0sCoAqmXxRHLL74V2lWiJ
L/zn0gS6zjvnlKD9yI4mKXGxMyEnhfkWmSCdWtXXGnj1Dij1lHwsnzYGBjJMCaWT8lKUAR2+
n3xh0rMbX9inW3lpQYagaKmJfLT1QfkGuaQnW7NIQ9oimnqmJYDPmd5ejG8N3TgtRFOk+Sux
xN+nn/Y8KYXLdpNQDJlGvtkI54Fgw4EGccpt0DKbHxm2DIFVLYrDUozBcjtvUuL/PlG1x3iU
/eJCYb5vocdUN1ipFVm4FhdW1fAv7wn+1YtBYHvCtrcuMi0PPlqBX1TVTv6uz9rN2iMj/oX+
7cIQZHGayClIuj9yIbtXgUpGYW+1u5uDN3Pw8IKdv8La1bzEV1U9NMULbne4M2zHWo3oSR4b
86/kZ5Hc83mP/Kobw3YSznPOGTRotd8XDnt7YfMye6jcSL81xp2g0RgmGH3RY+jwp/OceLJm
3oEaEduVKh3rGA9/FMrXoG8GWQZB4xpLIyJ4fPb6XXWOWi0l82L6Yog9evKj08tivWV0Yohv
ft7TN4Tw5CyEHZ05tY8Gsp2C5dXOWv013oPW9DP29/dgfeszQbd15aLt2kjn0pNBUiCX/whI
KaDBhi1sPVI/5Gokc7HdLQ==

/
show errors;
grant execute on prvt_idx to public;
CREATE OR REPLACE PACKAGE BODY sdo_tpidx wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
53bd 13b1
8rYpTqO+YfX0WI6gV9cbJ5nSr68wg81xHiCD8m+acvnVYAriX5jReBrq6kXuYfHWW+BitHff
oU76ZjcaMQzKmdzr8nOGv4dhcnp+yi6f/VVg/vtBBxf/OpD+dAtk/pYgqbNSXU+M136NQ794
A5aFDFbV567tOI8XKcr9I8FX/+EsCCTkLQfG3i8CTO3rygAsC2O3ApK3S4AkjMrSCMZ+ZTPt
jowrj2tdet6AP5ZwsjW00HGOtx+QI1K7220oTJk95OmETzSr8M97hAaAk6vwjH6pjPPchk6S
UqgiEndXdkGnbWxPpI6G9dMHFm5fSlJfrEbwFCgpiuQ4nADyJcyjubA9rVdXuDAda+YrBbSZ
Xt5vRZc4IVdke50A9V3oVucuO6wsfiWhbqJS1BxEvZN+D3oGTCRZuM9BgQaiKZe6xkb6hPvh
U2X//qgilZK8A8jtxHE9OL4kPBj9uvgyO2wvQwJubEML8HTGMC9GbsSnzgsPrCIna6fdNqih
BMzCKAfLm+YsDTaFGjlbY0lUR5VYEtLLMRavhGgMs4rJt5pWlseJZLkrbT2B6vUNh1VR+NfP
kH86ZtADBUIikC5OPogzzROsV/2QkI8EnIYDE4gjB0qFqs3iXJ7KuS7jpkSM8zMdxfJ+pDV1
YNOrG6EvyzYGBVzWmpSYZGnpYWevzSzQ8UcM03awzSi2ZjFzprbIPwTJa3gVYnzEcETTtvvx
5lF+i22ta0TDqPBdUE01izDOIQ9ysl110RdNv9N0cydIJbg63lKCChaU8FLnuFXQ4LxFe5d3
2aoIwaRJO+BxDWKwz4Z8GNQ9FKa/5GTQ6xm9VPOGk6ijTLS3TWqJ4PvYtSw2AnMR5sg+gdgh
ve+Sc6Soi27TA7S0xCCfb4ZrgFyl1bZ9yu14x8sZVrUjeLAU12Q+zqvG54WHF20p6hWT/4yY
5CMtMhRDUe55m/n1NJza365iA79niLrTMZTo1iRblhU4gfDPyW7Y51X9YL05Xx4rMJYkBG8n
r9eepMwGQNXXSSUvvsi9WJnrbS4e438VNZ8q89akcaI7Z8oajO0KfhPezO39kh2kuHk+Lb28
p6YzDuD7YyNQu5e2arm7GL1ifGt8IWnufKvxc4KX31qs8+xpQ45Lio4aQ6C85ZUNeY+v87bW
0QmxX0+/1wdyLYv5TuI6zbbYiHCEA6LZPX6HmI98mwjV/Yg9Cy204KwTbDd6lNxKvCCayM0O
Cj1Fd7y4iJiTqOLoWSUr1xS6H9YQJoJ/GFI+Rm3nPrhIdetqzwZSj6p+W/o+SrYVNYqc6Jpr
Fcq+Zy4ng2+r0DqUF4wzcEFPOzyqSLnonMd30Izzm/ScNX2FPDCnEfaRbRRKVSy79wB03C3t
TgqyVjR1D7ZhlPaI4tG1X+ooogAifnFRIrAfXZ21k6oyRRG8bjJrYcOKl5MGZqR40cKZuDSG
hZdNKPJaig7ivG7msVgsCnO/vzkOlAqKVIxpRxexgeY3c68raxYbyALhtY4YLIcWftJx5q9S
hEZRpsaZthjVFfCja30S7TvmzFnKmem1sGl60GnkaWU4b71Kq2bTRCHD9873FOYIlWJi7u62
QPObfxmKtvZv7gyZVyDRvzpoFWv11bBQqHZ4zqnf88X7iN4cZ2kEi62Rk+JSq5EwSZqJ7O9W
n406h/w9RwF0RinqkveXWSmD0YL5s3ooHvKcgtdOgdeVENyQCwUzef2tXPVxt1sJjelrHHGh
N8PYja1ZRL0zjGZs9wlvVmIdn75FD4s7scuxbtchIT5pmFjjCc9prqJ7rrLzAOaidXARl+iz
IYpBXY4UGu8MRON3VxheVufe6PCd3pr18O2y+bhSMqUfuOts3Vy96nifbc6xX4OQagqsamkS
KgYZJxYfhYq3m2da0znWi3N/qfl0UyrdM92mupJ4m1J2xAuWWrjfm4KbbBi6M4XETBOKaJ+a
9RmfyyiQSGKUXAqs7NiXGRQNT6K1ntez5cyk22yC9skXEkA/lqeFoPGebw7RzJTVWVx0oZO1
GjPLd9Ep0uMlL3l8TSz6tqgenCGCCYomTcolrFlDhwPlPhcr1aVxM0Xmqr6gIb2i4j/hSEjd
Ns+oJTYwdY9zPNPq5dLMDPTVQIXVN+yEjro6RoqIx3Bi1FbO0Aneb28zmMXX0GSIHlwLmVj8
1HQ7IwjCVlvrM9xeB3faHOArjLeDqviNDTNR7pGOb9cneaDY+6vVsEtutHf8DJW0IjOFG1LU
HpWSx4eJPE8KPQHhdV9kQTyfmebxSYmqasqhMTMVjpvldlYsWmmc3LAbkV6hxVrUjAEMMwfV
EAryVgERDUV8oQX6GrFq9OdeqInup+NwNhyc55ta9Tk0Gs1c/ivG3gNggxvcvab//JD6xoPi
fxwooVeD4dD01s6/utpej+sZ6NNlrsqmIiztgmMUGj8nZCV5QDG98rje4IIxHhDz25Dzou33
/7qTLClu4B3Z456Dcc31REzak+MR0dcWFkB3XOQgp+rV9x+jAFbB9ouDwijsATsGb1RotgJv
PCHtVEcDQ9KnsQywH01I8dk9Bvmfk59sDzELlxzJS0L65OXldep+tAjjN2MXAJ6iSe22eptx
52zEFZ/WJYnKodnNL3Tc7iiNwW3bolYe8/NG273b+hkWMUrz35M+tF3WbGeZa+AVitz2reQ7
hmWj2iX4yO5XKzhba5qbEEl7GJOgfhYqI2lPZeOuH5lApRNDfvljvJFMFKIpdmjFgBeofxBj
51BaW6BllOlyMHxowJHfr/Pi2XSvBtqxQKfDVIuGhSu9qP9bzBHZy1husjgQtTDQGScx6YQb
QmBagORcYMVs5odyH+vkyQ3PbFpetl0F5SAu6VwKyLkvtsDCZ63Aawk5qyVfKmAUXIDAIPcv
Wu9sf3K0YHe/6qsjt86OXp4gSQjurtMd0x5wbFDEPbSvWfr/rvLfafFU/ezXqEDbPDYP7xtj
bDUqtDSDQwCSPcwpc1H0NCiN96/KqB5/ra7XkPOtxLHaFAV6t9rbXpSHuhssDiz24bDcRTxB
d9bbC6eJXNdGQ4gSUqulU4naOWn18BmRAShY66CclVXXXd9UaDvRcPMAzeAg6QnuB2jlMSDi
RerPCwvJv+sy+efhobsRUSuBO8+C73Dzp4lCy57iVOL64aKUIFv7G6r98wPf89GzGXmnLt49
2le34CsHp1OzwSkGP7w1YsUu0XujAx3jEueUlCZ11WYEiZRtO/Gclunv3akCRNUqv0mpzXDS
ShspMPFh+p2IgQqj6RCAGBiXcDjEMMHI1ASehGoWq8RUjy2qVsOYbKxpQ6DPB8BktISWWaSi
7sD1oswm5o0AO1ZGo0lD1wFak3SmCAu/chZLN8WkwX+o4MzJmvHvxfFY8jMJyuTaA5zQ7T5O
wzHZKIiKOFqH7LbIB5DoNho8RQ/hJYkR2HI5uc8H6Ck58Dr5dFmH5SYMgdyyU/7hQawS35IB
iLjX6OgtD+vnSOsRvl6sTYqJSw8nexYT22kN9FQhduh+yn7mDvBfd+zdMVEQgmrKjeVIN0eh
cKHdyFrIXSlsbPgKKwUcWHMqbCttl1Bh2+WHvN9sK0NtsqcIcQVKk/LWr8MJ8UPuIGo1igqV
K9aTx5OhRSNw8vmTrvUg+oPAdoqYIPZ6hiqyOj85Gmnq2pfvhPr174y5IkBxqhnuxdTZmOM4
U0A1ragrzeRNv3+HGS5fH1LzHUAcwmkA1y2DoyQeZD+0tb69teokePkJKZliaY0iJ9wQekxF
MsRxHkqwUsSRowqQ+Vl2q7YHqr5u7rbodG2YOoGsC82fqpNQ+ZyxtaIKgV9puGCk7n6+ZzuZ
v/zKYL+2N2n2J6WhwjiPnWYwP3OZKcNB6FxABTJSlwIFj8FZ94o7jv/O6QWlmJ6j7eybTlEQ
OinJf7eOpvhSN4P5GICWEHjQWGGBCrvkirXsj6X5p1MeB5uB00O4ZBwimw6hKB+kXkGAIqZ5
r4LduyYSKGZEEctdta/OdkAoyhXRctfj8wVKZ82/5/LNkFlKtyFINwMbTYZmZeDIQamw/xPE
3MT7Oo5h4aVs31zaOJmC0BxIeA94eSBXR/9gk6Eo4hPTEIl8e5XMyvV3NeSORs9TW6SkURi1
cYDDd2GJx3g5Zo0XuJdMarjLwOIGxGMTtWAGfsD88OS1Pl7uzZjn1tg75Uue4CKWo5G/MUMn
WHKdXWIFh10xAfmVYUwF+XoPIOMHOAVK+Ojv5voxc/X7Nuw7uhTrRRI0nOv198RkwTqEQgzi
z9ZMLnTRl3kTnW6QtAo6ZXLbklUGEoFmrdmvh9nq0rj4/Js/Yv7/IppHLBRs1KqbGlk5RVyb
ZJm2H8pMVMEpiJbTUTHkwDWnVkgI3qa2oIzPl7n5NEhJTuSJIA0yw3lVKbkhZWuPlsYAfoV8
amNnSmrsYoTqxNJKJp4b5YousBxvYeiQWUriOhzMiKUKgP4qKJyDFERLjTqzm8fi7uevh1aD
FU+Uf4Wrx6uthIB1tAmDrv/Q5itwBthFmWrL5B0SYJZYkrBhn5+qxAplS13S+LviPyxReEzR
6q3T7tmOYx2gRc54tuQT5LZCP4Az5qUbFF4hSMjNTfe0uVuHjbh0cR1wp6aTmphksIM8M2Kv
eePHPoXxQQzQCpY8gqc86T4oe2P4K3e+W9meMNYGfeKvDY69rfxWtpbxDAgjcpIyLggQB8AH
TZErAllq17g2P03i5KQAUaOlZ4mOkzclw2HuOOybafmyAMydxZqeh5eH3cmIRJEx20moSq6B
VMMQVs0Gu+ewHACAUNQs67QeY+Q+ugwKmyrUC5sy0OBGhKBsE2pixLrD6ImNx2TsHRixFCOz
FFkml8FAHIoH6gK0gQGikOLrpk9SB+NqgMjIc/NxQfCFGHlvx2d0s8wGvOcpw4Y0lKhiXqfT
LvSCJk3U8i8hCoGdM/2/+IgA8JkYXvqtxPYI9MeckPUv+nntIRZdZxJSbFxvQCRFUHMFqvsS
YHI+

/
show errors;
commit;
create or replace type body sdo_index_method_10i wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
e
58e9 e39
s7aSfl6zJF6NOcXnsbzTTCzuNaAwg81xDMeGU/P/PeSUhXEUXD7Rupic21jDNRRWx4T3di+b
htvAO43bXIIzogo7ijiqz1DKWuEUpvEjXwD0wx6k8xT93bFzKquyZ4qobBFBNih9nLCx5DpD
3nO6+QHtc7EeXj9CCF80fE4vlwTJa9Di6i1yFRRgoY8HmRUVdfMj8BDx8Gr2JJ6CQPGUhDGE
n8uuFE5P43IOMKxTLzprRjsRXwY9wNwW0wazblYVU88ajoGc0DmhC+0RegaxjCnn8hVXyv4D
s5HAQylcry4k9IKfQTvPGbRZemcGDlXQ1XIluqhosDaCrNSo4qMbl2rOe1hRjusBQEh/gSB8
kC4eHAUkFLw7FAi7prfLKP8vvsHwbVJdawSWvZKlQz6qP5JitUIDP2jiJDAWnsGO61uJUHCI
XTi4T07TSvxMhUgvu9+G4XoRdqoTDJdQBKMbIOMrQFT5arUGytJ/DBZbN+LkBj+zLdvBpet8
hxYBPYbr4bMFPvEPR/HyAs44sySH31KEHFAU6d/bE6VChURPSppO64uB1QdyGE85yvUKvWtA
OL1mDhGKI/BKpBz9ItxGzh5/nKIj4EZAqaBV5JKbxzjJmh3c0fwbV2ZYrjCGVOfEVrhCF6V/
8euCQpYR3u7teozGqIkbjuSyv3i5b5vfGY8wsz0VIkhj+UbOwCCIkFZezKkfwxSn+zRFdAkj
2d0R/9lImxkua0IV5kmU5umN3A6nRhzTbMiJ1j2xWr3umBhYzdU5KKTpkh6hWh1w6lhWVi8f
8HiDGJ+qi0W7XjwRr43glA04dfkfh9ZtJB8J1desTJUKFuzKn4SXfd2tGm1/ADS8myf6XHIw
Icy04AH0P/EiGlZAh+cL2LI4JX2Bqb8XtpPa9aF/Yn0MFYNz95epVElawPtn/5DBLtOlpdVd
fqj5GPNC1rF1aqlX0RJcAntpE46BsZ0Ns7PlojSCaGKQXnbJFMG8cc5NCBGIX+gRoHEg+b7c
LHgFwc0eTtkRkrDZLMkk+UKjW7jvtQgBbXDSorUwVQ7zknP+nkNAOR5aR+4EXO0qnXhiXd18
wDe7RK+ngbsCTB16AkHyadeWxuS7brVMgDD3PmJ+z5d4GIPpoDQUNYXWyyyAs9LAxdLMKx5C
RWDsd+7o09in2VtTxWJC6AlNAqJ54RAqoAc/ns2O7jIsc1OgXimMKtGrpFQXX8hjzIXchJr+
f9L6vwGQAcTUJEcb6iNQMF1SuOqaFmLkaaDZvD8/GZ+z3L8YphF8D8mt/2XmQo3uZfBaQRAY
Rr4Na+x0C0MTOecHUnLWccdydUYMbPMIk96tcSdRMZsqLqcRlVzhYLfqfRfwORlVL/JpOOrg
ozqz2gqrVYBkuH/en/3HzVxc8r5NsHC6bqzi/F23Rlqfge+rOwZcQMHziCpnq1fbTNyNvmOL
zzvvJPRlBjEngvG+9+XlHqcZDW6PQSYrsMNNZYbIf8xbOnmMubxSY9pLd3182nqkaUksUNqo
24p8RgQc0imJuJWpYNyK6RiT6Rj7bjCWiXXNWr3+fmvaV4GKr94E1vQxwA0hdDkCn3++mJX3
UTxEoVPcKdZ/zfbL/jCohWmGAAFjNRkNlenpv5upx0+UMcUZAiZ+H7EsnSTlt+oytp4tQ9Nq
lCPVhbTu8x2GnI114CUAZ0NRgwB+hNj3qixzkB3fI/BAg47itA0ece0vjSzynRb0rL/69FFM
brbkYA/cNPMRBxdEyOY8CC5asxXOphoWgyBm6Zhh7aEt8h1o6p0XmC1r0ExXOQSuOWzoDYJq
q2G8qSvn6uqzP3OS0ms0OEzp1WaiedcroW1N2d6bjDvlXNkA8Axqkoj1WRbt5OjYBm8B8f+X
v20ErW/FmmIeMjLsYLHwEAJk89kWmRCVsAu+u1ff1DNlf2GYAgKNAlbIJ+Xqh/bPPEzMf9q4
XzGZuJRpJ1puFlo84uCo+etVrpyZiWXN5gIK60DJhLLihYwDOT1q+vx+IvUOVrqM1SSTOf+f
9pXzsMQLur1vumLbGeyohE8vE0kmTdhLFE4OeGhjAD8Z4ni7hSYNJYVAUdRIjdTX49uZn4xV
zAgDGUtaANgK9hY8vqLdfEcWZU9VnN8LGDaStHLi2Zmho2QdadPSEM1eVO0z3ul9hJr+V6ee
ls5sZKrnLrkVV66LJM2rWiGcYoN02h5dp9ziD2HQE3np4sufiCkq4w9wxTvhxDthPik/IqH9
nLnGJALwDP6GcKY74ZmWr3ujRKcXYzhZcnQCEM9pC9luVf2QpCU49aJgSbW4WrWm9xQuuTDD
S9awfR1/F9pqTgxjc8IR31rLoHtmxBTlBqYW9JME98CtoJaOaBScMxHgSQ+ctRLHwL+aLZiM
svixSY0SV2Adtl8Cub/J1Ob94GxeR2NKxJ7fkxikdR1rXwKwqAaqcON9CGF58kCfm2MDZEkP
kkjPKXDqnnYkzORzXm7N17heQGSQVQrrEj3ghAGAmBpORgYCGTj+c459RG4+42l1U6cZXtML
2uPFzPZjxolcRrku0Em6c6dhg5ByMMHUurn1e/HzKHt6CxHZptmwXUpAZk7P3noKdx6mUH7G
ABU/5j3eSZ1usnEwKsjJF/KK172ivkZgnlW+5wtxN/M5TOG7GSflyfauRflhRUg6S2uXu57t
DVMSA/lyi0KY+Y90xAUtYT5QSDkx4AVNwKdyq54Vn99DmHM1aQ2fOONwNbWDAu6IbG+flV3y
erkYlQ1XaNPW9gjEbDNhix/YvkRLdM0Vm1nHN1oTX9qM9cdGGfMOlueGYTcdR0vNGieaA/P+
KWLEf8iHTmu72X++E4D0tb+vkCGXwLQ4vH2cVvVySGzFAbdG1te0wsl9zsZtbZkEIQ3K5noV
ajbrryWOYXXovqwFoPxnf5KX8jXn3Q/5Nme2CNWxQe2Ume/5VtakS485ZVLRYmASTwNDO2o1
Qduz1i/E/YMGXvz9vTWYdk3Mz/Mvcer524TNvX16oWasC2uAgcF28j5xF5yD1JtChbh2w7oT
+3xC/2scoYiV5E9CiWP5NnMIhukLF43Y7Ovww7DuqxNrj/kqiwI7hsDsm7ryXFnKY/SurJRs
koYi9b+NStnLwE0yX/UUkXEF5VwYN5IZuMdi/ui5VUbNEVqaKhYRRKwJj+B/eNWDdCFGNi1o
VhTv7v8wfbS6SdoSfp6mXGph9UUZo1uQp7omoO+5B4PEoKpkv4MbeHGicutCVTkBGeNeu/ri
yZN72du6sHZqLV4Z1rKM6ug4/qLOM4nnqiT0uAG8z6B5jrAhkrUCBVfSwDH6rm5vR13R2baz
0C7SBhf4wFk+P+/CZCRknW7ntRAv+f1KKfx0C506nHNx8bES8T1z8tUgzXu+6PmPvbRXqX6b
D+o7LIZ6aLCx+2rU0p0APetybzPTXLCo0rY0gz6779IDUmW5pQ2Ffubr8c0kn5WA4qdc/afH
/f/DTowPxJY66+jjdYq8AynfBRxe/o2N2cdXmFHwxrNUBYh9xhS/vzlh70Bos110PyPEuqL+
kpgMOHKugHeNLlw9P2nr3GlSkUvhxmZR2xf8Ks+dah4T5O3OOsPKlyz5mW/oRjk=

/
show errors;
commit;
