Rem
Rem $Header: sdo/admin/sdosfs.sql /main/5 2009/02/22 13:17:55 sravada Exp $;
Rem
Rem sdosfs.sql;
Rem
Rem Copyright (c) 2000, 2009, Oracle and/or its affiliates.
Rem All rights reserved. 
Rem
Rem    NAME;
Rem      sdosfs.sql - <one-line expansion of the name>;
Rem
Rem    DESCRIPTION;
Rem      <short description of component this file declares/defines>;
Rem
Rem    NOTES;
Rem      <other useful comments, qualifications, etc.>;
Rem
Rem    MODIFIED   (MM/DD/YY);
Rem    sravada     02/20/09 - revoke from public
Rem    sravada     11/10/04 - fix bug 4000647 
Rem    sravada     07/29/04 - 
Rem    sravada     09/23/01 - fix hhcode funcs
Rem    sravada     06/05/00 - make synonyms for all SDO SQL functions;
Rem    sravada     06/05/00 - Created;
Rem


REM ---------------------------------------------

create or replace 
  FUNCTION hhndim (hhc IN RAW) RETURN BINARY_INTEGER IS
begin
  return md.hhndim(hhc);
end;
/
-- grant execute on hhndim to public;
create or replace  public synonym hhndim for mdsys.hhndim;

create or replace 
  FUNCTION hhlength (hhc IN RAW, dim IN BINARY_INTEGER := NULL) 
    RETURN BINARY_INTEGER IS
begin
 if dim is NULL then
   return md.hhlength(hhc);
 end if;
 return md.hhlength(hhc,dim);
end;
/
 grant execute on hhlength to public;
create or replace  public synonym hhlength for mdsys.hhlength;

create or replace 
  FUNCTION hhbytelen (ndim IN BINARY_INTEGER, mlv IN BINARY_INTEGER)
    RETURN BINARY_INTEGER IS
begin
  return md.hhbytelen(ndim,mlv);
end;
/
-- grant execute on hhbytelen to public;
create or replace  public synonym hhbytelen for mdsys.hhbytelen;

create or replace 
  FUNCTION hhprecision (lb IN NUMBER, ub IN NUMBER, lv IN BINARY_INTEGER)
    RETURN BINARY_INTEGER IS
begin
  return md.hhprecision(lb,ub,lv);
end;
/
-- grant execute on hhprecision to public;
create or replace  public synonym hhprecision for mdsys.hhprecision;

create or replace 
  FUNCTION hhlevels (lb IN NUMBER, ub IN NUMBER, pr IN BINARY_INTEGER)
    RETURN BINARY_INTEGER IS
begin
 return md.hhlevels(lb,ub,pr);
end;
/
-- grant execute on hhlevels to public;
create or replace  public synonym hhlevels for mdsys.hhlevels;

create or replace 
  FUNCTION hhencode (
           v01 IN NUMBER,         l01 IN NUMBER, 
           u01 IN NUMBER,         p01 IN BINARY_INTEGER,
           v02 IN NUMBER := NULL, l02 IN NUMBER := NULL, 
           u02 IN NUMBER := NULL, p02 IN BINARY_INTEGER := NULL,
           v03 IN NUMBER := NULL, l03 IN NUMBER := NULL, 
           u03 IN NUMBER := NULL, p03 IN BINARY_INTEGER := NULL,
           v04 IN NUMBER := NULL, l04 IN NUMBER := NULL, 
           u04 IN NUMBER := NULL, p04 IN BINARY_INTEGER := NULL,
           v05 IN NUMBER := NULL, l05 IN NUMBER := NULL, 
           u05 IN NUMBER := NULL, p05 IN BINARY_INTEGER := NULL,
           v06 IN NUMBER := NULL, l06 IN NUMBER := NULL, 
           u06 IN NUMBER := NULL, p06 IN BINARY_INTEGER := NULL,
           v07 IN NUMBER := NULL, l07 IN NUMBER := NULL, 
           u07 IN NUMBER := NULL, p07 IN BINARY_INTEGER := NULL,
           v08 IN NUMBER := NULL, l08 IN NUMBER := NULL, 
           u08 IN NUMBER := NULL, p08 IN BINARY_INTEGER := NULL,
           v09 IN NUMBER := NULL, l09 IN NUMBER := NULL, 
           u09 IN NUMBER := NULL, p09 IN BINARY_INTEGER := NULL,
           v10 IN NUMBER := NULL, l10 IN NUMBER := NULL, 
           u10 IN NUMBER := NULL, p10 IN BINARY_INTEGER := NULL,
           v11 IN NUMBER := NULL, l11 IN NUMBER := NULL, 
           u11 IN NUMBER := NULL, p11 IN BINARY_INTEGER := NULL,
           v12 IN NUMBER := NULL, l12 IN NUMBER := NULL, 
           u12 IN NUMBER := NULL, p12 IN BINARY_INTEGER := NULL,
           v13 IN NUMBER := NULL, l13 IN NUMBER := NULL, 
           u13 IN NUMBER := NULL, p13 IN BINARY_INTEGER := NULL,
           v14 IN NUMBER := NULL, l14 IN NUMBER := NULL, 
           u14 IN NUMBER := NULL, p14 IN BINARY_INTEGER := NULL,
           v15 IN NUMBER := NULL, l15 IN NUMBER := NULL, 
           u15 IN NUMBER := NULL, p15 IN BINARY_INTEGER := NULL,
           v16 IN NUMBER := NULL, l16 IN NUMBER := NULL, 
           u16 IN NUMBER := NULL, p16 IN BINARY_INTEGER := NULL,
           v17 IN NUMBER := NULL, l17 IN NUMBER := NULL, 
           u17 IN NUMBER := NULL, p17 IN BINARY_INTEGER := NULL,  
           v18 IN NUMBER := NULL, l18 IN NUMBER := NULL, 
           u18 IN NUMBER := NULL, p18 IN BINARY_INTEGER := NULL,
           v19 IN NUMBER := NULL, l19 IN NUMBER := NULL, 
           u19 IN NUMBER := NULL, p19 IN BINARY_INTEGER := NULL,
           v20 IN NUMBER := NULL, l20 IN NUMBER := NULL, 
           u20 IN NUMBER := NULL, p20 IN BINARY_INTEGER := NULL,
           v21 IN NUMBER := NULL, l21 IN NUMBER := NULL, 
           u21 IN NUMBER := NULL, p21 IN BINARY_INTEGER := NULL,           
           v22 IN NUMBER := NULL, l22 IN NUMBER := NULL, 
           u22 IN NUMBER := NULL, p22 IN BINARY_INTEGER := NULL,
           v23 IN NUMBER := NULL, l23 IN NUMBER := NULL, 
           u23 IN NUMBER := NULL, p23 IN BINARY_INTEGER := NULL,           
           v24 IN NUMBER := NULL, l24 IN NUMBER := NULL, 
           u24 IN NUMBER := NULL, p24 IN BINARY_INTEGER := NULL,
           v25 IN NUMBER := NULL, l25 IN NUMBER := NULL, 
           u25 IN NUMBER := NULL, p25 IN BINARY_INTEGER := NULL,
           v26 IN NUMBER := NULL, l26 IN NUMBER := NULL, 
           u26 IN NUMBER := NULL, p26 IN BINARY_INTEGER := NULL,
           v27 IN NUMBER := NULL, l27 IN NUMBER := NULL, 
           u27 IN NUMBER := NULL, p27 IN BINARY_INTEGER := NULL,
           v28 IN NUMBER := NULL, l28 IN NUMBER := NULL, 
           u28 IN NUMBER := NULL, p28 IN BINARY_INTEGER := NULL,
           v29 IN NUMBER := NULL, l29 IN NUMBER := NULL, 
           u29 IN NUMBER := NULL, p29 IN BINARY_INTEGER := NULL,
           v30 IN NUMBER := NULL, l30 IN NUMBER := NULL, 
           u30 IN NUMBER := NULL, p30 IN BINARY_INTEGER := NULL,
           v31 IN NUMBER := NULL, l31 IN NUMBER := NULL, 
           u31 IN NUMBER := NULL, p31 IN BINARY_INTEGER := NULL,
           v32 IN NUMBER := NULL, l32 IN NUMBER := NULL, 
           u32 IN NUMBER := NULL, p32 IN BINARY_INTEGER := NULL)
    RETURN RAW IS
begin
 return  md.hhencode(v01,l01,u01,p01, v02,l02,u02,p02, v03,l03,u03,p03,
 v04,l04,u04,p04, v05,l05,u05,p05, v06,l06,u06,p06,
 v07,l07,u07,p07, v08,l08,u08,p08, v09,l09,u09,p09,
 v10,l10,u10,p10, 
 v11,l11,u11,p11, v12,l12,u12,p12, v13,l13,u13,p13,
 v14,l14,u14,p14, v15,l15,u15,p15, v16,l16,u16,p16,
 v17,l17,u17,p17, v18,l18,u18,p18, v19,l19,u19,p19,
 v20,l20,u20,p20,
 v21,l21,u21,p21, v22,l22,u22,p22, v23,l23,u23,p23,
 v24,l24,u24,p24, v25,l25,u25,p25, v26,l26,u26,p26,
 v27,l27,u27,p27, v28,l28,u28,p28, v29,l29,u29,p29,
 v30,l30,u30,p30,
 v31,l31,u31,p31, v32,l32,u32,p32);
end;
/
-- grant execute on hhencode to public;
create or replace  public synonym hhencode for mdsys.hhencode;

create or replace 
  FUNCTION hhdecode (
           hhc IN RAW, dim IN BINARY_INTEGER, lb IN NUMBER, ub IN NUMBER) 
    RETURN NUMBER IS
begin
  return md.hhdecode(hhc,dim,lb,ub);
end;
/
-- grant execute on hhdecode to public;
create or replace  public synonym hhdecode for mdsys.hhdecode;

create or replace 
  FUNCTION hhcellbndry (hhc IN RAW, dim IN BINARY_INTEGER,
           lb IN NUMBER, ub IN NUMBER,lv IN BINARY_INTEGER, mm IN VARCHAR2) 
    RETURN NUMBER IS
begin
 return md.hhcellbndry(hhc,dim,lb,ub,lv,mm);
end;
/
 grant execute on hhcellbndry to public;
create or replace  public synonym hhcellbndry for mdsys.hhcellbndry;

create or replace 
  FUNCTION hhcellsize (
      l01 IN NUMBER,      u01 IN NUMBER,      lv01 IN BINARY_INTEGER,
      l02 IN NUMBER:=NULL,u02 IN NUMBER:=NULL,lv02 IN BINARY_INTEGER:=NULL)
    RETURN NUMBER IS
begin
  return md.hhcellsize(l01,u01,lv01,l02,u02,lv02);
end;
/
-- grant execute on hhcellsize to public;
create or replace  public synonym hhcellsize for mdsys.hhcellsize;

create or replace 
  FUNCTION hhsubstr (hhc IN RAW, slv IN BINARY_INTEGER, elv IN BINARY_INTEGER)
    RETURN RAW IS
begin
  return md.hhsubstr(hhc, slv, elv);
end;
/
grant execute on hhsubstr to public;
create or replace  public synonym hhsubstr for mdsys.hhsubstr;

create or replace 
  FUNCTION hhcollapse( hhc IN RAW,
           d01 IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhcollapse(hhc, d01);
end;
/
-- grant execute on hhcollapse to public;
create or replace  public synonym hhcollapse for mdsys.hhcollapse;

create or replace 
  FUNCTION hhcompose( hhc IN RAW,
           d01 IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhcompose(hhc,d01);
end;
/
-- grant execute on hhcompose to public;
create or replace  public synonym hhcompose for mdsys.hhcompose;

create or replace 
  FUNCTION hhcommoncode (hh1 IN RAW, hh2 IN RAW)
    RETURN RAW IS
begin
 return md.hhcommoncode(hh1,hh2);
end;
/
-- grant execute on hhcommoncode to public;
create or replace  public synonym hhcommoncode for mdsys.hhcommoncode;

create or replace 
  FUNCTION hhmatch (hh1 IN RAW, hh2 IN RAW)
    RETURN BINARY_INTEGER IS
begin
  return md.hhmatch(hh1,hh2);
end;
/
 grant execute on hhmatch to public;
create or replace  public synonym hhmatch for mdsys.hhmatch;

create or replace 
  FUNCTION hhdistance (type IN VARCHAR2, hh1 IN RAW, hh2 IN RAW,
           l01 IN NUMBER,       u01 IN NUMBER)
    RETURN NUMBER IS
begin
  return md.hhdistance(type,hh1,hh2,l01,u01);
end;
/
-- grant execute on hhdistance to public;
create or replace  public synonym hhdistance for mdsys.hhdistance;


create or replace 
  FUNCTION hhorder (hhc IN RAW)
    RETURN RAW IS
begin
 return md.hhorder(hhc);
end;
/
-- grant execute on hhorder to public;
create or replace  public synonym hhorder for mdsys.hhorder;

create or replace 
  FUNCTION hhgroup (hhc IN RAW)
    RETURN RAW IS
begin
 return md.hhgroup(hhc);
end;
/
-- grant execute on hhgroup to public;
create or replace  public synonym hhgroup for mdsys.hhgroup;

create or replace 
  FUNCTION hhjldate (ds IN VARCHAR2, fmt IN VARCHAR2)
    RETURN NUMBER IS
begin
 return md.hhjldate(ds,fmt);
end;
/
-- grant execute on hhjldate to public;
create or replace  public synonym hhjldate for mdsys.hhjldate;

create or replace 
  FUNCTION hhcldate (jd IN NUMBER, fmt IN VARCHAR2)
    RETURN VARCHAR2 IS
begin
  return md.hhcldate(jd,fmt);
end;
/
-- grant execute on hhcldate to public;
create or replace  public synonym hhcldate for mdsys.hhcldate;

create or replace 
  FUNCTION hhidpart (type IN VARCHAR2,hhc IN RAW,
           v001 IN NUMBER,       v002 IN NUMBER,
           v003 IN NUMBER:=NULL, v004 IN NUMBER:=NULL,
           v005 IN NUMBER:=NULL, v006 IN NUMBER:=NULL,
           v007 IN NUMBER:=NULL, v008 IN NUMBER:=NULL,
           v009 IN NUMBER:=NULL, v010 IN NUMBER:=NULL,
           v011 IN NUMBER:=NULL, v012 IN NUMBER:=NULL,
           v013 IN NUMBER:=NULL, v014 IN NUMBER:=NULL)
    RETURN VARCHAR2 IS
begin
  return md.hhidpart(type,hhc,
    v001,v002,v003,v004,v005,v006,v007,v008,v009,v010,v011,v012,v013,v014);
end;
/
-- grant execute on hhidpart to public;
create or replace  public synonym hhidpart for mdsys.hhidpart;

create or replace 
  FUNCTION hhidlpart (type IN VARCHAR2, hhc IN RAW,
           v001 IN NUMBER,       v002 IN NUMBER,
           v003 IN NUMBER:=NULL, v004 IN NUMBER:=NULL,
           v005 IN NUMBER:=NULL, v006 IN NUMBER:=NULL,
           v007 IN NUMBER:=NULL, v008 IN NUMBER:=NULL,
           v009 IN NUMBER:=NULL, v010 IN NUMBER:=NULL,
           v011 IN NUMBER:=NULL, v012 IN NUMBER:=NULL,
           v013 IN NUMBER:=NULL, v014 IN NUMBER:=NULL)
    RETURN VARCHAR2 IS
begin
 return md.hhidlpart(type,hhc,
    v001,v002,v003,v004,v005,v006,v007,v008,v009,v010,v011,v012,v013,v014);
end;
/
-- grant execute on hhidlpart to public;
create or replace  public synonym hhidlpart for mdsys.hhidlpart;

create or replace 
  FUNCTION hhcompare (hh1 IN RAW, hh2 IN RAW)
    RETURN BINARY_INTEGER IS
begin
 return md.hhcompare(hh1,hh2);
end;
/
-- grant execute on hhcompare to public;
create or replace  public synonym hhcompare for mdsys.hhcompare;

create or replace 
  FUNCTION hhncompare (hh1 IN RAW, hh2 IN RAW, lv IN BINARY_INTEGER)
    RETURN BINARY_INTEGER IS
begin
 return md.hhncompare(hh1,hh2,lv);
end;
/
-- grant execute on hhncompare to public;
create or replace  public synonym hhncompare for mdsys.hhncompare;

create or replace 
  FUNCTION hhsubdivide (hh1 IN RAW, cid IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhsubdivide(hh1, cid);
end;
/
-- grant execute on hhsubdivide to public;
create or replace  public synonym hhsubdivide for mdsys.hhsubdivide;

create or replace 
  FUNCTION hhstbit (hhc IN RAW, topology IN BINARY_INTEGER, type IN VARCHAR2)
    RETURN RAW IS
begin
 return md.hhstbit(hhc, topology,type);
end;
/
-- grant execute on hhstbit to public;
create or replace  public synonym hhstbit for mdsys.hhstbit;

create or replace 
  FUNCTION hhgtbit (hhc IN RAW, topology IN BINARY_INTEGER)
    RETURN VARCHAR2 IS
begin
 return md.hhgtbit(hhc, topology);
end;
/
-- grant execute on hhgtbit to public;
create or replace  public synonym hhgtbit for mdsys.hhgtbit;

create or replace 
  FUNCTION hhstype (hhc IN RAW, type_id IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhstype(hhc, type_id);
end;
/
-- grant execute on hhstype to public;
create or replace  public synonym hhstype for mdsys.hhstype;

create or replace 
  FUNCTION hhgtype (hhc IN RAW)
    RETURN BINARY_INTEGER IS
begin
 return md.hhgtype(hhc);
end;
/
-- grant execute on hhgtype to public;
create or replace  public synonym hhgtype for mdsys.hhgtype;

create or replace 
  FUNCTION hhcbit (hhc IN RAW, bit_number IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhcbit(hhc,bit_number);
end;
/
-- grant execute on hhcbit to public;
create or replace  public synonym hhcbit for mdsys.hhcbit;

create or replace 
  FUNCTION hhsbit (hhc IN RAW, bit_number IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhsbit(hhc, bit_number);
end;
/
-- grant execute on hhsbit to public;
create or replace  public synonym hhsbit for mdsys.hhsbit;

create or replace 
  FUNCTION hhgbit (hhc IN RAW, bit_number IN BINARY_INTEGER)
    RETURN BINARY_INTEGER IS
begin
 return md.hhgbit(hhc, bit_number);
end;
/
-- grant execute on hhgbit to public;
create or replace  public synonym hhgbit for mdsys.hhgbit;

create or replace 
  FUNCTION hhincrlev (hhc IN RAW, lv IN BINARY_INTEGER)
    RETURN RAW IS
begin
 return md.hhincrlev(hhc, lv);
end;
/
-- grant execute on hhincrlev to public;
create or replace  public synonym hhincrlev for mdsys.hhincrlev;

create or replace 
  FUNCTION hhgetcid ( hhc IN RAW, lv IN BINARY_INTEGER ) 
    RETURN NUMBER IS
begin
 return md.hhgetcid(hhc, lv);
end;
/
-- grant execute on hhgetcid to public;
create or replace  public synonym hhgetcid for mdsys.hhgetcid;

create or replace 
  FUNCTION hhsetcid ( hhc IN RAW, lv IN BINARY_INTEGER, cid IN NUMBER ) 
    RETURN RAW IS
begin
 return md.hhsetcid(hhc, lv, cid);
end;
/
-- grant execute on hhsetcid to public;
create or replace  public synonym hhsetcid for mdsys.hhsetcid;

create or replace 
  FUNCTION hhand ( hh1 IN RAW, hh2 IN RAW ) 
    RETURN RAW IS
begin
 return md.hhand(hh1, hh2);
end;
/
-- grant execute on hhand to public;
create or replace  public synonym hhand for mdsys.hhand;


create or replace 
  FUNCTION hhor ( hh1 IN RAW, hh2 IN RAW ) 
    RETURN RAW IS
begin
 return md.hhor(hh1, hh2);
end;
/
-- grant execute on hhor to public;
create or replace  public synonym hhor for mdsys.hhor;

create or replace 
  FUNCTION hhxor ( hh1 IN RAW, hh2 IN RAW ) 
    RETURN RAW IS
begin
 return md.hhxor(hh1, hh2);
end;
/
-- grant execute on hhxor to public;
create or replace  public synonym hhxor for mdsys.hhxor;


create or replace 
  FUNCTION hhencode_bylevel (
       v01 IN NUMBER, l01 IN NUMBER, u01 IN NUMBER, p01 IN BINARY_INTEGER,
       v02 IN NUMBER:=NULL, l02 IN NUMBER:=NULL, u02 IN NUMBER:=NULL, 
       p02 IN BINARY_INTEGER:=NULL,
       v03 IN NUMBER:=NULL, l03 IN NUMBER:=NULL, u03 IN NUMBER:=NULL, 
       p03 IN BINARY_INTEGER:=NULL,
       v04 IN NUMBER:=NULL, l04 IN NUMBER:=NULL, u04 IN NUMBER:=NULL, 
       p04 IN BINARY_INTEGER:=NULL)
    RETURN RAW IS
begin
 return md.hhencode_bylevel(v01, l01, u01, p01, v02, l02, u02, p02,
                            v03, l03, u03, p03, v04, l04, u04, p04);
end;
/
grant execute on hhencode_bylevel to public;
create or replace  public synonym hhencode_bylevel for mdsys.hhencode_bylevel;

create or replace 
  FUNCTION hhmaxcode(hhc IN RAW, maxlen IN NUMBER)
    RETURN RAW IS
begin
 return md.hhmaxcode(hhc, maxlen);
end;
/
grant execute on hhmaxcode to public;
create or replace  public synonym hhmaxcode for mdsys.hhmaxcode;


declare
begin
  begin
  execute immediate 'revoke  execute on hhndim from public';
--  execute immediate 'revoke  execute on hhlength from public';
  execute immediate 'revoke  execute on hhbytelen from public';
  execute immediate 'revoke  execute on hhprecision from public';
  execute immediate 'revoke  execute on hhlevels from public';
  execute immediate 'revoke  execute on hhencode from public';
  execute immediate 'revoke  execute on hhdecode from public';
--  execute immediate 'revoke  execute on hhcellbndry from public';
  execute immediate 'revoke  execute on hhcellsize from public';
--  execute immediate 'revoke  execute on hhsubstr from public';
  execute immediate 'revoke  execute on hhcollapse from public';
  execute immediate 'revoke  execute on hhcompose from public';
  execute immediate 'revoke  execute on hhcommoncode from public';
--  execute immediate 'revoke  execute on hhmatch from public';
  execute immediate 'revoke  execute on hhdistance from public';
  execute immediate 'revoke  execute on hhorder from public';
  execute immediate 'revoke  execute on hhgroup from public';
  execute immediate 'revoke  execute on hhjldate from public';
  execute immediate 'revoke  execute on hhcldate from public';
  execute immediate 'revoke  execute on hhidpart from public';
  execute immediate 'revoke  execute on hhidlpart from public';
  execute immediate 'revoke  execute on hhcompare from public';
  execute immediate 'revoke  execute on hhncompare from public';
  execute immediate 'revoke  execute on hhsubdivide from public';
  execute immediate 'revoke  execute on hhstbit from public';
  execute immediate 'revoke  execute on hhgtbit from public';
  execute immediate 'revoke  execute on hhstype from public';
  execute immediate 'revoke  execute on hhgtype from public';
  execute immediate 'revoke  execute on hhcbit from public';
  execute immediate 'revoke  execute on hhsbit from public';
  execute immediate 'revoke  execute on hhgbit from public';
  execute immediate 'revoke  execute on hhincrlev from public';
  execute immediate 'revoke  execute on hhgetcid from public';
  execute immediate 'revoke  execute on hhsetcid from public';
  execute immediate 'revoke  execute on hhand from public';
  execute immediate 'revoke  execute on hhor from public';
  execute immediate 'revoke  execute on hhxor from public';
--  execute immediate 'revoke  execute on hhencode_bylevel from public';
--  execute immediate 'revoke  execute on hhmaxcode from public';

   exception when others then null;
  end;
end;
/



