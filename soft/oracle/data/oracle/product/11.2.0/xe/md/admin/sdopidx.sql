Rem
Rem $Header: sdo/admin/sdopidx.sql /main/11 2009/05/26 12:07:43 yhu Exp $
Rem
Rem sdopidx.sql
Rem
Rem Copyright (c) 2001, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      sdopidx.sql - SDO Private InDeX routines
Rem                    Helper routines for sdo_index_method 
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    yhu         05/20/09 - bug 8327104: use ODCI interface for TTS
Rem    yhu         02/17/09 - change to tts_index_initialize
Rem    rkothuri    05/18/05 - remove index_update/ etc. fns from sdo_idx 
Rem    sravada     07/30/04 - remove drop package 
Rem    rkothuri    04/25/03 - remove idx_delete, idx_ins: no auto txns
Rem    rkothuri    04/24/03 - remove rbk_idx_chngs, autotx_ins/del
Rem    rkothuri    02/03/03 - 
Rem    sravada     09/18/02 - add tts for create index
Rem    rkothuri    06/21/02 - 
Rem    rkothuri    05/09/02 - add new insert/del functions using system txns
Rem    sravada     08/08/02 - move sdo_feature out
Rem    sravada     07/24/02 - add SDO_FEATURE sigs
Rem    sravada     04/22/01 -
Rem    sravada     04/22/01 - Created
Rem


-- drop package sdo_idx;

CREATE OR REPLACE PACKAGE sdo_idx AUTHID current_user AS

  PROCEDURE ie_crt_geom_metadata;

  PROCEDURE cmt_idx_chngs(schema varchar2, iname varchar2, 
		          iptn varchar2, tnum number);

  FUNCTION ENDIANCONVERT(entry   IN BLOB, 
                         fanout  IN NUMBER,
                         dim     IN NUMBER) 
  RETURN BLOB;

  PROCEDURE tts_index_initialize(  
    schema_name               IN VARCHAR2,
    SDO_INDEX_TYPE            IN VARCHAR2,
    SDO_LEVEL                 IN NUMBER,
    SDO_NUMTILES              IN NUMBER,
    SDO_MAXLEVEL              IN NUMBER,
    SDO_COMMIT_INTERVAL       IN NUMBER,
    SDO_INDEX_TABLE           IN VARCHAR2,
    SDO_INDEX_NAME            IN VARCHAR2,
    SDO_INDEX_PRIMARY         IN NUMBER,
    SDO_TSNAME                IN VARCHAR2,
    SDO_COLUMN_NAME           IN VARCHAR2,
    SDO_RTREE_HEIGHT          IN NUMBER,
    SDO_RTREE_NUM_NODES       IN NUMBER,
    SDO_RTREE_DIMENSIONALITY  IN NUMBER,
    SDO_RTREE_FANOUT          IN NUMBER, 
    SDO_RTREE_ROOT            IN VARCHAR2,
    SDO_RTREE_SEQ_NAME        IN VARCHAR2,
    SDO_FIXED_META            IN RAW,
    SDO_TABLESPACE            IN VARCHAR2,
    SDO_INITIAL_EXTENT        IN VARCHAR2,
    SDO_NEXT_EXTENT           IN VARCHAR2,
    SDO_PCTINCREASE           IN NUMBER,
    SDO_MIN_EXTENTS           IN NUMBER,
    SDO_MAX_EXTENTS           IN NUMBER,
    SDO_INDEX_DIMS            IN NUMBER,
    SDO_LAYER_GTYPE           IN VARCHAR2,
    SDO_RTREE_PCTFREE         IN NUMBER,
    SDO_INDEX_PARTITION       IN VARCHAR2,
    SDO_PARTITIONED           IN NUMBER,
    SDO_RTREE_QUALITY         IN NUMBER,
    SDO_INDEX_VERSION         IN NUMBER,
    SDO_INDEX_GEODETIC        IN VARCHAR2,
    SDO_INDEX_STATUS          IN VARCHAR2,
    SDO_NL_INDEX_TABLE        IN VARCHAR2,
    SDO_DML_BATCH_SIZE        IN NUMBER,
    SDO_RTREE_ENT_XPND        IN NUMBER,
    SDO_ROOT_MBR              IN MDSYS.SDO_GEOMETRY,
    ENDIAN_FLAG               IN VARCHAR2);

  FUNCTION  process_params(params IN varchar2, invert_mask IN number,
			   mask_str IN OUT varchar2, dst IN OUT number,
			   units_str IN OUT varchar2, mltpl_msks IN OUT number)
   	    RETURN number;


END sdo_idx;
/
show errors;


grant execute on mdsys.sdo_idx to PUBLIC;


