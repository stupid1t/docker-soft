Rem
Rem $Header: sdo/admin/loce111.sql /main/4 2009/04/03 14:48:19 sravada Exp $
Rem
Rem loce111.sql
Rem
Rem Copyright (c) 2008, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      loce111.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sravada     04/02/09 - bug 8400863
Rem    sravada     12/03/08 - drop AggrSetUnion
Rem    yhu         07/23/08 - add sdopopt downgrade
Rem    sravada     02/14/08 - Created
Rem

ALTER SESSION SET CURRENT_SCHEMA = MDSYS;

-- drop SDO_Aggr_Set_Union
drop public synonym SDO_Aggr_Set_Union;
drop function MDSYS.SDO_Aggr_Set_Union;

-- Drop objects related to sdopopt.sql 
----------------------------------------------------------------------
drop public synonym SDO_PQRY force;
drop public synonym sdoridtable force;
drop function MDSYS.SDO_PQRY;
drop function MDSYS.qry2opt;
drop function MDSYS.sdo_simple_filter;
drop type MDSYS.sdoridtable force;

@@sdoeoper.sql

-- Drop 3D WKT column in sdo_cs_srs and cs_srs
----------------------------------------------------------------------
begin
begin
  execute immediate 'alter table mdsys.sdo_cs_srs drop column wktext3d';
  exception
    when others then null;
end;

begin
   execute immediate 'drop trigger MDSYS.SDO_PREFERRED_OPS_SYSTEM_TRIG ';
   exception
    when others then null;
end;

begin
   execute immediate 'drop trigger MDSYS.SDO_COORD_OPS_TRIGGER ';
   exception
    when others then null;
end;

begin
   execute immediate 'drop package MDSYS.SDO_TOPO_METADATA_INT ';
   exception
    when others then null;
end;

end;
/

CREATE or replace VIEW MDSYS.CS_SRS AS (SELECT * FROM MDSYS.SDO_CS_SRS);

drop trigger MDSYS.SDO_CRS_INSERT_TRIGGER;

drop trigger MDSYS.SDO_coord_op_param_val_TRIGG2;

commit;

ALTER SESSION SET CURRENT_SCHEMA = SYS;

alter session set current_schema=SYS;
COLUMN java_fname NEW_VALUE java_file NOPRINT;
SELECT dbms_registry.script('JAVAVM','@sdounloadj.sql') AS java_fname FROM DUAL;
@&java_file



ALTER SESSION SET CURRENT_SCHEMA = SYS;

