Rem
Rem $Header: catpspec.sql18745 15-nov-2006.09:09:53 ilistvin Exp $
Rem
Rem catpspec.sql
Rem
Rem Copyright (c) 2006, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      catpspec.sql - CATPROC Package Specs
Rem
Rem    DESCRIPTION
Rem      Single-threaded script to create package specifications that are 
Rem      referenced in other package specs (in catpdbms.sql)
Rem
Rem    NOTES
Rem       
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    ilistvin    11/15/06 - create specs for packages used by other pakage
Rem                           specs
Rem    ilistvin    11/15/06 - Created
Rem

Rem advisor framework
@@dbmsadv

