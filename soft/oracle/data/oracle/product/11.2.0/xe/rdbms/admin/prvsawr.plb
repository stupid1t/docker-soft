/* -------------------------------------------------------------------------
 *   DBMS_SWRF_REPORT_INTERNAL PACKAGE
 *
 *     This package will handle the reporting for AWR. The report main
 *     routine will be called by the dbms_workload_repository.awr_report
 *     function.
 * ------------------------------------------------------------------------- */
/*
 * -------------------------------------------------------------------------
 *   DBMS_SWRF_REPORT_INTERNAL error code summary
 *
 *   -20001 : error in the report main, at the top of the error stack
 *   -20002:  internal error in ith_displayable_child routine when passed
 *            invalid arg tree_idx
 *   -20003:  internal error in ith_displayable child routine when passed
 *            invalid arg child_idx
 *   -20004:  invalid argument passed to dflt_align_for_type, internally
 *   -20005:  error initializing the report due to missing snapshot data
 *   -20006:  missing start value for dba_hist_librarycache
 *   -20007:  missing end value for dba_hist_librarycache
 *   -20008:  missing init.ora param
 *   -20009:  missing system stat
 *   -20010:  missing start value for dba_hist_waitstat
 *   -20011:  missing end value for dba_hist_waitstat
 *   -20012:  missing start time wait value for dba_hist_system_event
 *   -20013:  missing end time wait value for dba_hist_system_event
 *   -20014:  missing start value for DBA_HIST_LATCH gets and misses
 *   -20015:  missing end value for DBA_HIST_LATCH gets and misses
 *   -20016:  missing value for SGASTAT
 *   -20017:  Missing start value for DLM statistic
 *   -20018:  Missing end value for DLM statistic
 *   -20019:  invalid begin_snap/end_snap pair specified by user
 *   -20020:  invalid dbid/inst_num pair specified by user
 *   -20021:  missing start value for time model stat
 *   -20022:  missing end value for time model stat
 *   -20023:  missing start and end values for time model stat
 *   -20024:  failed to reset time zone
 *
 *   -20100:  invalid flush level specified to create_snapshot
 *   -20101:  no valid snapshots in range (bid, eid) for database id dbid
 *   -20102:  user name 'schname' is invalid
 *   -20103:  directory name 'dmpdir' is invalid
 *   -20104:  not allowed to specify the 'SYS' user
 *   -20105:  unable to move AWR data from schema to SYS
 *   -20106:  cannot move data from newer AWR schema
 *   -20107:  not allowed to move AWR data for local dbid
 *   -20108:  cannot move data from newer AWR schema
 *   -20109:  error encountered during move_to_awr
 *   -20110:  invalid Top N SQL value.  not allowed to specify 0 or 1.
 *   -20111:  invalid Top N SQL string: topnsql
 *
 *   -20500:  Invalid input to generate ASH report
 *   -20501:  Invalid input to helper function to create report
 *
 *   -20600:  Invalid (dbid, version) for DB Feature Usage
 *   -20601:  Invalid (dbid, version) for High Water Mark
 */
create or replace package dbms_swrf_report_internal wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
92b6 1f86
fhNqQeJ2IQzuPiibbjiu1Pzp1Hgwg1VMuscFU/EZweSeK9wBzW7yG2YyLK7oGGao9PYYkeeX
nEI4PmF2mGAtwvHAZRf2J6r44Y9EppZptzyKQaNcG4aFzkX98LlgjPusN95M7IIaVar5RdD+
JJ8oCEhzeZja265yB4G5Qg2zWeT8PdUuC6o62RZUYC1tNJMhsbTGmwB2WIcaQtmVNgKDciwq
7Zopccg8MK30SB2Ja7Ry7ItwzZd0O76Iq8GgY2CqzxJr+izUCetxxoS0OrLeRMm2xoJqBbQI
ZiHvWT6x84aIyP+m+x05VHV4LUOSt3UtCeEjH8Pl51WiUuPhJF8GcTRRbyE1FPO4tgLJXx0K
OXBQM04R7uGIkexhzQQUbzf18JF+IjfzI6zn2BZhkCkhy3NjF9oZ2PLoNP36pTHnHqIC32bw
/CEMBdDR8dQ5GlJJeWhEIbn83oYcDlKdnN+5Hase1qAIpiUwdd7bhLHDsS0qKJlvT2peu1ex
2iBY33KHkH8hKs0w02ZO8WtOM6BS3AKgSZ16sbK5zIkK1X/YAkFzQiu423eyjLT1kFgfrh/3
051mMCUiRmGalDp/n2+OP+pAFyFxk4sjq1tg1mDh9CbhCCopXWygc7uhEl/hUo78BbW/gyKm
y+wra0Srv0ehbCQ+doI0GwkOi+Arx9Go7Sjo2xNwSeIpTMIwwDa6KvVZDCtZIQFAyV0Hgc7q
bIqItQjt/MfoczoXRJJz9648ueaOd04LBV9ZXICNrMksIApVZMvVeXPG6/lLsRxh2jtgR2qG
64cFZLvLrgUXefgOpByxo2ULtfVgHdObiiHoLMwkI7ywzsg43qmh6POTc42l5SJcM/xEavsE
yF7rCvNB3irXtuOwwcztYCo2fBzQmly1sa2YLxYuTJpf/NR9BGNPe71/8uf30CXqtuAk8N5q
A34bwLMh8Mg968jIpmlgb1gGaVThwyEXoVl9R4uUXQrT8WaBtJgrIbtMXfqo+V0boXRBthJJ
od5sOv/paw0eEoKDU+WBS8f3G5j6SdcoEL++pbhMW3tY8ljCoYZXpVJROhQxstcFTtFY13Pe
qHhw3Y5pSk5ma6zr9J3aeBne7b+DfaT6oRn35LmQ38q1d7KbArNJmtIym40Zf4WcYLCAvM0y
1Na/ZCdVMhrQPnBQpIEWvu6pZzlzP5zIJZgjF1o/3nR07YA/BXgj1BEaDmO2QHPuqcAcKIEi
jcMs/kWkxSC/Iav1YDooqnjDZWN5V7m74fEz2vi+maiCL30v6e3XOVD1THu0zoX8N6u+Gf0z
YFU1IDD3ZEohZaSPhSiJ+YnXgwsza2EmWLGok6x1BOqvnj/Pn8STzf4sD2Q/iocDpg4uUEcI
EWhKdppt7wdX5gCSnMbRE5M2QV0sF50bGn1dqLYJ55vZB8a5ZFHn4tbvZ1s/OrzztPrKbJh2
xjGVhemZ7BLVuBNJ8TyXpCLV5936FkS0eqHhNqjsku4VX2aRCrianx6sNlnBg1s1nlinCZ7m
0UTCAja6L2fS/kKcxxrr+FYh1kJ2WAOM3SCtSkaTwXhBBaSvAMBAVmfG8O4K1+ucwdBkeB4H
avbNxutz7giG9oxOizLcnBpT0cz7/Uomx9aK9sbiTvkj3Ac9wdej+DRVVWS/LA8erdXCUz87
cKLCnfQsvHUPweedb6RNL+ziaCD89Hf15mIKp9RRanPraoSk60DlTUmhaDZvSQQuvauSsazR
lCWK8nz/XJVSSVPCAoUUFXPy9Jic4Zd2WN+hrAnY5keA8HLhT5004qjmzcc823vZ20/EbnrN
E1ZrRSDQPv9fAMc3VEbHSEr8kVzLPINO/B6Shh5fg24vI70NxlWS7oYenQFdpcCXPib69Ox8
rxn5OI3w0N5M1Peu+TYh54flcMB4KfVStD8KuoPDz5s/zH2RMv6Yn7QnzOZm9yS9faB34281
9f2G3aDIwkm1/90XqutbkG2ggH47rrIcjjPjDbbhbEGP3L04obikGx1SXN1wjoz790C/ayYv
NlYCvbNbzcjo96Cqh7aAPLlhE3PElFibIUjet8i0shhK1c6xfibyH9ptMOZ0ymFZ9txRCMyK
XfhqaiOwkwqYxoFyX0pZ+ypRDpBkT7uai/W4oiPuh9TOeKlYj2rt3zF+9JUGsLUGwo+tk7tp
jfk5fbI/cc08/qNLVUgnJg7Uw57twHOI6y8WnlMFlqNRa08DzGYU5pH48SvDFBWBcysFbe2o
01dGQN4LdTauvwrwYbpkuf7P40yjWTPJyr3xOL4ScQCqk8fTr+Rv1mPnHLW+ZC/Ws1PLkOcq
awgL/vUvNpPgBjKRRFujYGL+21S99/DKRojgRjc4AFTNM+G7QhGcpQ20Oie/ZBC3vcgt0gAW
x2WnBRdK6XiF4RdPfdCBVb/MecwRb4ixjIxV/EKd4D2A+uN8LVGTdoPGPjShNBJPg/PMNojj
yFSE3NGiPGVfHdWsQpVV/lX5KSz200Lf/c2ysAaY0ZBSNTwvSHgTcpGnaJyPUXaA7tiWW+Jm
oW72dlsUvxWT3iuhu8ZBsxUFVahJUnnUbb2hvc+ZOG5Wth6hcap8DZWi+GEFGTv++Cqx4gCo
DPgCfsENw4r0bwAQTE+7mhgw1VTgshhJ8LZTIYL3aNKD9WbiVQtOGMRzLSN7455jLQNn6aTa
7c0NWayXfx8CpoVr1729LsoGMJmgcIyefV9O5Pkekh6f/q7fwuGUsVjd6VJPIrsrRmDMjoqu
jBSe+6KvBQfW1FF1Zzdg0+xRGKJK6dPsk28pU8mR15mk5bv//Lj+hXtOYEqVd1AlT8j2HPSV
d1BTFhEDkmGdH72j8y1vlrASFGBCBcH+rZNSl+jW7Y6U2FEDWtQjvg8Q8w2E1bMDFjw10dJA
b+L65RNdGPbouKsjqj3H4EeSnjaU2wn3fcu2jPg309eFbI4X4iR/VRTlBzyN5Rw9fBciE3lh
T/rbkBT0T7dCcMS8urfO1++JhSwHqq2q8edaaGJ2ci+7vglPGghbJVT48uBWiIbl/xpduKOH
fmBInT8i5/ec2AuwDIrOHBSbhweD0vhB/+YCer2zz26nwoSsM8iX+hP8H82hM7lQt4S/BcHB
gVU3/0AoKK6/gzpDXTtwCyHSRSL6PPGBAp/sf/+s0vxK3CODEi7vhFnU+ErwxHbOhKCl79sx
tURov9uxWs/YBkjaztOP+Zb8EpK+qoO1QVKuNnxkyVi3qLvr+jZ8tQjp+PFXtJI6JJ1knWQN
lvbg+xQ3UP+dnj42QyACIXKY4L6Fmc5tWbYAms3Txv9xLUq1CW8OrbWePJq/2gZLfPmoLR3G
Da6XJB6SwbuuE/gj1HWnlIPVh6912wrFvHe0FgtFkUFFDHogxr8bm2jIlIFqi/j5UpBunIWR
heFFj7jexIjBnaKaWcsCa0RKv6MQDEAzzSmTr7p2iutOK8vOCasIQNoo/K9vAk39CGN6XbI6
LCkwQ8jHi/VF0CQBC+INBkHJ+1a9UPEwowBHnM4bzgp199Lu/nMsmpdZ3j/1xeiTPBqKbNca
4TcP/UBGYP8hTKuxUoRJj0DKaPHHDZHRGNMKk4+ISJfNsgCXMrWgjIgf/a9W1olTV34SJAuz
NK5f2etOzCJesZ1KOxb1gmB5znp+YSQh8ekcnwXXQHVg9LcA8wewVRYxtt/KrS7CtNW2eVq3
O4TpUVYEWAYcodrH8gIBAqgrinuaZLt1pGNrSWWMtA19wtoh78YkP+MCpIrgfKOFYDhZCj6p
A5atR/CruB6LfBIJ2NrVcO12YDxVa9ihPThIiG9FtwznfmmPi5ZvL7R8B+l4h5AupziMdqd+
RKVbH4lLUsuZi8So8BzKSCOA20zWY3e+kEcuo33Lf1pZkP0YNq0Yhqfmm1ttedGpuAA1kPWS
SWHhFYFDOOezSXVuHYiA5vbnFVL2+icHfD9ocX7PMabGzQK5XKxSYIB2W5rWSk+vzQJcpRot
6u5T7cqxfqke8uRd5+IVUhJrAnIBw5OTJt2BlVOTtKhc7lPtcGE4osRmSaZoS+Fb7sbNAatm
21954SmaU1+EpnHEcJZuMfNuh92oykoFmQif4DxmhJTiutuvimPZzQIU338zZqjS5bgQvXgy
k5823bLy5FqeHxzGUhJroYKVmyfonnjF7lOWJrhohxGDdIuxHom4yj8ATIUUES6OTraG8m7M
/CwzpjrcZLrxtkCGM2XHSaA8bwtp1OuW7lkvSGkV12bn3KAfK1iY4TqEIMPbr4uNGVQVYmvk
kpBmy9RkZB9d22c/YPt4+5seb7heyuLqibbjHh/S4c2aDqmZqeRARHagqoxRbmdoVErWT6fn
iSjLe0RxQyO84bAfuKDd4a+I6T/haQIe8lMvVhdkqdb8AgWZ1W0GrX68pHRGQgO9SBY76t0e
qa9ah8GlO1HvVz68yI55ZlZ+XG1TSnWqOQQxfK0H5ry6ccHVUkCJqpf0Cp7GzQEpyEYcX4Yz
FUouqVIJvNXFTudKBQzAymc25biEOD+/flMivu1XGvfEcaZSaIfyiokKv6+Hq+5T7VBJf3Ef
vnx2xJzVjvL43Sd2xJy+rtLg4bGAjEF8y2Xy8B7ZhqaLeC9zIO4waT9oh8LXy1B7FyHFhsoI
AdvNRHJHV2VqiKy0gVRNnSEVUhKTI2EslsmZyLXwsrMgYoJNekcrNx4YUsT73IeualIQLdx3
pngvioKj3VLmDf+ep7hoWnGwQJGXRAZdI6QEsL97FaKkhyqm/AaexL1QyFn1wP29SUSNZ0iC
V1pxvQ8xL3BkaW9vbQ7ycA928KCssuHyyYapauwZiPIy1q7iBNKN3ffndIvqcEqGGPLrNgkt
Q6eEEhFJFMpn7eHjymdIrdKWH0yoH9Qtuvs2Uoq9KUQbEcfztMlGSh2+Q6I/kUV0KehA4jae
vXzeb7el8E7Yw712b19xosU3FfH0Z6uJisaswIV675T9jSr43JQv+6lowPlqNGWvPS5MFvjA
ytumdLPA5pHRS93DvYpcHbPMcJ18jOYHhBZzG17FBFdWRyZwj018Jqw3C3+Z9qaOl52RK3nh
d21O5aYr4fAG255En2221Y601dzYaXA4skRMEinm81dFvUylBgy7ywHKboZapnv3mUxhKeak
V/vhHgQjZF6jS9aeryOulvvVy27q+7JCVxqwLond4thcV0vqadxDvZvFcJ13gEjH8o9nuYxd
9/LKxHbFTYr68g5LTy54rY9nSK0rr9vQimifonCoWF7hMvMWJH4aOadvXLSTqaljXJSOVKMr
NTdjMpSOVKNUNTdjjZSOFpxIhSioR9qSm6NuPF5vS+0SQn8ZphbDIHGCUbGfpq6ZWgOUyZgt
OpLyvV2nhmIfArBkid1BIe/4OEOG9FzdvOfbIhCmiNap4EevxKWhgE3z2mFtq6a+Q4+6dJrk
sQLFLYYfHmuX6/2kOl8vVHdrdcF1YaAA4jmrZS804JTFlUELKlDlacXLJpWhkszhvkPOU/O4
aXvb867JL8wHJSb3OWc4XLxLwHymV+Kfkd8Ol07apD5UWOIeug5s8Wl/i2fjO0pdI9wdSlWe
yuMLlRuEepD7qXOejzbhdoVnnzaU4KbmVXXY+ikdR92RPxR5WUxq6vvvvtMRDFpDvdE6fYcd
DocG59nIxmD07y8WkYWLRhqkHMN3QdvzP9jMTSywdjfuzBbKgLTwjz2Ua6yKWbCyl2WG8UOm
mQPmsq3rxRFXdGtAbqutX4yoPMiEdGSo/aQIVCvvcMSF84srYLDqXedEnkJKErKaQnxGq8Dh
RhzwjieFN22Z+G/yj1rhl/0f8o9U8shdt42XXm6kfzUnUIBrQG6ma6OUKHS3IpdwbEu6Xee6
vwRFU/gvktHh/i2WlT2upQMtHYjnTI39kks22DBHnZ4WlXYPp1y9dh+pUdiXxzSQZUvPoYSU
aUE7IjWwrWp88mdUMCFFRMzpp/JwZHQ2sgeok/8qaMBdq5sqaMBfOL3njlxBO6t0xlM2RrKA
xF7fm2iNjWeMFV1lGtO0YtVb1Mxlpv25K1U/++cyCrkOdDa+XL9UpubwNNrFHTWn66Tq456I
ShqObgCzjqP1//HflqiuIb18j7emiF+NN92ZTuJ01twc7XRA4ka6qKmuIbO810c5xYiOOHyN
dEQug/+gMbYuOMB1ee4FhUOPjK4h83XN0au4DF6OrJku9vwluSpCUfSP48g8ZqkiJg79RHag
aG66GA/Cyd22LnZb0SDHccyQxk8Rl7siFyhsJdkBdiD86zJb7XJuTCNaZGWwINehQeoalme+
sxvE5JUijOyxkueW/ve+R82T9DfOEZH/UTA3Rbgee5gXoFoUSXGAZzS5TPHjGV5YaKV1cRLl
jSDHHxRHbDi/R0nXwvnlUpR0MaTCqMIW8eRyKM1OivppMwqKY7ar/Ua/MdU+MB0/CD/Azc6Y
rDNCDF3XLtBV5FigzxJrpJ3SjOZdijn/CQ/VRZWNQSX60q85Oy29XcSHGzOX/4QTNWcPzBQl
8pZwcvqOWzVmLwI1zDpeSVko2LB7AZrgJWa+71Tk35g6dfZcXWeaA8xJbMHL8etg61FuKULZ
aB9b7cS7wB2v+WUUjGIKS7nNutMTXeUz8U1ZpoGFfGX6jpNRqPK9Ubd2qipVZSYGzsXjS4Pg
bPOfeIeR0eGpvmOqfQpYfWPPM2Wio5IiYtnp6MR08vBCal0zH6uT40RLpgqQ2jOyOLqrbtOA
MxdF0rQuc4ojhonNFfU0x5xP7BvbogrYOlN+cvC5Yjf7Oq3eFqYqRM77zXZvpqt/KscpYKOA
vfDkdUBuibHb/ilpXbQWcVHE0nMUFqNAQY4w8iP/Km40M9ga4O01BzZsHrha/zbLNdeooJZg
mLwE8JLgIbsMguN8M9dbLI/JKTuTlpth8rDv22rKMfjoA1KbK56r5XAaq0ULumTPvsdBaIAK
eg2fidKLEGKSzW055fCAxL+KM/lMzYtJfUWzRR47yJaZWym6EMAF7KhhxFouim5dyzF5E8bp
ZvDw4As3IJuV6Vio8w+79XvOaAjimxNW5RaRocOksoyx4zmu/wodqEEMvGO+Asagq3Jmsjib
axDhjLC80P9seWdCbrdEkYgiVhm+f59zFryfil0CvnkR48lxfAEuZYuMUVquJlwYBmJM/haI
o/NLRGVYzSHvat8eTv6HDRvWzDDpcmFHLpJ6Jx7cYs8S+sSqowwLowZiZjv6VOKElvwG9Ona
ZTcMxhgZDMf402WgaC+GUAm+N+hKGjI3GuDiSP2u/GqXbRfiiy2GbFLBRu7BRsx9oK99oH8p
oGspoPP8oDf8oAj8oDgeoLMeoDWKwy3YoNKPlKSolsB6SXtkKAuDCrWQpisfvkYnCD4DY+wE
62XV3pACoP/ao8mrO4JtPL6iHoprMry4GHTuea2UTfUcNHFWR6VhTAFjUmIDj0lQvaO481ca
cd+tnuXYicMRCUcKdWWk0EA7JoZgFnoXwXcxeAYOxSvIMhomQuS1CirB0MCEYvkPUXpM9agF
rzf2GSUZUPpmEPeWyZiewKmNeARnthd/u4bCao8CkWGT8g9x8yFgDLYnC9KZb+9RKmHmDMR0
47wSzPZbrND5T0YliypDKwJ9b0gSgxbPjukD3VeRnOU9vkV7jHTmBlHkSAmSNJMesgcrjDEj
IGtDPOYKr8xZ8TGzSuBcd0FZnM5jBVfRnQK9SKmWg9pC+mioPownJ51hFeCI1GUd5p6Vvpyv
/ra6FNK2ir1bqO4TRnlwGMVgEyZ1mNgOlfXzzmRrot3xZKs6bzyTIuvjc3nT15XLeOwgNr79
Lq1iUuAc6yc5JhgJxne6v0Ea1DcXK88DcLYUtwpl+iD4gFohaYl/X4uoFceaDS2f/9wN4DvF
3TceIZFVrEszZasE129Z8R+zcAz5H7PjhXxL2YqH8mo0QlgiIr76Mc2cUHWWUAYChvH/7DJ7
AW26XTGR8YePBA8HXhxp+qM8glVtqvgszV2kTg==

/
SHOW ERRORS;
