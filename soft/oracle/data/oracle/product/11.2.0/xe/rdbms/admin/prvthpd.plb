CREATE TYPE sys.ku$_dropColList AS VARRAY(1000) OF VARCHAR2(4000);
/
GRANT EXECUTE ON sys.ku$_dropColList to PUBLIC;
CREATE OR REPLACE PACKAGE kupd$data wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
bb0 47f
KgHX39ysCf+QOCMDyeDxFMaPNeYwgzsrLscFyi/NKpKUdsBLFyVKYxTiXi4oWsoTNjeehfTH
OGW3upYAoLWdmZJLFNW0gs0cemExxJiKvFvDV6oVuhg/+TqBP3P5M/iy7oOKBDVCO46U1Dzs
xUYyISHmAyP2Jt2IsDY3XHG6G3RVrLvEuxGNM9xZjtMqp383Pe7bcrrR2fLMOwc185djUDmZ
gvC7frdgw138aVh/GzYYsd4aEfTV1QOA7P13uqb1Exr3kduevrAYlAdtZx6YIYMJC06kK+yr
YNJCaW7dQQ5VYRWXfGoayI2tW/K5OK7UzZrmHSmZ64vP/rmGGh3NS0gTa2oOMI3dQYfySMuU
DUW4KSEvFOfyK78T+4UWGAy6WlzgTn99SIGPMxrXCmzYFk0nAzaCeQYH2xeXbMT4MFxrTscY
scQTmDjv0iuP8UD9UhPPTCB7Gfxxin8XNvBioq+OqHhdrFIY0QdsPvwqZegS5zJ3hTa24Diw
9yai5g4r77DTvTdn74X0XcPvqKthZq09WuNA7Vroq72pq/emma5azn9NOO60nkrE//oV5PI+
Ffsc8T3Jq91YJJGNRz4mlKU5g0PcvcoKayc3n1QvDrdN2V/l/V1aXzNUvtQbeKXwscaa2dJP
rOkWEYZxebldqmwgUXht2SG0qFnjMqsehKwyzCQZ1oTObh/NdXxEnvf8UBLv5plxxx0mVySc
WgPqe7x72tLBMK2ArLOHbaNdSrXn+hgYRM8WiqqCUlY/Can/vw5+Hr9X9YQJLWJo/2jfHGsE
194xId4iQP5nawTcNIJcfXsTD1jdN9V1cNbM/wHfJzrAg8DpLjui2jp8C2fQyPjMyW2nf1nS
X8ybrZwqhVw8i+GrgUmDhQ9HcwAE0kurSZm1qaOi6urXDJdP7yJ6jt8/DLIxZV8GHMq4Q6sn
JgkJKrf70AKWkIl8Cl+6OlapEhHJ4JCAgs2tKOMSG2DBfx8ddqe4W1+wwFaduL8divOHDjkc
KGP07JcHISLXVxR9KSf4OT6h6m2GyA+tM8RMiBbr2nSe3nbkjgHRCmp/nszC5RY3akEeGaEs
0pywkoNF6VSPuPsqleSP1sHWTnZdX8gs1ZQ2AD2Uvp9friRzRLlQO/c=

/
GRANT EXECUTE ON sys.kupd$data TO EXECUTE_CATALOG_ROLE
/
