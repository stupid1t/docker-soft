CREATE OR REPLACE PACKAGE BODY dbms_metadata_diff wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
3131 e46
VOraF6hjg01BspzHpXNcthTNKNMwg1Wr9tAaU38P7upXJaEhrXdY7rxjDfcRuP7IRGjZCxw6
rN/EA/qhvvD1WCgTqk+bt2XfIcdYcVH28EWv0ZweTEObccLkEJ/oXYUYSZspliT5x+QIOkOG
sNCWLMauJM/5WcLRRq6v1KwcB8cvdogKETzIJDTvsNj8fah0TvlSRzFQ0qCkMSl3syKFk+uB
k+u3yU3QoENQxHjkHY84//wMPCXzPEQihYkdAu5YM5pfRNm9kjZClUp26Ijl8qz0rrX1azZp
gdItSQGAo+wDYcqYfGlGRDfbLDb72LyhFQ5+1GuMQKits8QdAq2f5LbX+PcLsrkD353Zg/X+
VS5WcM48oU0+xX0C30REOV3G5kkUNs5TRjZlWTbTHaloAS7MzJxZL2fKU8tQi88hkCjGW7rL
C0+kvlaPsFFGkNJXsXWsTZfcfZepmcJHHtFgZjMQ0WLZ3n+giXE5SjtrmOJkNR2ys+BtN0Z8
eiXHMalHGAtV8CrNA2YeA1J92ScgO0ayNexbKMDR6mmwZyzK5Mpf+NirFixewh+6hzHMDoaF
0y2ru9kkRm5AG7sMPC98hOJRzMXjE45+XKWWUXnfGzhQJT8R/QCs5xauykqs5Pdwa7sK2B6h
qoD9OFqshbkm/d4Fwp+ayQGAOhKu9q2gA7S0nLJpTG8LPEHuOEYuNXQ1cmDR8aR+H0nzGUrk
pUPrTrEW9PmS5GU/tZsy5AXGlk/5+Xb7BUZpS8w1AuA3ZEma8aRgyri5jZb3mvOxQwv0maen
SVZZ5hsAb4+NAnwwqGB5m7cUtmJx39IRhc6lIXRlxEZ0+KzNPHTPQVo09AdaZuN3frhbOwT2
19Ob+AIqdaQLBLIQBA1f3bUFSU+UuL4Ylq+ndiTjN7y8fUM3CFkia7493eyAakPX+2sAMZ9s
b9YtVYUAoDf7Md0X+qTr4SSNYs+7TOs2vnHKYEFaaiPguuY0nGuCdDaS3VBcHBU+CevG0869
oDVNNTCGdUPaQetHuMO7cfw90OlJeufthT7p1mr6r1Yq8p1cBfaYxBucO/m6A7jcxANA7+aS
AM5rMxG67pqTsbNhmnjKa5DsbSi0Kgz/ZJRNbloBiF3CnExAC1yL7w/VyVlTvOBqou8tfo0w
foEakJ4JL66ux4286Ufr1G8Isso0+qTvcC8IH5ijE+Gz3FuZj2KoYVA3ApAeiBiQdUsru526
56VvR8KSqvj/fuSajvEG9Uxat65wt2qbExOajhmuYmMcHfUDbOLnYWyFeN8f2cNRciQAJNCa
tYbZ2U6xnAg62dmutRh634b8rkA1w73vs1TeKJvAlXtFVrKGODRmWMX/jp6Lzktpl4myohJK
PAuT8a2vRgqO+8CPFCXGboeifTCmNbY5BkiYoX23GG/354Tu5Xyb5gl1B+hreThinJP2UV+L
Pfes5f7Bwo/Wdjdcp8eUorMUFi1B0DKgrHtBuAPMd7tR45aw1wB4rOfrjul/fJcnCXSTlPQn
i/eLhCjz2OLezMmlGuUFQb1mV18GhLyDvO4y03w5L0DfRM9hqJ9GSMKj6XnD/DlMP4AeR6Sd
W9c3DlW9FvQRQ8v+eTz1abZ3/197+OPjkWyyZfghoKpwzRKHNJ2xXf0yb9KJslyWhAFaVoHl
X5GUhM19iI1HSubTVJrAK/Mncp6C80VJhcqRiEjLiXpGgqEF2DkHHT1B/dqijZn2gkvH7YXF
IPP1GQl8dB8i7oChKclryTEUI7ZjBHYcAPlt/0/0JX5mjiknOOk+yb8Srk3egFcwrLTKhn3k
H6/nNnfYrC2F6BAr4PKe1y14ojon8YwNMSbYpdzGdN/ml23T+QBDcSYy2BQgK2hFAqIfbJux
vYobhqgYotb7Pyg3XBMroK3+v9o1YhoKu0iHLuqV7ANqZZe+6vNTbcxAI/0xTLecI6PCVvN4
IWV2DBoAiIC/EKjVGLNGbPCvdIVDMxi2zU7aQbrQq2ibK/1Ui/KJiMHzNZ7Z4ye3IcxGBnT6
6oA7gk1CZdJ3sGRWn6DNZVAbhUVQpDfKyDbKBbyrNeMSd8DWJWAU4wgCM91WruysN1vUdsJd
du9mrEe33H4OnNB8U0zBYWWwihpRPvJgV7qGUrCrrUZXu1qEapEd+phNO1mh02yiNEG633HS
2QI1AfnbmtzBRzQ6lRw9yDeD05dVJOvgNJYpHZHox44L9kHOK0zclPjPoEM9UPQhL4iejAYW
CZ56N3Z9C6B++9EqSxb4fD8EfWezhhaS8fbjo9TBa+Mdzhsf0K6y9kEnMAltQyC/+ptcWRC6
QXKs7/9jkoiYmdeBgiCqagNNHbdaae1WJAtit9AP/lQHRc3BlNAImLfz7PDP62iMUbTw7uXH
BqAD0nqc+gm0o+NDBrhm32g47KEUIxkQPvWAp9KUNWyNmeNagYXuzGmot90lQsYFgDX7qioa
1x+/ARAmZwIsxfO8fCyZhfW3T7TtECRLAa6L6W1xlZaYgeSC/3mLERDvyR9yUiubk3pyQnKC
evaEOaQn/20LXVd1/9BXx79QWi8WV1D5LeJGUOmxcJ4VTkq25DgJWQ+AKIo1dTQJpsGDlS0H
Cs1Z7UDVlrpuSmw/i2J5sCYXnipdUPnTVQ+xzvu+lMXysEePRC547y4AVD4xaHJC1qkmhgfM
ycFtX3wp/dRgcJUufGFj2VEmZpXtsQmcZl4CUUxxqQbhREdVaEKDnJAr/YW/0okMge7wL8R6
py9//LqF883LK3ggkJSHh5eVz1lxR0tuTEEbhkEn4pv1/Ioes/QTbGa2LcU/9pd6y+VNhqnO
4bOqxwMx2klmY83O+L8leh/aMB4yYbEpP9Tx9RDwJcbFXztWSUHlM7C8L3y5pTnAb01j4zJP
XGyXAoRAhgN7YPXdTXwWz/gku0oIo4+fPAMTcrDCMikgVVWD7VcKzN81Y+oOuBPBn9oRq9gh
KayTWiW82Q9576z31giKxdq6A7GuXDM+GFnzSD9Hm6blmK7et8SZkfoxd4La2lguMoxe2Cew
VZHW5fKQq0hR+pi2v/IV4NMyjryQsd1NPEMzW53EU3xh0lZaJW74J0x3c6Xr9yd7JWL1zvpa
84gpXGVfL+2VWODUUJjhiavKjlkQk2mjfmbLvtvNHe01aFzDIPvyZEAuINzsByulf0f55goj
8YuUyHfvb7zw6+8D3O/xVMoLgc43+c1EVbWSmM0eE4yE9K3xVnZap9ZFEREgbfw3v1eyP6Q3
5K+2Nzv9ddVOHvRn4zqzOXXXSunMMk0q54CY/mp4C7L1nOD/9P9rGY6kTUK4whKebf+M4ZY/
TFKAtk8I9kVAHPbRUuYa7uG9GRJzTo+RkD8BAlUUqlB/ZfGeqd15D5LO4RqzPeBfXbUo9Sju
zxVk1uWTmLRwqnzLX4yBltVbS+gRYN21/JK75ZH3327SYilBzx4qRgXs7My8TYJoI9OgEQmH
68Al7Dx5SXMso+qTMUf/5LxdnrrbvnC+GYajchaGiwMVZNoKS9WThh+FewNCIJAs8S4mogF0
Cgm8Lt9drJNltDl4ZBG1JLYCjUnPnLwevuC65Xw4gGQ60BVKJ59kB0YKs6kdU1z87ni1OTtD
AKk=

/
