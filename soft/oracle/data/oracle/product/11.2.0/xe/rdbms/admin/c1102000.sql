Rem
Rem $Header: rdbms/admin/c1102000.sql /st_rdbms_pt-112xe/1 2010/11/04 21:15:47 cdilling Exp $
Rem
Rem c1102000.sql
Rem
Rem Copyright (c) 2009, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      c1102000.sql - Script to apply current release patch release
Rem
Rem    DESCRIPTION
Rem      This script encapsulates the "post install" steps necessary
Rem      to upgrade the SERVER dictionary to the new patchset version.
Rem      It runs the new patchset versions of catalog.sql and catproc.sql
Rem      and calls the component patch scripts.
Rem
Rem    NOTES
Rem      Use SQLPLUS and connect AS SYSDBA to run this script.
Rem      The database must be open for UPGRADE.
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    cdilling    10/15/10 - add signature and edition to registry$ table
Rem    hosu        08/09/10 - lrg 4817143
Rem    qiwang      06/04/10 - add logmnr integrated spill table
Rem                         - (gkulkarn) Set logmnr_session$.spare1 to zero
Rem                           on upgrade
Rem    nalamand    06/03/10 - Bug-9765326, 9747794: Add missing default 
Rem                           passwords
Rem    rangrish    06/02/10 - revoke grant on urifactory on upgrade
Rem    thoang      06/01/10 - handle lowercased user name
Rem    schakkap    05/23/10 - #(9577300) add table to record column group usage
Rem    hosu        05/10/10 - upgrade wri$_optstat_synopsis$
Rem    tbhosle     05/04/10 - 8670389: increase regid seq cache size, move 
Rem                           session key into reg$
Rem    thoang      04/27/10 - change Streams parameter names
Rem    hosu        04/09/10 - reduce subpartition number in wri$_optstat_synopsis$
Rem    hosu        03/30/10 - 4545922: disable partitioning check
Rem    bdagevil    03/14/10 - add px_flags column to
Rem                           WRH$_ACTIVE_SESSION_HISTORY
Rem    yurxu       03/05/10 - Bug-9469148: modify goldengate$_privileges
Rem    dongfwan    03/01/10 - Bug 9266913: add snap_timezone to wrm$_snapshot
Rem    apsrivas    01/12/10 - Bug 9148218, add def pwds for APR_USER and
Rem                           ARGUSUSER
Rem    jomcdon     02/10/10 - bug 9368895: add parallel_queue_timeout
Rem    hosu        02/15/10 - 9038395: wri$_optstat_synopsis$ schema change
Rem    jomcdon     02/10/10 - Bug 9207475: allow end_time to be null
Rem    sburanaw    02/04/10 - Add DB Replay callid to ASH
Rem    juyuan      02/03/10 - add lcr$_row_record.get_object_id
Rem    akociube    02/02/10 - Fix OLAP revoke order
Rem    sburanaw    01/08/10 - add filter_set to wrr$_replays
Rem                           add default_action to wrr$_replay_filter_set
Rem    juyuan      01/21/10 - remove all_streams_stmt_handlers and
Rem                           all_streams_stmts
Rem    jomcdon     12/31/09 - bug 9212250: add PQQ fields to AWR tables
Rem    juyuan      12/27/09 - create goldengate$_privileges
Rem    akociube    12/21/09 - Bug 9226807 revoke permissions
Rem    amadan      11/19/09 - Bug 9115881 Add new columns to PERSISTENT_QUEUES
Rem    arbalakr    11/12/09 - increase length of module and action columns
Rem    akruglik    11/18/09 - 31113 (SCHEMA SYNONYMS): adding support for 
Rem                           auditing CREATE/DROP SCHEMA SYNONYM
Rem    jomcdon     12/03/09 - project 24605: clear max_active_sess_target_p1
Rem    xingjin     11/14/09 - Bug 9086576: modify construct in lcr$_row_record
Rem    akruglik    11/10/09 - add/remove new audit_actions rows
Rem    gravipat    10/27/09 - Add sqlerror$ creation
Rem    hayu        10/01/09 - change the advisor/spa
Rem    msakayed    10/28/09 - Bug #5842629: direct path load auditing
Rem    praghun     11/03/09 - Added some spare columns to milestone table
Rem    thoang      10/13/09 - support uncommitted data mode
Rem    tianli      10/11/09 - add xstream$_parameters table
Rem    elu         10/06/09 - stmt lcr
Rem    praghuna    10/19/09 - Added start_scn_time, first_scn_time
Rem    msakayed    10/22/09 - Bug #8862486: AUDIT_ACTION for directory execute
Rem    lgalanis    10/20/09 - STS capture for DB Replay
Rem    achoi       09/21/09 - edition as a service attribute
Rem    shbose      09/18/09 - Bug 8764375: add destq column to aq$_schedules
Rem    cdilling    07/31/09 - Patch upgrade script for 11.2.0
Rem    cdilling    07/31/09 - Created
Rem

Rem *************************************************************************
Rem BEGIN c1102000.sql
Rem *************************************************************************

Rem =======================================================================
Rem  Begin Changes for XStream
Rem =======================================================================

Rem
Rem Add some spare columns for apply optimization purpose
Rem
alter table streams$_apply_milestone add
(spare8 number, spare9 number, spare10 timestamp, spare11 timestamp);

alter type lcr$_row_record add member function
   is_statement_lcr return varchar2 cascade;

alter type lcr$_row_record add member procedure
   set_row_text(self in out nocopy lcr$_row_record,
                row_text           IN CLOB,
                variable_list IN sys.lcr$_row_list DEFAULT NULL,
                bind_by_position in varchar2 DEFAULT 'N') cascade;

alter type lcr$_row_record drop static function construct(
     source_database_name       in varchar2,
     command_type               in varchar2,
     object_owner               in varchar2,
     object_name                in varchar2,
     tag                        in raw               DEFAULT NULL,
     transaction_id             in varchar2          DEFAULT NULL,
     scn                        in number            DEFAULT NULL,
     old_values                 in sys.lcr$_row_list DEFAULT NULL,
     new_values                 in sys.lcr$_row_list DEFAULT NULL,
     position                   in raw               DEFAULT NULL
   )  RETURN lcr$_row_record cascade;

alter type lcr$_row_record add static function construct(
     source_database_name       in varchar2,
     command_type               in varchar2,
     object_owner               in varchar2,
     object_name                in varchar2,
     tag                        in raw               DEFAULT NULL,
     transaction_id             in varchar2          DEFAULT NULL,
     scn                        in number            DEFAULT NULL,
     old_values                 in sys.lcr$_row_list DEFAULT NULL,
     new_values                 in sys.lcr$_row_list DEFAULT NULL,
     position                   in raw               DEFAULT NULL,
     statement                  in varchar2          DEFAULT NULL,
     bind_variables             in sys.lcr$_row_list DEFAULT NULL,
     bind_by_position           in varchar2          DEFAULT 'N'
   )  RETURN lcr$_row_record cascade;

alter type lcr$_row_record add member function
   get_base_object_id return number cascade;

alter type lcr$_row_record add member function
   get_object_id return number cascade;

alter table sys.streams$_apply_milestone add
(
  eager_error_retry              number /* number of retries for eager error */
);

alter table sys.apply$_error add
(
  retry_count           number,         /* number of times to retry an error */
  flags                 number                                      /* flags */
);

alter table sys.apply$_error_txn add
(
  error_number         number,                      /* error number reported */
  error_message        varchar2(4000),           /* explanation of error */
  flags                number,                                      /* flags */
  spare1               number,
  spare2               number,
  spare3               varchar2(4000),
  spare4               varchar2(4000),
  spare5               raw(2000),
  spare6               timestamp
);


--
-- Table to for ddl conflict handlers
--
create table xstream$_ddl_conflict_handler
(
  apply_name        varchar2(30) not null,                     /* apply name */
  conflict_type     varchar2(4000) not null,                /* conflict type */
  include           clob,                                /* inclusion clause */
  exclude           clob,                                /* exclusion clause */
  method            clob,                   /* method for resolving conflict */
  spare1            number,
  spare2            number,
  spare3            number,
  spare4            timestamp,
  spare5            varchar2(4000),
  spare6            varchar2(4000),
  spare7            clob,
  spare8            clob,
  spare9            raw(100)
)
/
create index i_xstream_ddl_conflict_hdlr1 on
  xstream$_ddl_conflict_handler(apply_name)
/

--
-- Table to for ddl conflict handlers
--
create table xstream$_map
(
  apply_name        varchar2(30) not null,                     /* apply name */
  src_obj_owner     varchar2(30),                      /*source object owner */
  src_obj_name      varchar2(100) not null,            /* source object name */
  tgt_obj_owner     varchar2(30),                     /* target object owner */
  tgt_obj_name      varchar2(100) not null,            /* target object name */
  colmap            clob,                       /* column mapping definition */
  sqlexec           clob,                              /* SQLEXEC definition */
  sequence          number,                      /* order of mapping clauses */
  spare1            number,
  spare2            number,
  spare3            number,
  spare4            timestamp,
  spare5            varchar2(4000),
  spare6            varchar2(4000),
  spare7            clob,
  spare8            clob,
  spare9            raw(100)
)
/
create index i_xstream_map1 on
  xstream$_map(apply_name)
/


Rem
Rem Add time parameters for scn
Rem
ALTER TABLE streams$_capture_process ADD 
(
  start_scn_time      date , 
  first_scn_time      date 
);

Rem
Rem Table for xstream parameters 
Rem
create table xstream$_parameters
(
  server_name       varchar2(30) not null,            /* XStream server name */
  server_type       number not null,         /* 0 for outbond, 1 for inbound */
  position          number not null,    /* total ordering for the parameters */
  param_key         varchar2(100),               /* keyword in the parameter */
  schema_name       varchar2(30),                   /* optional, no wildcard */
  object_name       varchar2(30),               /* optional, can do wildcard */
  user_name         varchar2(30),                           /* creation user */
  creation_time     timestamp,                              /* creation time */
  modification_time timestamp,                          /* modification time */
  flags             number,                              /* unused right now */
  details           clob,                           /* the parameter details */
  spare1            number,
  spare2            number,
  spare3            number,
  spare4            timestamp,
  spare5            varchar2(4000),
  spare6            varchar2(4000),
  spare7            raw(64),
  spare8            date,
  spare9            clob
)
/
create unique index i_xstream_parameters on
  xstream$_parameters(server_name, server_type, position)
/

Rem
Rem Table for xstream dml_conflict_handler
Rem
create table xstream$_dml_conflict_handler
(
  object_name            varchar2(30),                        /* object name */
  schema_name            varchar2(30),                        /* schema name */
  apply_name             varchar2(30),                         /* apply name */
  conflict_type          number,                 /* conflict type definition */
                                                          /* 1  user error   */
                                                          /* 2  row exist    */
                                                          /* 3  no row       */
                                                          /* 4  fk_violation */
                                        /* negative # reserved for sql error */
  user_error             number,                /* user defined error number */
  opnum                  number,             /* 1 insert, 2 update, 3 delete */
  method_txt             clob,                      /* error handling action */
  method_name            varchar2(4000),       /* error handling action name */
  old_object             varchar2(30),               /* original object name */
  old_schema             varchar2(30),               /* original schema name */
  spare1                 number,  
  spare2                 number, 
  spare3                 number, 
  spare4                 timestamp,  
  spare5                 varchar2(4000),
  spare6                 varchar2(4000),
  spare7                 raw(64),
  spare8                 date,
  spare9                 clob
)
/
create unique index i_xstream_dml_conflict_handler on
  xstream$_dml_conflict_handler(apply_name, schema_name, object_name, 
                                opnum, old_schema, old_object, 
                                conflict_type, user_error)
/

alter table streams$_apply_process add
( spare4                  number,
  spare5                  number,
  spare6                  varchar2(4000),
  spare7                  varchar2(4000),
  spare8                  date,
  spare9                  date
);

alter table streams$_privileged_user add
( flags number,
  spare1 number,
  spare2 number,
  spare3 varchar2(1000),
  spare4 varchar2(1000)
);

create table xstream$_server_connection
(
 outbound_server               varchar2(30) not null,
 inbound_server                varchar2(30) not null,
 inbound_server_dblink         varchar2(128),
 outbound_queue_owner          varchar2(30),
 outbound_queue_name           varchar2(30),
 inbound_queue_owner           varchar2(30),
 inbound_queue_name            varchar2(30),
 rule_set_owner                varchar2(30),
 rule_set_name                 varchar2(30),
 negative_rule_set_owner       varchar2(30),
 negative_rule_set_name        varchar2(30),
 flags                         number,
 status                        number,
 create_date                   date,
 error_message                 varchar2(4000),
 error_date                    date,
 acked_scn                     number,
 auto_merge_threshold          number,
 spare1                        number,
 spare2                        number,
 spare3                        varchar2(4000),
 spare4                        varchar2(4000),
 spare5                        varchar2(4000),
 spare6                        varchar2(4000),
 spare7                        date,
 spare8                        date,
 spare9                        raw(2000),
 spare10                       raw(2000)
)
/
create index i_xstream_server_connection1 on xstream$_server_connection
    (outbound_server, inbound_server, inbound_server_dblink)
/

create table goldengate$_privileges
( username        varchar2(30) not null,
  privilege_type  number not null,              /* 1: capture; 2: apply; 3:* */
  privilege_level number not null,           /* 0: NONE; 1: select privilege */
  create_time     timestamp,
  spare1          number,
  spare2          number,
  spare3          timestamp,
  spare4          varchar2(4000),
  spare5          varchar2(4000))
/
create unique index goldengate$_privileges_i on 
  goldengate$_privileges(username, privilege_type, privilege_level)
/

drop public synonym all_streams_stmt_handlers;
drop view all_streams_stmt_handlers;

drop public synonym all_streams_stmts;
drop view all_streams_stmts;

Rem Modify Streams parameter names 
update sys.streams$_process_params 
  set name = 'COMPARE_KEY_ONLY' where name = '_CMPKEY_ONLY';

update sys.streams$_process_params 
  set name = 'IGNORE_TRANSACTION' where name = '_IGNORE_TRANSACTION';

update sys.streams$_process_params 
  set name = 'IGNORE_UNSUPPORTED_TABLE' where name = '_IGNORE_UNSUPERR_TABLE';

Rem Grant SELECT ANY TRANSACTION to all Streams admin users
DECLARE
  user_names       dbms_sql.varchar2s;
  i                PLS_INTEGER;
BEGIN
  -- grant select any transaction to username from dba_streams_administrator. 
  SELECT u.name
  BULK COLLECT INTO user_names FROM user$ u, sys.streams$_privileged_user pu
  WHERE u.user# = pu.user# AND pu.privs != 0 and
       (pu.flags IS NULL or pu.flags = 0 or (bitand(pu.flags, 1) = 1));
  FOR i IN 1 .. user_names.count 
  LOOP
    -- Don't uppercase username during enquote_name
    IF (user_names(i) <> 'SYS' AND user_names(i) <> 'SYSTEM') THEN
      EXECUTE IMMEDIATE 'GRANT SELECT ANY TRANSACTION TO ' || 
               dbms_assert.enquote_name(user_names(i), FALSE);
    END IF;
  END LOOP;
END;
/

rem =======================================================================
Rem  End Changes for XStream
Rem =======================================================================

Rem =======================================================================
Rem  Begin Changes for LogMiner
Rem =======================================================================

Rem Set system.logmnr_session$.spare1 to zero
update system.logmnr_session$
set spare1 = 0;
commit;

CREATE TABLE SYSTEM.logmnr_integrated_spill$ (
                session#		number,
                xidusn		        number,
                xidslt  		number,
                xidsqn  		number,
                chunk   		number,
                flag                    number,
                ctime                   date,
                mtime                   date,
                spill_data		blob,
                spare1  		number,
                spare2  		number,
                spare3                  number, 
                spare4                  date,
                spare5                  date,
        CONSTRAINT LOGMNR_INTEG_SPILL$_PK PRIMARY KEY 
            (session#, xidusn, xidslt, xidsqn, chunk, flag)
	    USING INDEX TABLESPACE SYSAUX LOGGING)
        LOB (spill_data) STORE AS (TABLESPACE SYSAUX CACHE PCTVERSION 0
	    CHUNK 32k STORAGE (INITIAL 4M NEXT 2M))
        TABLESPACE SYSAUX LOGGING
/
Rem =======================================================================
Rem  End Changes for LogMiner
Rem =======================================================================


Rem 
Rem Add edition column for Service
Rem 
alter table SERVICE$ add (edition varchar2(30));

Rem =================
Rem Begin AQ changes
Rem =================

ALTER TABLE sys.aq$_schedules ADD (destq NUMBER);

alter table sys.reg$ add (session_key VARCHAR2(1024));

alter sequence invalidation_reg_id$
  cache 300
/

Rem =================
Rem End AQ changes
Rem =================

REM create a table to store the sql errors that occur during parsing so that
REM the next time the same bad sql is issued we can look up from this table
REM and throw the same error instead of doing a hard parse
create table sqlerror$
(
  sqlhash   varchar(32)  not null,
  error#    number       not null,
  errpos#   number       not null,
  flags     number       not null);

Rem =======================================================================
Rem  Bug #5842629 : direct path load and direct path export
Rem =======================================================================
insert into STMT_AUDIT_OPTION_MAP values (330, 'DIRECT_PATH LOAD', 0);
insert into STMT_AUDIT_OPTION_MAP values (331, 'DIRECT_PATH UNLOAD', 0);
Rem =======================================================================
Rem  End Changes for Bug #5842629
Rem =======================================================================  



Rem =======================================================================
Rem  Begin Changes for Database Replay 
Rem =======================================================================
Rem
Rem add columns to WRR$_CAPTURES and WRR$_REPLAYS
Rem
Rem wrr$_captures
alter table WRR$_CAPTURES add (sqlset_owner varchar2(30));
alter table WRR$_CAPTURES add (sqlset_name varchar2(30));
Rem wrr$_replays
alter table WRR$_REPLAYS add (sqlset_owner varchar2(30));
alter table WRR$_REPLAYS add (sqlset_name varchar2(30));
alter table WRR$_REPLAYS add (sqlset_cap_interval number);
alter table WRR$_REPLAYS add (filter_set_name varchar2(1000));
alter table WRR$_REPLAY_FILTER_SET add (default_action varchar2(20));

Rem =======================================================================
Rem  End Changes for Database Replay 
Rem =======================================================================

Rem ==========================
Rem Begin Bug #8862486 changes
Rem ==========================

Rem Directory EXECUTE auditing (action #135)
insert into audit_actions values (135, 'DIRECTORY EXECUTE');

Rem ========================
Rem End Bug #8862486 changes
Rem ========================

Rem ===========================================================================
Rem add new columns to WRH$_PERSISTENT_QUEUES and WRH$_PERSISTENT_SUBSCRIBERS
Rem ===========================================================================

alter table WRH$_PERSISTENT_QUEUES add (browsed_msgs          number);
alter table WRH$_PERSISTENT_QUEUES add (enqueue_cpu_time      number);
alter table WRH$_PERSISTENT_QUEUES add (dequeue_cpu_time      number);
alter table WRH$_PERSISTENT_QUEUES add (avg_msg_age           number);
alter table WRH$_PERSISTENT_QUEUES add (dequeued_msg_latency  number);
alter table WRH$_PERSISTENT_QUEUES add (enqueue_transactions  number);
alter table WRH$_PERSISTENT_QUEUES add (dequeue_transactions  number);
alter table WRH$_PERSISTENT_QUEUES add (execution_count       number);

alter table WRH$_PERSISTENT_SUBSCRIBERS add (avg_msg_age           number);
alter table WRH$_PERSISTENT_SUBSCRIBERS add (browsed_msgs          number);
alter table WRH$_PERSISTENT_SUBSCRIBERS add (elapsed_dequeue_time  number);
alter table WRH$_PERSISTENT_SUBSCRIBERS add (dequeue_cpu_time      number);
alter table WRH$_PERSISTENT_SUBSCRIBERS add (dequeue_transactions  number);
alter table WRH$_PERSISTENT_SUBSCRIBERS add (execution_count       number);

Rem ===========================================================================
Rem End changes to WRH$_PERSISTENT_QUEUES and WRH$_PERSISTENT_SUBSCRIBERS 
Rem ===========================================================================

Rem ===========================================================================
Rem add new columns to WRH$_BUFFERED_QUEUES and WRH$_BUFFERED_SUBSCRIBERS
Rem ===========================================================================
alter table WRH$_BUFFERED_QUEUES add (expired_msgs                    number);
alter table WRH$_BUFFERED_QUEUES add (oldest_msgid                   raw(16));
alter table WRH$_BUFFERED_QUEUES add (oldest_msg_enqtm          timestamp(3));
alter table WRH$_BUFFERED_QUEUES add (queue_state               varchar2(25));
alter table WRH$_BUFFERED_QUEUES add (elapsed_enqueue_time            number);
alter table WRH$_BUFFERED_QUEUES add (elapsed_dequeue_time            number);
alter table WRH$_BUFFERED_QUEUES add (elapsed_transformation_time     number);
alter table WRH$_BUFFERED_QUEUES add (elapsed_rule_evaluation_time    number);
alter table WRH$_BUFFERED_QUEUES add (enqueue_cpu_time                number);
alter table WRH$_BUFFERED_QUEUES add (dequeue_cpu_time                number);
alter table WRH$_BUFFERED_QUEUES add (last_enqueue_time         timestamp(3));
alter table WRH$_BUFFERED_QUEUES add (last_dequeue_time         timestamp(3));

alter table WRH$_BUFFERED_SUBSCRIBERS add (last_browsed_seq           number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (last_browsed_num           number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (last_dequeued_seq          number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (last_dequeued_num          number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (current_enq_seq            number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (total_dequeued_msg         number); 
alter table WRH$_BUFFERED_SUBSCRIBERS add (expired_msgs               number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (message_lag                number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (elapsed_dequeue_time       number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (dequeue_cpu_time           number);
alter table WRH$_BUFFERED_SUBSCRIBERS add (last_dequeue_time    timestamp(3));
alter table WRH$_BUFFERED_SUBSCRIBERS add (oldest_msgid              raw(16));
alter table WRH$_BUFFERED_SUBSCRIBERS add (oldest_msg_enqtm     timestamp(3));
Rem ===========================================================================
Rem End changes to WRH$_BUFFERED_QUEUES and WRH$_BUFFERED_SUBSCRIBERS 
Rem ===========================================================================

Rem ==========================================================================
Rem Begin advisor framework / SPA changes
Rem ==========================================================================
Rem in 11.2, we added a new parameter EXECUTE_COUNT to control the
Rem execution count in the sql analyze when doing SPA.

BEGIN
  -- add new parameters to existing tasks. Note that the definition 
  -- of these two parameters will be added later during upgrade 
  -- when dbms_advisor.setup_repository is called. 
  EXECUTE IMMEDIATE 
    q'#INSERT INTO wri$_adv_parameters (task_id, name, value, datatype, flags)
       (SELECT t.id, 'EXECUTE_COUNT', 'UNUSED', 1,  8 
        FROM wri$_adv_tasks t
        WHERE t.advisor_name = 'SQL Performance Analyzer' AND 
              NOT EXISTS (SELECT 0 
                          FROM wri$_adv_parameters p
                          WHERE p.task_id = t.id and 
                                p.name = 'EXECUTE_COUNT'))#';    

  -- handle exception when upgrading from 9i. The advisor tables do not exist
  EXCEPTION 
    WHEN OTHERS THEN
      IF SQLCODE = -942 
        THEN NULL;
      ELSE
        RAISE;
      END IF;
END;
/

Rem ==========================================================================
Rem End advisor framework / SPA changes 
Rem ==========================================================================

Rem *************************************************************************
Rem Resource Manager related changes - BEGIN
Rem *************************************************************************

alter table resource_plan_directive$ add (parallel_queue_timeout number);

update resource_plan_directive$ set
  max_active_sess_target_p1 = 4294967295;
update resource_plan_directive$ set
  parallel_queue_timeout = 4294967295;
commit;

Rem Update WRH$_RSRC_CONSUMER_GROUP (basis for dba_hist_rsrc_consumer_group)
alter table WRH$_RSRC_CONSUMER_GROUP add (pqs_queued                number);
alter table WRH$_RSRC_CONSUMER_GROUP add (pq_queued_time            number);
alter table WRH$_RSRC_CONSUMER_GROUP add (pq_queue_time_outs        number);
alter table WRH$_RSRC_CONSUMER_GROUP add (pqs_completed             number);
alter table WRH$_RSRC_CONSUMER_GROUP add (pq_servers_used           number);
alter table WRH$_RSRC_CONSUMER_GROUP add (pq_active_time            number);

Rem Update WRH$_RSRC_PLAN (basis for dba_hist_rsrc_plan)
alter table WRH$_RSRC_PLAN add (parallel_execution_managed    varchar2(4));

Rem This is needed for bug #9207475 to allow AWR snapshots to include
Rem the currently active plan
alter table WRH$_RSRC_PLAN modify (end_time date null);

Rem *************************************************************************
Rem Resource Manager related changes - END
Rem *************************************************************************

Rem *************************************************************************
Rem Change the lengths of module and action
Rem *************************************************************************

alter table SQLOBJ$AUXDATA modify (module varchar2(64));
alter table SQLOBJ$AUXDATA modify (action varchar2(64));

alter table WRH$_ACTIVE_SESSION_HISTORY_BL modify (module varchar2(64));
alter table WRH$_ACTIVE_SESSION_HISTORY_BL modify (action varchar2(64));

alter table WRH$_ACTIVE_SESSION_HISTORY modify (module varchar2(64));
alter table WRH$_ACTIVE_SESSION_HISTORY modify (action varchar2(64));

alter table WRI$_ADV_SQLT_STATISTICS modify (module varchar2(64));
alter table WRI$_ADV_SQLT_STATISTICS modify (action varchar2(64));

alter table WRI$_SQLSET_STATEMENTS modify (module varchar2(64));
alter table WRI$_SQLSET_STATEMENTS modify (action varchar2(64));

alter table WRR$_REPLAY_DIVERGENCE modify (module varchar2(64));
alter table WRR$_REPLAY_DIVERGENCE modify (action varchar2(64));

alter table WRH$_SQLSTAT modify (module varchar2(64));
alter table WRH$_SQLSTAT modify (action varchar2(64));

alter table WRH$_SQLSTAT_BL modify (module varchar2(64));
alter table WRH$_SQLSTAT_BL modify (action varchar2(64));

alter table STREAMS$_COMPONENT_EVENT_IN modify (module_name varchar2(64));
alter table STREAMS$_COMPONENT_EVENT_IN modify (action_name varchar2(64));

alter table STREAMS$_PATH_BOTTLENECK_OUT modify (module_name varchar2(64));
alter table STREAMS$_PATH_BOTTLENECK_OUT modify (action_name varchar2(64));

alter type SQLSET_ROW modify attribute module varchar2(64) cascade;
alter type SQLSET_ROW modify attribute action varchar2(64) cascade;
-- type alert_type is used for AQ messages
-- Turn ON the event to enable DDL on AQ tables
alter session set events '10851 trace name context forever, level 1';
alter type sys.alert_type
      modify attribute module_id varchar2(64) cascade;

-- Turn OFF the event to disable DDL on AQ tables
alter session set events '10851 trace name context off';

Rem *************************************************************************
Rem END Change the lengths of module and action
Rem *************************************************************************


Rem *************************************************************************
Rem WRH$_ACTIVE_SESSION_HISTORY changes
Rem   - Add columns to ASH
Rem *************************************************************************
alter table WRH$_ACTIVE_SESSION_HISTORY add (dbreplay_file_id NUMBER);
alter table WRH$_ACTIVE_SESSION_HISTORY add (dbreplay_call_counter NUMBER);
alter table WRH$_ACTIVE_SESSION_HISTORY add (px_flags NUMBER);

alter table WRH$_ACTIVE_SESSION_HISTORY_BL add (dbreplay_file_id NUMBER);
alter table WRH$_ACTIVE_SESSION_HISTORY_BL add (dbreplay_call_counter NUMBER);
alter table WRH$_ACTIVE_SESSION_HISTORY_BL add (px_flags NUMBER);

Rem *************************************************************************
Rem END WRH$_ACTIVE_SESSION_HISTORY
Rem *************************************************************************

Rem *************************************************************************
Rem Bug 9766219 - BEGIN
Rem *************************************************************************

BEGIN
  EXECUTE IMMEDIATE 'REVOKE EXECUTE ON SYS.URIFACTORY FROM PUBLIC';
  EXECUTE IMMEDIATE 'GRANT EXECUTE ON SYS.URIFACTORY TO PUBLIC';
EXCEPTION 
  WHEN OTHERS THEN
    IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
    ELSE RAISE;
    END IF;
END;
/

Rem *************************************************************************
Rem Bug 9766219 - END
Rem *************************************************************************

Rem *************************************************************************
Rem OLAP changes - BEGIN
Rem *************************************************************************

-- More limited grants will occur in olaptf.sql
BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_TABLE FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON CUBE_TABLE FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAPRC_TABLE FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_SRF_T FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_NUMBER_SRF FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_EXPRESSION FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_TEXT_SRF FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_EXPRESSION_TEXT FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_DATE_SRF FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_EXPRESSION_DATE FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_BOOL_SRF FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/

BEGIN
 EXECUTE IMMEDIATE 'REVOKE ALL ON OLAP_EXPRESSION_BOOL FROM PUBLIC';
EXCEPTION
 WHEN OTHERS THEN
   IF SQLCODE IN ( -04042, -1927, -942, -4045 ) THEN NULL;
   ELSE RAISE;
   END IF;
END;
/ 

Rem *************************************************************************
Rem OLAP changes - END
Rem *************************************************************************

Rem *************************************************************************
Rem WRM$_SNAPSHOT changes - BEGIN
Rem *************************************************************************

Rem add snap_timezone column to wrm$_snapshot
alter table WRM$_SNAPSHOT add (snap_timezone interval day(0) to second(0));

Rem *************************************************************************
Rem WRM$_SNAPSHOT changes - END
Rem *************************************************************************

Rem *************************************************************************
Rem BEGIN: Insert users in default_pwd$
Rem *************************************************************************

Rem Created Procedure for inserting into SYS.DEFAULT_PWD$

CREATE OR REPLACE PROCEDURE insert_into_defpwd
           (tuser_name                IN  VARCHAR2,
            tpwd_verifier             IN  VARCHAR2,
            tpv_type                  IN PLS_INTEGER DEFAULT 0
           )
AUTHID CURRENT_USER
IS
    m_sql_stmt       VARCHAR2(4000);
BEGIN
    m_sql_stmt    := 'insert into SYS.DEFAULT_PWD$ values(:1, :2, :3)';
    EXECUTE IMMEDIATE m_sql_stmt USING tuser_name, tpwd_verifier, tpv_type;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE IN ( -00001) THEN NULL; --ignore unique constraint violation
      DBMS_OUTPUT.PUT_LINE('User: '||tuser_name||' already exists');
  ELSE RAISE;
  END IF;
END;
/

Rem Insert values into SYS.DEFAULT_PWD$

exec insert_into_defpwd('ADLDEMO',  '147215F51929A6E8');
exec insert_into_defpwd('APR_USER',  '0E0840494721500A');
exec insert_into_defpwd('ARGUSUSER',  'AB1079A1727006AD');
exec insert_into_defpwd('AUDIOUSER',  'CB4F2CEC5A352488');
exec insert_into_defpwd('CATALOG',  '397129246919E8DA');
exec insert_into_defpwd('CDEMO82',  '7299A5E2A5A05820');
exec insert_into_defpwd('CDEMOCOR',  '3A34F0B26B951F3F');
exec insert_into_defpwd('CDEMORID',  'E39CEFE64B73B308');
exec insert_into_defpwd('CDEMOUCB',  'CEAE780F25D556F8');
exec insert_into_defpwd('CFLUENTDEV',  'D930962979E34C47');
exec insert_into_defpwd('COMPANY',  '402B659C15EAF6CB');
exec insert_into_defpwd('DEMO',  '4646116A123897CF');
exec insert_into_defpwd('EMP',  'B40C23C6E2B4EA3D');
exec insert_into_defpwd('EVENT',  '7CA0A42DA768F96D');
exec insert_into_defpwd('FINANCE',  '6CBBF17292A1B9AA');
exec insert_into_defpwd('FND',  '0C0832F8B6897321');
exec insert_into_defpwd('GPFD',  'BA787E988F8BC424');
exec insert_into_defpwd('GPLD',  '9D561E4D6585824B');
exec insert_into_defpwd('HLW',  '855296220C095810');
exec insert_into_defpwd('IMAGEUSER',  'E079BF5E433F0B89');
exec insert_into_defpwd('IMEDIA',  '8FB1DC9A6F8CE827');
exec insert_into_defpwd('JMUSER',  '063BA85BF749DF8E');
exec insert_into_defpwd('MGMT_VIEW',  '919E8A172B2AAB87');
exec insert_into_defpwd('MIGRATE',  '5A88CE52084E9700');
exec insert_into_defpwd('MILLER',  'D0EFCD03C95DF106');
exec insert_into_defpwd('MMO2',  'AE128772645F6709');
exec insert_into_defpwd('MODTEST',  'BBFF58334CDEF86D');
exec insert_into_defpwd('MOREAU',  'CF5A081E7585936B');
exec insert_into_defpwd('MTSSYS',  '6465913FF5FF1831');
exec insert_into_defpwd('MXAGENT',  'C5F0512A64EB0E7F');
exec insert_into_defpwd('NAMES',  '9B95D28A979CC5C4');
exec insert_into_defpwd('OCITEST',  'C09011CB0205B347');
exec insert_into_defpwd('OEMADM',  '9DCE98CCF541AAE6');
exec insert_into_defpwd('OLAPDBA',  '1AF71599EDACFB00');
exec insert_into_defpwd('OLAPSVR',  'AF52CFD036E8F425');
exec insert_into_defpwd('PERFSTAT',  'AC98877DE1297365');
exec insert_into_defpwd('PO8',  '7E15FBACA7CDEBEC');
exec insert_into_defpwd('PORTAL30_ADMIN',  '7AF870D89CABF1C7');
exec insert_into_defpwd('PORTAL30_PS',  '333B8121593F96FB');
exec insert_into_defpwd('PORTAL30_SSO_ADMIN',  'BDE248D4CCCD015D');
exec insert_into_defpwd('POWERCARTUSER',  '2C5ECE3BEC35CE69');
exec insert_into_defpwd('PRIMARY',  '70C3248DFFB90152');
exec insert_into_defpwd('PUBSUB',  '80294AE45A46E77B');
exec insert_into_defpwd('RE',  '933B9A9475E882A6');
exec insert_into_defpwd('RMAIL',  'DA4435BBF8CAE54C');
exec insert_into_defpwd('SAMPLE',  'E74B15A3F7A19CA8');
exec insert_into_defpwd('SDOS_ICSAP',  'C789210ACC24DA16');
exec insert_into_defpwd('TAHITI',  'F339612C73D27861');
exec insert_into_defpwd('TSDEV',  '29268859446F5A8C');
exec insert_into_defpwd('TSUSER',  '90C4F894E2972F08');
exec insert_into_defpwd('USER0',  '8A0760E2710AB0B4');
exec insert_into_defpwd('USER1',  'BBE7786A584F9103');
exec insert_into_defpwd('USER2',  '1718E5DBB8F89784');
exec insert_into_defpwd('USER3',  '94152F9F5B35B103');
exec insert_into_defpwd('USER4',  '2907B1BFA9DA5091');
exec insert_into_defpwd('USER5',  '6E97FCEA92BAA4CB');
exec insert_into_defpwd('USER6',  'F73E1A76B1E57F3D');
exec insert_into_defpwd('USER7',  '3E9C94488C1A3908');
exec insert_into_defpwd('USER8',  'D148049C2780B869');
exec insert_into_defpwd('USER9',  '0487AFEE55ECEE66');
exec insert_into_defpwd('UTLBSTATU',  'C42D1FA3231AB025');
exec insert_into_defpwd('VIDEOUSER',  '29ECA1F239B0F7DF');
exec insert_into_defpwd('VIF_DEVELOPER',  '9A7DCB0C1D84C488');
exec insert_into_defpwd('VIRUSER',  '404B03707BF5CEA3');
exec insert_into_defpwd('VRR1',  '811C49394C921D66');
exec insert_into_defpwd('WKPROXY',  'B97545C4DD2ABE54');

commit;

drop procedure insert_into_defpwd;

Rem *************************************************************************
Rem END: Insert users in default_pwd$
Rem *************************************************************************
Rem *************************************************************************
Rem Optimizer changes - BEGIN
Rem *************************************************************************
-- lrg 4545922: Turn ON the event to disable the partition check
alter session set events  '14524 trace name context forever, level 1';

declare
  type numtab is table of number;
  tobjns numtab;
  tobjn  number;
  property number := 0;
  sqltxt varchar2(32767);
  tmp_created boolean := FALSE;
begin

-- check whether we are already in new schema
-- if we have upgraded it, this will throw 904 error that will be caught
  begin
    execute immediate 
    q'#select synopsis# 
      from wri$_optstat_synopsis$
      where rownum < 2 #';
  exception
    when others then
      if (sqlcode = -904) then 
        -- ORA-904 during reupgrde: "S"."SYNOPSIS#": invalid identifier 
        -- has been upgraded successfully before, do nothing
        return;
      elsif (sqlcode = -942) then
        -- ORA-00942: table or view does not exist
        -- wri$_optstat_synopsis$ does not exist. this may be caused by
        -- errors in last upgrade: wri$_optstat_synopsis$ is dropped but
        -- tmp_wri$_optstat_synopsis$ is failed to replace it.
        -- recreate wri$_optstat_synopsis$ (we might lose old data)
        execute immediate
        q'#create table wri$_optstat_synopsis$
          ( bo#           number not null,
            group#        number not null,
            intcol#       number not null,           
            hashvalue     number not null 
          ) partition by range(bo#) 
          subpartition by hash(group#) 
          subpartitions 32
          (
            partition p0 values less than (0)
          ) 
          tablespace sysaux
          pctfree 1
          enable row movement #';
        return;
      else
        raise;
      end if;
  end;

  -- there exists synopsis table in old schema
  execute immediate 
  q'#create table tmp_wri$_optstat_synopsis$
    ( bo#           number not null,
      group#        number not null,
      intcol#       number not null,           
      hashvalue     number not null 
    ) 
    partition by range(bo#) 
      subpartition by hash(group#) 
      subpartitions 32
    (
      partition p0 values less than (0)
    ) 
    tablespace sysaux
    pctfree 1
    enable row movement #';

  tmp_created := TRUE;

  -- get all the partitioned tables that have synopses
  -- must order by bo# because we are going to create partitions
  -- using "add partition" statement
  select distinct bo# bulk collect into tobjns
  from sys.wri$_optstat_synopsis_head$
  order by bo#;

  -- create range partition for each partitioned table
  for i in 1..tobjns.count loop
    tobjn := tobjns(i);

    -- check whethre low boundary has been created
    if (i = 1 or tobjns(i-1) <> tobjn - 1) then
      -- we haven't created a partition with highvalue tobjn yet

      -- check whether objn-1 is a partitioned table
      begin
        select bitand(t.property, 32) into property
        from sys.obj$ o,
             sys.tab$ t
        where o.obj# = tobjn-1 and
              o.type# = 2 and
              o.obj# = t.obj#; 
      exception
        when no_data_found then
          property := 0;
      end;

      if (property = 32) then
        -- tobjn-1 is a partitioned table
        sqltxt := 'alter table tmp_wri$_optstat_synopsis$' ||
                  ' add partition p_' || to_char(tobjn - 1) ||
                  ' values less than (' || to_char(tobjn) || ')';
      else
        sqltxt := 'alter table tmp_wri$_optstat_synopsis$' ||
                  ' add partition p_' || to_char(tobjn - 1) || 
                  ' values less than (' || to_char(tobjn) || ')' ||
                  ' subpartitions 1';
      end if;
      execute immediate sqltxt; 
    end if;
  
    -- high boundary
    sqltxt := 'alter table tmp_wri$_optstat_synopsis$' ||
              ' add partition p_' || tobjn || 
              ' values less than (' || to_char(tobjn + 1) || ')';
    execute immediate sqltxt; 
  end loop;

  execute immediate 
  q'#insert /*+ append */ 
     into tmp_wri$_optstat_synopsis$
     select /*+ full(h) full(s) leading(h s) use_hash(h s) */
       h.bo#,
       h.group#,
       h.intcol#,
       s.hashvalue
     from wri$_optstat_synopsis_head$ h,
          wri$_optstat_synopsis$ s
     where h.synopsis# = s.synopsis# #';

  execute immediate 
  q'# drop table wri$_optstat_synopsis$ #';

  execute immediate 
  q'# rename tmp_wri$_optstat_synopsis$ to wri$_optstat_synopsis$ #';

exception
  when others then
    if (tmp_created) then
      execute immediate 'drop table tmp_wri$_optstat_synopsis$';
    end if;
    raise;
end;
/
 
-- Turn OFF the event to disable the partition check 
alter session set events  '14524 trace name context off';

-- #(9577300) Column group usage
create table col_group_usage$
(
  obj#              number,                                 /* object number */
  /*
   * We store intcol# separated by comma in the following column.
   * We allow upto 32 (CKYMAX) columns in the group. intcol# can be 
   * upto 1000 (or can be 64K in future or with some xml virtual columns?). 
   * Assume 5 digits for intcol# and one byte for comma. 
   * So max length would be 32 * (5+1) = 192
   */
  cols              varchar2(192 char),              /* columns in the group */
  timestamp         date,     /* timestamp of last time this row was changed */
  flags             number,                                 /* various flags */
  constraint        pk_col_group_usage$
  primary key       (obj#, cols))
  organization index
  storage (initial 200K next 100k maxextents unlimited pctincrease 0)
/

Rem *************************************************************************
Rem Optimizer changes - END
Rem *************************************************************************


Rem=========================================================================
Rem Add changes to other SYS dictionary objects here 
Rem     (created in catproc.sql scripts)
Rem=========================================================================

Rem upgrade registry$ table
alter table registry$ add (signature raw(20));
alter table registry$ add (edition varchar2(30));


Rem *************************************************************************
Rem END   c1102000.sql
Rem *************************************************************************
