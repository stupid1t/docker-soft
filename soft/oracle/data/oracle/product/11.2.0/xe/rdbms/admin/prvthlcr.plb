CREATE OR REPLACE TYPE lcr$_procedure_parameter wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
73 9e
g6rFngZ2WZSvxYphHF2wJmlSbdEwg5n0dLhchaHRCfSWFpeu3C4+YvLwllpW8kcuYqFiCbh0
K6W/m8Ayy8xQjwlppcfSMlxnqXzGyhcoxsrvsrYdHR0upEREDtMfxi+AFGyv968jSj/358zR
LvY5plcc/fQ=

/
CREATE OR REPLACE TYPE lcr$_parameter_list AS TABLE 
  OF lcr$_procedure_parameter;
/
GRANT EXECUTE ON lcr$_parameter_list TO PUBLIC;
CREATE OR REPLACE LIBRARY lcr_prc_lib wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
16
26 59
mfrs049O4TCxSMWVNPSHjdEZtHUwg04I9Z7AdBjDhaHRGJYW8kpy+lkJ572esstSMsy4dCvn
y1J0CPXJpqalqJl1

/
CREATE OR REPLACE TYPE LCR$_PROCEDURE_RECORD 
OID '00000000000000000000000000020015' 
AS OPAQUE VARYING (*)
USING LIBRARY lcr_prc_lib
( 
  MAP MEMBER FUNCTION map_lcr RETURN NUMBER,
  MEMBER FUNCTION  get_source_database_name RETURN VARCHAR2,
  MEMBER FUNCTION  get_scn                  RETURN NUMBER,
  MEMBER FUNCTION  get_transaction_id       RETURN VARCHAR2,
  MEMBER FUNCTION  get_publication          RETURN VARCHAR2,
  MEMBER FUNCTION  get_package_owner        RETURN VARCHAR2,
  MEMBER FUNCTION  get_package_name         RETURN VARCHAR2,
  MEMBER FUNCTION  get_procedure_name       RETURN VARCHAR2,
  MEMBER FUNCTION  get_parameters           RETURN SYS.LCR$_PARAMETER_LIST
)
/
GRANT EXECUTE ON LCR$_PROCEDURE_RECORD TO PUBLIC;
CREATE OR REPLACE PACKAGE dbms_streams_lcr_int wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
ef f3
YekQ2UIdjZu7RUEoYaIVhv0ajBAwgxDw154VZ3RGAP4+wNgfDw6wU/0NtOhsE5QSo+Ta5/xr
zfgLxWYj8+GCt3wPo/W6cbZu0P6osguqrXUZ3BBFImDdTRMl1m1m2+zgyvt0kzmlWFyRsAFq
j6sZ9xGDsVwlG1zcwAMclOYfb8kN9t7L5qsCD9pd//9U5MGIEQaIysSsY7u5KLtzajDbHOTC
yz5NL+zcyPENdQD7K3I14A==

/
CREATE OR REPLACE PUBLIC SYNONYM dbms_streams_lcr_int FOR 
sys.dbms_streams_lcr_int
/
GRANT EXECUTE ON sys.dbms_streams_lcr_int TO execute_catalog_role
/
