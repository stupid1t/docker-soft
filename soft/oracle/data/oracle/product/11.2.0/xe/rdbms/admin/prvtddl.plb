create or replace package body dbms_ddl_internal wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
134d 7e3
s96YPbGAJw5MA0v7uapH89aUAn4wg2NUeSAF3y8ZF7WeZrLKvV7wGohhYd45adN5A/da8FFu
MA/jYb1LDkI+JfX4JSydhm2R/GJSo+9SEQgwNBK5ub7H8m5BBmw1vXLffQaG1+oyUlI74kgs
0bsgJHHvo9cvwiRjjz76JzVpRWYpJvGs0ZyiTUSUiWKUfpyihS5NT9U/1ONnM+lYOgPUZkw4
CMVBa5APYB9qRDUkZNWVXwNKOOcIo/lf0ZrCLDxcYVB5EHdFVEwUonqAXtflRVa+yBSCs97Y
8ldyvcn3LNfuruLCZiByQpb2hS3zJt+NZvaTLhgUXtQqUEaAW/dvWb+UR0GOes+Bg1qnXvG9
AVR+jgHxVpBYkaExR3ATbOljJ6HHxXjHpMxsOj440iAGY8CKsK5f0852jFZgY77O3mySzIqw
Lq4HzODwwhvqFEkmosAZFIB0HkVHSxT4fepUF8pQ2D4KZ5EPbSH77HnVYMCWxuCW/y0faZbX
b6wf50Fy/sOjmTV1wxSM8Sfrt/4S48bJxEDa/sx5JFdltIDCt2QX1eHPr7Jmfdv+nZw8dQFN
ufTeTYDbzmiKNKavgwQI4MNaSPJRAVmRwlZ4sx90wTlmuKLe9ECV1LuhGfayJVZ+bvpC8vKy
ldgyyKjUlNtECkRk7m5GgDtFnzhHfHCZr5VxcKPzc5PxFSKeD7x7e3swVQwhKH+c3kYB0UxH
/UxGa+q+qlEdC8/mbOS9c1Me2f3/SEHlKmwXs/qvjvp6gD2+DNNpHw0/x/saC4+/m+7fQ1GF
WkIwsueKzb2jjzRm/zJUAGY+6S6jX6YEHbeHIoNkZ5fMyuTRJ6cnLJJgxKBbaH6+0eashaNL
QHFixPHHEwvUnrfXEk8kJHE1DBC0gnLA7OcOZhPL8OmlUH5BzL7mPgd6AMQBw+D7e2cBlEXG
bxILnq4m9vPwPjLAEajXf7o8RQqwxoJPBrr+K1MJV1uVngPwH/JFQFce7PxJ4c5zaBkrq+3v
19f75yfUykYJJm3gglVwo/H1jkIRPv9u7IEEVgxLoXjNkwKXk5yclzoJcKlcx0oyEjwuc8Di
KCV2RUDfenR0yUsNNcsDymD7cab+U42f3+MjIzbDx0wOv0l9pbSWVXl5Mg7873rcONbhR5EJ
QnhPesFuDwLrU7mpity0y7MANfv/npcRY/s8fuyPGKAWKP/LL3nyh2RMoyrPUEOKxogFpHmh
gAqQ0adIMvCciK2WlGKXYVMR3wm6mEcjK0Tb0NWEEXNF5LOsrKm1jYSMTxlbS14bsP47TJA4
MHyVedlvJSJ2zuEISa9hLCFzaut0Niw2N9Ggysvh8PxR/8c9Gp6KDS7g/DmrdHv/XUR7kbcf
IUwRYQsggO7ZjHhbID/QWzVfs7lOEKXpEL6dkv4KI1NeT/m+5NKt4znN4rKLuur5pVLLMv/S
FupS7i4Kg3DtmS45RwJZPemrg4Z5GzliiHWXnZ2YvMOL11TTDruauqHqziMfTFXhkxXo0JbP
nmbS7jPhovmYc58LGHjj2gSsrR4kIfJpG/mRO3hlwg5fTYnBMvu5Z8Aq1FFqtItX2FHcl5IW
yMfxPNN6AVijL1jiaAj5ALmsKv/2ZT/Xj6L/+zWZuOom/6IXGEFHUfPlCOineeCB3oXBxKf/
Vo1SaT56+UDVumpwGLMtIH6bIJqvzstj/G4zvbOwDYb/faAUfL4VsY4ACx0xreqXNJudqo3h
lplfWQujj4lGpVxNEnx7YIYDDJdt0wyVroQ4nJhHjtwl+ik0cGlojE42ieiNngp7JhrnLht1
fbVv6WE+ozJz3gvtmst+4gRoH+yP61XXnCgo6ixkltbff26hu2mG1O95fQj56IBoOYliJ4vE
HwSVhVhAH+TDjC0tMHl/UtNVUFMMzV00c8RpMkAMRFMMprIrIdfmNZd2Ubp9u8hl+mLzzgD2
xR9ARVFIA5BFMynV7rhy+qSy0GE/9oVb6z0IlrUd15mfNA==

/
create or replace package body dbms_ddl wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
47c0 12fa
KBYcAFAF5DoqtPmZooQ4Kk0mU2swg826KV4FWm+acuSUkJAo8SeUnHnnFIQtXMJoOUSTCRRY
2/jpAd36ZdlctMd5CMa7Hpi6nJbJQCak4S07uT9zuf4eavTBTxKwBLIG7o5MDxliGB4euo6R
EucID86ZUZmWdEDpQTmwsFdim96s8+bObdC3euOMC323GhkptIRDLDGw51HKxB4Wvn3NNf5N
u3YDJsL6EY7O+0qbZqHWbdNYLujFzKxCRe25P9AuoskcQjVUorctYNvQjFb+UXavhvvcs/5J
fXmqqp3+N1l21E+ZJBxJh0hZcdqLDvi8gEkYBjTdD6xWuzzyzAajgTmfJHYvOxEUI/33fczX
p97SeFGzFyEzLqv4/bO0W6sHu8soWatb/bNXDvwWLQhI79O7qaK7Dv4ZGlJT8/CEDrO7pM98
3nZ5oobjmwjiBYaiGhNk6aJTwptdYqruICH0z+b9EPI0d/0F0qr17mP5WJysQyQuBnYobcxu
Sqg126gzZiFGTVhGrZfrvZ/na47BtyHxjFS5ZwYgohyXYJjQhQrHX6M/hXw0VUsvFGcntPmr
hoVLFNvzoQCtQG53e/Qifqmu1lqFiF8v64fINavnhIlPOIMWlKIT0Ca4sy+SXgUtJqg43dne
P9RW3UcYISUxkchDPvYy+i/EwQ62L9ENmErfjQ5gD5pRGX8SWT3h0LJl5yUdKcGl3HyyzeqU
Zxhng0IgqB1C/cm0brCHw4ueGLIlBc2+AC5xjzX+rShgA8xJhZKYXD0mkecwFvA70kThcnK3
FK9+ycE6iEHDdcP/6uCJ9lWPjQgzQH8fsM+PE41HJ04I3xDoIpAeYgwFUOo2QvHUIhzcCXSh
+/i3dh8xsFAzhlekb4j/wmYigNbKmyXer0wx6aKUMrJoS1F0sV+JAz0qyTpXsZs2sEDrIdxt
gn1sBVPHCJFg1LDvcsaFM0xMGOjnO1oiM/9S9xLWLx8+HO6OgCIbT31W3/bjNH88dQsogrV0
HVubfwsx/fBLrnHod2jUn7aZ7HR5frdlpZ5UZEuvMZqCztzCOKfQ03AodmPjJICpSKxawch6
sHEe0P4n8L2RqM6bPimjCOupV6tLCXn1oCcRGGq+usxvdDc0L+dpdB9GtOhXK71eKyFl1y2x
eqEYSYgEnsFCL5DJwivmnohCyKSkh47MFfeCJW3D6nVfciAh8hIJh0f3B4A094jo9Nic4Uh+
3op4KstbOC+QxPcB1fs5Eq3V1tDMR+cYxhJ278HODOIAgP3kadMhKYkwzb9EJCr6xrkIKysm
/hRsWlXYLy8zoagNMGZ3A2GRiNhTcsHIjdwgXVwrcvwlp1BHLZQScFReKvHQNPSmvSJ1wwZX
ZMMLrVTSEj9gK8ZmlqAmQKS9XDBnfB7mAXVdNQRd68uIWzrKbWqZfl7F4i+zAblDZW0DH+Oy
i3Z5Na++jT8GZcF8ajJyprx9guMmloIOTZ6p2N7KgeHneJahFd3+FlfoPMSarNr1OO6RLrkX
nMTHoM9n6ii/0Hilp+UHI9Wzb+S3X5kNszkfVWiWP+SSgYv8yT+dotCueCSvfSBORCMw5rEo
pSO1S6JOD0hvDNn3GIujOesxR7VEQOEzQi/eYfU8b0SgeUzXufqJ5kqwOlVqJY6A8uCoQzZK
KURuAKcW3ZoD3ugmqeFip0nKc4NvMrLCGqQjKS/14Cm73oa4BKNZyzUC+1hFaQ5+rJgnqL9H
xxUwdl3GEWQGLyObyag19KaG9GEPOKLV18t8bSqYTP9PUIaK/CQDIMcu70el5tCcKLGdo8Zc
j+YWUGY321k0VFRlRHctt2y0KGBoRpilDTBuD1Wp+NeneCj+lsaPy1btprYyMQ3b25spiwV4
MFUYvdyqAhs25w5pTCJv9o/fiwReIzO0YF7TRsO02x4b1tkxKpLen/iGdhACz9K4OmrkJ/Sn
nSL/nYBLU6GgtGXmL3dp5hYqwiXqxcvNbaKaOSagrtfWq23IvKw3Rtc/En447iegAiGyRFHG
nkZoMZIDiZli+N72erLoYIdXcx/KBq+ApjrPAkvzVOkLbk+VamUjlZMcXwBpz6uq/9s/Y7Ql
qJWhVouRgIUxIisSE8kP36HzSZ71WAAzFDl8tIJ7ofV2gl8Kpc3a4gNH/WrxtKZrIUtWpC4m
OBA7XK1MaBi+g6jqQlZbx6ubGP3InEvC+yI6kVHLt/zVPGlhQx/vWsualNZ8M3Kd75SxlpuK
mAm+lIFvPWDa1Q1He8uN4/FvYGOiqL0utAWoy380xzbe42a5ACoTk3ktFLFabyyq/obg9NHB
fe4c+bxmIrDVzsrMZhuW7o0joWcLCTlVIlCz0op4SU32inC9LGZKLXVVAPEEqj/fOQAkun0p
7sEC8jCgqcNmNSVg9tPBHb1zxjrrlJGeKgJAcpdgoAG0ITeAvtNLEeNEIYSDdyw9StFsr3E9
k42nCpnvJG6RQib1eOggspiTA/gxzWS1kIwKuqX7CYRPT95fL2LdEsnMmzgtNtgDVBlxYoXH
6KHwyFZTxbFTe1qgMOKirF7GIQ9Ca4Aze2GcyaGZAvFsOi7p4EdhmY6ihOQBEjdIuWTBnX5s
yLrGf4ZgwVS9vLZF84G0JvBuOTBQKo81YlTIB8xS5V2zy3XS3AfUsIxVs0B85tWD46oSoDV4
lYdw6g3iM3jNXjw1po0YjpjJrCeDzw1CvF40Rc8e1+NWkfULdiZKNC3bLRFkB1F/VCIqpfWY
/VrJRiTjWyvWefSzqBZI4rMG/JpOmIhFT1XX9K+TxSOoA7s01XOY7LwVzdVltfYNEinqvvtW
MV0yX8zIFfw7L9qBkmEkNVuzoxPui9B7qLTsRnt2jxTO+UULaF6zPzU/luHX9vgGDRK0NJyD
o2TN6WzIBx+LGd8AgVZZOSaGZcvpBzY81Caot3+yhkLTAWkFXiNGoYvYUCaYiF4iJ3yxetPy
J8EhiCjaTYfULUr5cdpK+uVLt3ko3IKLMaSLCKWtxguHHtscOICdAR1jcuUFkC+QKTeunmdR
HTfBT3h7Y+fg+piewO30FYfq/jbqeFN2C3WxO1Q3T87y7/GQ8d48a1lbLIY1ByLRYqV+VbXc
21ToujXjZPl016didLf3Em8q8oLo723Wm1Cf5VyQT0KTr8WsHy5juaGqujnFF1UqMoshLcSU
F8DCI28InBrn1L0L2H7jcOcib1SyunDVMGtIVhfNw+LQPbY7Nnn9lfxVxECNmsrK/bmf+ioK
q6g1TVmVkpSMVxvV1tG6cvbkffTych853JYyJ8ckwAuiHhBRbCdMmS5bHV3h6VWmP9j2eIDi
wyZ2KNn9sGaXn0ZFXP6oILqXkWpi2CRe9CrZuoGpP3nmzx0EazL2gSxp4i4cv9WxUkXHvugR
O8+ww9cwjzRZLh8J9igCXHkm4ngjL+teNTsCqYM+EZbtyupAUbij+ny1W4VniOSWkaUfZVT0
dKDkpdOFZ0CMb7BDys84pjdH7emNItd2K4OKgkU+rmLNBa/nkUgTpk4yOK62kfNPBe2HsK51
jIITbnnIz3fpP7iZ5sLrbLXYl7ngAr6kKh4BnZwnHrPsBEJmmYF7Wi5BulzG2NRU8qG6NSEq
z0lwG2RFW1gZE+lQ0crY88+tBkhkTTqUptL4BkGKXn2uWcMLoChJhgiOKsSwCBcklxMF6hUb
MdUEPAlC7kwvBnKIMQEhPyEJmDXl6vAjYfsAY1/UodgtpAn7euyQDNevaXIWaO/pLM60NzGM
4RzJUuX21hacIdZ6kccq1YWi3/2Wcm6CduAkgAAh8oZ/7FUZb+QJsxUJxF1WwiCZDMIHT7vB
rsg5nZ2KzG7ZmG/1nI16ubriOId1cwaFQ8D6mcI0RCsMauFuAA4Ts26WkUwzVoSGHsseukB5
l2pAMI6iBJ5UV4jWYcrdXbRB2QZUbLocYUXxlQjKhUxfuLlPCO5DQs42qS4bUz+rMqoVi07k
tiX1AHUqLPyjkyzB5RUt38AqcKgDHE0EeDWvbwRT4MQXHcMXpyfyOummuqRNj6FLlMxNInla
eWT16ccyg++aboq8INx7KpzDjU4Cf8gLJt3SrocriOvS44xzWTmg1SbmgUk+YC/YyQg8WDJg
JlPkqWNvw4qbVTQ9yOkj2wx+yg2pBEyGi0FpOmjcTVhFP9qFJbx4BsP7TTRpZExfkbJVxaaO
6LCyd+Q7COqCurVU8Dut/+vJSMKgoERLop2T3uR9/TdPXpY+0eS8suTeSeV6qJm4JXBXZVev
hI6uwFQtklJlKp3SXvlKdDoYX3XIv3zOqdIcCWDDN4Zdek3HHYlGWJKs/9vkaI+jATX8MECl
q6BODAQFX+6KXHxxYT/ppsrDXVLuYyZ4De4dbM1mkliikN/eXKLTGAZRXRE2ChwIAlzN2hYZ
V0GiYIS1Vahks8+vY/VR8AwItNY6DPDk2ibmz5T8ie7LmL4X2mY/rC+Z5qnhaQTG9FB7wQzB
gJFxDhmhLXigWMURgJwMhVwLW0pxdZkxN46q/igifVRVXxp6Zyj5uXm+gGerHQQmEDIHSwr/
2Q88nxmfYQW7LKQc7cUe3nbtSPhcwkt0oZNc9Fzweoohz41MizbNukKA5Ji/8gojJ8G/YJHf
fzeZqk2CugEnTo4Xifz8uCmqtzQvufbRtwoLJxSOrXW4hFfIUcBFFU/FqGoFTCJM76cLeFQ8
6/TEE9w0YvUGc8x73sDeAMw6L+NL4obLK8BI0R7GofW89Em3lDcMgK2BsYUnGztSGd9WRRVA
Liuj3kTURUhGzVP+h5T+FDG9XxLkxLX7AL6y3w==

/
