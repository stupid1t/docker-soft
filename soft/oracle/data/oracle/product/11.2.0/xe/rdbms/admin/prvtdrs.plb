create or replace PACKAGE BODY dbms_drs wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
23f5 920
6A227xfMPCUuh1+k9okBewYCxNcwg82TuiBuYI65TvkY751WBdNEZG6igWzTmWrJq6eoHVeB
tvInCvI9oMyKw+WgJLzHHcl1NwINPZxvbEpqbtm/0YLBuBsojMTub69A23i4+AhOEBD4gRy4
LaaFPvRpccw2IT4WI8HuVhvzj3QRzhlbGAvKXL39bgLS6NST9GHj4tE2F1NVAt0PsxRfLzD3
lgHY0XCzH+1ru3rIwjcMwdBQDgCtYFlfVCqBOpm4enb9Wt7fok+dhoPO0+pdjvPzYHPHMaZF
y97SGxr2j0tRjozY2FjT/S9fQqlY/JtniWMcjSXoErd/vpGVfeeTX/psbW++uqdk4eCWoLwh
EHrsb+m5CbzX3rl5zPQLGBhuCifgj8sEC//zYiD8VmhriF1W1USXQM90xcaRONdtzhBUI2yX
GOwUgcuwPtFpw1Pku6fYoldeCy7trMMhZS6sBJRj7mjuepOCPY/nuXslFG2ysZ4qz392UzJd
iV8DRjDkB6Sej2QXTY9d3KQ6y41rWxSWf/qeuGNNgoqmbn+v3iixplJxzZsif1+Eb4Vxa3S3
dhe60Dhs/UIirUoDqMpfzquzrIjaOi+OWaGrSJmpQlnaz7J8t/QhpXZbuXxeHSActjgHx3Rg
q/DIHwFUK0R0lAqvZ5cwvu+hzASaiRS9Upey/kqZKYbg+FPKJNGD7EiUdP8QVwSu841Bu+mp
iudSOuNr+nWijvOrjLTWybsuJGLUCyooUx3x1Zgn+YCI6klGPCBR7Pi0XrVKXFWE0JuScbAo
NeBxbyNu7tOChDIc0LYfbABxpwcv2qhqFskfyCKPsJ+BS6qIT33leBFtgqiR5k0QbGmiWwEt
Z0/Dk9sKGc+l1Q5N2BYSLloYFVngo31IPCy3kxvnH3EDjkpejsT1CeymSde2Gut5uoW3GvtS
RPuJIGlhEdoI3EM6lImg5tN08pkhZ4lGfi7MUuoU8m3AqdwZkiuuX0lHBvd0n2PbdckXIhGQ
FruWyadRQmMXhyZY6qOLMi9OowcmAB1Y81ZQ7Hvu9bqHhnHGGKm7MHJxyZ1s1sCFqXYBSws4
IdZB4mStRWOtRROtRfetUQetZq5u4qQ9ZW4AttMpcNwxywRjLU7UWnbqmDLPZColC7Iwv2md
mp62mvdMJf2lCxX2CaGhowOWOGREtF9jR/rDqoVrPw2nPiPqLa4nKhYGjisi5a+n6N1m3hvM
pKH4bKuMFNgl08LFLipAn3o9t71ihKZ5hGZwZaOYg/YyoWlLwoOT9bOO2EjPsOjmvBQg1H4/
C6lk+17NGwo6QdP/bfnylvjsTnXgSGtc0i7TqMZWOluMAG/e+BRLas0EyuQKNXgVUybxXkGp
GZOlhgxDOPnAebwhg7IiD/GvtooYCsSkGhEPnk4qmQXjeLjqeaOlCPEanIe6gVGzny0ATJLI
vs3qjRJGOyXrVUaxjeKDNIMXibLFPl8Gznyny4DaTav7aorDE6rxEy56AFugNp8ySswYVAtN
5JpfyfCWdH32K06P+/TMdekWkEnMLgpbHUXpAdeAYRTVyKCFyOXXSvhPbF9TDMF07er4ebQ7
J2fN2VZ+rVm1YWSwpkHLYfqZHwbJeeffBf3ceD4y1Q+3Kq8EavHJS801N5PCtO9U6MNfEFXW
m69/kuPu4PJk/aQ2njJ4nb87q4IwHzfrjM3FofHhjXDPT2axDIhexjWvHtCLY+FlZnJcjNO9
7/9kp5ydGQDPRQL4rii/6kkMtLsqKAO1Vq6PHlUAujG7sh+7/tm3wWScH225SOU+EZI376DO
O2rOvkN4wX+0H2a88xYPv0OX0YkYutK9B4gF/K1fKxce30H7Rf6EREC9aoj4iYF76ENDMnUr
ESYqRBh1KkRl01ArzxIfdamAd+SB7v4gesCC0ucDgPWFmj8wARtPDDq9aKr4zqX+H8vdnPWr
y5VCARUEjRfys+6ymW6PJOnMw/1ea4nB1b5yIVWMsGku3yRCipYY7EpaIDJKrX4cQhHS/6z6
sFq/EQSmVBGnnH4PilpADOYPD98KpcYHctd/OOy+mtqdqlRA5DoIxJgPHPyuxBhkThN6h0Gd
vM/iKaMiI235nUm11RJlGTz98hlmo20Fk7iHchWFYbYfxM1AI1JFYQXsQZEyMfYzluSNmJRi
CUbAZGfgd94hcZ41WsSw/yb1W+bIS4UWPppPOTf8IV2kAA8s/DsKTIcFnJcHOVr/BV2o4P55
wgvnfhlI+DtvnbZrWGvRZ/I0CwDPU9ntQ3vnhyKZh3q6W8GwZP9y///WqbtLAhq1uSyNpCr7


/
