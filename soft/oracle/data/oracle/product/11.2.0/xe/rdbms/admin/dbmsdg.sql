Rem
Rem $Header: dbmsdg.sql 05-jun-2006.07:11:20 svivian Exp $
Rem
Rem dbmsdg.sql
Rem
Rem Copyright (c) 2006, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      dbmsdg.sql - Package header for dbms_dg
Rem
Rem    DESCRIPTION
Rem      This package is the public interface to the Data Guard callable API.
Rem
Rem    NOTES
Rem      Package body is in $ADE_VIEW_ROOT/rdbms/src/server/drs/prvtdg.sql
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    svivian     06/05/06 - 
Rem    nkarkhan    05/24/06 - Project 19620: Add support for application
Rem                           initiated Fast-Start Failover.
Rem    nkarkhan    05/24/06 - Created
Rem

CREATE OR REPLACE PACKAGE dbms_dg AUTHID CURRENT_USER AS
  --
  -- This function is used by an application to initiate a Fast-Start Failover.
  -- The broker will determine if the configuration is ready to failover
  -- and then signal the Observer to failover.
  --
  -- The caller can pass in a character string to indicate the reason
  -- a Fast-Start Failover has been requested. If a NULL string is passed in
  -- a default string of 'Application Failover Requested' will be sent to the
  -- observer.
  --
  -- RETURNS:
  --   ORA-00000: normal, successful completion
  --   ORA-16646: Fast-Start Failover is disabled
  --   ORA-16666: unable to initiate Fast-Start Failover on a standby database
  --   ORA-16817: unsynchronized Fast-Start Failover configuration
  --   ORA-16819: Fast-Start Failover observer not started
  --   ORA-16820: Fast-Start Failover observer is no longer observing this database
  --   ORA-16829: lagging Fast-Start Failover configuration
  --
  FUNCTION initiate_fs_failover(condstr IN VARCHAR2) RETURN BINARY_INTEGER;

END dbms_dg;

/

CREATE OR REPLACE PUBLIC SYNONYM DBMS_DG FOR SYS.DBMS_DG;

