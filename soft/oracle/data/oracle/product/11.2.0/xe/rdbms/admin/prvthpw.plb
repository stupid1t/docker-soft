/* WORKER header */
CREATE OR REPLACE PACKAGE kupw$worker wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
314 1ce
SAxRy8xK1nkFx7wzKEWHRupzVk8wgxAJAMcVfHTNR2Sl3aaxwvUBCX5sUvQk3GH6f0L821Ac
vwD5P413uBRnDc5WAPq7ANnWaDXWBs5OEIdBndzGcrepjro7RlmiEQvc+6FnVLjjSS20focf
xH9oAK30xfOeHYMYPQ0FZ+NaMayvCP+VfzNnwkQjqhPvTCBYFZ1aAlwnwmENtrdYjWFnW9Gi
fsg8BRXNPlxj1gcWBWjagwGks0bNTNlM6uNGcMLMWFCpcjVhz2/6cXbFwsc9PUuZ7ag6SeB+
0levf5djFkYyJTpqslPO02v8pQaGCyAbhQaGdr4/Grhad/qXTZCUKsunFAWtaTFwfpDFBjTZ
31gp36XP/1x+v8xvtkwJTrDoIt8ETgcKmxw+pZgKwErRaMKQs5AQOSiIsTXY4m3Mv1BG9zFz
5g5c4JzPnXa8boi85OGqWk3K

/
grant execute on SYS.KUPW$WORKER to public;
CREATE OR REPLACE VIEW sys.ku$_table_est_view (
                cols, rowcnt, avgrln, object_schema, object_name) AS
        SELECT  t.cols, t.rowcnt, t.avgrln, u.name, o.name
        FROM    SYS.OBJ$ O, SYS.TAB$ T, SYS.USER$ U
        WHERE   t.obj# = o.obj# AND
                o.owner# = u.user# AND
                (UID IN (0, o.owner#) OR
                 EXISTS (
                    SELECT  role
                    FROM    sys.session_roles
                    WHERE   role = 'SELECT_CATALOG_ROLE'))
/
GRANT SELECT ON sys.ku$_table_est_view TO PUBLIC
/
CREATE OR REPLACE VIEW sys.ku$_partition_est_view (
                cols, rowcnt, avgrln, object_schema, object_name,
                partition_name) AS
        SELECT  t.cols, tp.rowcnt, tp.avgrln, u.name, ot.name, op.subname
        FROM    SYS.OBJ$ OT, SYS.OBJ$ OP, SYS.TAB$ T, SYS.TABPART$ TP,
                SYS.USER$ U
        WHERE   tp.obj# = op.obj# AND
                tp.bo# = ot.obj# AND
                ot.type#=2 AND
                t.obj# = tp.bo# AND
                ot.owner# = u.user# AND
                (UID IN (0, ot.owner#) OR
                 EXISTS (
                    SELECT  role
                    FROM    sys.session_roles
                    WHERE   role = 'SELECT_CATALOG_ROLE'));
GRANT SELECT ON sys.ku$_partition_est_view TO PUBLIC
/
CREATE OR REPLACE VIEW sys.ku$_subpartition_est_view (
                cols, rowcnt, avgrln, object_schema, object_name,
                subpartition_name) AS
        SELECT  t.cols, tp.rowcnt, tp.avgrln, u.name, ot.name, op.subname
        FROM    SYS.OBJ$ OT, SYS.OBJ$ OP, SYS.TAB$ T, sys.tabcompart$ tcp, 
                SYS.TABSUBPART$ TP, SYS.USER$ U
        WHERE   tp.obj# = op.obj# AND
                tcp.bo# = ot.obj# AND
                ot.type#=2 AND
                t.obj# = tcp.bo# AND
                tp.pobj# = tcp.obj# and
                ot.owner# = u.user# AND
                (UID IN (0, ot.owner#) OR
                 EXISTS (
                    SELECT  role
                    FROM    sys.session_roles
                    WHERE   role = 'SELECT_CATALOG_ROLE'))
/
GRANT SELECT ON sys.ku$_subpartition_est_view TO PUBLIC
/
CREATE OR REPLACE VIEW sys.ku$_view_status_view (
                status, owner, name) AS
        SELECT  o.status, u.name, o.name
        FROM    sys.obj$ o, sys.user$ u
        WHERE   o.owner# = u.user# AND
                o.type# = 4 AND
                (UID IN (0, o.owner#) OR
                 EXISTS (
                    SELECT  role
                    FROM    sys.session_roles
                    WHERE   role = 'SELECT_CATALOG_ROLE'))
/
GRANT SELECT ON sys.ku$_view_status_view TO PUBLIC
/
CREATE OR REPLACE VIEW sys.ku$_table_exists_view (
                object_schema, object_long_name) AS
        SELECT  u.name, o.name
        FROM    sys.obj$ o, sys.user$ u
        WHERE   o.owner# = u.user# AND
                o.type# = 2 AND
                (UID IN (0, o.owner#) OR
                 EXISTS (
                    SELECT  role
                    FROM    sys.session_roles
                    WHERE   role = 'SELECT_CATALOG_ROLE'))
/
GRANT SELECT ON sys.ku$_table_exists_view TO PUBLIC
/
CREATE OR REPLACE VIEW sys.ku$_refpar_level (
                refpar_level, owner, name) AS
        SELECT  sys.dbms_metadata_util.ref_par_level(t.obj#), u.name, o.name
        FROM    sys.obj$ o, sys.tab$ t, sys.user$ u
        where   u.user# = o.owner# AND o.obj# = t.obj# AND
                bitand(t.property, 32+64+128+256+512) = 32 AND
                EXISTS(SELECT * from sys.partobj$ po
                       WHERE  po.obj# = t.obj# AND po.parttype = 5) AND
                (SYS_CONTEXT('USERENV','CURRENT_USERID') IN (o.owner#,0) OR
                 EXISTS (SELECT * FROM sys.session_roles
                         WHERE role='SELECT_CATALOG_ROLE'))
/
GRANT SELECT ON sys.ku$_refpar_level TO PUBLIC
/
