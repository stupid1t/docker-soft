Rem
Rem $Header: rdbms/admin/initsjty.sql /st_rdbms_pt-112xe/1 2010/10/13 20:03:54 srirkris Exp $
Rem
Rem initsqljtype.sql
Rem
Rem Copyright (c) 2000, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      initsqljtype.sql - initialization for sqlj types
Rem
Rem    DESCRIPTION
Rem      load java classes required for sqljtype validation 
Rem      and generation of helper classes.
Rem
Rem    NOTES
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    srirkris    10/04/10 - Add dbmssjty and prvtsjty
Rem    varora      09/01/00 - Created
Rem

call sys.dbms_java.loadjava('-v -r rdbms/jlib/sqljtype.jar');

@@dbmssjty.sql
@@prvtsjty.plb
