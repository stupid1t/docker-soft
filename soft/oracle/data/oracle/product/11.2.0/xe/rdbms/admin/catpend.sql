Rem
Rem $Header: rdbms/admin/catpend.sql /st_rdbms_pt-112xe/2 2010/12/06 04:43:34 cdilling Exp $
Rem
Rem catpend.sql
Rem
Rem Copyright (c) 2006, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      catpend.sql - CATProc END
Rem
Rem    DESCRIPTION
Rem      This script runs the final actions for catproc.sqll
Rem
Rem    NOTES
Rem      This script must be run only as a subscript of catproc.sql.
Rem      It is run with catctl.pl as a  single process phase.
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    cdilling    10/26/10 - call catproc_noxe.sql for SE/EE specific packages
Rem    pyoun       01/16/09 - bug 7653375 add random salt confounder
Rem    shiyer      03/26/08 - Remove TSMSYS schema
Rem    dsemler     02/07/08 - Add APPQOSSYS schema to schema list
Rem    achoi       02/01/08 - add DIP, ORACLE_OCM
Rem    rburns      01/19/07 - add package reset
Rem    rburns      08/28/06 - move sql_bind_capture
Rem    mzait       06/15/06 - add TSMSYS to the registry
Rem    rburns      05/22/06 - add timestamp 
Rem    rburns      01/13/06 - split catproc for parallel upgrade 
Rem    rburns      01/13/06 - Created
Rem

------------------------------------------------------------------------------

Rem
Rem [g]v$sql_bind_capture
Rem   must be create here since it has a dependency with AnyData type
-- should be included in some other script
-- causes hang in catpdeps.sql
Rem
create or replace view v_$sql_bind_capture as select * from o$sql_bind_capture;
create or replace public synonym v$sql_bind_capture for v_$sql_bind_capture;
grant select on v_$sql_bind_capture to select_catalog_role;

create or replace view gv_$sql_bind_capture as select * from go$sql_bind_capture;
create or replace public synonym gv$sql_bind_capture for gv_$sql_bind_capture;
grant select on gv_$sql_bind_capture to select_catalog_role;

Rem Reset the package state of any packages used during catproc.sql
execute DBMS_SESSION.RESET_PACKAGE; 

Rem
Rem add random salt confounder for bug 7653375
Rem
insert into props$
    (select 'NO_USERID_VERIFIER_SALT', RAWTOHEX(sys.DBMS_CRYPTO.RANDOMBYTES (16)),
NULL from dual
     where 'NO_USERID_VERIFIER_SALT' NOT IN (select name from props$));


Rem Check for XE edition, do not run additional scripts
Rem Run catproc_noxe.sql if not XE, else run nothing.sql
Rem catproc_noxe.sql consists of extra actions that will be executed
Rem in addition to  catproc.sql. These actions only need to be run 
Rem on an SE/EE database and should not run on an XE database.

VARIABLE noxe_filename VARCHAR2(30)
COLUMN :noxe_filename NEW_VALUE noxe_file NOPRINT 
BEGIN
  IF :server_edition = 'XE' THEN 
     :noxe_filename := 'nothing.sql';
  ELSE
     :noxe_filename := 'catproc_noxe.sql';
  END IF;
END;
/


SELECT :noxe_filename FROM DUAL;
@@&noxe_file


SET SERVEROUTPUT ON

Rem Indicate CATPROC load complete and check validity
BEGIN
   dbms_registry.update_schema_list('CATPROC',
     dbms_registry.schema_list_t('SYSTEM', 'OUTLN', 'DBSNMP', 'DIP',
                                 'ORACLE_OCM', 'APPQOSSYS'));
   dbms_registry.loaded('CATPROC');
   dbms_registry_sys.validate_catproc;
   dbms_registry_sys.validate_catalog;
END;
/

SELECT dbms_registry_sys.time_stamp('CATPROC') AS timestamp FROM DUAL;  

SET SERVEROUTPUT OFF


