:
#
# $Header: shutdown.sh@@/main/osit_unix_7.3.1/2 \
# Checked in on Sun Aug 20  4:34:05 US/Pacific 1995 by wyim \
# Copyright (c) 1995, 1997 by Oracle Corporation \
# $ Copyr (c) 1992 Oracle
#
svrmgrl 1> $1 2> $1 <<!
connect internal
shutdown immediate
exit
!

