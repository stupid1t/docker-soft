create type aq$_jms_userproperty 
oid '00000000000000000000000000021020' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
9e be
dQnF08C0REARKTs8QTNiDmUnuJkwg5n0dLhcWlbD9HKXYkqWYtxixRaXllpixSbDj8B0K6W/
m8Ayy+4ljwlppcfSMlyprHzGyhcoxsqyhB0dLqQOceeFLxal0pn7vUQHNpUHrZJGRWY+eGWp
5f6ZgQIM4THc4jXKzJbGfMYwTcxnXlMhO4imxlobAg==

/
create type aq$_jms_userproparray 
oid '00000000000000000000000000021021' 
as varray(100) of aq$_jms_userproperty;
/
create type aq$_jms_header 
oid '00000000000000000000000000021022' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
e8 e3
ayTH9Lv67niACQp/X71pk12Un/0wg3n/LZ4VZ3Q5cMESGHSkoy8mA17Ya2FNvy4J7uiMUKzn
y/z41r8RSfjQOlqbCr82aoKZ/sbaaOg4bodwad25uuV3XTmp+pA0o1rzlv2KwqkqTPWkzOv5
lupd/ONuINnCWjyZUsdU3/VI6CQKMTMD0expnNg6K7h+pC3fFAh+ijxBbBqx3kpHfS4tuhCm
Wkvf8Q==

/
create type aq$_jms_message 
oid '00000000000000000000000000021023' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
109 fb
J8GAUKWxuei8pyqcrFnpN09yJlwwg0zwfyisfC8CkPjVSHCp2/v0E+sNcTZmDs1EkScE4/S9
1u61SW+6sginEjoZ7RJ6cb3U6pFPTuDB3pPUxjbhsqDCZpVmb6ZEONyrFY9iwlx52LASwf0n
bsXHu79NXDd7bSje7NifLtSPWFZkXGnfNObo2gWZKiK2DnZ1R9BIltpEqprQ3jC7hKv40Luf
bhBiDyXyLFzceaPhp9zjXaASHQumAFQ=

/
create type aq$_jms_text_message 
oid '00000000000000000000000000021024' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
84 b6
7gjKJHRtSEoKPCevFA7tFooUSd4wg5n0dLhcWlbD9HKXYkqWoTsW9HIuYtHcWa5cj8B0K6W/
m8Ayy+4ljwlppbgy9VKyCabhxmeNMJaajzDj48tp0dsqZhQZxj2uNa/rljbRL4t0w3aLBF2K
7gSRGjjgcHAO9GmKuxoOj3QUviE7iKbIAqiB

/
create type aq$_jms_bytes_message 
oid '00000000000000000000000000021025' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
84 b2
BqFD6cKH3W4nNYwL8NBQCEExrY4wg5n0dLhcWlbD9HKXYkqu5VahYkpyLmLR3FmuXI/AdCul
v5vAMsvuJY8JaaW4MvVSsgmm+8ZnjTCWmo8w48jLaSU+ZdsqVxnGPa41r+uWNqjTXQTiDqYU
IV90dHQh98OS2vtYZ/EM6+z7pljCZnY=

/
create type aq$_jms_stream_message 
oid '00000000000000000000000000021026' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
85 b2
DlJD/eh23AREqb33WjvCn+YYTlkwg5n0dLhcWlbD9HKXYkqW0T7yLkfwci5i0dxZrlyPwHQr
pb+bwDLL7iWPCWmluDL1UrIJpvvGZ40wlpqPMOPIy2klPmXbKlcZxj2uNa/rljao010E4g6m
FCFfdHR0IffDktr7WGfxDOvs+6bXFWY3

/
create type aq$_jms_map_message 
oid '00000000000000000000000000021027' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
82 ae
zXz2G8NAEJ1oZkMBPQLrWVyAphEwg5n0dLhcWlbD9HKXYkpyLlbjci5i0dxZrlyPwHQrpb+b
wDLL7iWPCWmluDL1UrIJpvvGZ40wlpqPMOPIy2klPmXbKlcZxj2uNa/rljao010E4g6mFCFf
dHR0IffDktr7WGfxDOvs+6ZsZ6iC

/
create type aq$_jms_object_message 
oid '00000000000000000000000000021028' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
85 b2
nSPuK2M/z61nPWgB4JXWaz3oCIMwg5n0dLhcWlbD9HKXYkpyrpfyLtH0ci5i0dxZrlyPwHQr
pb+bwDLL7iWPCWmluDL1UrIJpvvGZ40wlpqPMOPIy2klPmXbKlcZxj2uNa/rljao010E4g6m
FCFfdHR0IffDktr7WGfxDOvs+6Zq7mZ+

/
grant execute on aq$_jms_message to public;
grant execute on aq$_jms_text_message to public;
grant execute on aq$_jms_bytes_message to public;
grant execute on aq$_jms_stream_message to public;
grant execute on aq$_jms_map_message to public;
grant execute on aq$_jms_object_message to public;
grant execute on aq$_jms_header to public;
grant execute on aq$_jms_userproparray to public;
grant execute on aq$_jms_userproperty to public;
create type aq$_jms_messages 
timestamp '2002-10-23:15:20:01' oid '00000000000000000000000000021000'
as varray(2147483647) of aq$_jms_message;
/
create type aq$_jms_text_messages 
timestamp '2002-10-23:15:20:02' oid '00000000000000000000000000021001'
as varray(2147483647) of aq$_jms_text_message;
/
create type aq$_jms_bytes_messages 
timestamp '2002-10-23:15:20:03' oid '00000000000000000000000000021002'
as varray(2147483647) of aq$_jms_bytes_message;
/
create type aq$_jms_map_messages 
timestamp '2002-10-23:15:20:04' oid '00000000000000000000000000021003'
as varray(2147483647) of aq$_jms_map_message;
/
create type aq$_jms_stream_messages 
timestamp '2002-10-23:15:20:05' oid '00000000000000000000000000021004'
as varray(2147483647) of aq$_jms_stream_message;
/
create type aq$_jms_object_messages 
timestamp '2002-10-23:15:20:05' oid '00000000000000000000000000021005'
as varray(2147483647) of aq$_jms_object_message;
/
create type aq$_jms_message_property 
timestamp '2002-10-23:15:30:00' oid '00000000000000000000000000021010' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
1c6 164
vAzAzPvsWFHgXnY++Sluqw+ySz0wg3nIrgxqZy+VAMFeimsHNqJ+RPN68BOoShqYBVj07Osl
hC9G4Hk0zs3XT6vWzUFAc+1YeCCgyxFhT23OvPaG4t4Fzbq2/gfQEMfOktxnX6ow5gqh7AQL
4bcXJYNZ0G6HMxTG08magqxkNwSJNPiu2Be5g8l9iLQi4WQlQ/zR/SEOGB4OwHucswnK9asM
0830d8BEE+lu43YfSwQjt1MmSxUD3305b408V4NdDlfKs+J7E1lQQ4X3m/TkSKZEHCpVvp7A
adQNjozI7mxMGUph5gIdOwSuG/UUOTM4yD7WtkuEQKwQlPnTaHEXoZ+qCB0jYd7J

/
create  type aq$_jms_message_properties 
timestamp '2002-10-23:15:31:00' oid '00000000000000000000000000021011'
as varray(2147483647) of aq$_jms_message_property;
/
create type aq$_jms_array_msgid_info 
timestamp '2002-10-23:15:33:00' oid '00000000000000000000000000021012' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
42 7d
qYa+eAU9tfTW859HL9khQnNQ6HQwg5n0dLhcWlbD9HKXYkquVsXy2F9yYtxyWfRyRwzZwcB0
K6W/m8Ayy8yPJY8JaWmlsp/+9fslnjVK7qtAyexx7Dx0pq8G04I=

/
create type aq$_jms_array_msgids 
timestamp '2002-10-23:15:34:00' oid '00000000000000000000000000021013'
as varray(2147483647) of raw(16);
/
create type aq$_jms_array_error_info 
timestamp '2002-10-23:15:33:00' oid '00000000000000000000000000021014' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
86 aa
X56m1OMX6PFhLRNKhCxg7X8Qyhswg5n0dLhcWlbD9HKXYkquVsXy2F+uYsWXlhhyRwzZwcB0
K6W/m8Ayy8yPJY8JaWm4sp6bKBgopSgzdAjS/h2eFjCSAn3KkeTklI+mXNR4tKTRtosEXYru
BJEaOOBwcA76Y97Xpqb4K6ic

/
create type aq$_jms_array_errors 
timestamp '2002-10-23:15:34:00' oid '00000000000000000000000000021016'
as varray(2147483647) of aq$_jms_array_error_info;
/
grant execute on aq$_jms_messages to public;
grant execute on aq$_jms_text_messages to public;
grant execute on aq$_jms_bytes_messages to public;
grant execute on aq$_jms_stream_messages to public;
grant execute on aq$_jms_map_messages to public;
grant execute on aq$_jms_object_messages to public;
grant execute on aq$_jms_message_property to public;
grant execute on aq$_jms_message_properties to public;
grant execute on aq$_jms_array_msgid_info to public;
grant execute on aq$_jms_array_msgids to public;
grant execute on aq$_jms_array_error_info to public;
grant execute on aq$_jms_array_errors to public;
CREATE OR REPLACE PACKAGE dbms_aqjms wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
1b51 629
NZYUqkpmPfYNOrEh2Ctow0y0kZQwg81ULiCG3y/NMZ2sBxzfZ0WFSC0Ad303UdTCwtM+t4m2
T+NvAJS0hC21LH3I32wJT4nzfsRnAhRadrx4LCBXduVrioJiqeQ6xpxki/6dtZKbjVArLNlu
Syvyj4OgnhRrJ8TvRopo6pOxyJzDHSksG1MlifbxjW6XbT6+QOE3nflgm9grQEEZv0sAwOdI
dU0eCSCGfXD4VUjBc+641FwIt91DcPj98IVBN96MlrJ+m7AHMU0UEPQ+JVR6RwnSODIP3a5i
oCYarqdsB2LIKNzzEHbJUvubs2zi+ybyYy1Z8deRze6m05uRgmuU7xop529wJBKTvlBAK40w
LTmx8TxicdCM64uIlHel5S6y8jHwiRTOLw2dfwEfsuWw8pEYaUHy3r2guVPTJuCJweXettuc
l5QOerN5mH71xWA6XZxuvYppzijFmT2UtjRlEfU+Uwp2oW3s8e7rOzVtYz8fggDxTz860ikp
rzGoB6eCmPGODi6GMzOYTIU9WrME3B4Fh9Wt+Vde+krDP892ZNhLyNaN/FEZ6+vQ1EDe/TtM
hGManFuZjQ66+lsxWKWZXNrr/AXaiykbcQZ5c2RTRp+CT4ylmMf/ZSFl0m2LkPMXrA/A5Skd
vVTBebuE73hyiF07Nheh5jjanF9OLwAYpGtks/FykRTUoJNP1KJ0e+hEtj1vR8xNj6ncoO8U
l5bfkjmeBdoynpNHDqhduA55Y/oHREoDAGxWaAWQhlPNii1UYpoUsSsmYZegXFE7mzYeF17m
TnYfx+QTyZQEdGB/k8kMiKuyHCbi+mGrBuZwScogCidB6QyXPTWFkitZ/i1L5aCGQBXvxhy2
6W0df8LjK0k2JRc+EZ8U4bcxXwxNxMnP1DEUUrtpIaFHQXj8OzwnN/8b6jXxLTeLBUMrqcSk
fLA9iwghcveEXOAoRXWjXY9X7PLw5+gXMcZDaP1iR1w2HlgvlGkZfxwRY3AmbttCTtRVxVJF
NDXoH8J6D8KUn8G7SewOVLffaAAZtbFhaLqKwsrWgoAy1oKrBHgbBpTm/lh6jTq0wELa5+AS
7bBCQGobtXqyq1vois3sgnyx3oLXQDIDXEC+uUuKtCfOuWVlShBlkiqdG4xP0BBlIxKgYkCz
mCBMgNaf42EHjK+rRTzBj+KPIm3/cj2SjpbPuF3i5uLNMhOUfu6vfvOEi7dZaJkqLJIAXtzI
DTfuOi02zr2ZY3rQxEaVVkIThHvTJNKj0B53Mp7+I9XTzpItT+jBZKyxMiJ9OjLywo89E9WF
+jKtTY8qD6D/xkttBuforTCVjLrwmDNNGKdhcpEDuajVAJhdxE5qxo9SsddXFxF17ESpALJg
xx7AfC1goS/26TdKCidE4ZdIX39dq/q1/BCx/FIWS/YGFvwtIGzOSHcy7Ctvjw8fvPQZDcUG
1j2EFBBCyl4/Fr9HAurCLe+0UOq5fgv/UdOIO3QALRj2FHCSMlk0wKjPcYGRIw7w7TgwmffW
BMGxLs7wtBLA8RV+qnM53i2XHXJz2fzBo82qzltTlgVZ

/
SHOW ERRORS
CREATE OR REPLACE PUBLIC SYNONYM dbms_aqjms FOR sys.dbms_aqjms
/
GRANT execute ON dbms_aqjms TO public
/
CREATE OR REPLACE PACKAGE dbms_aqjms_internal wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
342f 50d
6qyB9e1brBPioGLhV3WSGiFk8ZIwg80TLiDrfATGviWu7h0H3Ku2dK83GNa3etk0bNwn2jEj
wkCPpqyhNM+qOLAu/DQ8TOtf8v84E2YkZDoknWGfnOC1bwepvM/5OoAi5JLiPz8S5PiB/oZq
KEY6fkBpnl+20amew4c/5r7d/ECAFsnVoU2pLwSSzr3tTBa2lphF4Tn75t8bnG2jjlh8k60E
fBLEUDHz/ElfN5YUyahkFa/tKCbKvWLPgnbUBCpkJ/Cb8ts0uSl27fml+XMTA2Za7u8IqIUE
yjTPbAMh3eEaRVjyjU91lqFpTi4R74ZOiIMyREFf5+0qEF11OrF1TKNUx1TJZxDIIW0APWuw
kQ4M+lPSt6ZFCn3egOfgDh8CTwC33jPgzWphpNeHyomRUQOJeaRFOZQqht0WSjWHPojlr2kl
Ex+dGrz4RsHCvPHanop8H+UadMtH5i6IPBiqRVp4625/K58KQZmej54X92chbWb2qmCGs4ve
zUx2UGPTqPrZo7quaCL9tO6hVX6XsHYdr3xDCdgJdWS1XfkqEc4Rc4MfDWFi+C1PYhc/993f
EhIFplO0q7F+mMW89L7DEJaJiB3BJwnOzzyMtM12jP/A8lti9Y7pO87piC1xUypcx9G6i2Pe
qwhhNt8KmX5SbXCV5nCsdTjHFfSAZmYrpIh4FF7/AmPnMp43sAaGQwIjApu3Vxvg/Vg1Gjmt
6kzBOAIFZWVqv00Xy9VlOci300TaZeISFokmjwT9yt4AdxwFZpMZRgkboy7GX+49Rd+MVd1U
FYrXsdlyF5YNe9/FsqOfQHugFVts0Y+HWU1Oo1dJn0kzs5UiTZ8gbKmuFv4jerC/sP6h7v9E
E2mKxauPtCZVJnMRJyzPA0rrPppL/bNuJZAlSUMjBlVZFjKULiCoew3aDa7lM6ZggwIxD7Aj
0CJt180u+J/q3py6/8b3SdZX+q8W08EUIT1Sbkt3zqa1JF9dpBtRAYI/kSojyLGUFTwhfPh5
ZC9cW7Lj3nne2aFaLVALZWd5DU3KwX9wJeml0at08zROYxfdvSQEyUTw37dCBPDUMXzHWsBa
wFp76RSBinAk2pxgg4P1cMBwwHB76XyBkCpZNeiofPDCvc1LXUtdS/UYVPwAlCMzv1mMdgRX
fK18rXyQXRkJwb0ZFMOr2yUWB9rLDMsMywyw42MZ6zzBznacyVbYy490BFwP+4ArIaPfrw0L
7TqVQhO3fhyX/6pQtHbZQahcq1CmeRo7ExptK39RlW2qeZjfIA==

/
create or replace type aq$_jms_value 
oid '00000000000000000000000000021040' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
90 a6
/yJjndUcJ8BvvwuYtU4zUcyRkJ4wg5n0dLhcWlbD9HKXYkqWDEc+LlyPwHQrpb+bwDLL7iWP
CWnn+yap4Z1pD0mxyrLvLqTRfEkiR7NboeEU/IruBDa2OS0qdK8YhHHv5o8VIvncyn+whS+x
cE0OtmlfDOvs+6YsjKjN

/
show errors;
create or replace type aq$_jms_exception 
oid '00000000000000000000000000021041' wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
d
92 b6
jQg0PYDmFTVtQpkufvgWaEThd/Uwg5n0dLhcWlbD9HKXYkquO1rcYhb6R3Jej8B0K6W/m8Ay
y+4ljwlppfXMqR2daQ9JscqkRA5L9qJnNE+MZNVaV0d/hTFqf1pqhd1nXlsUH46Ou/2K4qlW
F56NmctS9TNAq1u3gn+m8sDeo4KmpsgEqEc=

/
show errors;
create or replace type aq$_jms_namearray 
oid '00000000000000000000000000021042' 
as varray(1024) of varchar(200);
/
show errors; 
alter type aq$_jms_header replace as object
(
  replyto     sys.aq$_agent,
  type        varchar(100),
  userid      varchar(100),
  appid       varchar(100),
  groupid     varchar(100),
  groupseq    int,
  properties  aq$_jms_userproparray,
  MEMBER FUNCTION get_boolean_property ( property_name   IN      VARCHAR)
  RETURN   BOOLEAN,
  MEMBER FUNCTION get_boolean_property_as_int ( property_name   IN   VARCHAR)
  RETURN   int,
  MEMBER FUNCTION get_string_property ( property_name   IN      VARCHAR)
  RETURN   VARCHAR
);
show errors;
grant execute on aq$_jms_message to public;
grant execute on aq$_jms_text_message to public;
grant execute on aq$_jms_bytes_message to public;
grant execute on aq$_jms_stream_message to public;
grant execute on aq$_jms_map_message to public;
grant execute on aq$_jms_object_message to public;
grant execute on aq$_jms_header to public;
grant execute on aq$_jms_userproparray to public;
grant execute on aq$_jms_userproperty to public;
grant execute on aq$_jms_value to public;
grant execute on aq$_jms_exception to public;
grant execute on aq$_jms_namearray to public;
CREATE OR REPLACE PACKAGE dbms_aqin wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
9
87d6 a95
nEGIPPKUebGXuu4j44jj6e/vq94wg81MLtBdGy/XQqoCDZeo7ztg2hO6e/RGeYMM1DmWj1iB
vWyXbAeiGJu1Y6kkg3AL/UUSDQPrKoMo3qIAGFH8ALiEmGAx78W4m8EsqrWwP7X+ZKrTd4XQ
FK7CQMdZYyc2i73TIBkU+mOqbrUUgMMOWdNgzFh0JIiM5mbFt4lp27rpTdsCubWwD/F6O0bm
T2mHVdroW/DpiJX65El02XUcOZwWhpTFPbA5eM5ZS73dzgIkF1PfSNF/aqYXpmpUXPC+yubs
KS031oA0pldZ6wzlnsE2oXZDinP6R85NEoVUXPOIB1JlA2ihrRJvV59luRKChgz6woBugB3S
yTmphhgAlRJQEy6mGMJ2oY6ZfHWT863T0RrlSy/oSNbudgNq47aQiwDcnOPOTmjwLxIFfkb0
llXLi10C6ed/h1TJK89P8/dGby3LsslzRow0Lv+FBHnJivb/ZaTKSfYdiq7JAIF3RnKbIqYL
fILnURsBiNB8dTFsX9BfT8wVkFgIosKSdJ+dscmfMRFLSVXK31USA0FCjVhdfVnN11bq0iDA
cVHfHE2CA/dvhJ5H+UhaANQaRO5nKBscRvx74gQg74uLaq8pzH/iJYsw2xDlSAT5V0XZJZq4
6PbEO1KJYVnJTPRRFx9mS6JbghdlY8V8lXGZqvCD8WvPmSkQH60tfDH0YRc+PY0SGX2X5k+8
6ljJKPQSKurTY2z57tdXfB6cbXtmpAsbruMXLPO1pUjJIVN4bT3l98hf9ofqk/TsvidyX/F2
LMxel0bfAd6ofNFcoW+XjkppKQ/3RXfcfePfjf8NRBVW2+//EpkaHxZcuDx3giq0OX+Zl73G
2BenjZSCdu12F9k9w+NJKLZa16E9eH+tp+64L9Ub4R7jiz91cC8A8auhBgyV16loKatUFBD/
kxCVibFxT+4UMezYc/uHfA+/B81FQSjQUSckGLviNR3H5XTVRt6CkZRvkthXp25wLUCAE8UN
Um7AfGHNCSJ4ipra1EUE15/y8Z6Aq8FT1CHGIfbXFX8SoJOoG3OdHCzSOg2jhB/osQJKYmUP
XnSc6gh0XpqnIvHmx8/f42TiDQk8ZOC4toXgUs+TRmk+f53y4xWtSb51Mt8v4SZJOmUCIJYs
VWcoiBpHBiu7f1f//MBJBoSqvJQO7loi9iQgQVlCwUwdMwQWcjpjBCMa8QHY9X/JUqNqcG6O
RQEmxczIz4sHSqfaWz9fX1Li68nPIVI8VEHjyj4+9H4AXkt7p+vrIReITcYUug+52B2teu4m
qUymDLVFwZkDETZZbTFkgw2R4ZJGllgfOASuljgyzCT3s2uF5KUiGfBuurs59rHQUiwnlaK1
1Hrl6jyE1LR1tO56c0tgSgQr0lo1935TIO/34ytov9OAnl6qgSeqVU7EjJMbo70tq50qjpC3
ex/1dcWnhr6WMj7Ypa0WPMfQ2vbW7ZfwZ/Rn9Gf0eGD0aguEH8nbMPiCl2MjzoE7ACiIJvSL
ji01X17vjxmY1sG3ASRbHr6pgT7If8gI2UkLHuunZNnGeK4KZ2NnY2djZ2Ozw5cnGgeefqvU
6GdRZ1FnUWdRpc+mTR9NH00fTf5s4g83ELExsa7t2RcDToaEc/rzsg2qWgvWGBPDkW4aTmpH
9oRKzuTA7y1mEXyDnH/Rn33E/EPSf2C/HgsVa330jgwXn1JF9qvaNHamhjiLDQsimByBSSkf
SZxvW+DURSXp8M2MlbmQGkL2xLptG6smz/TuO4frtHi1C4YBiyMyxd1itZNRENpkxUS0qD5S
XAzzmk+1ngUjolk5DNhvcdqjfgjYv6FhMRO/+2/g7tHE5tP6xNujDBS1vjqpI5kr2T2OM/pR
PbQDPrNxAcvZwfaUpNQwh0MFjp2UTLEqEUs8ZTJKA4zsExWtJDjLmzmbOZs5mzmbOZszl115
F7oEDYL1gIZq/YALOLKSgyBmxqo1eTV5NXk1eTV5ncwNgQDJ7QCvpBJhePHK9cr1yvXK9cr1
yvVkXn3/BQmHMtq+P/Y2FG8UbxRvFG8UM7oSwNqKT9SdP6OanciyNMs0yzTLNMs0+QFCc8kk
h052HPvX4dfh1+HX4dfh1yMI7DL59MgeImrHlcKwmKEe2xRY/eW+qBPWwCOdwyPz7iYFMbAw
4redXAb1RC6jliGpmBG9aOnWmBF4vx8uIxkCwsj7hKufagO7Y0zkgfBE6OttW5+MF7IjOuLW
nEJOec489RhPXPolpfsicbX8whckEQqAg/JWZyJnImciSOoidtO08SiwqH1bfQflabL8+/z7
/FKY/DjMZ8K+OkurrJ+pn02o7zUiZyJnIkhzEolxRAj6BwUvjjaOor0mTDtnNWc1ZzVI6jVx
b7RpL8SoFoUe6i+dRolM1qHR9WVQlbpPB7w83K8wrlR5nVtSUlPlHr9sC9SIHoMwnf0YhhVf
1trqX9Od4zr/bHWnUWBq1QeuYvixZ6PNZcOnPpcc/vOCYNdbX068MrmapEVYBLCGtFDY2ZaC
SuHZWRf0e8ArWGXpoCqRcE+YBB6ihJ9SKbpobDMeYv3cttbHVM1s7QmWJuvZpb/s/TeBZj00
NjQSKRcebixYkMjaAhYB2NvjRcZXrvkf0Qb60BmFLVJVfMFfR/3XZF1q58T1Z5Zx1Jx3tiPO
uapFADNt

/
show errors;
CREATE OR REPLACE PUBLIC SYNONYM dbms_aqin FOR sys.dbms_aqin
/
