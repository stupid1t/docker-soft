Rem
Rem $Header: rdbms/admin/catpcnfg.sql /st_rdbms_pt-112xe/1 2010/12/10 04:42:58 sramakri Exp $
Rem
Rem catpcnfg.sql
Rem
Rem Copyright (c) 2006, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      catpcnfg.sql - CATPROC CoNFiGuration
Rem
Rem    DESCRIPTION
Rem      This script runs required configuration scripts to set up
Rem      scheduler and other required objects
Rem
Rem    NOTES
Rem      The script is run by catproc.sql as a single process script
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    sramakri    10/12/10 - bug 10153891
Rem    ilistvin    11/13/06 - add catmwin.sql
Rem    jinwu       11/02/06 - move catstr.sql and catpstr.sql
Rem    elu         10/23/06 - catrep restructure
Rem    rburns      08/25/06 - move prvtsnap
Rem    rburns      07/27/06 - configuration scripts 
Rem    rburns      07/27/06 - Created
Rem

Rem Resource Manager
@@execrm.sql

Rem Scheduler objects
@@execsch.sql

-- Scheduler calls to AQ packages
@@catscqa.sql

-- Svrman calls to AQ and Scheduler
@@catmwin.sql

Rem on-disk versions of rman support
-- dependent on streams
@@prvtrmns.plb
@@prvtbkrs.plb

REM change data capture packages
REM prvtcdcu calls into prvtcdcp
REM prvtcdpe calls int prvtcdpi, so prvtcdpi must be before prvtcdpe
REM NOTE: must be placed after Streams packages
REM NOTE: must also be placed after Data Pump packages
REM NOTE: must also be placed after internal trigger package
REM Bug 10153891: do not load CDC packages for XE; modelled after
REM usage of server_edition in catpend.sql

VARIABLE prvtcdcu_noxe_filename VARCHAR2(30)
VARIABLE prvtcdcp_noxe_filename VARCHAR2(30)
VARIABLE prvtcdcs_noxe_filename VARCHAR2(30)
VARIABLE prvtcdpu_noxe_filename VARCHAR2(30)
VARIABLE prvtcdpi_noxe_filename VARCHAR2(30)
VARIABLE prvtcdpe_noxe_filename VARCHAR2(30)
COLUMN :prvtcdcu_noxe_filename NEW_VALUE prvtcdcu_noxe_file NOPRINT 
COLUMN :prvtcdcp_noxe_filename NEW_VALUE prvtcdcp_noxe_file NOPRINT 
COLUMN :prvtcdcs_noxe_filename NEW_VALUE prvtcdcs_noxe_file NOPRINT 
COLUMN :prvtcdpu_noxe_filename NEW_VALUE prvtcdpu_noxe_file NOPRINT 
COLUMN :prvtcdpi_noxe_filename NEW_VALUE prvtcdpi_noxe_file NOPRINT 
COLUMN :prvtcdpe_noxe_filename NEW_VALUE prvtcdpe_noxe_file NOPRINT 
BEGIN
  IF :server_edition = 'XE' THEN 
     :prvtcdcu_noxe_filename := 'nothing.sql';
     :prvtcdcp_noxe_filename := 'nothing.sql';
     :prvtcdcs_noxe_filename := 'nothing.sql';
     :prvtcdpu_noxe_filename := 'nothing.sql';
     :prvtcdpi_noxe_filename := 'nothing.sql';
     :prvtcdpe_noxe_filename := 'nothing.sql';
  ELSE
     :prvtcdcu_noxe_filename := 'prvtcdcu.plb';
     :prvtcdcp_noxe_filename := 'prvtcdcp.plb';
     :prvtcdcs_noxe_filename := 'prvtcdcs.plb';
     :prvtcdpu_noxe_filename := 'prvtcdpu.plb';
     :prvtcdpi_noxe_filename := 'prvtcdpi.plb';
     :prvtcdpe_noxe_filename := 'prvtcdpe.plb';
  END IF;
END;
/

SELECT :prvtcdcu_noxe_filename FROM DUAL;
SELECT :prvtcdcp_noxe_filename FROM DUAL;
SELECT :prvtcdcs_noxe_filename FROM DUAL;
SELECT :prvtcdpu_noxe_filename FROM DUAL;
SELECT :prvtcdpi_noxe_filename FROM DUAL;
SELECT :prvtcdpe_noxe_filename FROM DUAL;


@@&prvtcdcu_noxe_file
@@&prvtcdcp_noxe_file
@@&prvtcdcs_noxe_file
@@&prvtcdpu_noxe_file
@@&prvtcdpi_noxe_file
@@&prvtcdpe_noxe_file


