Rem
Rem $Header: emll/admin/scripts/catocm.sql /main/9 2009/11/04 06:43:52 glavash Exp $
Rem
Rem catocm.sql
Rem
Rem Copyright (c) 2006, 2009, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      catocm.sql - Create and grant privileges to the OCM user.
Rem
Rem    DESCRIPTION
Rem      <short description of component this file declares/defines>
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    glavash     10/30/09 - remove set echo off bug 9073306
Rem    glavash     07/09/09 - remove 8222370 changes
Rem    glavash     05/28/09 - randomize password
Rem    glavash     05/28/09 - remove set echo on
Rem    glavash     08/19/08 - change password on account
Rem    dkapoor     08/03/07 - grant specific table acces
Rem    dkapoor     01/04/07 - remove drop of oracle_ocm user
Rem    dkapoor     07/26/06 - don't use define 
Rem    dkapoor     06/06/06 - move directory creation after installing the 
Rem                           packages 
Rem    dkapoor     05/23/06 - Created
Rem

create user ORACLE_OCM identified by "OCM_3XP1R3D" account lock password expire;

-- provide correct privileges
DECLARE
  procedure grant_table_access(p_table VARCHAR2)
  IS
  BEGIN
    execute immediate 'GRANT SELECT ON ' || p_table 
           || '  TO ORACLE_OCM'; 
  EXCEPTION
   WHEN OTHERS THEN
     -- ignore any exception
     null;
  END;
BEGIN
 execute immediate 'GRANT SELECT ANY DICTIONARY TO ORACLE_OCM';
 grant_table_access('DVSYS.DBA_DV_REALM');
 grant_table_access('content.odm_document');
 grant_table_access('content.odm_record');
END;
/

