:
#
# $Header: obsprm.sh@@/main/osit_unix_7.3.1/1 \
# Checked in on Sat Aug 19 14:49:45 US/Pacific 1995 by wyim \
# Copyright (c) 1995 by Oracle Corporation \
# $ Copyr (c) 1992 Oracle
#

#
# obsprm.sh
#
# Brute-force way of print out obsolete V6 init.ora parameters
# Return code is 0 if no obsolete parameters are found.  Othewise,
# return code is 1.
#

trap 'rm $TMPDIR/op.$$; exit 2' 1 2 15

if [ $# -ne 1 ]
then
    echo "usage: $0 <filename>"
    exit 3;
fi

list1='
bg_priority
calls
context_area
context_incr
cpu_count
db_block_cache_protect
db_block_compute_checksums
db_block_hash_buckets
db_block_max_clean_pct
db_block_max_mod_pct
db_block_max_scan_cnt
db_block_max_scan_pct
db_block_multiple_hashchain_latches
db_block_timeout_write_pct
db_block_write_batch
db_handles
db_handles_cached
ddl_locks
enqueue_debug_multi_instance
enqueue_hash
enqueue_locks
free_list_inst
free_list_proc
gc_sort_locks
instances
language
log_allocation
log_blocks_during_backup
log_buffers_debug
log_entry_prebuild_threshold
log_io_size
messages
row_cache_buffer_size
row_cache_enqueues
row_cache_instance_locks
row_cache_multi_instance
row_cache_pct_free
savepoints
scn_increment
scn_threshold
sort_read_fac
use_row_enqueues
user_sessions
wait_for_sync
mi_bg_procs
'

TMPDIR=${TMPDIR:=/tmp}; export TMPDIR

OUT=$TMPDIR/op.$$; export OUT

rm -f $OUT

for i in $list1
do
    cat $1 | sed -e '/^#.*/d' -e 's/[ 	]*=/=/' | grep -i ${i}= >> $OUT
done

#
# Special processing to find all dc_ parameters.
#
cat $1 | sed -e '/^#.*/d' -e 's/[ 	]*=/=/' | sed -n '/[dD][cC]_[^ 	]*=/p' >> $OUT

if [ -s $OUT ]
then
    echo "> FOUND THE FOLLOWING OBSOLETE V6 INIT.ORA PARAMETERS:"
    cat $OUT
    ret_code=1;
else
    echo "> No obsolete V6 init.ora parameters are found."
    ret_code=0;
fi

rm -f $OUT
exit $ret_code;
