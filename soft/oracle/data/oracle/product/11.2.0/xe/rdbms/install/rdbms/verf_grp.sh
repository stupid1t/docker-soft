:                                                                               
#
# $Header: verf_grp.sh@@/main/osit_unix_7.3.1/1 \
# Checked in on Sat Aug 19 14:47:42 US/Pacific 1995 by wyim \
# Copyright (c) 1995 by Oracle Corporation \
# $ Copyr (c) 1995 Oracle
# verf_grp: Check is $1 a valid group name in current system.
#
# Expected to be called by rdbms.vrf. 
#
for grp in  `cut -d: -f1 /etc/group`
do
if [ $grp = $1 ]
then
echo $grp
exit 0
fi
done
exit 1

