create or replace view sys_uncompressed_segs_base
    (OWNER, SEGMENT_NAME,
     PARTITION_NAME,
     SEGMENT_TYPE, SEGMENT_TYPE_ID,
     TABLESPACE_ID, TABLESPACE_NAME, BLOCKSIZE,
     BYTES, BLOCKS, EXTENTS,
     RELATIVE_FNO, SEGMENT_FLAGS, SEGMENT_OBJD, segment_objn) 
as
select NVL(u.name, 'SYS'), o.name, o.subname,
       so.object_type, s.type#,
       ts.ts#, ts.name, ts.blocksize,
       s.blocks * ts.blocksize, s.blocks, s.extents,
       s.file#, NVL(s.spare1,0), o.dataobj#, o.obj#
from sys.user$ u, sys.obj$ o, sys.ts$ ts, sys.sys_objects so, sys.seg$ s
where s.file# = so.header_file
  and s.block# = so.header_block
  and s.ts# = so.ts_number
  and s.ts# = ts.ts#
  and ts.bitmapped <> 0
  and ts.name not in ('SYSTEM', 'SYSAUX') -- exclude admin tablespaces
  and o.obj# = so.object_id
  and bitand(o.flags, 128) != 128 -- exclude bin objects
  and o.owner# = u.user# (+)
  and u.name <> 'SYS' -- exclude sys owned objects
  and s.type# = so.segment_type_id
  and o.type# = so.object_type_id
  and so.object_type in ('TABLE', 'TABLE PARTITION')
  and bitand(s.spare1, 2048) != 2048 -- exclude compressed segments
/
grant select on sys_uncompressed_segs_base to select_catalog_role
/
create or replace view sys_uncompressed_segs (tabowner, tabname, partname, tsname, tbsid,
segsize, segobjd, segobjn, segment_type, total_indexes) as 
select 
  uncmp.owner, uncmp.segment_name, uncmp.partition_name, 
  uncmp.tablespace_name, uncmp.tablespace_id, uncmp.bytes,  
  uncmp.segment_objd, uncmp.segment_objn, 
  uncmp.segment_type,
  count(i.obj#)
from obj$ o, sys_uncompressed_segs_base uncmp, ind$ i
where 
  uncmp.segment_name = o.name and
  o.obj# = i.bo# 
group by 
  uncmp.owner, uncmp.segment_name, uncmp.partition_name, 
  uncmp.tablespacE_name, uncmp.tablespace_id, uncmp.bytes, 
  uncmp.segment_objd, uncmp.segment_objn, uncmp.segment_type
/
grant select on sys_uncompressed_segs to select_catalog_role
/
create or replace package body dbms_space wrapped 
a000000
1
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
26b2b 805a
c7LJDIQ1qyAZBBA3MbwSFORbkVowg80Q3sf9rcKPPyqq6XlZv8ig2Sehn/5HWqeGzJGzi4KT
kQNaLJgtrw5sRTt4e+SiRdAf+Qht21im003F8sJVACtUZZMCgvcfZbb0obD4UU5WDErG7Xj6
KQVtgeBzOELC8jnpJREh8aLkE+1YkaGwBGIj1gaVBjH1ek9W6JMf8i211OscjictDzPrEmGF
2oxWzCSW2uu5Tv/1nYERv8SQ/GXWh+A/mzp9VxOXnIz/Humq7AFd2f/Z/3C0rRzJbeAlKcmb
bBKxInRfx1cWyXxp30rewS4esMaYfnb6U65HwUs36Nra0W/WNCRhSeXB+g/+fT52KMZVH5WP
tZHbmDQkBMlETLvBOjA8vakCQBgqUgDgEpe97IJBjnfHpzDP5E+TvzmmQioSvHUuAzTG8m9O
qECPvCdfVgA/ATX3xYKFbZNJF20Xg+RSnEigMCrNbd/vzS7U1s9gVvM6gv1fZcw1z8mEiC4l
uDpCr4ULfwuZgqB/95IpdDGX6tFuv0/QsAZOCDB6R3u+DW53w0Lr6UTdqIXDhyhouUERKnoj
wUk8uTiTwZk51VHHveYi+XBzuc+jfBabPbtJmwUhE9ewQojR+0WFkpn53SddyE6dZ5xfFmbb
0weDr8waJwpikgKa+vVo1yNO3qc0dZTGTuAVCsQYijEW2RdtCAp6C7ZyKgcut7jG5dMX11Pc
U5cagpCyydZSuVzyI5OdHVmoTygH3kKw4VONe0PnvC90IYhyF0ORFPY0WAGU1UdOQveBJ4Fr
5cO7kbQ4EdfVAWQXph1w+Mp8YQrXW5jpMocoXJ6x7s8XZc16/TK0CdfC3k/0sSt4XFOxItIe
zlqDJBz5/co+hTjILQnHcNkV/I3innfgmQmUwRhISRfxzyF1WDw7rXm+p370xTsBF2bDAgWH
2V1tVsnT77n5MHv8R8kUXk2jswGy9yNz4GSHgDTmO8szHp9LDwPWgDLaMMMD1sYy2jBxZB1A
gCoSjypu0WGMNemjjSl8qt92LLUSaYzlHQWwo2WD4lSEpE07dHfDRMKxt6+gySxF3KQNyRNK
aTu6hHnWOyGs+AS8fdwD/195A87/vNrJBbG0b8vyCM2qpC5rTpuIEbV2JMX69hZopHJOk2Ml
tuhyTnNCjRUyzFqK1SD2As8RAzJRK+lIXjZNOmBXYVbCoHKc8gp3VOmTjEDCK0tzAE/JIhNK
B42ngC5XCk0gbZvP9wRx1SvghhxdbaF4hujjGqNjE05dvQUXyeucWGEeXZfODrp1LsGTq8QO
S6/RnmEQ/xKVOyewFCU+0eEWg0/BTHWdVuRlE8uSIG+VTinKqi1wq8cgUAsML4TrS+Q0PfD6
bwWerW2TF5JWE6EnZNsj5ajpkemJr6Lg5s834XwgLPHlDCB7QdKSMyPdh7TC8wpPym6Hn0BO
X8nn7H6X2/EE9NpHNa85mjfzr/i9F/R83/bbr/tq4ymo6qt/HpA9rf9BssBMsUAT/K2Usput
yUKWV35LHJNOt+iZ3NxPeer516VODYg/uacQVxFWLmDfRWNvZmPcxCYCgst74GhaCEWwM+zo
PCOt8EZvG7IdALThlg7e87d1akwa1wfvaLFZEA8H6R4D7961MDD2Wvw3fCqlobNoKoPKaFf1
dWFjUqrtngxowGFj7tjiVdgYMdgnOnPszbuxfrXM1zMsPX61UtczLLH2T2FYPSFYEEhm6tRh
uu/1q05eLApeRi0mr2BRrdZCJq/ljqhZJ8io6R5Z34FUnIIejVENUHTnUoi/tbVJOGeaXwIP
9C2lDC8Nf4IZqaD9lGnFbFCVZOOBaRpG3wbVbqMvbKMyb8UyPKkUizm+tpaSZtKwmvtO/Bdm
/JYGT2KLhr1404gqTv22OtUSdnwn6qWt5JMaEwfUmkvOExYaoqjUtSMVS/Nq68OMjw8r3KPP
7hMfm5/tyBRhkhnGpx2Xd6wJJ2fI/Dpjsy8Mfh2i1jxXj/DdPXUEjY6K10652JXW0eXYzfWb
eydVuwFVOBfs+vyVX/MG4Gi7HzFZxr7y23IS/OkGNKvJvv2k0Yf03E1hKErxTFEBMwRmILDP
P94lWewmXLuvQbiJac3NJjNwl4r4fjQvC97X6NbMCnUsE0BSVS99UEsco5SDebyRaWdikJOI
v9fxGVKdVNKSmbhBMhjzRkXQpuoUsgqpEISI9CJfyuGLx7ww2wj7XAnbwYw5z+mEvz0e78MO
lw23fD+FfMm5vwbvqXULzHbHjzg8kAhllUplybps6Esc9KMvh6JQsT/soNubjAfXLATLig0x
q4oNBeT2BUjcpkZfieDWLP7CK7sxy5z9Lnwark7JRZzfx3wYdBMpP4PPIjAzNFgi8pNJJhfa
TF9Ku8Qznyy1hsadtdJq2LSzBxZuzPWLA3/E6x8dYLwmbrVNTqO6e84dPkf46wiFiPxSF/u8
AwJX46+16PrYzkpzewMjOpDmOX8yQBOCQNErWOpygkDPfjQDBiD69xXC3BkDAASVyx/TgMD1
4qW42W0Jhs9XhYIZzTRwMhMGNevV/4/REyhiOj9uC5J9prRLsT9NzO5pTyMIy2F16n2pGPjy
0Yjrye3qxujpjgiWLZp3aj17MfYFANj/BA2BEkCu4URur318sn/zkpRP44HlT4bLs9V/PBdh
ElQo50pl429ugq5TCBfOcNBzGO4qHfc5Lz+V9RNCGR5dVXgZCjftU86U/WBP+gkq080IIFTm
NpgabE9/B+y0y35fb0q3BFDQZkSEW1HZijJ4hqORhLd0Cw2CK0VatA+Gkrmk0BhEhuy3On4H
c9GmMbouTACtmFncmry6ltyaYj+TeW/ZgZWLzSNSCLkjDeBxHl389TLIIYYw+SleCeR6NslE
LBOhwC35AlzxcgMp9Xcid2JZ2tZmt8em813i57cQO8b4tc/kck/5vLWu1KCOWol9dq60omIc
0Q8RZWUlk7fQQiF20EknVnN+PBC5lx1UW/R1Jt3dww6TGMjvYgBqarOWROyhf+6J6qChKWmC
coqBcBsrMaul/jow1FKu9m/RlTgBlcttyFpUR4Bia9mIEQxXtGI6zw+4P9He27M1N+KXAKoa
ykIRyTiMeXTHrfeyB+jRT5FqJZ6OkAOhmygkdSpml8uSodlY6x3WeK4C0SUzYO4dwxZiKDJV
hHF2C5D55N3elVYfHHCKgsqmQEQT12t4hFk2kk4dHyar58rdgViOR8YuvxlKTUulFv1viByk
B1bxKK6Q0RAO09eGORwjC2DIcJCcIQ32jb+pAR2fUMbIwOwjGJO3eERRC8NuYND9smCPkCTL
+P1IFHGwdkiOTSknzCN8xxsOZ9Y1mcM8n52CVn7AOE63+G2kLW2q7IoYvYNU+4AEOBdrnv5b
XmFsMTGzKMrO2/Hnh1s7NRKAFoiACTGl+AhgdC1BBz/zmFz+UIFeH9hdGGvtlD2Fr8yKzWoa
m7Pr3aO2A/EE5mFoFiB/Il1qYqbdzFM1eeOkdnQAAhepjgf8BMRetEF1B5zhshQZkgpNuNCE
Dr9a7faTtNfzyPYVyl2ieuAN8tZBRrkOoySSVzTx6I0n8wxnw2CFDwmdnqjDLPJsX2S0cemS
9Pd0r679XPrvIkVqOzWgHoyqqZOY9VAR21wcF9GopIUvbs4ZpEptX3cIjZTnv8ck3t4NgQft
VXPGm49EYbgCR1Qcqt4OKHtWQbFA5WmuXewRoRNGYbopuCDebvGfbFhY5iLD3WAT5pPdVtaz
UGMjQZ12Yc4GDbnE65HEYsPHk9AKXZadXlLZ5rVhiczGrPEwQCgXJFh+vmvkc5GBOMefMiZ4
iUsTRjtdQ5gDTj2jzWwMxPHJSTGJ+Fr5dM1m12HMN25qFaP2Eg1GQThksNtAP+60IVUCDcLj
V65j9+kkeVQ3tTm2dxzEIDHLbvDloIAwu4usafQjOVwRRzU5f0jSePMN1GjKNPvCKAOv1nmk
5dHTn1/CMfVQYvE64wUzq7TD/kxHK1tkY2J5IsDN9cBi/Sf9a2A9GzLqNK/uICrcBG9EO+OI
s3mljXym1Hq+NeVQ03phygk7ffuCjpztowUUndTixPBO6LheTkqA8CU0TkjNk79I8GbXZfLh
j04BiogDyayo79Ma51y3ZQtoXDHRdXXvbnhZZSEDKte+5DxR+wfrwF8sy4eRoU6v/tfyR/48
QrvAEFKcbuodQMm8ioE4MDWto8NMswuRVOhdEb5SgYKIK9b2CJ8QMyQKmHFfCw1OM2/mvL2W
RMH3Yj5Iwj6oplUBJ1Dx3NmXlcrqLSzUCUo82WLbp8O6EKp6oIUDN72OMxbCng9cxKS8vHEj
+pOUoSAr5NiK4nyYu4kXnl6Wjpt6WoCDxpHZgocE7AnmsqNjnhgfpf4gwgRsdr9+VqBDa9qv
SZc89xSN7lOQES1fZGy6LKvvkK/3Uz4CMNbDRVjlrmBtphQChkkct/cDHNJYFVqtQPICn1Tv
VqmAoCKwlwuMZPq2Sf5JkjVt69jAGPaYZffDKEcO+QvMJLsAmVMOX6lmlFlSRdUWjimgEqXJ
dyit7X2gI8+nkaRVk6q0QitGD2r3m187YI8NhozbvC4Ug/2nOiAgFweG+2XS+ohOqbK+8Ts3
KwVdtsaijJcmxMCEeewl0gxTo/Vw+0ciPQPk3Hx/CMOuTPpNQB4ovwGUQ670U158NReFC3d8
3+SGr19SsKd2i4zfHrixLVUMJ937fKftDtiz7U8OXM66JpPgyKtpgC+jzDuNfavsLjAwCprf
hH9GXT18eyeY2F6MQVM7cJVNoIg140i25g6Z31gbRT4oicnFgFKPoLYfE6xrNUAOFFIaGE5j
jXQRTSJPYJe0afhLNW/dG9n3YOa5kGUQ++tLzXHfeSf8w+ydd5KTKsrpFbQsvb8IWXz1uPJa
Dcqsh259drQANPoY6APsGiAq/HEybDQBSjMkWh8sV7p6IEEMn0YT1UsRqLpfFW4nHJksAVC2
ttKOj54VDoX5Ig5gJncbARqgIW5BEUSH5SZ27ORkskmdYLB9Z/G0CBHbxfWPzPNtQM10B20p
epaFDosbMFjss0cfhuptzElPv0B4EQ4khRLW5JNwdGjGs9jINoTXboAO/0q3+nDxr0AmsN+y
/GNcCavGiUishEgb64psqjiONtbXFbMXZlLI/czbh8uicff/XNVPF3LU3H3rSAUndY2FDAxZ
8gxHbjDoqrRsoDPWBXzri2p035YMXQjTcZbLpwR/lSqo4IxFC7Um8mv8NhD6sNwQCHcYXIbl
JuvnYV+b/uvx/pCP+PqHWYgBu/4MGSTFQrVX+T98QckkgJqd/HTJAbsft96PEAdmy+dDgSwJ
bPjWbha+gKJoPnSKucaHIhg0z1gu0K8thGljVPJXlWoLoHuCYKfdRN2BhOjzYW3MKsv63zHP
CqD6GFwgtl8jXKtD9+LDpK2qiA5HviKu0NzpGwXzS5AGRE1vlZmWWZM0l4cXVa0kWBWE3QHK
V0sAmKB0eaQstcHcF98TuWMSWToX4BxUsiQI09Xt0PJOMK3luCBbAYjWnl8pUit9pAQyo6rb
GOhQe27CCBNP8B+pEWKwPAuTCrIjiQ9tO7v/TS/MEOSCjAktuv71StwfIe7zthrCfrjbaQln
ljeqegcwCOSVU5rQb5q/GMGzJdBp3huogEi8dIBMRC4N6xKtjylT+2w24BqhCOi0Qiqkz1sg
8MgXLG4ZUn8trHqhvycNX6dhWiP3b3FDBxDQibn16T4znSwc3HCYq8vQPDqz0B/Gd+Et9HQa
/BO6b52GJpVSofmnAIKW9G8wpHDnWQPbUw8Z/YDCaSClCc8tuqloSU0Z86k8J0QZ/B/AryYr
3R3h6XtC/6LVJLDUac366133AO3e0PprhnjsvHRkUM9n63gG2lm/VkUiatZi/3ji/my+GZzJ
I/p44sSksV6i/8OnpxScHAUVy7RBA2yaDRAvKdGI35eTwPgDfCqb9n+po08U5OBAccgomtUN
nX88ZHUtI9yI25G+4XsW8ex23zrU4AtWPDlySIj2Vel0XBopZ6qdImGfHxichAciMls+K8I2
WTsyzmp59nzi4bbvSyCj2Xjx5TUJWuWqGy25vVacfrV79W29H1kAjVozEwTYNQM9xKuYfFTc
4pJynxT96NmdS6LS8hmpl+MdTr7FmL+xUQNliD+MFSDDziA+orWJGkFa0O9yZVtGQdCAhkIt
dS7aQqhWpAeydosmFRwNu/haQljppirAJyyhH9/LYC+5yWYOIiXR5UpR3YCQsWL/WHC9BOwa
+moAlARlffQEYh/MRTp+e24ySv1XftVAlgeyZeDlapwz7hyUBaICTL6wLAa3kUR+sSJEWlgR
+zpqykH/5vCLIzK8ezEUE0OSDx6kzphX2i7NEabnD2+76p1k3CLmcNLg7ZOHINP5vy2tmFTz
XDwsmuJ4sTsKxLUNA2h+4iqwh2Q/vpLNwDJMhwEsXL5x1HutVTgytHpvEyQi2I5yyQ3OsSU/
IYb53ei2fRHHDVdCnTRIrVyy/hk0TFvlvNqYaL5UteYdp500RYdi/9GaTLPpVw+d5DA/Qfwk
otqx6h/56+vE2ZWb0rEDgT2BrJZrVFqeFKrYvz+YIvjLGJa0Dtxy0aqTan2Xl57dZLYzqieB
ByuRkG6SO+QzPzLtnjjs26Ln3SChjgAAuiD/0a7fOyix6knP3xewzd00geeIy4U1QQP8UXKR
Ovc76BGps9f7C73Z2evC93xhKPZjvPEqptVZOvKVMqEzbBnR1u/VI/zy+8Lhu6PDtO/smCJM
TEkgtVBX9hFlYCfxbi89SbAKgfRMwId8nzZHKJUIzEeXF97fxOEk07gKIIr8mcIKxWjeQsIx
rb5FU3AwdSc0ma7hGJnOqyBQknOd4hnt1WQDu3e+VmSskNyx6p1MaIlSSHJJMeMEG+L7h9zV
sdWpmvyb9T7yS7uadeLgUUvIzBsYmXaTdnWUwSBsFwAJ5DOfVNhVZGoezT+yEknsOAJSQ+9U
Pwo+MAoPWl+Z3zI6HkKZl2KWlmTfdk+J1ifaf7gd4k/3z5ATsZ1/M5avtL/rJ4Po7gbmHqKq
nSmQmtLcr46doCv0v//kGloGSN2kgh1juCto6QzLQM1ufciiMnM8eQcpUyvak0sUuC61cTnM
LflfSDhljYvzkOEBW8xAL5TAXyOw0HnQp5wsnMHkyA2TIxCHRuLrrSQPcaqR8h8KY1bRbFw8
u2Q/gm7ygejdPWEXa8zTpHGKGIxAmJKK8iYNsNRFGM91hV3vwkapgXUsWC9hJDj+tmCdnsHH
j2eil+umhATDl3JvGnwoSPFBY1XtoJhPnyyTvQf14gyCBIl9wnvUbGsxCGlqZAGf2MUAmFZX
P/PepQ1ZpmCE1w5usdtCfrJoXKUtpPJ/pukgu3VjujU5ajnQRqbcTNKBG1QYFU95rCH5CUJ7
zYiJ7wgXE+am1nPr1/9uYRVyZOEqICzVnZ5GaOEfncsh2Eo3GpILd/tSzsyRqA0taaTk1nFE
6VP7P7ylhXYqXT94V8wwkcsfGN3qfX36XrO3Q8xGt1iKAnsaKbCUoRZryu0gt+31wk/ctEqf
m1TYmT8l3dHeBTJ6l55BsF/B12ctNmUQrLhjHNeRWdP45x6idkKLsB/NHYbHu/RX1xEp4HYj
txDjC6mhRVapT5cLlBsSMEL0/025issdVDsjDv84gD01oVWG3mxI6ErBuVM3zVuAkWk2TRUl
bZ3v4k6gnOUB0do3bwXNjorzmHnVKWS5dwChIDgDXCW8Al+NHfVKp8Oy4UqAmcAPvhZX75l7
sQn0UbdpgxdMsmztG+vH/KwWnA9LR45IJIhfZjEiMrO5Tv/KB67Iog4WFUQ6Qjz6Fiumy9gr
FmvcLXtYkLqE3+c3545KDQFgDbuakJaxu50gYb33jtaL2zgVd9t+4bJHecIsltuweEJ2jTWz
N0A5OGTPuARdBlsXrtidUyiFnIQUkooIqxIhSM1OnPjS2YDkQ4yE7Hfp0pnIU5BqVB84X6Js
e7mD6cqVpnZv8iHRDVXXPS4xS/C+yjsR/9CDv5xcge7ijCnGh4LglMrqAWuVzX/3fR3yfcfS
rs024FzOCn7OfMMCZw8Q0SRU+tQLkFvKVEGiEQBixKP1gT4Nl9xgj7Dp3/Q55rvo25gwYaoK
JE5D+yhn4E8lTNq7shysOmYYHaytQ3l27oBz0PHG+Qa6angPHE3vDIdqGdg0W6DNYuCIEk0/
7DN7s2N49kKddITPP0NHBx49ZBOBqKobLQ3y2nABIi8fPano2RPhEgM/p2q13ye9mAPRf+sN
In0Xt/LHVOK1bCepYJf9iJBMvNoRP5LbTiCpAvhdAM9E3H7QN5yHGINuA6VbLJXWvpI/YrxO
lNhlUUM6Oj36wAN7TNgkVq4resGR49QKoP5SLngBFp3y1bEKt05DMZWbzToYJ0cLmVyDephF
h3P+g0haxaY95bVhegUwgQLAaRlnJSfc6knq+oh4+5x92s1a3GwNncBDjBlFQntkGd1C8kj1
x02sorBmhKiXY4Ya6VHfH3THoqXJaTlc68329BPl4OjoML0YjWKsLtWDSqLfzwvnmmZXlyfh
QnFWAnfFRgDR4p2KSGiRUfi6pbetskLL6BRxMzNMAejWBXVpR5PJ8j0zpzGvlaV1WsOfEZp0
nmwb6/toDFZh22XQWKqGxEbEdG+whyaCKJEqu0ggxyxNurCo4sYu2JEO+zluX6+YbqkwQ4T5
0j4GxTgMws+Z3BGHh/8aGtSi1DilxSdAbDKe7lBv7d3b2WBsNEM0NEYHl0s/EWxrXrXYWfWQ
hyCzAc9C3StUhz+lwLOv9HA6r73+hyuTuM/kT0I9rO3yRLCTsATBUw6Tx5iPDNQg42+k1+pJ
T1466KzqasKUjWBmCIbBTNbTzt/yTAYsEsWC/W7PZ+l93/W5Tdv/tcniN8oSs3lGXoGbMwOx
1uArL7v1uWI6hGzUx8CGDPZ+CSmXx/hAx8EWSsfzKiMO0J/AgmSTB8MdeiqpLio2XrI+jQWg
N34VcI6H3fqoKzRa2AMEg2bnV0IU9wM3ch5UOSJhsEjcxmruUrD4UYiWOSwzt0IZ/ZyOigFz
fnvXI6E9QAOzkXrNWZqJiR6fRXID8WXmZEJBMiMxoBoWE6E8Bzbwn63qNdnGegGfCvpwGVid
SBve4SwxDhOLVneol5fYDdtPB5qCzaFomp+RCtZr1wvKdIaOUT5wHZ8tRWgUEwqiOWkIfSa/
2QNOPd6sv431gus9wBAyvZ6+BqJ+HZ+BM53cle9njj1AbAk71oDiydjb1a2MeeiMwcwy0+U0
lmQethO8V2jLrqi3wavJGnMhWWsw66TJxtZxVbzdqZJ46UAJ2tP5SZM9BRtGcNjG4FLiJfPz
B+RWnzgtvcHh0HoSbJ8qCX6k3r3o3O2V6FHi73Wpii7yOR2C7Ag6hE6ZG3OL5KMn5IE3iW1K
6/YeiLSe6Isl6d+NvCXCqEm9dl7XXiJGwzP3o9bct9H4l2Pjpb5RTp8mdhEnAd5g+W47bCVZ
FG+fV1juK88R6SXTVFjytGOrVyZGIXLz+9mcng0+lC8ySmE8ZmWNCrrTBSuKuLi+h0wgqQJ+
GWTT1Obitedl88Hg5yCZ1b3cVRfeSXXLGqfieKMeHPxDq2ZSrh9J2dnrA+tDi9HRDmHEhNGA
f+rUXD1PgLfNXBFArFoeoI6HXuEp1sO97b+exKh48vfTlak8UEMSQjR4JeYBPXPLn7K/S4hL
dxEP2YuYj1GFI5SOzU3ic5OiK4RPOXIN2OmzqwkcqdNJiFQlev/KB1hB6CtDUQbiFZF0j7lw
IvQAqBuPRYazkXXXXbNSLpv/znOcoxWeiuabNMB6UrDGDVO5PZUHHqJOAw3uNLwiyc41JxYk
6RQcX4QljuyrwJa9otEaI76SkL1GDWX255ELI7tdkvyRDYdkU5sF44DQ5Sp56xezssvWp5b+
qznzSlrtfRNRzOqzTakA3+yO1kHQ0cSM4XfzgR8suQmDmhuNjVBYl0GWMgepye5MW6gwY6uH
LaZBKiqc5qnqya977Fq9zWg4AQqroaWe7RTKHpFvHM4h7GciwJZbKLxZw6wuHdepfisRC3Sw
TYpYU+qg85H94T2VB2gbZv7mkT7Fb3R+pnb/eFYFSrbBxhDf1jSc7f9wmoFaf6RCWMEZuYaq
5M7+T5+Ql3uz9MXem97fjqkw65/lzzOJKJW+KQ6pHn9fipzU2ju780AAOynznCIRpNAh5zke
iO3w2BdrieI4TIT2uZUv/HW1If5PZe1XHgKgZmX+nTKkKN7cfFAojITvQDmANXp8+cPtdX64
Xudqec255At4viAjgKj0r+db7Pgymg9MOzVszjBC4fI3yOIllwNhil43b8nqE0OtTJDaUbLj
8ssQZ3D7EzwHtmmm/1kBdSMFmOxv1+pqwb0dQYL2dNoA8XAL5JGcka2LJtSk1Rg5kD9lUgXs
TAhLzRb2e2FCXoyKV1+PcRRtZb9nU/NowHpIpsSUeNRC4Z2a0LTanxrlL0Hnrbi/n6NnKpRS
fDW2U11bcllyXBm3kQfL+i1ef0O5RSlyKPHtVI7aOUHiTtJahvcQBXoYfFDi3PAiKyMUVbSo
ReGH+CGh3tWr0Y1HZryGyfFYmuwnmRdYCIcDZ+Z6KNQJIZj3m8VPjrTM6zZIFLZPGDpNRBi5
4WBugT8vrVZHzRnFNwXH6IBOz4aWgt9yUY8o1Ot4cZyfZCxd/lBLLZTPICoPZOK3rygLLcQG
uzDx1WC+p2qmKMxd0vm64BIcBOevY4GOhMOcjwjuyo0pJt/PKS7cpGP5NnDAsNZJ5LT9ryTt
eManVRUS64wZdNTgHymdf4M3Y6i0K/sMngb6DDdNW7oQXLe6k1DOI1Is9/skde1MU9ZdF9J6
eSk+aK51DlUFedW8amPWZ5KEu8WuVcvxmeYbFmXunADZAD0taF76PXyxycUZ78VCWyC4+wGy
fOzhr9l5YLmfmo5HjCZbJZ+MsHQParYH51+vii+723fkeccBRgut/iKEr7lO2PgVARFn4J7X
VQrAfwPrbl+RkXfR1rJYD8BDhzbwmu4Dpvz5uZAflZEJR4fLKpdOURX6+JLaMNpaeybktBBY
doyZkNraWf4S16fNVM+mR10qm/MO+OAQnwgIbs17HxfqmeOL9qu36QyBXYcyEadnpjxeeeO6
hd5AXa5N072qG56fdWGidJI/9XLnDhfT52UB2rW3/gq3qj7dSVv6/JIjZIhFPHY1Ba+/X9Q2
p/hdS3XJJKrM+0FT088rz1v+EMA39vS4Xsjucl06E+BQAQVY6l5AhHxtQitLiIHC7thO3SWV
9bPePCmtbAUpkKCShApcitm637tqRkcCEySFLisTew3+bhPS2wgMMzcPDpcyULvx4E9eMsQo
QGrLEvsxSKRdSbO0e0CFYIHkPRHIv7C96lm/jjBVCQbUXBb7T6UMviIv0y32sfEC0PXzxOwf
LPs182Z64WZsg5EkgMDi2mhJC5yPcjey3VsE9xANa2SCJmAQiiUelTdIb0exsLx61LgrwIJz
8O2IXeT5aDH96qpq/2Ry4k44vX3q/IsDRp/mMYcbkqfr4yImx8rHAiU9Fv1s9RjqYGhC2CZh
zGBBq2PZ3oJanHXS2B/8YEPTUba2fpUtWM0PjBZHMtZqRexiXtH13nh9vQ1R/OscjZfcFhtw
eHAhBm8cPRtUqlN/J2McbRK89LxFrSIuceiNbYicEUMVzS9Q/15taSDlChIb7HaOe4wi2hxJ
DCiBQUKjA+zh5rl579/iGfmxbA+Zs+hZQTIjDkHSMIt/6D6k44Gs+QxqMbSDT0xIK5weA9cs
J2sqEQXuarVagTIoEJrPcgsa9KJiziiIGDS4iZho/fvpLKfF8EQTAWkBdl0Bo4zRs91/54OW
tr/ZZ5o4lS1Z3m70jFbYspUDl4lLxZ6n7evq7PNjEs4jgN2jhvC2bRL7HaOPTr81t/CkPdNv
zX/uuf7qLt26ZrziymDrUR/lw5ff2LhMdAVdqvb5QMoKRB/87CGXFl/SRKYn17b7OdAL5meE
sRdV5TfnHnj2x4KEsqDrotb0BCd+cC+Khd3b8GLVgeEku/9h7nvfmCYnT/eKOXtVQVV9dOGZ
9+YOZnIm2SdIFd7hhKDRQAZ8DjGyqv6IMbLS/oi0suw8LjGysnDbxy8wKnI1c7d6gGtjcsdp
9hxyf4V2hq2r20ZFFv3jpXYDZ3yvkRLoau7U3Sshs8hmbsY5va9CdJH8wBwcTSEHaHpPkLXj
BD4Z/jN+TcdxoMOEk64JzuqVAn3zo3tyGtqWQYjfkSnKBpDJhL/iTqX5dP1F1ZBbrh7ofoUC
0SsjfBHwg9YSv1jIEH69sb9qykcSs/EsG+6f0G23W/BVnhKYY0lCAlHJ69NrZO5AEPDXbJVN
BslpRlDZiqfgq3YVIaC9moe3ggB4OzXUw3uefonJBw7ElAdDPnS8LwdDEJo5BypCBLwn3x6y
6+4NkDh4U2qOINXFEFJK/cG+w51cjSTGDoosNQ5027q4ER0YBe5Q/xW0foPYnsnryecBfoN3
sDGg3vSR6yv+UzDjLrqUxUverVTbOypM8WiDoCyPPOPYDFMN5jsvxDxilb4N1GlPupqKNruj
BIQ45AH3j9ddYnGJuN4TrjoDXnN0k8mQbLQr5bjnKmJwSPZzMhtygnYIoln6IBcT/itwTfTb
x+al4Rd6IkHdqErJ2nWCRuKxD0GQqjSHKgVrFGYRawxiYRQn69NcC1NJzNmixbpT99Po4kQ8
JFCn446dh4gQ1YxWXLFxmYy42+V7vT2Ibf0XQ4iU0U/o2saTQYFbA98TXmYkBdN3RbXDaKeT
FJDLMpbFEiIz4KJSf3jq9saBsL97zrx3K2ooHz3ECNF3RHX5YkuHKjFTvCg6EdAMpv7/GbDg
zOHSSMvRLCleTADzweGt9c4pQdi/d0bHANefamRHG1vrB9CAocd7cj30HSrCG0z6aNSXpy+a
Ef8Z+gwud7pzDaBjJgXTuv1z8s8zKiwC5rxhA7scXC/jfSchhQ0NzcmltK15k1KCdRQ/n/NS
xAcadYT+7JlYbrq2icsphlaTHJfHtxHZJVoR+mU8COECxmixX+BE8NwdLWTTeaj7UczWvwUv
3NQHN3GTgQHnX6AmG+abv3slqzvV0sm8ky083BVJeHHV9RGZTzjyaX0h+j3NAu+0vohxXMki
C5Ij+kiwGyQ52nuI3zs+Ki5gMX57feVerMXVgXwSWLKE9NQHMfU55rcI70G+1e7BTp/K0+Ys
k43Pr4NquBwbnpelRF7IVnek1p1vgwAV0d8WJeDrpP7CPAimObMJKpJq/wNmLjM/J2osPxCo
xpOQ8tpIqOyUCF2ElTbf7bd+g9dX9q8N7uxSy99RARwRJHivVhwk7JQz9PZkpmp2DJH2zFWS
oUo72g0BRQ9RI54Uag42nkS0txWW2Hm6yvKyw8+fbCf4R/Jzwc/w5IAcdSXKO88l7CzqQ3Tc
lUEmAyVmht/vULsTsuocbWf9V9BFO+F61cvTQkah9bKr5BFz2biB1XIC5WcEvaNQtwDAjG9I
dBPspVIJ2H22fZ8ncQCmlAH3IXoAmbks6uoH2VLVxU1Xit3dhtUMZGRBB41ojvB3ew87dgxe
gqP4NC/9exFK4/SeYZegJe5HS+uTNUdg/+FbqjE7AxuJ0dQxMfdKmceW7cQ3pr3JOrke+Bvs
kXyQwAw71Lats9/3VJM7x96jfbinYG39olJVK8dx08j602xkBpohHIgxbDa3qX9Sgjlg4GoI
0PEs7RITlyilxHZpzAdDHcY6O+xDgPjNsjdXn5plM/MyMIh9ykI9WIvkvGfdxujZUCbN9CJu
IEqLxGqIgKTdzfP6gx6suRvdCii8ZaafsNaIoi48qXCIdO+/jw6lEYyIHnKnh37FEsUtwxhO
COGJc0QfhayWBdhEHsbOGuZQy6zDydjyL54Dq4JkhlO2itgKCQ+nBT8CnmPRRnCRo4UWFZ4R
bPKN+pWTv8cRPyLHo/r7W4XTZdr2ZxfQkC/BRWoOashN6xA4Pgc81MI4YOPiF8iKrsVHCbMg
i4Z2NOaYWYCnJZf1cAZPDz94KVmIJ3JUqqwkGJFVeggdhf63CtnXgT06BuBCvuQBRDk0RLfK
taNWAkMuyzXQPhGY/H1tsIMHgcfXjpBdwO2SAbL807uZIU0xughVkRX+txJ9N/QoKBUtWuft
CUoMzWIG98SrFh7dVvBqfNXwDzmKCYK0g3BMUmZ1Uj39ZSQm0mMHirKdJY9C235+fb9KZT1A
3uFM+RTWlCKGloD0PVNEbk3Q5iGcxHRXYU9yeLZ7ZXGXXgWqKPNO65yZWrJPMr0TV9OnaS+R
yWQvT+7sNzTa/20xoB1C9WoyjJ9iOsUS1+w44NiV1H//Y4FM9UZ3A1voaBHIchdYXNG+T4/Y
eE2lTgjP/B+ynLsyGVOV/iwTe69p4xcxfggnyP7UZFNhrUeSgie5S7THt92WNsz6S1TWg35D
Bdh14RlYw3700N58q1dhYqBkCRImnIQaoUfdw3eJJ0arsoOBm8N3VcFZ1gurCAYxuM906lxH
yeuvf7pROTPxTQcnwYN3SVKTH9P1g6fdhaKBn5HUOJkdgmxgUiTNGRe1MyyatfioQsBpJlHl
3FdVLQnli+tKUbd/D4LNlGW2b4zR246B8fvLhWvZoU3ezPqpcPOobm0KTRVv4dny0t+QwUuq
uuTBzrSh59tBEUsYBRxG9gkj6LT2Dbuw0Ff4mNZg5qxY+1RnkrsRmbZAO+zEPUCVMKOmMQfP
l4OFYA1JzR7bALJ6BUHtAY/4++29EQoLR72DG3D90Y0xPyjcwgyBetVFXsGB0IsqHpMN9jpf
izrm026z/JMKZc1c380HdBVMhfFvMOpZRTWEwJb9xV6g/stg7w+Sc1jX9yCTIt+cWw76Hpz8
i5mbY//N6TzD3uomnHtOHzrxl7kCVuTDpM22dFMdO40Sodr7LY3dzfst6sYmTcLG/HvQjcY4
P0eXcmj14F/w/sbceNC/v1yAyOqHI/24Yj8Q83zn9tAmZf2jy+FlDFjyNkkLKcIXcBgtTFl7
40oFC+SgiZezGnSCYQhj7ugXRQIUiflEdLKXCgEFnBTGiR5l3UDxp5BUl7LjUlHRO4WfesUy
/2A65j04r5/bry+hWQoQ7koqbWWp7n0kBUxUYUH7+Ox+b6aDfDm5/HB590CAl3VgZ4CLUtBq
NfNUgNx2vr5WJrZQs+R0nSWt6KaLrYWrRlR0eLkj1PU7Sb3ycrBuSMmNinx4AQJuvBpueeUr
UO7UoD7XLNDSN1K6/yQLe29PyUDmBnogw6SanJ97FEm7ZDwKDF05mTqYigodBkjKU3jdyhgM
AN7grAn1haFQMUMPdnAXw4v1o5S39PWf30lSiPSVll8Kf6ZE1PmwhGDivawdUYc1PHr4fIHS
NcIX+x0x26hgF4/uyw9xWjgtXnQDfiWD9izx0aPdxULrtBqZhtLC1QPqyv/11JYLN3lvlRGl
5Dr5Czmps/0zhtStUsB8EtAZaJHi4m2nciucCvZT52VYr2yNqbcr4rpb7wKbv6YjgcbXXAm4
z7lu+SNhvEXcGS2a8GBO6I2no0ye0QbEV+MuJycxpm4sF9JiERP+icDaek2n92FG4gUJSr3t
ly7bxfKbkSe3qidzXAwUJiGUl3kTHLKEeiI7At8UCqjeGgaO4b/GPgG+wLZ7GijjyfjJcBTx
8yHX0t0c6+Ix03LHlZO/A1azGzdO7LvEHV4vf9bTrkayPwy8E3O8ezpUiJ39jVTRF2WXC5bC
ya0U2EwqhuI4SXS+7/QehGjO2S2HCURSbNmyOoLcKK4i4v9rVQroQUXR6r6TqRaqw/+KaAzJ
cEyzjDsOMOHbalvlsQd+IUgE4iSUl1vlceNhbj6zMKJrSa4ank7S3mqB9jlxnGCzx/VnoJme
PjeJuOSj21Ex06PaNX6ZS0gjZFjSk4eMpghzcLArKewTqGBqN4AFuayDx2rcX355DAvBZ+iW
MSDFdP1hFHorutx5RVr3a2Zcn/R3tMrIrnZJgYF8fCb1hDCPwxUnx5tBUn5jHHFkPPgG4YuF
rUSRZFyocCaXvBF+mUO67kCU3jgelZtm8QvH8/Q0A11UmpMluGZKSvEmkFnibvcZdnAe0CCW
/99ZfVJNKTG6Um/5EL2B96ifOA0yN+JAnZz2WrmgBChuTTdGsbSeT29RaYi2aCsNJr31PaG2
RFJtmUIBgeMh2zmyPgRjcV8zXCEoKeJLf2wZosSIN/ZMaMj+NkH0Ew9KkJjhHQAWEhSdecBX
lnUGsdIVu7hr7s51wZogG0h9pNFju69fp+b+9tQYOsT2YP09v/ZA8jXFrfMshRtOEKB/HsDX
VDVSXhfYRr7PgivFF2Z8t60/NKxiC8qL1reLXwh3OIHlL2VC3fHFt2HYBaA8U83QlymZwrm9
WRqGSdbMjWDKB/HXEkKohNqudpVC1f3cmAAMMmc38o63FX8DaPUQDhSFsK7XRCJFU92hmm/j
3zqxamDPIjzoH1LJTwLqMW4435ZhvM+lGUB4/6msH+BxZFMCOGqfcGha3aDvqHKN/ERK5B34
yV8FH8w2CsdSCQM40SHkOo1+mZAK0+X9XXZxKKuL/BbKuejMDPLEyHnNAw62D81QI3IkbpuB
YJIFQaCfIyVJS1bmwAZDFvkJ2TgAGSuyOtOZvqqGAor5lX2XPB682+x65cIbs1zCM92LbRIt
bRkIWGau0F/2JDDObvm02d2lXBcMYdfodIBKIO80IwpOlJ0L6pqMhbsx7b7YJ4n0XSNRVg5A
ekKsxCuDdF2kpg27VTdMbZPgv3Kfmcm7uoLPIN6S4nktK9+bRVUZvJA0QGN6GLs4x62dzOwp
Jxd6ZGCAduuYcYfYz06tN+OUzd0N102eLNnwTRuBfHvP88Di1NdU814mAShj0LjIaxfPK8W5
3XvZ7gJmMuQdNFdp3iIqxmc0xDw6srViIpjuB1zuTqhsxvZqF/ZsX/1hUstKL5cUg4MiB2tn
GB0CeuD74zxvU602BHsS9xjAv6vmTXdAhnc+JLuFtyuPakqLNgfba56ESOejinRo9rhDL8qc
3RFuckEfRu7aQk3c4o6LdWOgSLpfnahSi6m2rI6a8C/956KTT70KOMZGbFBEriKHljyc2SjC
NPsjBaunQRBd2BOLsps0iAdL3sgsGTW1xQPMlTnP4+xMIjPiIeVC+W6l568Hq2zUTFHVBuOc
lC4gL2KUXW4bZUftOIJ4RjhcnIi5i5OCPYuRHb2JtKoeHdphIrzOw4HXBQ3H3RRwy1RBT9bj
N98i58T0JxophsuUPSeQZRa3eAG8g2BoLMZCWH29NWj0/csbgy8ioT83CPPvhlXtgL02jgaA
tc6wSeyQagdhZZBZ2h1+N+bSuxuafFHjKWzpyZGz4djaYraj3k86jjinlIVfwVWO1Gn1OUtg
HRtibidjyR1eV+pXBPRowTS9aGe69JO2EUooZsCsAnQscyLKvcvKQ2fDltPBPXyZbHd/r5+H
4QdAW2VXh9ZvgwICToNPJtFnLkBbpx8D3gy0COxbFvGeOtlSje+1HOfneihNruciPw8mVWW2
oqMLke1R5YGSdFl3zQycSWd7E8tgO+A/fkBt9rAwfig76s3fshERjlzmuovxgVo7PDsWQJ0z
5bQUpXk1vM6syNhD1B6s/xnMJcYx1d54snRR2snsK13nQbaQsp9TnmWM2hzmxnpHaB5Kc4j/
gELzJij6uQLp266DHrVZdSuxT8mTYxVuFEqaR8KsVxcs8ROHArS+at3e5DcfHSYaiFRulXOl
PBt/7tKBfWkGiNarzPxpNMyAo0ujs4OcF3iP2eGKGxkpjWhCAhl0tBjQi2oxcGVHaH9z775P
euCdJMKanR4cRUjRxevWQgJI1UTI5jmZSKprmqWDXbKwi9CDk8uZGch1WQo+kmLsfvYpvJeB
mECUckap94MOLcQ3w5xL/5noO6BW3WNT12ECAVzFhn9pC83m31YdOBeJy6k1By1vgVtV47oi
KFyFLrIDpntj4lGSpLmVLp+fR7c4sEsJisWJARDjXShuW+UGOzUSBxCLwUiDda4KJ34FW+hC
xu1rFkYqkr+JabTivz8yOm1e0CePsCyqz9JeS/fZ0B8g6rGU7CnJ1mt4YvmbB5GdpH6qx58t
oyCnHyxmgpHAAdk8qkyHTzQPh4Ro4QgwtdTr4HGv1O18dh2EvjQTLV2zr+M3Ro8uM2kQUXh+
aKRCb/pmXCXRpDrlkS9XY13GW0VsUHDDKFdSY0mDuvk40bNUJ6Ij5UMyGEZ/Yjf3pXnasXJC
EJj4uJOpxvKN5a3tGUtRJ8fakoRZE4gVeTh8Ynpm6FrS2sjbjjNrY4MHaVuHAZAfcILBz46b
Sa4DvHqcScMVazQ1Was6UlKmA7cFzFhPsyhhooLdVlC0ze8a5aBo6bqZRPtTaX/ea/yU2O9L
ybxiY3mKc5AXjMvEIUOGszCHVsnr3IfjRnIHSjvmRZ/xrxGJgTlwXSdBrHjAjTSYvEVa7Pqw
rhE45M9eonM9uyz/nBecvfgSWG5MSOJoK4+6t1sppR9+e6KYdOuUzzZod3X9oLJlj5PNsij1
lgl8NqwKgaXLaAjFnsP4LDwA1D2sluKI9NYoL9xh+QvtHY9UpYwMmioeFwF7TO26P3qaFBl8
zRmDSFGk12VzzL0fD/sbTpDgpoCib5VyhP+8o+76CuneMbLoK8ethrdiqVO0sAi0Py71Dt8q
Ajh4QvjGyQ2nTWDgmHiEN3qd7Q9MZPEDXrQNwBEYuDJVW+qUssYGehMSd8O97qClWzOa17XI
hPhzBs9OxgdcHpuxTjw9QIHEcK4lXSMyLTT5sUVVGZNzvL6HQX6jtVXkBSj+qleYz7+BqrUd
J6m02ph6THO8VE9zcuU6tT+0S/E7M5rszZAPK/1rNXNFeiANlTS11kEwz4s3NWzBF7WjbK85
E9eL/smRmgNkqFCoiy6v/2/pYCWleFcumWq7NbXXUTAipF2WWRsedaLCy+XqiS7OIRLXxeai
wwpmyX8eWTfnRpEKoC7E3LEgbGiRP8ts9NiQYf8ZCOAr9RttpoW6LTamPe1Mn8mm4WYqg8Lu
UYgucOHc4rtnJpWKU7WymGFq0f0arMFZObd0FGlyla0F+q9dlHF3Fnb+TfMzR1cfnrpPwFzz
Fg7b07Eav7lvjSCkT4RaOwkICz6W+2HvVb7ITWAYSAPGCNmKRQYA7pk2El8abG3EUmZpTLxG
U6l+NJEkZc1/npTwzKdWTAdLuIdDoyr8yFwH9t/Icz97HCtRFsfuDw3wbcdzww3yBsDr9EcQ
T8NoZlyMgVSZdCgDKdl+qGRABARrUaOpfcRamY93OcO2JoYv0OXEXoL1lttvAn4EYwlfYj5g
RmWeDX/K6dkTRQzqPI6jISTeSh2DwmF2VycIwR0V/8/BnZa3QqqklzS25P7sLwhPv/LhCQYJ
9pOAihgY5qXsKt3XqPt6f9NXcD3KX/1TIBuVi7f0wpPayH5OdNR2wFT/LhrDeLBrLaWbW89e
7LD0kAzMhDPwFu94ArIPZ2RWHFBNRk2seBoU+v29GSUQ2+lX16ZGI+QNtWpfX2sz4Fcih45O
C6actGBFz18kw9scBsU0yMYZqyozaXXY261elpE9UR2CVlJutgnLdECaeNVxH2YTsfob8+dX
1I6O3Cb17sambER8QiUJ0ALlhbskQ+jg+0/vNlYe5Rat2O+1paDhbsfFs5mItz4UzIMdSfiC
4o/E9H4rNx4glBXD26fyhaxHsA9/P3dnSqr2R86/5hjnxefWcVwUzuvRJNSmlbagIe6AtqPw
EvP79wbERl++sOm+Wsfy4GCW1mh4k0rxp9WdZ6ZcGhjYB57vsiMKXc5owv6ssk1n8BcOWmgD
0uYgTUVWLL/TQZl1AeG7o3n9cr6hkARuATMfYH0KmtHqsSbk4ReqJehGQmZ+VdfoyhFWHBPX
jmyptj3KiPoHQ4GLfsefMj4sNzn/7JjG/dOnM+vE2UQefjcMZDKIKQlQ1jgEqkucVpau1w7b
TVCPa0ciiaEOiLNKAC8WCOOdlNwEWrBjCGLp41gnoreoGxlEUo5plcxbLbQ/QfJRQJy8oTuL
3DMjpzrQupIAosKgQl8suUpF//d9PLxu6t+TjRp/AMZNvlkPd4nkSWaRKrVFRqq7gi8A/rTc
uVhWOsvVad5VPXO7aSNxdMmSYHgzVeMhKl5rwQ5evubsOgnyT9bolZC+cze/VvGDhthXV4I0
i6MNk2+bYfs61aeUweqOA8CrwpNvi+orJuex5RWg2GUhwWUILD1CnJXtfLXFAYfR+Bib4sr+
nptkEtG1wK0EizgiHYT+tKMnDKXieqGR2OoJNd44BGZQxbhh+h7HjRnD0JBlP2c+WqGIgLJL
dyCeL4sCxDu1aOKSx4TNwRzXNcUWKqoJXOhZnL77gRBiGupYudgqZkEQry4HdyS4L253Nfxx
cs8M7JftElLp6Z0oHj6N1q/c4tEiZ2EAV9PXYMnBXDPyghVDjmLFfT/Z9r2jgD3OfuHE/FUm
8WNYUPB3ChKO2qBCOETJBO5AlDHuD1zaZfIYJV+l/AsB4yXpEUxNzd9MhLW9PmcPp/hmtwuU
OX4+en/o/cHEposSIhPDydlnSkq6DXQ3RI5Y3roU9FSTDPREn5YelYq6gkS4E5tuLIo3lWED
sZOoDafTCyYGMfHHRFka7DoIQXqLBzAc+rlNVRpwuN08V4C+W+VwJdJUbr7m3cUHkwX8/j2j
wmVxRnD2PBKcsFVtx6BGjxFAY424ghOQ6VeTdY1cK7vnyclTWGqF5Ztk2pGHRmJfzI1kJMIH
qPmVh95qDU783Fk4jfXyEracw8xshm7wHqn+0wmZNEw5YQLlTadHXJR5xhOHhPr127r68V3F
qy5t/t2CM0pisffnySgmK27WFj3yz7PljSU7NqLyxBPo8nCGoSmj2JSqHvQ9YoIb5lXmjLGL
CsC32iXGFI5esvZ+A8CG0cQ5GkTGBbPM+0gPbFDjBXotL8iwnvIqo+/7Bpd+WqbDybVI4eZC
6K+atLGR1/IHRAlJfvY4+XbJcsJYknSSGbGFDrvdnVOyUuCYowFgwUGF3ivEDfd0ZG71jIkM
0Kmbf9MabUsY7y3oPPIao/TcoWY7YGtvQkiAN/Men5FtHwn3uann9mk46WXYhAdPjS9qp27b
sCblQEjCwFlbJnBzGRkGC+3wL2N5lCXaTZ17XBUcPsq2hvsp3Io6O3mu5SMTccZGtX0K99Z/
LOsPWe6/FvSedNBFbvy+E7xGCilv5smO1xCkNsTwRVjkvlnhhe6qmUD7e71Y37ZUClpYnlDm
nk7ELR92rYvhcpYDRCyobDqyM7yiEq95fMzfFVzjJTbWUk6QfpUXJ3xYpge+mN16nO0W3FEu
LOXX1u/yIwdyi1OhZmGlR2cXX/xWlaSQYtXaNDkfgmhmtfJfRf/yp/YeGKMtJY96FaPaK1X0
hPWoRCK+AHK+0jWU1ghHi7cSZb9BPhUQ/Y7VlCRy2pXg5GUTMweG6lBYoREfBokrbtrAg3wN
MNiuL2UV80ywwIluMqN4KzLbiZ4ywRhqnjqwOZQe0RkF0ZMX9WiTRFVl1Pr5Trm3LSWlIw3V
qmtIiVnDSkIowtHnWnymdKTKUx/UFDlQ4CNFExvM38uCy8UgGtukX6MlcGWoNDha2xTyQXw+
0//xrKrsodteKw29VvX/jSg7JwFizFqztuUCby7NVgrqw5neZJEDk2LHN6fHQDsFnEsJyyLZ
hW0fIY/BTl/tTctDuYwRAqWak0h6dxpFz6hojrAgQw0Qt8x4qrPhs1qErdmk2v3pO/XzSLaj
p0W2v0p3iVOiHhJgtsUqHFjO6toh3rw+06anNzyBY4OpgbgtpCnXNwxMywsEdJDf+mxTU++8
5h0UxyYrPcliKtQKPe4QRQlsb2d8qSNdD9DODEuY0Ujr2NaRdvPbllXwmDpebwYrXFg79wu/
Zw0qODVglS4RpJxWBePJZEqOKpIa3pTx90YhWrBFU1PDtCBcMmumYQxHHX8Ce0+hYoMrbu60
7yVLoR01WG/XW3Q1rs4rRGcc9T/r0otdSG/80Yd3BkETY77zCYpLhxcqN7GTuufY07L+40mG
p3p582tO332GkEozGw3fEna1+BGc3/PnvOWnbP9eAG/vYh08RGpfmoc/eiTGqrZFUcl1nAqB
yTKjFXiN9hIyvpMv6KlnRAs5pRn2GU0upuLLaEta+0vnrF+ykwRBBsJpryRsadbY66XXz2AT
nvKIyS5IiBa8hsCN1u+cs7Rbzzv4+XT3pHUesTBqTSCO5ujx9vZNuYp9b8+Tok6jAPc1Ev0q
8OWHf/oE4vyLOUh3CS9CgiayLVsSMJm5l9/PYPqFfFZNNti91XT/xjdif/OZdArAHqcLIV8J
BZnM373fj1oY/GEfrvIYkXrEXcZri7gd/Vb24p+4hFBYI8Ay+QjmkiGG34NO1XKGtiHr5jG6
fCUnOWItUAtQQRCn4TJoWQx3zlb5ylGa8oDQqcIdGnMqUDIBUTM+RWWtsWNDE0E1wn/xei/t
eTYmkPmVwI5CGaj4VKCpWHeyQyc8JGSGO9vfz/oGWYHPhH8zhH8OfP4mxqonuA5lHKJB/giP
o58ZxjtnJjcyKOTDALuGM+3XQizqDWfJHL1TaVN610526IAIevFbjnjA0UTyj1otuO/FrWYy
jQmt9rJdNtJVK8uqmL4T49AS0kAL3oU6AigSSyyHq52/YbkZ+GhuhMDEaMIdE13k4SOYHMUO
dpMhEzzfqP9K2fKtZbAYhtyHLbl4NAWfluDD2qvcmjWpEcFMDmfLaN8rZ4DKgGCJtC/iXBl4
fMjqoz7WNFHI724pzAbFtnuyGOyKwoycjZnLxnLTxnzNOtLyJGBVv8n7DXycnNkrtKae1ShK
R/S+ToVG2DjtyqOyQ1ICRkFoowuTq9vy+iak90oiPxCKUYJYdFtVy67WWC6wht4e7vnR+DgS
0H+dbXit7aM7t9kKLsKSc37t3mEMR2S93qXd3DaUNWeSL37+0KdGBrztqcYJE6j1UhDGnGzy
3rUE3LnedOHNasU/YgC1UG4KCLkU553TGIbdES9v9KtA16a8jYNXEPJNybt0Y2GZMOJitQ32
vVzOqfZoAzePmvP6qQ60esuBJRk65/vx7pldFPE5rRaDNovz5uVgdWYeJUHBMjS/DDZp1tO5
jVNC1n7uGW+RN30K82YfZZQLSGzgiQcYs0TNYx6hIAXLIPLBdPA6UBZgi0DuaNJwwCaOJrgR
vqP9rx3XKkBpi+HjPLLoR+UJk0xbGm5HXOVbpMtD7gQ9ckzqAKoQ0IW4zUGGsKwER1MF9XBE
+ELfi9RKz9+FwYgq36wl+lRKg4imUXucjSip7LpZ9wnDLtRAwxOeaNHJwr02DoAvoHIBzMVS
H0t3XsXUFGjM5Ffk3y1/r3B1nmeYlK1coerVt2kV4f0E6KIpj/hJro4AehD8nTK/4zDKQZ+A
G3gRokupd3j98OWvGM8Dk+xPtFGgyEZ+d7R3aHEkOb69C2TveC1/jCHd45S8RCaIxNan53JO
CsRPXsCnWAwguo/uQ01WpWlj0zfq6WgauWI4Cc4Rhskqg7RKfNvtm+JH4b3wmmhnfnMBGolD
NplaJhqL8A/UfDPBiSs419VmJ+eh1uf8S6wOqKJfturF4KHv356GBO9iQrqFAqr2giybRxAq
OZ3dq5I6yBaEW/KMBVOkBtVJe6y0fuHjOA3njFYt8hL9fK718RLiJWyXMcLYJD/OuDLtk5Ab
6pyP75QbqZDJE8OUt2D6QA7yV7+bSaPs00DryqeFcSte3pBIeY1NKpLE4N5IUVBk92PpMRPt
jOL/GflZ0kaMdyelAB6XS2RGtVwZPrUCTdmeZXoUNAzMnpiU0wpj30ZlRI1psc3Tw8CAjmyQ
IioW/JpplbILiupNQ0AJ2XO+RjUqipVB2QmhuGo7pMKaV9wB7q/NV28T03gyaBFeBkbJpsxz
9OerJafAF26bM3oxwyUlFH3dn8rKEp41jwOZGBhhhmL/aLTUG76Tm7JBAQ09Ov38TcmTTNwQ
W3C+aknCYSL7FDziCTLB+clm/GJUPQ3JCTIgkVwKagZI8wKXd9R7LSqNhidh9CYm25fgNyGJ
qbzM3fKUDHPwsjtc19ig9z0DGiP8bhBcyb1PCuBYKzTIxhxxgxLs9mFPOjJCP5/y7J5HtsdO
oHpprCXwRAbYT38E10GWyXRjI22v4N+Hji3aKYjcF+ucS3zt5s+5dB/Y+lKHRwSOm9jq7pL1
Qjg3lMtiD39Aes18oyf0mGFE3uNKg+hDW+hoEchyF1hc0b5P5KAMbz8iOcZPuiFxvLFChm+a
qn78Dh16lKm6prhbf4/CrYIOQGbXKU1O3cVG4NdtMeQERE7m0Cet2ykbBPs095mkJyyA4QrJ
lTIIQsSVunOo3jhUCHm9J3XZN7N+fnsRNe9ZSXBnHxH/E/fW8hwb5O0D4cXk8LkkaA3k0soh
OWeJfcrl0rXrSHNKRHuc9TsjOyVWCtnslHd3w5Rsm+MJM3+g5/yS6j26rbbko4i2CdbMyQTy
+w9cUazPaKSuGRQ029iC7E18t9jyqGoHaSbTp45Zfnw4rEbwoPhR3fP+BoIdue/cfXOt1pPJ
GK0CP0ZlvsdXXCi6iSn/sObACsbArXfjzkzr4IcCjMyEjwOpDO4pg4pMbyMHkfZq7u6reXtq
Gr6xWQRVa9aANDXKCd9jBxyjiON/XXtM+TdDCUAKkgT07n0rYV3FvEejoDqJeCkWbDIi7FjE
om0oa5XfjFx6xR4047IHp37uIhVYn6WezcI1Rq/QezwdOwM2p5LTUwMFcNB9Hin3KIfitEv+
WfSKWRLa1m6Ben1iiu8+nyCULfNiAqZiLDZWAlhUL7BHZY8yjLPjVMdVlOFm9o1ImR1yaBHi
I1MuRdj09gXWGmYBvoY13UVPoFaewKEWWbYch7o7q/TDHfeGuJ/ud8II1zecAqpsUGr4tKbs
j67M+wqibKctUil7hVuCCH/lCZsxusIW0/9YPcVif0pzRjYgGpsh1fWCVEBeXgEBwWqTWf3L
5zM3k8WVS4cKPqPmVA/aiJggwKau/lnvDuyvB4TizJTa7X5112VjE/P+5kBIfmjY4qoAj5wq
m0xAt4lcu7hrKH5L4JpC7NPfRvp1PQL77W8d5RAqQ4En7a3W2Rs4BE8iKrbOn6Z4WkXtoPqE
vGaxnnh8123/gsb/C+27dAeG0Tr5wa230QN4M9rLgfuMaAAITGqHzzBwCHS2G86jmyo3JXA2
iEsg+CNe2HhzbInehUfqqpCqs1Us63PJkIYEALiNbuWjCAQIqVcaJtcWN6j78lJqnhy0/9zY
U3kyf/8tuJQBQq9pxGKU886ZU6RIRE4GPaAsC0oI0vwtxWwX1IS52CcqdvbV5dsrmJo42Mi3
gJgbZfsh5QbxUwibXDxC81n++/3DEcx2dMQYHN/YC5qFaKaU2wj24H5lmx9M+9hTf5KzX63e
j3akPTHNIwM0GRTqaLRhzkOS/MTX+JWLI0S/X+Sfc0o+OXqekspv+3Mpt/B7eoai4X+x2Ksj
/a1DdYV6lGhoF7EG3W+WqD5gMVp+flICB6UFKckH1h+TJW5/r/4AnvnpH15NlJWzCP7p6Msi
YMvr3MyR6zHc/xExl2JKf9MbS7Y7Bq0p/ZfrfdOpCjyNX78Qvh3ZPl38m8+vRP3kOriVQe7j
P0ke6MLb4r+idrXGgfz1UkV7mla2+b9rHJaqkz0RyUI/PzLCaMh5CL+UgwamtHF5tPXh1hY6
JlU3eML9n0sZrZiRoL+9we8ihzQC/eXNu43KI0VoVojN+bLnzmVxNXZrVLUtGTrNknO/bYH8
nEQebWtgk0kuZP+sgYnYZMUrNHCUlpwsPLuvqapYovYqOqaTTpqvfma380lnBKOE620D610W
kx5yLcuPpVsKn5IrPO4sxcO46p78RXLyEzEx2LU5KmTOB5sGz5NCQvkmNKzsPFyVQohd8413
ePjjdHzR5DMW3qG8wC3k7WMcxLaram/58pdDt9PGIljvLJdO6BmTXPjD/4yfdhOR1jKmJh42
Zwv8QGQ9pq8GGS03YUeadYF4yXPwk6oLPE8Ck09MPME8HtXsOG2+OnoFetDCeRborcTlU/nO
PN5OCE8CJrBKoxzCCfr2BY5kH8F7szP/ReIup8CbwFHjupMtzUwnFruHt7IOBW2iQL6xVX7h
hHmFS4hhiMkFB97utMG9+FQRmk0TNhRtfiyu4tzqWwFudWKjCE5L2rkmkBmVkh++mZFyAyn1
d+jt1WioXpUOR6a2ZVSVzZdzr7hTBF2zFi9EM2bgo8XU603NU01wzgFjyZA0E5RDbxmDyUB6
EpUFeMlAwIYdAUBTENrbmANOx+gvOxMZySLqsKAVyNM4J/snVuX3C+5QjDqMNxvgpJHSyv66
W9PA3pkUiVB/caWVKJrTzaA5zTyHEoDXWnv97okDdmH+lwi67jUn7yLmenwq+DdPwhc6GV34
DXABy4zGz8MbQXSd1kZEJ5F1mUNIBMUs5krAy8ZvaEZsoz3gLmV9koIDijr7/1rvZaBq7K18
o5iV+SIQJR7oEOePcH1SgSPL7Ag38XGRO4ffbjJvhcRwJ79nzQBNadD4dg4ZdInJPB6W2Vy4
iYaTF30zSfzp+GqB9aodJuMCxa1NdFxnk8uUB1CXYUc9EZiI1UWCjr+a3N9LGC+9IemoACqI
vtO9UKJcxk8sTl6LIIoowDCVpMx77P8/PjRpQ5xBtCkM8F84hAsP5MGt7zQVKvkSS6lD6Cju
ooWQ6tPS+X56uRDoh29Vz4tH830wVwSyyfTBWs0MTZhwWAGM9EIBQf5D5shDgdXSduPY4y5x
iU2C9syEWViC8FF0S3aNHuFv9MEiRpysxSlmrqYxpibbfM3BWrom2EuPyAO6CVR14sK0cWLi
6kaX5qHxyCzP14nB0HHPqZZxxY6ibWAxS6x3Yk9Rxf8pzPJqp1UWgCCSjWcd/ExQtQVN4h7j
PFjsqKzdj+tv0RIUowRTccTPhD4P8cz7scNV7C1YgOLf6Jqgoh15D9HfenRO1UIV+EsJ+Kyb
Hp/d9nn7+gMd88Hdqzb7camhLt4bs/BosK6UyXYgZzJJjl/W/TLGwbuorL87fPnYzCtQ9GB0
CD1gnvlZ4M1BBcjRPWllNVrn1aeE/hsp2a+EKL2+l1baEKwrD2kLagCLJSdYP2PrUL/xA6gE
FuU8Uo7XtoIjwWdFKgoR72tLfAOCTfAaguhMT09MbnnT3KXJX/ncajE8Qmetd9nbbE8N6sgV
abzzcXLLfyXX6cW425zTbVwx8I5DWrotERkQIkFmzMnJEph1AZba1f3GBDa3WvXAlGeNjfS4
B1tdytbkxPwjiY2+s+N8auKuL91/KNO5ui2fDAFnNujZv8ChQioh6y9rPozWOEqSA8ujgrBN
gZiw+hzyw1VaJ7uYW49l+LgyvGjY9Yq/cLhjLA3mnrCOwq9LlCKkb9+Zq2YwWmuyEkUHhuf6
nz9aYSmt8kHMDIqEUUhjNxv6EEW66gzCIAG7/WkkO2BL+ahZNFQN3hly87/Gz/SLdCfLMMqN
iyXKxN89yjrIijP9uZpEuQtM9f1oH9qp/e68idENUrUzFI/N7iK7INpoH8gauCTARaxo0YUa
yioz0nFgZLf6gT36J+2HkJ49lITx/hdFYTOxxGGtLf6vwqpVbuRKBQXJ7Pr5Q96bOL6HCktC
tE++64ZXWQKF6v63V+UK9VRQ2mYpd+cWO+Etgfm6ncANms96DKM/XuIa6xLRnH6zRtm6LVQD
rVYvO33Me4Gz2tEsm7VAK5OWuBWyOScWgVk5Dt6nHWvYAnsr1iUcTCvN++VZxGOtgB0Lrk+i
92WVJEsvcwrQfmynnEm7ZOIo8a1WYCy+J1dzgaH25+bj/LWoBSp+AA1l7lAoCQfLUi7wFRDX
GJDsWnURTQf9gNZXGpna4dgg6we6VCDl8ISxZpOCEE4JcvpHae1dUZNlBQc5gIcALHSCM5s+
C3JuKvujhWtdI46n2lM2ua29mwkOwcs0pFzKltYy5niipnsIwcFnQm1MD7WK/gPZD/EKTl7z
pnVdbh8qejl/nJEOKVHWKQbrsEC2ISGEGQWuT1BQVbcPSoNQylM00GVsWu+zG8YEhrjxfBPj
Jhxb+mMBBUNWe61Qo+MDGQFhozjfSwrqfPNp4oNAyUp/upwQi/s3BL5XY+E3uBkwVyjE35Zd
6vFqGwn0hg66S5Y1vepII96JZyughzFvXjilMHd0+LJ35ctE+wMjZLtvSMYq2g3a0i/ll0FV
asjFE6SZMsUE4LJ42deH+opCKP26CPk7V7Urz53FsWSXTgpS5OO5k/cexNGU4g2hvUs7dk2W
smG3W3h3lsgOuMTlCcftQ6QUm90KsTPoCU8aAotzx8LU/R1b4HNisV57rXTsN6UrHtHb6r8g
hU+XrYqSpb0jXR0ReEx/+5Kfgbn8I41U3CVA0H8nTu/cXwqIBs2ok3k+hOVNJ9/rYwcwvpIQ
A7Tn6qloiT7VqlR1eEICgUq9eKV+0zwb58d/1ueuVp+xS78ZGOD1UgWVWgv7o6H97lB4vLsn
fsTWlR5C7aMPPzRa8is9qXitFh0gtoP1V1utrsLiVNO2onVw6ak2jB9mVcrk2HSi+xjaGOLM
apsE784sPp3wy5Hv93OpfkEvuvugKCGPIJQe6S3HdHDZoIOKEH8Y3KxTC2+1yU1eslJW04A+
h0iKL03j6uIqGQorrwSd8HKp6AfNkp1PoAYRGU2tusM9XR5o7IXnA/7NUxJhk+5ymJc3MrzW
s7NXaZWE04ooIIGDeIuJb29oovXA1L61Q1yoE8cxF3QWaaU7WrJElf1o64zKsWn0xj9aM58F
eCmGoU3QW58sMO1ohBuNiZG6LhltT2i4y93Dv+sEMgjoQexJV7wWuHDo7Lu/bQFWLCyzct8M
KeJt+X5bs1fRaMXivRLM13i2GUifDIF89K8PKOuYB3cnGc8+gROPQEKyYSdis51LCmiun7/5
s8s+axb0K7YnKu3PQmslzwEbhjgXpymjWgPi7VLkUX7T0GiD4RPYtC0aD/xumULc6BX78lot
jHKFiUNYxaHFXPE15eBFTZgAQuu8kxsm+xC5+yVmHNBP123bImMmKMVlJ/BuJAJR5ReHd3IJ
jR+hKX/spd7Q/FsiUDcUz5JEpOmepKe9bvS9duEmMDld3ErYSCv8uUq44eXv9xAnKAFPJVbq
E1Cfn88d7Bn6KHazfTHXbnRhkScV7UwIAwOZvTaIgFVemigp7KPrFIMLDb8zNCP7Inb0A8zE
ilPdg4DdZY44LEjtusqsQaiNWz7TqPflSLVrQrOLJd5PJcrdfSBALEq3YejjecEFVkn4+YzP
iFmhmAAYU4OF6tZl0OxwDWaHR3vG5xPYoHrdqPvwdz3mniQl220aWQzgGTSlAyEI5nGXg6Vm
wyXbrVa6POF2p0OxeFRD2wHRa269YtpDRNzwzEn/cpdR7pF9A94Les1/Y4/OLgZupPSkGXlr
bIZp9/2E2klAjqnAjS122zqc4yHph+Co2VXZTlzDLucW66Z7ge17y0Y0ueLiruqx+gUkX/Su
8TkGfmbr+0HoU3QM2SWJuBuIxHTlfZEkL5gn7/GoRt5672UuC6iukg00oilaUZygq1LjAr/z
4SxQRxVNASGH59IMN+9jCc0MCNsl24EHE7mduPmUO9Z8QsNBP2Mm0w1oVx8MlYT+3RAeGQ77
lAjRZkUC480sURMHlr4gaWw3+C3oHwZR8VG5r89J/gwNOzUyQ6oSYiSzYUTmsXiWiLRbOuAl
14/Y7fjCdfDJI24n8KP4mAYcdWJ3tdqXk7ZQcwtYF4x3JSUETyfk3DJtXF0t1Ehtaw0oasaV
tZKr915GDyBc4rZCHmlOdx2LhCaTHwyIUVyoSvTPJdrAqeeRbBbxioMVCvVQ41peWYk+YGMl
K3Dr8UVWO82LfOpWLtNXjP8aQpeJwteeFwra+EgtenOU0z29AopdAO+YRF3ADInwuTI8AtVQ
OYzMPP31yYggu1DeOorA7eCg2cOwCp5ieHpSOh6b5IYnVXu09oCYjT5DL33LDD37vvgKyjM6
L7coBiG9NqJVNxrqCB+bkTlFdMRK+4kHwW1wXguL25trNTmmJfMHPDr31N8SHcAXuJgjjx93
krzyAq59I2ySVTDsMtqFCXGA+p4nYJBED3ymbmOrfzqCdq7kaHqkY4hDnzpCDpBCDpBCDpBp
nmz4YSHP6qtrvsjuUgM+wSKZUlhZBMP0IWq4eRg9pMnFCvMJdpXHG3HA2oTzdQ1vkFjaI0vP
KcZ9Ok6vA8F3fu6tXIfjBwLYyUHQgB0b2slZihT5x2d6v3qG0YzGa0Y+P/knf1jL1F4ZvnM0
6xceHnqaG7AvsPn/4OHa11KdD5HRFUYZ9cbvYB5+RAJsJTR84+0CrJ4IeUkMsQTX/UNwKxgN
o9OMUdljKyh3q/YuaI9KMEaEWfNlUQMCfWt5bVxJjGm9sfJakLDWXqAndRmU7TDhHewHaz4p
0zOgUHhdCjb26TAarkRwO68WaQcKpKMBC4Zhi0uB+7zXgTGVlSaFBPd2uP5dyo7fc7N2jr2u
2sM6czGWu0JMdep9hyKaf72c+oiPJKEz7Wgr6OzUEUfEBM53kiJ3hbhUnEAiVpZjV5Hspn7f
uuU+OHIM3+psv/FNI8B8wqPCh4fPVxcYiwEpYxvl+x1KYa7ZdccnwKA4ziE69/gcjj8AoaM5
ZztvQJDnWnAEmkjasEhuJON/A4dc2bgkCgXsM0OMxa5092ZuDQWi1cHK8Mn5TcFbkXIN6yZx
NSeo4M3XUIPwZbKZL2oK2UC6h7e3R+C+Unucx9hpDwZvf/s2bmG/Z9mfQSljOG667vTNevay
tJ5RPQYqTxvSoo2xCp7Q/sVgvmJgEG2BrSHM/xIMZcOIO+bgbsKY72MVj1GGbaK44KZczvx2
AZ0dzno1amEOC9foGHHAwa1Nn10x0k6iVZHRpzZujLXk95449i/vIaWfdyofBueLnpe49BPg
b0hHtCt8mhz8ut1VT7sOoxVDg4+w0eAqkoms5QJb3XalfValG38vk4gsTa/GZPN5FPdtOnZQ
J4i96Awm5noTGJC6HiqfzP0KVBSFzg2UVn151IPNd9Jrv/C/aCjQQJiGIn4wrSvb2taxVdIL
ah5v+BFaD3JUWAQJftZaLTeoXDzWjhMfCX7aEcN8DG26qAX6HoPEO8uw+sAQP4E4RPy+dtFN
igxTrFp662onotyslgJzoXdQC9Acewwi0fVyx+tOgD3HU8XhjU2ofg9JURysTBN6RN5ld4MS
9Q07kLeuCt7pzFLgcQW53zwxzupXbe4CYvQTsIyfnfiDi4GmC5H9+hi03Sx7akbKD/2sp8oI
qbqMKXWmU/S+j8evQuLuemQdFzJLBQdOenTBQmxX6y6cmaPaabTQCoxy/uxTe+FOVWl5JKLY
uVF+7hBqsEhhbHb5svuaUwzj52H69XROCAv6Ut8o4Xaudgpvsi1M7GAVkfNxNjvsEBBtZwmU
tcmWFWLiNyVD7/YO+4tkNZATFhq7hNTbiWhD5LHt1K0l4Zp7x+ltBZwN1hrgMj/Er2GNXFPc
VjCwm3qHdBuZz0VGNmC3+NPs95+sNeYzZ7npYGWFT7ID6WSCHrjTh9IHkBfSEd30LH8JQJqg
IO7R/kBKkAbsfqb5ZlfnK7QzDlLCmXqqpDDiTPc8NZmbI2LueZm5q8jZYuSjNln6Q6k5ICXx
0vSZBqKoCRJlovwbUpNlG9ln3TTYEOp1qaT9goCYEsIl7RL+2PbXO4/I/P0TlBrW6HFd2yrg
dgMwCek3Kzn33bWMvkqv7zNUUreEdH4zzyJYketv4NDNaX77NjPHbvYIvAmzYZSN6QcUdMkG
75PQxhag1tTzuJXEuN1SsxBB8HtuSQUtl7N9Dn1si7PsjQY9l2ZRjI07BLIk/BY2BKGlYvu8
l4BaFfKnng9vQlIWTHWeuMv2/+UhtKQMRZHpBlt7QQmQVyuX5bB44fTQUA2NUUpS5PStxRWu
FDu2cx5mubMfCgq4e62jvKSh3qcWadSACatSH1fKOE8jtLLGmWMYZEUX7yVafW5s3fuWVdXk
Rwo8QccB6WsEH1wlzCftoN5rq/VFAEVgpZHQMjE+4uZowQtE2CcyEw55zHro3E4Kk50md5rD
/CfFMkQBxDBefEfNCox4oKMTqYG0QVviQOArs9C2CP/ZfbQytgblKCFYmIY7Xi486Sm1c1JC
P2E6avTmfIAT39nEwQo8KQVtDVdbDYR9XGKdm6+H7HaKA5qAiv5/+HP3c/nbP7X/P7WvP7Ux
QKWTx8WZ3a/vRy59fun5gxZtEsZxPXAQ5cxXGawV/3fGIdAA2b2hoo/Efwyj9H8+QnmCiZIP
tSzsd2/w

/
show errors;
