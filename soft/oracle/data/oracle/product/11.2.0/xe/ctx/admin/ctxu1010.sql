Rem
Rem $Header: ctx_src_2/src/dr/admin/ctxu1010.sql /st_ctx_11.2.0/1 2010/08/11 21:21:50 rpalakod Exp $
Rem
Rem ctxu1010.sql
Rem
Rem Copyright (c) 2004, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      ctxu1010.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      upgrade from 10.1 to 10.2
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    rpalakod    08/06/10 - Bug 9973683
Rem    rpalakod    04/30/10 - u1102000.sql
Rem    rpalakod    04/29/10 - Bug 9669751
Rem    rpalakod    09/11/09 - Bug 8892286
Rem    rpalakod    06/07/08 - 11.2
Rem    yucheng     06/22/05 - load 10.2 upgrade scripts 
Rem    gkaminag    10/07/04 - val proc to sys 
Rem    gkaminag    03/22/04 - gkaminag_misc_040318 
Rem    gkaminag    03/18/04 - Created
Rem

Rem  =======================================================================
Rem  
Rem  ******************** changes to be made by SYS ************************
Rem
Rem  =======================================================================

@@s1001002.sql
@@s1002000.sql

Rem  =======================================================================
Rem  
Rem  ***********************   end of SYS changes  *************************
Rem
Rem  =======================================================================

REM ========================================================================
REM set schema, Registry to upgrading state
REM ========================================================================

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

begin
dbms_registry.upgrading('CONTEXT','Oracle Text','validate_context','CTXSYS');
end;
/

REM ========================================================================
REM 
REM ******************* Begin CTXSYS schema changes ************************
REM
REM ========================================================================

REM ========================================================================
REM Pre-upgrade steps
REM ========================================================================

@@ctxpreup.sql

REM ========================================================================
REM 10.1 to 10.2
REM ========================================================================

@@u1001002.sql
@@t1001002.sql

REM ========================================================================
REM 10.2 to 11.1.0
REM ========================================================================

@@u1002000.sql
@@t1002000.sql

REM ========================================================================
REM 11.1.0 to 11.2.0.1
REM ========================================================================

@@u1100000.sql
@@t1100000.sql

REM ========================================================================
REM 11.2.0.1 to 11.2.0.2
REM ========================================================================

@@u1102000.sql
@@t1102000.sql

REM ========================================================================
REM Post-upgrade steps
REM ========================================================================

@@ctxposup.sql

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

REM ========================================================================
REM special case; default policy ENT_EXT_DICT_OBJ
REM ========================================================================

PROMPT creating default policy for ent_ext_dict_obj

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.ENT_EXT_DICT_OBJ',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM special case; default policy AUTO_OPT_OBJ
REM ========================================================================

PROMPT creating default policy for auto_opt_obj

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.AUTO_OPT_OBJ',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM special case; default policy STOP_OPT_LIST
REM ========================================================================

PROMPT creating default policy for STOP_OPT_LIST

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.STOP_OPT_LIST',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM
REM ****************  End CTXSYS schema change *****************************
REM
REM ========================================================================

REM ========================================================================
REM Registry to upgraded state, reset schema
REM ========================================================================

begin
  dbms_registry.loaded('CONTEXT', '11.2.0.2.0');
  dbms_registry.valid('CONTEXT');
end;
/

ALTER SESSION SET CURRENT_SCHEMA = SYS;
