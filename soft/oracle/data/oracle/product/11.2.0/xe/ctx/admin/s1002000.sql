Rem
Rem $Header: s1002000.sql 10-aug-2005.17:34:49 oshiowat Exp $
Rem
Rem s1002000.sql
Rem
Rem Copyright (c) 2005, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      s1002000.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem     This script upgrades an 10.2.0.X schema to latest version
Rem     This script should be run as SYS on an 10.2.0 ctxsys schema
Rem     No other users or schema versions are supported.
Rem
Rem    NOTES
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    oshiowat    08/10/05 - feature usage tracking 
Rem    yucheng     06/22/05 - fix bug 3003812 
Rem    gkaminag    02/21/05 - gkaminag_test_050217
Rem    gkaminag    02/17/05 - Created
Rem

grant select on SYS.snap$ to ctxsys with grant option;
grant select on SYS.GV_$DB_OBJECT_CACHE to ctxsys;


