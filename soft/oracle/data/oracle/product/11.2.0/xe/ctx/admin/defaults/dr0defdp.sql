Rem
Rem $Header: dr0defdp.sql 27-mar-2002.14:31:53 gkaminag Exp $
Rem
Rem dr0defdp.sql
Rem
Rem Copyright (c) 2002, Oracle Corporation.  All rights reserved.  
Rem
Rem    NAME
Rem      dr0defdp.sql
Rem
Rem    DESCRIPTION
Rem      drop default preferences.
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    gkaminag   03/27/02 - gkaminag_bug-2283146
Rem    ehuang     03/27/02 - Created
Rem

PROMPT dropping default lexer preference...

begin
  CTX_DDL.drop_preference('DEFAULT_LEXER');
end;
/

PROMPT dropping default wordlist preference...

begin
  CTX_DDL.drop_preference('DEFAULT_WORDLIST');
end;
/

PROMPT dropping default stoplist preference...

begin
  CTX_DDL.drop_stoplist('DEFAULT_STOPLIST'); 
end;
/

PROMPT dropping default policy...

begin
  CTX_DDL.drop_policy('DEFAULT_POLICY_ORACONTAINS'); 
end;
/