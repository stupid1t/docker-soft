/*==========================================================================*/
/*==========================================================================*/
/*==========================================================================*/
create or replace function dri_sublxv_lang wrapped 
a000000
1f
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
178 140
80ISBVjcDFB7jcXA1aKlDowGnUcwg3nQNW0VfC8CAP7qR2KyoiYjAzBaBd79dJNaZYdtZuPK
HHGfMU8g/fA8lXHmGdkSTi4u6NL82d3uI/y8qjPBnZq5NJh26zt3Rg50fmNiut4fei19+rxG
or42Zwzr3bhRbtGCQf5As+KZi0mEb5W1ojfwZS24DTmAAk4z/6hw0SOnsxM7BE//hE/lqLJr
KktWC3bPbTnxNyk1R8XYbN5ymzVgXoFYddjYZmQXAW1DG6XxfnzS9MGeZmWZpafImoBfSdtP
hNUnByAc3pDQizO/Ir77ky+6SQ==

/
show errors
alter view ctx_index_sub_lexers compile;
alter view ctx_user_index_sub_lexers compile;
alter view ctx_index_sub_lexer_values compile;
alter view ctx_user_index_sub_lexer_vals compile;
/*==========================================================================*/
/*==========================================================================*/
/*==========================================================================*/
create or replace function dri_version wrapped 
a000000
1f
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
8
127 10b
Xd8KnhMcMzoG098hDR6Hqza8YjgwgzLQfyisZ3QCkDqUVd1GyLeNvbvlceECaKSFn23LVuxw
BrvEsBFZd98ppYHtizKxt/VFbFf/t52Qix71rdY1sf2sayHCxS7Lg2Avj4vw8XHnLdFBV7UV
umj2BmNlo94FAVdR9ejE/wN6a7ziXs512ORMimQNMlh+iZZcjPooEVbGcyC9f+uA5eo0pfIO
6UaM29HP2W2EAjY7Cs4cpRoRf1syVsJjC9/3xSu/R/2i1g==

/
show errors
alter view ctx_version compile;
/*==========================================================================*/
/*==========================================================================*/
/*==========================================================================*/
/* dri_move_ctxsys moves CTXSYS tables, along with their indexes and */
/* constraints, to a different tablespace.                           */
create or replace procedure dri_move_ctxsys wrapped 
a000000
1f
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
7
264c c53
tLwYkkHsVSeozr+htt0PCohxvQEwgw2jeSAFV9O8cvmUNuzWGjyw1Lm8pvaRH8JZSUeXUQqc
3zqRMLYy83ET0m7Sn5ldxrt0ocyJopfcYRNv8BbLB11HgnJ0JuKCHOvpWMzA7Zgxz0X4qg2+
I85O+R7tJGYadcETIaPMFQmb0ZcGAxYTSqG1W6KxiAxtsq63DI5Bx0FQFmyZRV+XGsrZFvre
yqKTShiWsYnbelWRIRtWSaVtF5msx+YXIZwD0wAmtYtXBB7cYS8D6EMn3II6qzKth9pcOLBU
47Otf5LTpx/JmtL4u/AZZQEgCPNFo4w8iEzCmQ2eMP1HYItAOHNTOM6NaRWc8PMlRO3KTgxl
F96JTEjd7NKJyQP3UOyxCy2ytgTNo62rYJNe1SCnP892dC7775uj1mn6o/mNEzWvx8b4bkHi
FqbKiwGjBLvMUvMB6eo/zOKyACpxP3v3WvOrbUfIfG6lhA5FSWpV6ze5kYxrVazUju0NPKgt
Nrc3oaf99TX36XAtaLH29kREQKYY7rpD61LN1c/GDHyZ0DqSwp9V53rxmbfyA4+50BeMrQAB
KVp0dB8ha0SjhHPzqpx67wg2MpKt0sZEQnOblKIo132sia1VGvSqv5Q0z9eGaYpLy6x/7pvp
pHDToJBPdUmBS6KybYRpCFHvNEfOGF2cQgSEyCz3MFb9egBmcSrO5HarPJNGVkemHe6aI1bf
QiWnqqo5/bYJTbtErTdDj4Tq9onRGo7iXFqTb1VaN9XunC9+o9wpI8mBEbeG+JzGhh1vH+Ah
jEe/M7lZJi8RmWy7DWxEb3z/lKas86FXD5lrx6IxrULpG4KzYHU6wTVNrR9FhvRIYQIhE/Jr
RTEf00e95idMHDq4xApIP51stTu805oSz3FH6jobl8fslKgumqc0DuaNlnc8U7eThgizmedf
Jo3N5Sg94kXt5bn6yBveLLIyO9gr1tkRfuPV1PRfQoVVYr7iIisolL6DGQ95ZNUon/KvvREu
kaERTFBTVsok4iv1r2Yw5icwz0az13Xz7eMpRXB2FavudJFppk03uO6Tg9HgWiNNBWCtDuYl
4fN3NBQ/5uvrBkdkoVbKJLZa9vDmwoXyq+LZ5EcPkYohemJ4wIVAyYhy1NsKByMLdj5MFrH7
C3gvJTg1lp6naa3hRLhCfj4lhBPm9q7aEDifXX4NOTs6uilDyShlzJ6f1YqKM5sNGMLs+tTb
1GJpMtvPNXOt1cqR5GY25g9qd5wLm2q/+/kd+Bc0qTsSPnIrzOGm1p/qnMlXlQsGiKYyop6P
2wbhE22KEX/BkT2I1aEKTT41UoQgLfF4/KlPe5I6+InJFqZdQbPSkLtDCIhM6X7uREqoFWK9
+I3TKCvOIHLFYZ44KSXWSSdHqW4WZdymV1Rx9ZTfiV6CShpuQMclv/TQlWcWxUYIUqXyfy+8
NhkvbglG8a71BxakdxF04zx998UAlBEGF2Oi1akhRo790L2RMCDwLeGdDDb/lqlYRal22om0
kz+u/Ir0CjZNcKwSRpU0vVbHlzbMsGsV7AfLWelhFOZat4BL5nbSbVa6EDyhYg0H+GZRI7Wh
ZJjVUZnPjvf2YfR99AbcIydtvXx44LR3HmA/zfpBDvhjN20vU+EPygJ1brqMLYFwEz0Lh+0W
nqEmJFoGWK2msiFUH97Ljyqm2k3bqxYNbsrCsXHR2ySwziMn8zyvOwTCizptGwl00p9MXupW
7b9FHLi1U21Ydfq085T7yNRZHiUBRrrNERGQk/ctjZrNiG2T4bLRjUvatp/0t7KRBZJoygO5
fJPlz9lfz5lbC8Bf3m88Kn8mtq5y0YOROLxKZXtsmxY7xZz7qwLxKeN8Tkc1dlaXg4gxNUpW
bk5zjpd0YEFfsK+mAoOIw3MVjEsizvzEfVjHd0ywr3x+42HwkTVU096iqLQWPNp0Y8HVDuZu
iGQpRNWDzthrhKPpZEFlRG82TPKqA6sXh30GMVPBKbA5mYoCVKMb9EzUaZU5wyb9LBrumXLR
UeK4Guhv582b/MValwsL38C4e221IrCCNQZWIm6zk16OS+3PyxWusMEdgkBK53WRyOydiHgo
yIJt2l1ZkopovwvEey+ZOK2vEWd+MxXNMYpVe7iB7LJPv31xSzoacm95JS1vrecu2MA36Na5
IqVxbX8Dc85pzYVqM8TQlAhWi1sLRS4ZveSSadyKI3rA7Zj4WeK5SLssnwUTBVswTkeI/WV3
PHkha2LsHh5BHWOfh28e00rxAfu88iYoM3x6PeKGpWBeGQZYJDyY4bc39EKuUMsG+Y03WJY4
c7hu1HZ0+85PKdLGeDdcyLSWIe44cWr1VrlSrJO/Eb+qRgT+b0wxK7lND1jU/GwfEb1PX1Yx
yZbCB5efjzAf8URTSEhkCiT76q7h91PIc0mzAyjFSB1FirGC4MdvePAR8JuoBePr2hVg6cCc
8QCsT4ka6PhyBQryg0W+euyB9CYQFSf9wwHcLbi1Aak+w0G7qDczCL2H7s2utkrFwH/Q3wbg
77+USjRQWzXPi7sRYVnOEnIyvNTa/X6pNDTFwKFR31xFPgv/o/rocwFDF8B+9HlD3xD+5xua
EM4w/sYOVVuvkW8j3PW7Ap+ZsdYOYRxcvDXkhi3tumvx7kkIASYfNjzghilYy89QXob3zYEy
2teTGxKeAA3fgpQTqE+/eE9hmGSSnhkJLpNm51/+Q/mLM7/piVvbCJoO26Hau1Pb6K7o/73i
lIB7rcMgpNK+yMMzCfTefEl+firZx6kLzMcEwIXHXhM3QZbEeLX4Cfu4uIQHlAZyrP+Hinae
QWpQ62T2y9TQ1ngoqHwIecSAcGo+tCnmCFXXrHVBrpJwrWcKOUDXlegvBmcFFdfSmqcFEpVM
eTQKTA0QTaUV10K4tgefEsO/JSYVvMF6dTG5ENp7w5Ct0kMD/Rj/6zvOZULj5e3IUKt5vWtz
raCd54IOTAhoKkVFk4h7lnhAt0/MuyIQGVz+BrogD+upDDMv+vsBOrYKwIx0sVcEcEQ39XMs
esMhDYfj0+0/fycsOhJPiXksefAAHrfsc3w+Ih+GVHw8G5ZRzup6zKzxPQQo8ju1BOggFtsO
i+uBig+q+zs9C/o=

/
show errors
/*==========================================================================*/
/*==========================================================================*/
/*==========================================================================*/
create or replace context dr$appctx using drixmd;
/*==========================================================================*/
/*==========================================================================*/
/*==========================================================================*/
