Rem
Rem $Header: ctx_src_2/src/dr/admin/ctxe112.sql /main/3 2010/05/09 21:34:35 wclin Exp $
Rem
Rem ctxe112.sql
Rem
Rem Copyright (c) 2010, Oracle and/or its affiliates. All rights reserved. 
Rem
Rem    NAME
Rem      ctxe112.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      Downgrade from 11.2.0.x to 11.2.0.1
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    rpalakod    04/30/10 - d1102000.sql
Rem    surman      01/28/10 - 9305120: Creation
Rem    surman      01/28/10 - Created
Rem

REM ===========================================================
REM set schema, registry
REM ===========================================================

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

EXECUTE dbms_registry.downgrading('CONTEXT');

REM drop all packages, procedures, programmatic types
@@ctxdpkg.sql

@@d1102000

REM ========================================================================
REM Registry to downgraded state
REM ========================================================================

EXECUTE dbms_registry.downgraded('CONTEXT','11.2.0');

REM ========================================================================
REM reset schema to SYS
REM ========================================================================     
ALTER SESSION SET CURRENT_SCHEMA = SYS;

