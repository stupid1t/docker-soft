Rem
Rem ctxpreup.sql
Rem
Rem Copyright (c) 2002, 2008, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      ctxpreup.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      This script contains common pre-upgrade steps.  Developers
Rem      should keep this up-to-date so that it is compatible with
Rem      the latest versions of everything.  But, because it runs 
Rem      before any data dictionary changes, be careful that it is 
Rem      also compatible with the lowest supported starting version!
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    rpalakod    01/10/08 - add temp version of ddl.set_attribute
Rem    wclin       03/02/07 - lrg 2885465: cleanup dummy impl type and ops
Rem                           first
Rem    gkaminag    10/28/05 - 
Rem    ehuang      08/02/02 - ehuang_component_upgrade_2
Rem    ehuang      07/30/02 - Created
Rem

REM ========================================================================
REM temporary versions of ctx_ddl routines
REM ========================================================================

create or replace procedure dr$temp_crepref(
  p_pre_name in varchar2,
  p_obj_name in varchar2
) is
 l_owner# number;
 l_pre_id number;
 l_obj_id number;
 l_cla_id number;
begin
 select user# into l_owner# from sys.user$ where name = 'CTXSYS';
 select dr_id_seq.nextval into l_pre_id from dual;
 select obj_id, obj_cla_id into l_obj_id, l_cla_id from dr$object
  where obj_name = p_obj_name;
 insert into dr$preference 
   (pre_id, pre_name, pre_owner#, pre_obj_id, pre_cla_id, pre_valid) 
    values 
   (l_pre_id, p_pre_name, l_owner#, l_obj_id, l_cla_id, 'Y');
    
 commit;
exception
  when dup_val_on_index then
    commit;
  when others then
    raise;
end;
/
create or replace procedure dr$temp_cresg(
  p_pre_name in varchar2,
  p_obj_name in varchar2
) is
 l_owner# number;
 l_pre_id number;
 l_obj_id number;
 l_cla_id number;
begin
 select user# into l_owner# from sys.user$ where name = 'CTXSYS';
 select dr_id_seq.nextval into l_pre_id from dual;
 select obj_id into l_obj_id from dr$object where obj_name = p_obj_name;
 insert into dr$section_group(sgp_id, sgp_owner#, sgp_name, sgp_obj_id) 
 values                      (l_pre_id, l_owner#, p_pre_name, l_obj_id);
 commit;
exception
  when dup_val_on_index then
    commit;
  when others then
    raise;
end;
/

/* Simple version of set_attribute - for default preferences ONLY
 * Restrictions:  
   1.) NO ERROR CHECKING!!!
   2.) For boolean values, enter '0' or '1' only
 */

create or replace procedure dr$temp_setatt(
  p_pre_name in varchar2,
  p_att_name in varchar2,
  p_att_val  in varchar2
) is
 l_owner# number;
 l_pre_id number;
 l_obj_id number;
 l_cla_id number;
 l_oat_id number;
 l_datatype char(1);
 l_aval varchar2(500);
begin

 l_aval := upper(p_att_val);

 select user# into l_owner# from sys.user$ where name='CTXSYS';
 select pre_id, pre_obj_id, pre_cla_id into 
        l_pre_id, l_obj_id, l_cla_id
   from dr$preference where pre_name = p_pre_name and
                            pre_owner# = l_owner#;
 begin
   select oat_id, oat_datatype into 
          l_oat_id, l_datatype
     from dr$object_attribute
    where oat_cla_id = l_cla_id
      and oat_obj_id = l_obj_id
      and oat_name   = p_att_name
      and oat_system = 'N';
 exception
    when no_data_found then
       null;
 end;

 /* insert into dr$preference_value */

 begin
   insert into dr$preference_value(prv_pre_id, prv_oat_id, prv_value)
       values(l_pre_id, l_oat_id, l_aval);
 exception
   when dup_val_on_index then
     update dr$preference_value 
        set prv_value = l_aval
      where prv_oat_id = l_oat_id
        and prv_pre_id = l_pre_id;
 end;

 update dr$preference
    set pre_valid = 'N'
  where pre_id = l_pre_id;

 commit;

exception
  when others then
    null;
end;
/

REM ========================================================================
REM drop DummyIndexMethods, dummyop, and ctx_dummyop
REM ========================================================================
drop type DummyIndexMethods force;
drop operator dummyop force;
drop package ctx_dummyop;


REM ========================================================================
REM dummy index implementation type in case t scripts need it
REM ========================================================================

create or replace type DummyIndexMethods authid definer as object
(
   key          RAW(4),
   objid        RAW(4),
   tmpobjid     RAW(4),

   static function ODCIGetInterfaces(ifclist OUT sys.ODCIObjectList)
            return number
);
/

PROMPT Create dummy implementation type body ...
PROMPT

create or replace type body DummyIndexMethods is

/* ----------------------- ODCIGetInterfaces ------------------------------ */

static function ODCIGetInterfaces(
  ifclist out    sys.ODCIObjectList
) return number 
is 
begin 
  ifclist := sys.ODCIObjectList(sys.ODCIObject('SYS','ODCIINDEX2')); 
  return sys.ODCIConst.Success; 
end ODCIGetInterfaces; 
end;
/
show errors


REM ========================================================================
REM dummy operator in case t scripts need it
REM ========================================================================

create or replace package ctx_dummyop authid definer as
    function dummyop(Colval in varchar2, 
                             Text in varchar2, ia sys.odciindexctx, 
                             sctx IN OUT DummyIndexMethods,
                             cflg number /*, env sys.ODCIEnv*/)
      return number is language C
      name "contains"
      library dr$lib
      with context
      parameters(
        context,
        Colval,
        Colval INDICATOR,
        Text,
        Text INDICATOR,
        Text LENGTH,
        ia,
        ia INDICATOR STRUCT,
        sctx,
        sctx INDICATOR STRUCT,
        cflg,
        cflg INDICATOR,
/*
        env,
        env  INDICATOR STRUCT,
*/
        return OCINumber
      );
end ctx_dummyop;
/

PROMPT Create dummy operator ...
PROMPT

create or replace operator dummyop binding 
  (varchar2, varchar2) return number 
     with index context, scan context DummyIndexMethods
without column data using ctx_dummyop.dummyop;

grant execute on dummyop to public;
