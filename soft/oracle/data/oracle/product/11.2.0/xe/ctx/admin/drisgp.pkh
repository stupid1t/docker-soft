rem
rem Copyright (c) 2002, 2010, Oracle and/or its affiliates. 
rem All rights reserved. 
rem
rem    NAME
rem      drisgp.pkh - DR Internal Section GrouP management
rem    DESCRIPTION
rem      This package contains internal code to create, manage, and drop
rem      section groups
rem
rem    NOTES
rem    MODIFIED    (MM/DD/YY)
rem    rpalakod     07/25/10 - Backport rpalakod_bug-9749745 from main
rem    surman       02/12/10 - 9364581: Add new_sdata_section and
rem    rkadwe       04/15/10 - XbranchMerge rkadwe_incr_mdata_lim from
rem                            st_ctx_11.1.2.2
rem    rkadwe       03/26/10 - Document Level Lexer Phase 2
rem    rkadwe       03/11/10 - Document Level Lexer
rem    rpalakod     02/24/10 - Bug 9409038
rem    rkadwe       02/09/10 - Remove MDATA section cap
rem    nenarkhe     10/30/09 - GetSections
rem    nenarkhe     10/19/09 - HasSectionType
rem    rkadwe       06/22/09 - Remove Field Section Cap
rem    surman       12/09/08 - 7540290: binary_integer to number
rem    wclin        10/10/08 - change GetSectionType() to GetSection()
rem    nenarkhe     09/10/08 - MVDATA support
rem    rigandhi     06/06/06 - name search 
rem    ymatsuda     02/17/06 - sdata section support 
rem    gkaminag     10/16/05 - column mdata 
rem    gkaminag     09/30/05 - cdi indexing 
rem    gkaminag     10/21/03 - mdata phase 2 
rem    surman       09/09/03 - 3101316: Add drop_user_section_groups 
rem    gkaminag     09/16/03 - mdata 
rem    gkaminag     08/19/02 - gkaminag_security_phase2_020620
rem    gkaminag     07/22/02  - 
rem    gkaminag     07/02/02 -  creation

create or replace package drisgp as

  SEC_TYPE_UNKNOWN             constant number :=  0;
  SEC_TYPE_ZONE                constant number :=  1;
  SEC_TYPE_FIELD               constant number :=  2;
  SEC_TYPE_SPECIAL             constant number :=  3;
  SEC_TYPE_STOP                constant number :=  4;
  SEC_TYPE_ATTR                constant number :=  5;
  SEC_TYPE_XML                 constant number :=  6;
  SEC_TYPE_MDATA               constant number :=  7;
  SEC_TYPE_CSDATA              constant number :=  8;
  SEC_TYPE_CMDATA              constant number :=  9;
  SEC_TYPE_SDATA               constant number := 10;
  SEC_TYPE_NDATA               constant number := 11;
  SEC_TYPE_MVDATA              constant number := 12;

  SEC_DATATYPE_UNKNOWN         constant binary_integer :=  0;
  SEC_DATATYPE_NUMBER          constant binary_integer :=  2;
  SEC_DATATYPE_VARCHAR2        constant binary_integer :=  5;
  SEC_DATATYPE_DATE            constant binary_integer := 12;
  SEC_DATATYPE_RAW             constant binary_integer := 23;
  SEC_DATATYPE_CHAR            constant binary_integer := 96;

  SEC_ZONE_FID                 constant number :=  1;  

  SEC_FIELD_LOW_MIN_FID        constant number :=  16; 
  SEC_FIELD_LOW_MAX_FID        constant number :=  79;
  SEC_FIELD_HIGH_MIN_FID       constant number :=  1001; 
  SEC_FIELD_HIGH_MAX_FID       constant number :=  1333;  

  SEC_MDATA_LOW_MIN_FID        constant number := 400; 
  SEC_MDATA_LOW_MAX_FID        constant number := 499;
  SEC_MDATA_HIGH_MIN_FID       constant number := 5000;
  SEC_MDATA_HIGH_MAX_FID       constant number := 5999;
  SEC_MAX_MDATA                constant number := 100;

  SEC_NDATA_MIN_FID            constant number := 200; 
  SEC_NDATA_MAX_FID            constant number := 299;
  SEC_MAX_NDATA                constant number := 100;

  SEC_SDATA_MIN_FID            constant number := 101;
  SEC_SDATA_MAX_FID            constant number := 132;
  SEC_MAX_SDATA                constant number := 32;

  SEC_MVDATA_MIN_FID           constant number := 300;
  SEC_MVDATA_MAX_FID           constant number := 399;
  SEC_MAX_MVDATA               constant number := 100;

/*-------------------- create_section_group  ---------------------------*/
/* create a new section group */

PROCEDURE create_section_group(
  p_group_name   in    varchar2
, p_group_type   in    varchar2
);

/*-------------------- drop_section_group  ---------------------------*/
/* drop a section group */

PROCEDURE drop_section_group(
  p_group_name   in    varchar2
);

/*-------------------- drop_user_section_groups ----------------------*/
/* Drop section groups owned by a user */

PROCEDURE drop_user_section_groups(
  p_user_name in varchar2 := null
);

/*-------------------- add_section --------------------*/
/* add a section to a section group */

PROCEDURE add_section(
  p_group_name     in     varchar2,
  p_section_name   in     varchar2,
  p_tag            in     varchar2,
  p_section_type   in     binary_integer,
  p_visible        in     boolean,
  p_datatype       in     varchar2 default NULL
);

/*-------------------- remove_section ---------------------------*/

PROCEDURE remove_section(
  group_name    in    varchar2,
  section_name  in    varchar2
);

PROCEDURE remove_section(
  group_name     in    varchar2,
  section_id     in    number
);

/*--------------------------- get_default_section ------------------*/
/* get the default value for the section group preference */

function get_default_section(
  p_dstore_pref in varchar2,
  p_filter_pref in varchar2,
  p_dtype_pref  in varchar2
)
return varchar2;

/*------------------------ copy_section_group ------------------------------*/
/* copy section group info into index meta data */

function copy_section_group(
  p_idx_id     in  number, 
  p_sgroup     in  varchar2,
  p_rcount     out number,
  alt_I        out boolean,
  p_new_sdata_section  out boolean
) 
return dr_def.pref_rec;

/*----------------------- IndexAddSection  ---------------------------*/
/* add a section to an already existing index */

PROCEDURE IndexAddSection(
  ia          in  sys.ODCIIndexInfo,
  idx         in  dr_def.idx_rec,
  sectype     in  varchar2,
  secname     in  varchar2,
  tag         in  varchar2,
  visible     in  number,
  NewSdataSec out boolean,
  dtype       in  number,
  alt_I       out boolean,
  skip_lk_com in boolean
);

/*----------------------- LoadSectionMD  ---------------------------*/
/* load section metadata from the index values table */

PROCEDURE LoadSectionMD(
  p_idxid    in         number,
  p_fonly    in         boolean,
  o_sectab   out nocopy dr_def.sec_tab
);

/*----------------------- GetSection ------------------------------*/
/* return type of section given name */
FUNCTION GetSection(
  p_idxid    in         number,
  p_secname  in         varchar2
) return dr_def.sec_rec;

/*----------------------- GetSections ------------------------------*/
/* return sections associated with given index */
FUNCTION GetSections(
  p_idxid    in         number
) return dr_def.sec_tab;

/*----------------------- HasSectionType ------------------------------*/
/* return true if section group has section of given type */
FUNCTION HasSectionType(
  p_sec_grp  in         varchar2,
  p_sectype  in         number
) return boolean;

/*--------------------------- field_to_mdata ------------------------------*/
/* make the metadata changes to convert a field section to mdata */
PROCEDURE field_to_mdata(
  idx in dr_def.idx_rec,
  fsec in varchar2,
  read_only in boolean,
  ftyp out number,
  mdatatyp out number
);

/*----------------------- IndexRemSection  ---------------------------*/
/* Remove section from index metadata, used for doc lexer */
PROCEDURE IndexRemSection(
  ia          in  sys.ODCIIndexInfo,
  idx         in  dr_def.idx_rec,
  sectype     in  varchar2,
  secname     in  varchar2,
  sectag      in  varchar2,
  secfid      in  number,
  skip_lk_com in  boolean
);

/*----------------------- SecIsMDATA -------------------------------*/
/* Returns true if given fid/ttype is MDATA   */
FUNCTION SecIsMDATA(
  sec_fid    in         number
) return boolean;

/*--------------------------- CheckUpdMdata ------------------------*/
/* check that mdata section is updateable, and return token_type */
FUNCTION CheckUpdMdata(
  p_idx     in dr_def.idx_rec,
  p_secname in varchar2
) return binary_integer;

end drisgp;
/
