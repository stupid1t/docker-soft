Rem
Rem $Header: t1100000.sql 17-oct-2005.15:20:47 gkaminag Exp $
Rem
Rem t1100000.sql
Rem
Rem Copyright (c) 2005, Oracle. All rights reserved.  
Rem
Rem    NAME
Rem      t1100000.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      upgrade indextype implementation types from 11.0.0.0 to current
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    gkaminag    10/17/05 - gkaminag_upg_051018
Rem    gkaminag    10/16/05 - Created
Rem

