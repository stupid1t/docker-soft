#!/bin/bash
#
#
#
# Copyright (c) 2005, 2011, Oracle and/or its affiliates. All rights reserved. 
#
#  NAME
#      createdb.sh - Create an XE initial database
#
#  DESCRIPTION
#
#      This script will create the initial XE database,
#      run catalog, catproc, install Oracle Text and XDB.
#      It will also create the initial
#      init.ora file and an spfile at the end
#
#      It will initially remove all existing files
#
#  ARGUMENTS
#
#      -force:    Avoid the 10 second delay
#      -shiploc:  Put files in /usr/lib/oracle/oexpress
#      -dbchar:   Specify the character set of the database
#      -filedir:  Location where database files should be created
#      -logdir:   Locations where the log files will be generated
#
#  NOTES
#
#    MODIFIED   (MM/DD/YY)
#     svaggu     01/21/11 - Updated user.dbf limit to 11GB
#     bkhaladk   01/18/06 - recompile invalid xdb packages 
#     rpingte    01/05/06 - revoke execute permissions for UTL_FILE,UTL_TCP,
#                           UTL_HTTP and UTL_SMTP for public
#     rpingte    12/06/05 - bug# 4776339: set local host access
#     rpingte    12/06/05 - bug# 4778605: set time zone
#     rpingte    12/08/05 - remove maxsize of sysaux
#     sravada    11/28/05 - add Spatial locator to seed 
#     rpingte    11/16/05 - add init.ora parameters and limit user.dbf to 5GB
#     chliang    10/23/05 - add exec permission
#     rpingte    10/17/05 - move htmldb to loadhtmldb.sh
#     rpingte    10/07/05 - set ftp port to 0 & http port to 8080
#     rpingte    10/03/05 - set ftp port to 0
#     rpingte    09/29/05 - Increase shared servers to 4 for HTMLDB
#     rpingte    09/21/05 - add options for filedir and logdir
#     rpingte    09/20/05 - Load HTMLDB only when lang option is specified
#     rpingte    08/17/05 - remove dbs and log directories & use OH for epg
#     rpingte    09/08/05 - load_trans.sql for htmldb
#     rpingte    09/07/05 - rpingte_db_creation_scripts
#     rpingte    08/25/05 - use ORACLE_HOME
#     bengsig    08/19/05 - Creation
#

# Check validity of ORACLE_HOME and set SID
if test -z "$ORACLE_HOME" -o ! -w $ORACLE_HOME/dbs
then
  echo ORACLE_HOME must be set and '$'ORACLE_HOME/dbs must be writable
  exit 1
fi

force=no
shiploc=no
dbchar=no
dbcharset=us7ascii
fileloc=no
logloc=no
filedir=$ORACLE_HOME/dbs
logdir=$ORACLE_HOME/log

while [ $# -gt 0 ]
do
  case $1 in 
    -force) force=yes
            ;;
    -shiploc) shiploc=yes
              ;;
    -dbchar) dbchar=yes
             dbcharset=$2
             shift
             ;;
    -filedir) fileloc=yes
             filedir=$2
              shift
              ;;
    -logdir) logloc=yes
             logdir=$2
             shift
             ;;
    -help)
        cat << END
Usage: createdb {-shiploc | -force | -dbchar us7ascii | -help}

  -shiploc create database in shipping location
  -force   silently overwrite database
  -dbchar  specify the database character set
  -filedir full path to create the database file in, default $ORACLE_HOME/dbs
  -logdir  full path where the log files will be generated, default $ORACLE_HOME/log
  -help    this help
END
        exit 0
	;;
    -*) echo createdb: Unknown argument $arg - try -help; exit 1
        ;;
   esac

   shift

done

export ORACLE_SID=XE
echo ORACLE_HOME=$ORACLE_HOME
echo ORACLE_SID=$ORACLE_SID

if test $force = yes
then
  :
else
  echo "Existing database will be erased, hit ctrl-c within 10 secs to cancel"

  trap "echo Cancelled...; exit 1" SIGINT

  sleep 10
fi

# check to see if database is already running?
pmon=`ps -ef | egrep pmon_$ORACLE_SID'\>' | grep -v grep`
 
if [ "$pmon" != "" ]; 
then 
    echo database is already running
    echo "$pmon"
    echo Please shutdown $ORACLE_SID before running `basename $0`
    exit 1
fi

if test -z "$filedir" -a -d $filedir -a -w $filedir
then
  echo filedir must be set and $filedir must be writable
  exit 1
fi
rm -rf $filedir

if test -z "$logdir" -a -d $logdir -a -w $logdir
then
  echo logdir must be set and $logdir must be writable
  exit 1
fi
rm -rf $logdir
mkdir $logdir
mkdir $filedir
rm -rf $ORACLE_HOME/flash_recovery_area
mkdir $ORACLE_HOME/flash_recovery_area

if test -r $ORACLE_HOME/dbs/init${ORACLE_SID}_DEFAULT.ora
then
  rm $ORACLE_HOME/dbs/init${ORACLE_SID}_DEFAULT.ora
fi

cat <<END >  $filedir/init$ORACLE_SID.ora
db_name=$ORACLE_SID

control_files=$filedir/control.dbf

undo_management=auto
undo_tablespace=undotbs1

sga_target=376M
pga_aggregate_target=224M

sessions=20
open_cursors=300

remote_login_passwordfile=EXCLUSIVE

compatible=11.2.0.0.0

diagnostic_dest=$logdir
audit_file_dest=$logdir

job_queue_processes=4
shared_servers=4
db_recovery_file_dest_size=10G
db_recovery_file_dest="$ORACLE_HOME/flash_recovery_area"
END

echo 'dispatchers="(PROTOCOL=TCP) (SERVICE='${ORACLE_SID}'XDB)"' >> $filedir/init$ORACLE_SID.ora

# Create the password file
orapwd file=$filedir/orapw$ORACLE_SID password=oracle entries=5

export ORACLE_PATH=$ORACLE_HOME/rdbms/admin

sqlplus /nolog <<END
spool xe_createdb.log
connect sys/oracle as sysdba
startup nomount pfile=$filedir/init$ORACLE_SID.ora

whenever sqlerror exit;

create database 
  maxinstances 1
  maxloghistory 2
  maxlogfiles 16
  maxlogmembers 2
  maxdatafiles 30
datafile '$filedir/system.dbf'
  size 200M reuse autoextend on next 10M maxsize 600M
  extent management local
sysaux datafile '$filedir/sysaux.dbf'
  size 10M reuse autoextend on next  10M
default temporary tablespace temp tempfile '$filedir/temp.dbf'
  size 20M reuse autoextend on next  10M maxsize 500M
undo tablespace undotbs1 datafile '$filedir/undotbs1.dbf'
  size 50M reuse autoextend on next  5M maxsize 500M
 --character set al32utf8
 character set $dbcharset
 national character set al16utf16
 set time_zone='00:00'
 controlfile reuse
 logfile '$filedir/log1.dbf' size 50m reuse
       , '$filedir/log2.dbf' size 50m reuse
       , '$filedir/log3.dbf' size 50m reuse
user system identified by oracle
user sys identified by oracle
/

-- create the tablespace for users data
create tablespace users
  datafile '$filedir/users.dbf'
  size 100M reuse autoextend on next 10M maxsize 11G
  extent management local
/

-- install data dictionary views:
@?/rdbms/admin/catalog.sql

-- run catblock
@?/rdbms/admin/catblock

-- run catproc
@?/rdbms/admin/catproc

-- run catoctk
@?/rdbms/admin/catoctk

-- run pupbld
connect system/oracle
@?/sqlplus/admin/pupbld
@?/sqlplus/admin/help/hlpbld.sql helpus.sql;

-- run plustrace
connect sys/oracle as sysdba
@?/sqlplus/admin/plustrce

-- Install context
@?/ctx/admin/catctx oracle SYSAUX TEMP NOLOCK;
connect CTXSYS/oracle
@?/ctx/admin/defaults/dr0defin.sql "AMERICAN"

-- Install XDB
connect sys/oracle as sysdba
@?/rdbms/admin/catqm.sql oracle SYSAUX TEMP;
connect SYS/oracle as SYSDBA
@?/rdbms/admin/catxdbj.sql;
connect SYS/oracle as SYSDBA
@?/rdbms/admin/catxdbdbca.sql 0 8080;
connect SYS/oracle as SYSDBA
begin dbms_xdb.setListenerLocalAccess( l_access => TRUE ); end; 
/

-- Install Spatial Locator
connect sys/oracle as sysdba
create user MDSYS identified by MDSYS account lock;
Alter session set current_schema=MDSYS;
Alter user MDSYS default tablespace SYSAUX;
@?/md/admin/mdprivs.sql;
@?/md/admin/catmdloc.sql;
Alter session set current_schema=SYS;

create spfile='$filedir/spfile.ora' from pfile
/

alter user anonymous account unlock
/

disconnect

-- recompile invalid objects
connect sys/oracle as sysdba
begin dbms_workload_repository.modify_snapshot_settings(interval => 0); end; 
/
begin dbms_scheduler.disable('AUTO_SPACE_ADVISOR_JOB', true); end;
/
spool off
exit
END


err=`grep "ORA-12701" xe_createdb.log`
if [ "$err" != "" ];
then
  echo Invalid Database character specified: $dbcharset
  exit 1
else
  echo Database Created
fi

#
# Install HR demo
echo exit | sqlplus "sys/oracle as sysdba" @?/demo/schema/human_resources/hr_main HR USERS TEMP oracle demo_

# grant connect to hr
sqlplus /nolog << END
connect sys/oracle as sysdba
grant connect to hr;
revoke execute on sys.UTL_FILE from public;
revoke execute on sys.UTL_TCP  from public;
revoke execute on sys.UTL_HTTP from public;
revoke execute on sys.UTL_SMTP from public;
grant execute on UTL_FILE to XDB;
exit
END

sqlplus /nolog << END
connect sys/oracle as sysdba
-- recompile invalid objects
@?/rdbms/admin/utlrp.sql;
spool off
exit
END

# Save the oldfashioned init.ora file
mv $filedir/init$ORACLE_SID.ora $ORACLE_HOME/dbs/init${ORACLE_SID}_DEFAULT.ora

# create spfile pointer if shiploc
if test $shiploc = yes
then
  echo "spfile=$filedir/spfile.ora"  > $ORACLE_HOME/dbs/init$ORACLE_SID.ora
else
  # It was created the right place anyway
  :
fi

exit
