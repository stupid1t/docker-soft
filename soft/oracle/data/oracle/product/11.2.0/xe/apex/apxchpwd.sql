Rem  Copyright (c) Oracle Corporation 1999 - 2007. All Rights Reserved.
Rem
Rem    NAME
Rem      apxchpwd.sql
Rem
Rem    DESCRIPTION
Rem      Used to set the password for the Application Express ADMIN user.
Rem
Rem    NOTES
Rem      Assumes the SYS user is connected.
Rem
Rem    REQUIREMENTS
Rem      - Oracle 9.2.0.3 or later
Rem
Rem
Rem    MODIFIED   (MM/DD/YYYY)
Rem      jstraub   08/08/2007 - Created
Rem      jstraub   09/04/2007 - Added HIDE to PASSWD accept (Bug 6370075)
Rem      jkallman  07/08/2008 - Change FLOWS_030100 references to FLOWS_040000
Rem      jkallman  10/02/2008 - Change FLOWS_040000 references to APEX_040000
Rem

set define '&'

set verify off

prompt Enter a value below for the password for the Application Express ADMIN user.
prompt
prompt

accept PASSWD CHAR prompt 'Enter a password for the ADMIN user              [] ' HIDE

alter session set current_schema = APEX_040000;

prompt ...changing password for ADMIN

begin

    wwv_flow_security.g_security_group_id := 10;
    wwv_flow_security.g_user := 'ADMIN';
    wwv_flow_security.g_import_in_progress := true;

    for c1 in (select user_id
                 from wwv_flow_fnd_user
                where security_group_id = wwv_flow_security.g_security_group_id
                  and user_name = wwv_flow_security.g_user) loop

        wwv_flow_fnd_user_api.edit_fnd_user(
            p_user_id       => c1.user_id,
            p_user_name     => wwv_flow_security.g_user,
            p_web_password  => '&PASSWD',
            p_new_password  => '&PASSWD');
    end loop;

    wwv_flow_security.g_import_in_progress := false;

end;
/
commit;
