Rem  Copyright (c) Oracle Corporation 1999 - 2006. All Rights Reserved.
Rem
Rem    NAME
Rem      apxxepwd.sql
Rem
Rem    DESCRIPTION
Rem      Changes the password for the INTERNAL ADMIN user
Rem
Rem    NOTES
Rem      Assumes the SYS user is connected.  
Rem 
Rem    REQUIRENTS
Rem      - Oracle 10g
Rem
Rem
Rem    MODIFIED   (MM/DD/YYYY)
Rem      jstraub   08/01/2006 - Created
Rem      jkallman  09/29/2006 - Adjusted current_schema to FLOWS_030000 
Rem      jkallman  08/02/2007 - Change FLOWS_030000 references to FLOWS_030100
Rem      jkallman  07/08/2008 - Change FLOWS_030100 references to FLOWS_040000
Rem      jkallman  10/02/2008 - Change FLOWS_040000 references to APEX_040000

Rem

set define '&'

set verify off

alter session set current_schema = APEX_040000;

prompt ...changing password for ADMIN

begin
    
    wwv_flow_security.g_security_group_id := 10;
    wwv_flow_security.g_user := 'ADMIN';
    wwv_flow_security.g_import_in_progress := true;
    
    for c1 in (select user_id
                 from wwv_flow_fnd_user 
                where security_group_id = wwv_flow_security.g_security_group_id
                  and user_name = wwv_flow_security.g_user) loop
                    
        wwv_flow_fnd_user_api.edit_fnd_user(
            p_user_id       => c1.user_id,
            p_user_name     => wwv_flow_security.g_user,
            p_web_password  => '&1',
            p_new_password  => '&1');                    
    end loop;
    
    wwv_flow_security.g_import_in_progress := false;
                              
end;
/
commit;    
