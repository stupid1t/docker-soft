/* Copyright (c) 2003, 2010, Oracle and/or its affiliates. 
All rights reserved. */
 
/* 
   NAME 
     spen0.h - Port-specific part of PL/SQL Execute Native interfaces

     **!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     **!!!! THIS FILE IS SHIPPED FOR NCOMP.                        !!!!
     **!!!!                                                        !!!!
     **!!!! If you change it for a bug fix, you will need to make  !!!!
     **!!!! sure it is re-shipped also along with the new binaries.!!!!
     **!!!! Please make this note in the BUGDB along with your fix.!!!!
     **!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   DESCRIPTION 
     This file defines port-specific macro for PL/SQL Execute Natively
     compiled units.

   NOTES
     Remove slg macro definitions once we start shipping core macros.

   MODIFIED   (MM/DD/YY)
   nmuthukr    04/30/10 - x64 <-> X86 unification project
   mvemulap    04/28/06 - bug 4993016 fix 
   mvemulap    01/05/06 - bug 4728671 fix 
   dbronnik    01/24/06 - Bug 4993012: pull in PL fixes to base file
   stolstoy    02/07/04 - Align jump buffer (slgbuf) for win64
   cracicot    07/11/05 - ANSI prototypes; miscellaneous cleanup 
   bwadding    06/13/05 - ANSI prototypes; miscellaneous cleanup 
   dbronnik    11/22/03 - dbronnik_bug-3263250 
   dbronnik    11/20/03 - Creation

*/

#ifndef SPEN0_ORACLE
# define SPEN0_ORACLE

#ifndef ORATYPES
# include <oratypes.h>
#endif

/*---------------------------------------------------------------------------
                     PUBLIC TYPES AND CONSTANTS
  ---------------------------------------------------------------------------*/

#ifndef SL_ORACLE

#include <setjmp.h>

#ifdef WIN64
struct slgbuf 
{ 
  jmp_buf slgbuf_jb;
  ub8     pad;
};
#else
struct slgbuf 
{ 
  jmp_buf slgbuf_jb;

#ifdef ORAX86_64
  char pad[16];
#endif 
};
#endif
typedef struct slgbuf slgbuf;

#ifdef WIN64
#define slgset(p)     (sword)setjmp(*(jmp_buf *)((((ubig_ora)&(p)->slgbuf_jb) + 0xf) & ~0xf)) 
#define slgjmp(p, w)  longjmp(*(jmp_buf *)((((ubig_ora)&(p)->slgbuf_jb) + 0xf) & ~0xf), (w)) 
#else
# ifdef ORAX86_64
# define slgset(p)    (sword)setjmp(*(jmp_buf *)((((unsigned long)&(p)->slgbuf_jb) + 0xf) & ~0xf)) 
# define slgjmp(p, w) longjmp(*(jmp_buf *)((((unsigned long)&(p)->slgbuf_jb) + 0xf) & ~0xf), (w)) 
# else  /* ORAX86_64 */
# define slgset(p)     (sword)setjmp((p)->slgbuf_jb)
# define slgjmp(p, w)  longjmp((p)->slgbuf_jb, (w))
# endif /* ORAX86_64 */
#endif

/* SPEN_JMPSET: This is the same as slgset but the function is
 * invoked indirectly.
 */
#ifdef WIN64
#define SPEN_JMPSET(setjmp_func,p) (sword)setjmp_func(*(jmp_buf *)((((ubig_ora)&(p)->slgbuf_jb) + 0xf) & ~0xf))
#else
# ifdef ORAX86_64
# define SPEN_JMPSET(setjmp_func,p) \
    (sword)setjmp_func(*(jmp_buf *)((((ubig_ora)&(p)->slgbuf_jb) + 0xf) & ~0xf)) 
# else  /* ORAX86_64 */
# define SPEN_JMPSET(setjmp_func,p) (sword)setjmp_func((p)->slgbuf_jb)
# endif /* ORAX86_64 */
#endif

#endif

/* SPEN_SETJMP: Define this macro only if on this port 
 * setjmp is implemented as a function whose address can
 * be taken and stored in the pen_kernel_vector array.
 */
#define SPEN_SETJMP _setjmp

#endif                                              /* SPEN0_ORACLE */
