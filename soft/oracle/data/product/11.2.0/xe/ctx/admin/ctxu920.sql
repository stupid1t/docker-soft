Rem
Rem $Header: ctx_src_2/src/dr/admin/ctxu920.sql /st_ctx_11.2.0/1 2010/08/11 21:21:50 rpalakod Exp $
Rem
Rem ctxu920.sql
Rem
Rem Copyright (c) 2002, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      ctxu920.sql 
Rem
Rem    DESCRIPTION
Rem      component upgrade from 9.2.0 to 10i
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    rpalakod    08/06/10 - Bug 9973683
Rem    rpalakod    04/30/10 - u1102000.sql
Rem    rpalakod    04/29/10 - Bug 9669751
Rem    rpalakod    09/11/09 - Bug 8892286
Rem    rpalakod    08/10/09 - autooptimize
Rem    rpalakod    06/07/08 - 11.2
Rem    yucheng     06/21/05 - load 10.2 upgrade scripts 
Rem    gkaminag    10/07/04 - val proc to sys 
Rem    gkaminag    03/18/04 - version 
Rem    gkaminag    08/22/03 - drop unneeded ctxcat update procedures 
Rem    ehuang      01/21/03 - use default version number
Rem    ehuang      12/12/02 - add parameters
Rem    gkaminag    10/29/02 - security upgrade
Rem    ehuang      07/29/02 - component upgrade 
Rem    ehuang      07/02/02 - move from u1000000.sql
Rem    ehuang      06/17/02 - ehuang_component_upgrade
Rem    ehuang      06/11/02 - Created - component upgrade
Rem    ehuang      05/21/02 - add mail_filter_config_file parametr.
Rem    ehuang      04/27/02 - ehuang_mail_filter_020420
Rem    ehuang      04/20/02 - Created
Rem

Rem  =======================================================================
Rem  
Rem  ******************** changes to be made by SYS ************************
Rem
Rem  =======================================================================

@@s0902000.sql
@@s1001002.sql
@@s1002000.sql

Rem  =======================================================================
Rem  
Rem  ***********************   end of SYS changes  *************************
Rem
Rem  =======================================================================

REM ========================================================================
REM set schema, Registry to upgrading state
REM ========================================================================

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

begin
dbms_registry.upgrading('CONTEXT','Oracle Text','validate_context','CTXSYS');
end;
/

REM ========================================================================
REM 
REM ******************* Begin CTXSYS schema changes ************************
REM
REM ========================================================================

REM ========================================================================
REM Pre-upgrade steps
REM ========================================================================

@@ctxpreup.sql

REM ========================================================================
REM 9.2.0 to 10.1.0
REM ========================================================================

@@u0902000.sql
@@t0902000.sql

REM ========================================================================
REM 10.1 to 10.2
REM ========================================================================

@@u1001002.sql
@@t1001002.sql

REM ========================================================================
REM 10.2 to 11.1.0
REM ========================================================================

@@u1002000.sql
@@t1002000.sql

REM ========================================================================
REM 11.1.0 to 11.2.0.1
REM ========================================================================

@@u1100000.sql
@@t1100000.sql

REM ========================================================================
REM 11.2.0.1 to 11.2.0.2
REM ========================================================================

@@u1102000.sql
@@t1102000.sql

REM ========================================================================
REM Post-upgrade steps
REM ========================================================================

@@ctxposup.sql

REM ========================================================================
REM reconstitute ctxcat triggers (packages should be valid)
REM ========================================================================

ALTER SESSION SET CURRENT_SCHEMA = SYS;

begin
  for c1 in (select u.name idx_owner, idx_name, idx_id
               from ctxsys.dr$index, sys.user$ u
              where idx_owner# = u.user#
                and idx_type = 1)
  loop
    begin
    execute immediate 'drop procedure '||
       'ctxsys.dr$'||ltrim(to_char(c1.idx_id))||'$u';
    exception when others then null; end;
    ctxsys.drvxtabc.recreate_trigger(c1.idx_owner, c1.idx_name, c1.idx_id);
  end loop;
end;
/

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

REM ========================================================================
REM special case; default policy ENT_EXT_DICT_OBJ
REM ========================================================================

PROMPT ...creating default policy for ent_ext_dict_obj

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.ENT_EXT_DICT_OBJ',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM special case; default policy AUTO_OPT_OBJ
REM ========================================================================

PROMPT ...creating default policy for auto_opt_obj

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.AUTO_OPT_OBJ',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM special case; default policy STOP_OPT_LIST
REM ========================================================================

PROMPT creating default policy for STOP_OPT_LIST

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.STOP_OPT_LIST',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/ 

REM ========================================================================
REM
REM ****************  End CTXSYS schema change *****************************
REM
REM ========================================================================

REM ========================================================================
REM Registry to upgraded state, reset schema
REM ========================================================================

begin
  dbms_registry.loaded('CONTEXT', '11.2.0.2.0');
  dbms_registry.valid('CONTEXT');
end;
/

ALTER SESSION SET CURRENT_SCHEMA = SYS;
