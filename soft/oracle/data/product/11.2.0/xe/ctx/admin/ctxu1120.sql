Rem
Rem $Header: ctx_src_2/src/dr/admin/ctxu1120.sql /st_ctx_11.2.0/1 2010/08/11 21:21:50 rpalakod Exp $
Rem
Rem ctxu1120.sql
Rem
Rem Copyright (c) 2008, 2010, Oracle and/or its affiliates. 
Rem All rights reserved. 
Rem
Rem    NAME
Rem      ctxu1120.sql - <one-line expansion of the name>
Rem
Rem    DESCRIPTION
Rem      upgrade from 11.2.0.0 to latest version
Rem
Rem    NOTES
Rem      <other useful comments, qualifications, etc.>
Rem
Rem    MODIFIED   (MM/DD/YY)
Rem    rpalakod    08/06/10 - Bug 9973683
Rem    rpalakod    04/30/10 - create auto-optimize policy
Rem    rpalakod    06/07/08 - 11.2
Rem    rpalakod    06/07/08 - Created
Rem

REM ========================================================================
REM set schema, Registry to upgrading state
REM ========================================================================

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

begin
dbms_registry.upgrading('CONTEXT','Oracle Text','validate_context','CTXSYS');
end;
/

REM ========================================================================
REM 
REM ******************* Begin SYS changes **********************************
REM
REM ========================================================================

ALTER SESSION SET CURRENT_SCHEMA = SYS;
@@s1102000.sql
ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

REM ========================================================================
REM 
REM ******************* End SYS changes ************************************
REM
REM ========================================================================

REM ========================================================================
REM 
REM ******************* Begin CTXSYS schema changes ************************
REM
REM ========================================================================

REM ========================================================================
REM Pre-upgrade steps
REM ========================================================================

@@ctxpreup.sql

REM ========================================================================
REM 11.2 to next version
REM ========================================================================

@@u1102000.sql
@@t1102000.sql

REM ========================================================================
REM Post-upgrade steps
REM ========================================================================

@@ctxposup.sql

ALTER SESSION SET CURRENT_SCHEMA = CTXSYS;

REM ========================================================================
REM special case; default policy AUTO_OPT_OBJ
REM ========================================================================

PROMPT ...creating default policy for auto_opt_obj

declare
  errnum number;
begin
  CTX_DDL.create_policy('CTXSYS.AUTO_OPT_OBJ',
    filter        => 'CTXSYS.NULL_FILTER',
    section_group => 'CTXSYS.NULL_SECTION_GROUP',
    lexer         => 'CTXSYS.BASIC_LEXER',
    stoplist      => 'CTXSYS.EMPTY_STOPLIST',
    wordlist      => 'CTXSYS.BASIC_WORDLIST'
);
exception
  when others then
    errnum := SQLCODE;
    if (errnum = -20000) then
      null;
    else
      raise;
    end if;
end;
/

REM ========================================================================
REM
REM ****************  End CTXSYS schema change *****************************
REM
REM ========================================================================

REM ========================================================================
REM Registry to upgraded state, reset schema
REM ========================================================================

begin
  dbms_registry.loaded('CONTEXT');
  dbms_registry.valid('CONTEXT');
end;
/

ALTER SESSION SET CURRENT_SCHEMA = SYS;

