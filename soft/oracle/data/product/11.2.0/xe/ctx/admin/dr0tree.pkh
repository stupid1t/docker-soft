--
-- $Header: ctx_src_2/src/dr/admin/dr0tree.pkh /main/2 2010/05/26 17:56:44 igeller Exp $
--
-- dr0tree.pkh
--
-- Copyright (c) 2007, 2010, Oracle and/or its affiliates. 
-- All rights reserved. 
--
--   NAME
--     dr0tree.pkh - CTX_TREE package
--
--   DESCRIPTION
--     Package header for CTX_TREE package
--
--   RETURNS
--
--   NOTES
--     <other useful comments, qualifications, etc.>
--
--   MODIFIED   (MM/DD/YY)
--   igeller     05/12/10 - Adding optimization mode for $A and $F trees
--   nenarkhe    12/22/09 - changing drv$tree to a sequence
--   nenarkhe    09/17/09 - register_index()
--   nenarkhe    09/05/09 - remove_node
--   nenarkhe    08/18/09 - ctx_tree.get_roots()
--   nenarkhe    06/29/09 - Creation

CREATE OR REPLACE PACKAGE ctx_tree AUTHID current_user AS

/*------------------------------- TYPE DEFINITIONS ------------------------*/

/*---------------------------- register_index ----------------------*/
/*
   NAME
     register_index

   DESCRIPTION
     registers an index with the CTX_TREE package

   ARGUMENT
     index_name		- the name of the index

*/

PROCEDURE register_index(
  index_name      IN VARCHAR2
);

/*---------------------------- add_node ----------------------*/
/*
   NAME
     add_node

   DESCRIPTION
     add a new node to a tree associated with a MVDATA section and index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     path               - the new path to be added to a tree 
     id_list            - return value of the corresponding node ids
     force              - complete successfully even if the path already
                          exists 

*/

PROCEDURE add_node(
  index_name      IN VARCHAR2,
  section_name    IN VARCHAR2,
  path            IN CLOB,
  id_list         OUT CLOB,
  force           IN BOOLEAN DEFAULT FALSE 
);

/*---------------------------- get_node_id ----------------------*/
/*
   NAME
     get_node_id

   DESCRIPTION
     looks up the node identifier of an existing path in a tree 
     associated with a MVDATA section and index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     path               - an existing path in a tree 
     id                 - return value of the corresponding node id 

*/

PROCEDURE get_node_id(
  index_name      IN VARCHAR2,
  section_name    IN VARCHAR2,
  path            IN CLOB,
  id              OUT INTEGER
) ;

/*---------------------------- get_children ----------------------*/
/*
   NAME
     get_children

   DESCRIPTION
     looks up the children names of the node identified by a path in a tree 
     associated with a MVDATA section and index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     path               - an existing path in a tree 
     child_list         - return value of the children node names (comma-sep)

*/

PROCEDURE get_children(
  index_name      IN VARCHAR2,
  section_name    IN VARCHAR2,
  path            IN CLOB,
  child_list      IN OUT CLOB
);

/*---------------------------- get_nodeid_list ----------------------*/
/*
   NAME
     get_nodeid_list

   DESCRIPTION
     looks up the node identifiers of all nodes in an existing path in a tree 
     associated with a MVDATA section and index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     path               - an existing path in a tree 
     id_list            - return value of the corresponding node ids

*/

PROCEDURE get_nodeid_list(
  index_name      IN VARCHAR2,
  section_name    IN VARCHAR2,
  path            IN CLOB,
  id_list         IN OUT CLOB
);

/*---------------------------- get_roots ----------------------*/
/*
   NAME
     get_roots

   DESCRIPTION
     looks up the roots of all the trees associated with a MVDATA section 
     and index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     roots              - list of roots (names)

*/

PROCEDURE get_roots(
  index_name      IN  VARCHAR2,
  section_name    IN  VARCHAR2,
  roots           OUT CLOB
);

/*---------------------------- remove_node ----------------------*/
/*
   NAME
     remove_node

   DESCRIPTION
     Removes specified node from the tree associated with the MVDATA section
     and Index

   ARGUMENT
     index_name		- the name for the index
     section_name       - the name of the MVDATA section
     path               - input node path to be deleted

*/

PROCEDURE remove_node(
  index_name      IN  VARCHAR2,
  section_name    IN  VARCHAR2,
  path            IN CLOB
);

PROCEDURE DropSequences(
  idx_owner       IN VARCHAR2,
  idx_name        IN VARCHAR2,
  idx_id          IN NUMBER
);

PROCEDURE regindex(
  index_name      IN VARCHAR2,
  purge           IN BOOLEAN
);



END ctx_tree;
/
